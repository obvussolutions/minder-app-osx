//
//  ScheduleViewController.swift
//  Minder
//
//  Created by Febin on 4/11/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa
import AVKit
import AVFoundation

class ScheduleViewController: NSViewController {
    
    var meditationBreak: MeditationBreak?
    var dateSections: Array<String> = Array<String>()
    var sectionItems: Array<[AnyObject]> = Array<[AnyObject]>()
    var fullItems:[Activity] = [Activity]()
    var currentScheduledVideos:[Activity] = [Activity]()
    var calenderColumnDates : [NSDate] = []
    var activitySelected: Activity?
    var recordingSelected: BallPathRecord?
    var daysName:[String] = ["SUN","MON","TUE","WED","THUS","FRI","SAT"]
    var calendarTimes:[String] = ["12am","1am","2am","3am","4am","5am","6am","7am","8am","9am","10am","11am","12pm","1pm","2pm","3pm","4pm","5pm","6pm","7pm","8pm","9pm","10pm","11pm"]
    var fullTime : [String:String] = ["12am":"00:00:00","1am":"01:00:00","2am":"02:00:00","3am":"03:00:00","4am":"04:00:00","5am":"05:00:00","6am":"06:00:00","7am":"07:00:00","8am":"08:00:00","9am":"09:00:00","10am":"10:00:00","11am":"11:00:00","12pm":"12:00:00","1pm":"13:00:00","2pm":"14:00:00","3pm":"15:00:00","4pm":"16:00:00","5pm":"17:00:00","6pm":"18:00:00","7pm":"19:00:00","8pm":"20:00:00","9pm":"21:00:00","10pm":"22:00:00","11pm":"23:00:00"]
    var activitiesByDates:[[Activity]] = [[]]
    var collectionCellsItemsId:[Int:Activity?] = [:]
    var started_time: String = ""
    
    var videoFinished: Bool = false
    @IBOutlet weak var weeksContainer: NSView!
    var playerItem:AVPlayerItem?
    
    @IBOutlet weak var noRecordsView: NSView!
    @IBOutlet weak var calendarViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var calendarViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoContainer: NSView!
    @IBOutlet weak var videosScrollView: NSScrollView!
    @IBOutlet weak var labelIdealPathExists: NSTextField!
    @IBOutlet weak var recordingName: NSTextField!
    @IBOutlet weak var labelDateTime: NSTextField!
    @IBOutlet weak var buttonRecordsDelete: NSButton!
    @IBOutlet weak var buttonSaveRecord: NSButton!
    @IBOutlet weak var labelDescription: NSTextField!
    @IBOutlet weak var labelDuration: NSTextField!
    @IBOutlet weak var labelTotalMeditationTime: NSTextField!
    @IBOutlet weak var labelTitle: NSTextField!
    @IBOutlet weak var buttonReplay: NSButton!
    @IBOutlet weak var buttonPlayerClose: NSButton!
    @IBOutlet weak var calendarView: NSCollectionView!
    @IBOutlet weak var scheduleBox: NSView!
    @IBOutlet weak var meditationSettingBox: NSView!
    @IBOutlet weak var playerContainer: NSView!
    @IBOutlet var playerView: AVPlayerView!
    weak var delegateGuidance: DelegateGuidance?
    var currentViewType: GuidanceViewType = .activity
    @IBOutlet weak var progressReplay: NSProgressIndicator!
    
    @IBOutlet weak var popupMeditationBreak: NSPopUpButton!
    @IBOutlet weak var popupNumberOfBreath: NSPopUpButton!
    @IBOutlet weak var popupInhaleLength: NSPopUpButton!
    @IBOutlet weak var popupRetailInhaleLength: NSPopUpButton!
    @IBOutlet weak var popupExhaleLength: NSPopUpButton!
    @IBOutlet weak var popupRetailExhaleLength: NSPopUpButton!

    @IBAction func ShowRecordsClicked(_ sender: Any) {
        //configureCollectionView()
        noRecordsView.isHidden = true
        if (sender as! NSButton).state == 1 {
            currentViewType = .record
            (sender as! NSButton).title = "Show Breaks"
            GetRecords()
            self.weeksContainer.isHidden = true
            self.calendarViewTopConstraint.constant = 0
            self.calendarViewHeightContraint.constant = 307.0
        }else{
            currentViewType = .activity
            (sender as! NSButton).title = "Show Records"
            GetGuidanceActivity()
            self.weeksContainer.isHidden = false
            self.calendarViewTopConstraint.constant = 39
            self.calendarViewHeightContraint.constant = 267.0
        }
    }
    
    func updateMeditationValues() {
        popupNumberOfBreath.selectItem(withTag: (meditationBreak?.numberOfBreath)!)
        popupInhaleLength.selectItem(withTag: (meditationBreak?.inhaleLength)!)
        popupRetailInhaleLength.selectItem(withTag: (meditationBreak?.retailInhaleLength)!)
        popupExhaleLength.selectItem(withTag: (meditationBreak?.exhaleLength)!)
        popupRetailExhaleLength.selectItem(withTag: (meditationBreak?.retailExhaleLength)!)
        if let tmt = meditationBreak?.totalMeditationTime() {
            labelTotalMeditationTime.stringValue = "\(tmt)"
        }
    }
    
    @IBAction func MeditationBreakChanged(_ sender: NSPopUpButton) {
        MinderManager.sharedInstance.settings?.breakSelected = sender.selectedTag()
        MinderManager.sharedInstance.SaveSettings()
        meditationBreak = MinderManager.sharedInstance.settings!.breaks[(MinderManager.sharedInstance.settings?.breakSelected)!]
        updateMeditationValues()
    }
    
    func updateMeditationBreak() {
        meditationBreak?.numberOfBreath = Int(self.popupNumberOfBreath.titleOfSelectedItem!)!
        meditationBreak?.inhaleLength = Int(self.popupInhaleLength.titleOfSelectedItem!)!
        meditationBreak?.retailInhaleLength = Int(self.popupRetailInhaleLength.titleOfSelectedItem!)!
        meditationBreak?.exhaleLength = Int(self.popupExhaleLength.titleOfSelectedItem!)!
        meditationBreak?.retailExhaleLength = Int(self.popupRetailExhaleLength.titleOfSelectedItem!)!
    }
    
    @IBAction func MeditationBreakSettingsChanged(_ sender: NSPopUpButton) {
        updateMeditationBreak()
        MinderManager.sharedInstance.SaveSettings()
        updateMeditationValues()
    }
    
    @IBAction func MeditationBreakStartClicked(_ sender: Any) {
        self.delegateGuidance?.AnimateMeditationGudeRing(meditationBreak!)
    }

    @IBAction func postureMenuClicked(_ sender: NSButton) {
        self.meditationSettingBox.isHidden = true
        delegateGuidance?.switchMeditationMode(false)
    }
    @IBAction func meditationBoxCloseClicked(_ sender: NSButton) {
        self.meditationSettingBox.isHidden = true
    }
    @IBAction func meditationBreakClicked(_ sender: NSButton) {
        self.meditationSettingBox.isHidden = false
        delegateGuidance?.switchMeditationMode(true)
    }
    
    // MARK:ReplayFinished
    func ReplayFinished() {
        //dispatch_async(dispatch_get_main_queue(), {
        MinderManager.sharedInstance.replayState = .none
        self.buttonReplay.title = "PLAY"
        self.buttonReplay.image = NSImage(named: "Play")
        //})
    }
    // MARK:ReplayTapped
    @IBAction func ReplayTapped(_ sender: NSButton) {
        switch MinderManager.sharedInstance.replayState {
        case .none:
            progressReplay.maxValue = Double(recordingSelected!.posturePaths.count)
            delegateGuidance?.Replay(recordingSelected!.posturePaths, gyroData: (recordingSelected?.gyroPaths)!, isReplay: true)
            buttonReplay.title = "PAUSE"
            buttonReplay.image = NSImage(named: "Pause")
        case .play:
            MinderManager.sharedInstance.replayState = .pause
            buttonReplay.title = "PLAY"
            buttonReplay.image = NSImage(named: "Play")
        case .pause:
            MinderManager.sharedInstance.replayState = .play
            buttonReplay.title = "PAUSE"
            buttonReplay.image = NSImage(named: "Pause")
        }
    }
    // MARK:SaveRecordTapped
    @IBAction func SaveRecordTapped(_ sender: AnyObject) {
        let savePanel = NSSavePanel()
        //savePanel.setTitleWithRepresentedFilename(recordingSelected.)
        savePanel.title = "Save Recording"
        savePanel.canCreateDirectories = true
        savePanel.showsHiddenFiles = true
        savePanel.level = Int(CGShieldingWindowLevel())
        savePanel.nameFieldStringValue = "\(labelTitle.stringValue).mndr"
        savePanel.begin { (result) -> Void in
            if result == NSFileHandlingPanelOKButton
            {
                var appDirPath = MinderManager.sharedInstance.appDirectory
                if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
                    appDirPath += (MinderManager.sharedInstance.userLogged?.userName)! + "/Records/"
                } else {
                    appDirPath += "_/Records/"
                }
                appDirPath += self.recordingSelected!.name + ".mndr"
                do {
                    try FileManager.default.copyItem(at: URL(fileURLWithPath: appDirPath), to: savePanel.url!)
                } catch let error as NSError {
                    let myPopup: NSAlert = NSAlert()
                    myPopup.messageText = "Record saving failed"
                    myPopup.informativeText = "Ooops! Something went wrong:\r\n\(error)"
                    myPopup.alertStyle = .warning
                    myPopup.addButton(withTitle: "OK")
                    myPopup.runModal()
                }
            }
        }
    }
    // MARK:RecordsDeleteClicked
    @IBAction func RecordsDeleteClicked(_ sender: AnyObject) {
        let confirmPopup: NSAlert = NSAlert()
        confirmPopup.messageText = "Record delete confirmation"
        confirmPopup.informativeText = "Are you sure you want to delete this recording ? Recording will be no longer available."
        confirmPopup.alertStyle = .informational
        confirmPopup.addButton(withTitle: "OK")
        confirmPopup.addButton(withTitle: "Cancel")
        let res = confirmPopup.runModal()
        if res == NSAlertFirstButtonReturn {
            var appDirPath = MinderManager.sharedInstance.appDirectory
            if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
                appDirPath += (MinderManager.sharedInstance.userLogged?.userName)! + "/Records/"
            } else {
                appDirPath += "_/Records/"
            }
            appDirPath += self.recordingSelected!.name + ".mndr"
            do {
                try FileManager.default.removeItem(at: URL(fileURLWithPath: appDirPath))
                let myPopup: NSAlert = NSAlert()
                myPopup.messageText = "Record deleted"
                myPopup.informativeText = "You have successfully deleted this recording"
                myPopup.alertStyle = .warning
                myPopup.addButton(withTitle: "OK")
                myPopup.runModal()
                GetRecords()
                videoCloseTapped(NSButton())
                NSLog("You have successfully deleted this recording")
            } catch let error as NSError {
                let myPopup: NSAlert = NSAlert()
                myPopup.messageText = "Record delete failed"
                myPopup.informativeText = "Ooops! Something went wrong:\r\n\(error)"
                myPopup.alertStyle = .warning
                myPopup.addButton(withTitle: "OK")
                myPopup.runModal()
                NSLog("Ooops! Something went wrong while deleteing record:\r\n\(error)")
            }
        }
    }
    
    @IBAction func RecordEditClicked(_ sender: NSButton) {
        delegateGuidance?.EditRecordName(currentRecordName: labelTitle.stringValue, currentRecordSystemName: (recordingSelected?.name)!)
    }
    // MARK:UpdateReplayProgress
    func UpdateReplayProgress(_ progress: Int) {
        progressReplay.doubleValue = Double(progress)
    }
    
    // MARK:GetRecords
    func GetRecords() {
        dateSections.removeAll()
        sectionItems.removeAll()
        
        var appDirPath = MinderManager.sharedInstance.appDirectory
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            appDirPath += (MinderManager.sharedInstance.userLogged?.userName)! + "/Records/"
        } else {
            appDirPath += "_/Records/"
        }
        print("\(appDirPath)")
        if let enumerator = FileManager.default.enumerator(atPath: appDirPath) {
            var dicRecord: [BallPathRecord] = [BallPathRecord]()
            while let fileName = enumerator.nextObject() as? String {
                if fileName.hasSuffix("mndr")  {
                    let date = fileName.substring(to: (fileName.range(of: " ")?.lowerBound)!)
                    if !dateSections.contains(date) {
                        if dateSections.count > 0 {
                            sectionItems.append(dicRecord)
                            dicRecord.removeAll()
                        }
                        dateSections.append(date)
                    }
                    let ballPath: BallPathRecord = (NSKeyedUnarchiver.unarchiveObject(withFile: appDirPath + fileName) as? BallPathRecord)!
                    if ballPath.gyroPaths.count > 0 && ballPath.posturePaths.count > 0 {
                        dicRecord.append(ballPath)
                    }
                    NSLog("Record found: \(fileName) ballPathCount: \(ballPath.posturePaths.count) gyroPathsCount: \(ballPath.gyroPaths.count)")
                }
            }
            if dicRecord.count > 0 {
                noRecordsView.isHidden = true
                dicRecord = dicRecord.reversed()
                sectionItems.append(dicRecord)
            }else if dicRecord.count == 0 {
                noRecordsView.isHidden = false
            }
        }
        if dateSections.count > 0 {
            dateSections = dateSections.reversed()
            sectionItems = sectionItems.reversed()
        }
        calendarView.reloadData()
    }
    
    @IBAction func videoCloseTapped(_ sender: NSButton) {
        videoFinished = true
        if currentViewType == .record {
            buttonReplay.title = "PLAY"
            buttonReplay.image = NSImage(named: "Play")
            recordingSelected = nil
        } else {
        playerView.player = nil
        /*dispatch_async(dispatch_get_main_queue(), {
         NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: self.playerView.player?.currentItem)
         self.playerView.player?.removeObserver(self, forKeyPath:"rate", context: nil)
         })
         playerView.player = nil*/
        let result: String = ""
        let completed_time: String = Date.GetDateString(Date(), format: "yyyy-MM-dd hh:mm:ss")
        MinderManager.sharedInstance.SetExerciseStatus((activitySelected?.program_id?.intValue)!, schedule_date_id: (activitySelected?.schedule_date_id?.intValue)!, schedule_id: (activitySelected?.schedule_id?.intValue)!, modified_id: (activitySelected?.modified_id?.intValue)!, result: result, started_time: started_time, completed_time: completed_time, completionHandler: {_ in})
        activitySelected = nil
        
        }
        delegateGuidance!.ReplayClosed()
        MinderManager.sharedInstance.isActivityPlaying = false
        MinderManager.sharedInstance.replayState = .none
        playerContainer.isHidden = true
        calendarView.isSelectable = true
        calendarView.enclosingScrollView?.hasVerticalScroller = true
        /*buttonRecord.isEnabled = true
        buttonShowRecords.isEnabled = true
        popButtonCollectionStyle.isEnabled = true
        buttonTabRecords.isEnabled = true
        buttonTabActivities.isEnabled = true*/
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.wantsLayer = true
        self.meditationSettingBox.isHidden = true
        configureCollectionView()
        if MinderManager.sharedInstance.appMode == .normal {
            GetGuidanceActivity()
        }
        getCalendarDates()
        
        meditationBreak = MinderManager.sharedInstance.settings!.breaks[(MinderManager.sharedInstance.settings?.breakSelected)!]
        popupMeditationBreak.removeAllItems()
        var index: Int = 0
        for _break in MinderManager.sharedInstance.settings!.breaks {
            let menuItem: NSMenuItem = NSMenuItem()
            menuItem.title = _break.title
            menuItem.tag = index
            popupMeditationBreak.menu?.addItem(menuItem)
            index += 1
        }
        popupMeditationBreak.selectItem(withTag: (MinderManager.sharedInstance.settings?.breakSelected)!)
        updateMeditationValues()
    }
    
    override func viewWillAppear() {
        noRecordsView.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
      scheduleBox.layer?.backgroundColor =  NSColor.init(calibratedRed: 5/255, green: 59/255, blue: 69/255, alpha: 1).cgColor
        meditationSettingBox.layer?.backgroundColor =  NSColor.init(calibratedRed: 5/255, green: 59/255, blue: 69/255, alpha: 1).cgColor
        playerContainer.layer?.backgroundColor = CGColor.black
        videoContainer.layer?.backgroundColor = NSColor.init(calibratedRed: 5/255, green: 59/255, blue: 69/255, alpha: 1).cgColor
        videoContainer.layer?.bounds = (videosScrollView.layer?.bounds)!
        weeksContainer.layer?.backgroundColor = CGColor.white
        var weeksContainerSubviewIndex:Int = 12
        for views in weeksContainer.subviews {
            if let weekView = views as? NSView {
                weekView.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
                if let theLabelView = self.view.viewWithTag(weeksContainerSubviewIndex){
                    if theLabelView is NSTextField {
                        let theLabel: NSTextField = theLabelView as! NSTextField
                        let calendar = NSCalendar.current
                        let components = calendar.dateComponents([.day , .month , .year], from: calenderColumnDates[weeksContainerSubviewIndex - 12] as Date)
                        if let day = components.day {
                            theLabel.stringValue = "\(daysName[weeksContainerSubviewIndex - 12]) \(day)"
                        }
                    }
                }
            }
            weeksContainerSubviewIndex += 1
        }
    }
    
    deinit {
        MinderManager.sharedInstance.replayState = .none
        NotificationCenter.default.removeObserver(self)
        self.playerView.player?.removeObserver(self, forKeyPath: "rate")
        print("Deinitialize the Schedule View Controller")
    }
    
    func getCalendarDates() {
        
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier(rawValue: NSGregorianCalendar))
        let dateComponent = NSDateComponents()
        for i in 0...7
        {
            dateComponent.day = i
            let newDate = calendar?.date(byAdding: dateComponent as DateComponents, to: NSDate().startOfWeek as Date, options:[])
            //print(newDate)
            if let date = newDate {
                calenderColumnDates.append(date as NSDate)
            }
        }
        //print(calenderColumnDates)
    }
    

    // MARK:GetGuidanceActivity
    func GetGuidanceActivity() {
        dateSections.removeAll()
        sectionItems.removeAll()
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            
            let appDelegate = NSApplication.shared().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            
            var arrScheduleDates: NSArray = NSArray()
            let fetchRequestActivitySchedule = NSFetchRequest<NSFetchRequestResult>(entityName: "Activity")
            
            fetchRequestActivitySchedule.propertiesToFetch = ["schedule_date"]
            fetchRequestActivitySchedule.resultType = .dictionaryResultType
            fetchRequestActivitySchedule.sortDescriptors = [NSSortDescriptor(key: "schedule_date", ascending: false)]
            fetchRequestActivitySchedule.predicate = NSPredicate(format: "user_email == %@", (MinderManager.sharedInstance.userLogged?.email)!)
            do {
                arrScheduleDates = try managedContext.fetch(fetchRequestActivitySchedule) as NSArray
            } catch {
                NSLog("Failed to fetch employees: \(error)")
            }
            print("arrScheduleDates: \(arrScheduleDates.count)")
            
            for _date in arrScheduleDates as! Array<NSDictionary> {
                let date = _date.value(forKey: "schedule_date") as! String
                if !dateSections.contains(date) {
                    let fetchRequestActivity = NSFetchRequest<NSFetchRequestResult>(entityName: "Activity")
                    fetchRequestActivity.predicate = NSPredicate(format: "schedule_date == %@ && user_email == %@", date, (MinderManager.sharedInstance.userLogged?.email)!)
                    fetchRequestActivity.sortDescriptors = [NSSortDescriptor(key: "schedule_id", ascending: false), NSSortDescriptor(key: "schedule_date_id", ascending: false)]
                    do {
                        var activities: [Activity] = try managedContext.fetch(fetchRequestActivity) as! [Activity]
                        //print("Activity: \(activities) ")
                        // orders according to schedule time
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
                        if activities.count > 0 {
                            activities = activities.sorted(by: {
                                let dateTime1 = "\($0.schedule_date!) \($0.start_time!)"
                                let date1 = dateFormatter.date(from: dateTime1)
                                
                                let dateTime2 = "\($1.schedule_date!) \($1.start_time!)"
                                let date2 = dateFormatter.date(from: dateTime2)
                                
                                return date1?.compare(date2!) == .orderedAscending
                            })
                        }
                        //
                        sectionItems.append(activities)
                        dateSections.append(date)
                    } catch {
                        NSLog("Failed to fetch employees: \(error)")
                    }
                }
            }
            fullItems = []
            for i in 0..<sectionItems.count {
                for k in 0..<sectionItems[i].count {
                    fullItems.append(sectionItems[i][k] as! Activity)
                }
            }
        }
        calendarView.reloadData()
    }
    
    private func configureCollectionView() {
        let flowLayout = NSCollectionViewFlowLayout()
        if currentViewType == .record {
            flowLayout.itemSize = NSSize(width: 132.0, height: 140.0)
        } else {
            flowLayout.itemSize = NSSize(width: 85.0, height: 40.0)
        }
        flowLayout.sectionInset = EdgeInsets(top: 1.0, left: 2.0, bottom: 1.0, right: 2.0)
        flowLayout.minimumInteritemSpacing = 1.0
        flowLayout.minimumLineSpacing = 1.0
        flowLayout.scrollDirection = NSCollectionViewScrollDirection.vertical
        calendarView.collectionViewLayout = flowLayout
        calendarView.layer?.backgroundColor = NSColor.white.cgColor
    }
    
    //MARK: get schedule videos of the  time
    func getAllScheduledVideos(activitySelected:Activity) -> [Activity]{
        let activities = fullItems.filter() {
            let theTime = activitySelected.start_time
            if ($0.schedule_date! == activitySelected.schedule_date!) && (String($0.start_time!.characters.prefix(2)) == String(theTime!.characters.prefix(2))) {
                return true
            } else {
                return false
            }
        }
        return activities
    }
    

}

extension ScheduleViewController : NSCollectionViewDataSource {
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        if currentViewType == .record {
            return dateSections.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        if currentViewType == .record {
            return sectionItems[section].count
        }
        return 8 * 24
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem?
        if currentViewType == .activity {
            item = collectionView.makeItem(withIdentifier: "CalendarViewItem", for: indexPath)
        
            var appDirPath = MinderManager.sharedInstance.appDirectory
            if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
                appDirPath += (MinderManager.sharedInstance.userLogged?.userName)!
            } else {
                appDirPath += "_"
            }
            
            
            guard let collectionViewItem = item as? CalendarViewItem else {return item!}
            
            if (indexPath.item % 8 == 0){ // time
                collectionViewItem.textField?.isHidden = false
                collectionViewItem.imageView?.isHidden = true
                
                collectionViewItem.textField?.stringValue = calendarTimes[(indexPath.item/8)]
                collectionCellsItemsId[indexPath.item] = nil
            }else{ // thubnail
                collectionViewItem.textField?.isHidden = true
                collectionViewItem.imageView?.isHidden = false
                let calendar = NSCalendar.current
                let calenderColumnDatesIndex = indexPath.item % 8 == 0 ? 0 : indexPath.item % 8
                let components = calendar.dateComponents([.day , .month , .year], from: calenderColumnDates[calenderColumnDatesIndex - 1] as Date)
                guard let theDay = components.day else {
                    return item!
                }
                guard let theMonth = components.month else {
                    return item!
                }
                guard let theYear = components.year else {
                    return item!
                }
                let cellDay = 0 ... 9 ~= theDay ? "0"+String(theDay) : String(theDay)
                let cellMonth = 0 ... 9 ~= theMonth ? "0"+String(theMonth) : String(theMonth)
                let cellYear = String(theYear)
                let dbDate = "\(cellMonth)-\(cellDay)-\(cellYear)"
                
                let timeIndexValue = indexPath.item/8
                let cellTime = calendarTimes[timeIndexValue]
                let dbTime = fullTime[cellTime]
                
                if let theTime = dbTime {
                    
                    let cellActivity = fullItems.filter() {
                        //if ($0.schedule_date! == dbDate) && ($0.start_time! == theTime) {
                        if ($0.schedule_date! == dbDate) && (String($0.start_time!.characters.prefix(2)) == String(theTime.characters.prefix(2))) {
                            return true
                        }else{
                            return false
                        }
                    }
                    //print(cellActivity)
                    if cellActivity.count > 0 {
                        let fileName = appDirPath + "/Activities/\(cellActivity[0].schedule_id!).png"
                        //print(fileName)
                        if FileManager.default.fileExists(atPath: fileName) {
                            collectionViewItem.imageView?.image = NSImage(contentsOfFile: fileName)
                        } else {
                            if cellActivity.first?.additional_file != nil {
                                collectionViewItem.imageView?.DownloadFrom(link: cellActivity.first?.additional_file)
                            }else{
                                collectionViewItem.imageView?.isHidden = true
                            }
                        }
                        collectionCellsItemsId[indexPath.item] = cellActivity[0]
                    } else {
                        collectionViewItem.imageView?.isHidden = true
                        collectionCellsItemsId[indexPath.item] = nil
                    }
                }
            }
        } else {
            item = collectionView.makeItem(withIdentifier: "CollectionViewItem", for: indexPath)
            guard let collectionViewItem = item as? CollectionViewItem else { return item! }
            let recording = sectionItems[indexPath.section][indexPath.item] as! BallPathRecord
            collectionViewItem.textField?.stringValue = "Duration: \(Int(recording.posturePaths.count / 10)) s \(Int(recording.posturePaths.count % 10)) ms"
            print("recording name \(MinderManager.sharedInstance.FetchRecordingName(recordSystemName: (recording.name)))")
            collectionViewItem.recordingName?.stringValue = MinderManager.sharedInstance.FetchRecordingName(recordSystemName: (recording.name))
            collectionViewItem.imageView?.image = recording.image
            if let textField: NSTextField = collectionViewItem.view.viewWithTag(1) as? NSTextField {
                textField.stringValue = ""
            }
        }
            return item!
    }
    
    func collectionView(_ collectionView: NSCollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> NSView {
        let view = collectionView.makeSupplementaryView(ofKind: NSCollectionElementKindSectionHeader, withIdentifier: "HeaderView", for: indexPath) as! HeaderView
        view.sectionTitle.stringValue = dateSections[indexPath.section]
        view.imageCount.stringValue = "\(sectionItems[indexPath.section].count) recordings"
        return view
    }
}


extension ScheduleViewController : NSCollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> NSSize {
        if currentViewType == .record {
            return NSSize(width: 456, height: 30)
        }
        return NSZeroSize
    }
    
    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
        if currentViewType == .record {
            return NSSize(width: 132.0, height: 140.0)
        } else {
            return NSSize(width: 85.0, height: 40.0)
        }
        
    }
    
}

extension ScheduleViewController : NSCollectionViewDelegate {
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        guard let indexPath = indexPaths.first else { return }
        if currentViewType == .record {
            recordingSelected = sectionItems[indexPath.section][indexPath.item] as? BallPathRecord
            labelTitle.isHidden = false
            labelTitle.stringValue = MinderManager.sharedInstance.FetchRecordingName(recordSystemName: (recordingSelected?.name)!)
            labelDateTime.stringValue = "Date: \(dateSections[indexPath.section])"
            labelDuration.stringValue = "Duration: \(Int(recordingSelected!.posturePaths.count / 10)) s \(Int(recordingSelected!.posturePaths.count % 10)) ms"
            labelDescription.isHidden = true
            buttonReplay.isHidden = false
            buttonSaveRecord.isHidden = false
            buttonRecordsDelete.isHidden = false
            progressReplay.isHidden = false
            playerView.isHidden = true
            
            playerContainer.isHidden = false
            collectionView.isSelectable = false
            
            //buttonRecord.isEnabled = false
            //buttonShowRecords.isEnabled = false
            //popButtonCollectionStyle.isEnabled = false
            //buttonTabRecords.isEnabled = false
            //buttonTabActivities.isEnabled = false
            
            videoContainer.isHidden = true
            videosScrollView.isHidden = true
            progressReplay.doubleValue = 0.0
        } else {
            videoContainer.isHidden = false
            videosScrollView.isHidden = false
            progressReplay.isHidden = true
            if collectionCellsItemsId[indexPath.item] != nil {
                activitySelected = collectionCellsItemsId[indexPath.item]!
                currentScheduledVideos = []
                currentScheduledVideos = getAllScheduledVideos(activitySelected: activitySelected!)
                PerformCollectionClick(activitySelected!)
                setBelowVideos()
            }
            self.buttonPlayerClose.isHidden = true
        }
        collectionView.deselectItems(at: indexPaths)
    }
    
    // MARK: Set other video below the video
    func setBelowVideos() {
        videoContainer.subviews.forEach({ $0.removeFromSuperview() })
        // create vidios button below
        for i in 0...currentScheduledVideos.count - 1{
            let view = NSView()
            self.videoContainer.addSubview(view)
            let button = NSButton()
            button.image = NSImage(named: "Posture")
            view.addSubview(button)
            let videoWidth = Int(videoContainer.frame.size.height - 10)
            view.frame = CGRect(x: 5 + (i*videoWidth) + (i*5), y: 5, width: videoWidth, height: videoWidth)
            view.layer?.backgroundColor = NSColor.black.cgColor
            button.frame = CGRect(x: 0, y: 0, width: videoWidth, height: videoWidth)
            button.target = self
            button.action =  #selector(videoButtonAction)
            button.tag = i + 10
            if i == 0 {
                button.isEnabled = false
            }
        }
    }

    // MARK: when video click, play the video
    func PerformCollectionClick(_ activity: Activity) {
        print("Clicked: \((activity.schedule_id?.intValue)!)")
        activitySelected = activity
        var appDirPath = MinderManager.sharedInstance.appDirectory
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            appDirPath += (MinderManager.sharedInstance.userLogged?.userName)! + "/Activities/"
        } else {
            appDirPath += "_/Activities/"
        }
        let filePathMP4 = "\(appDirPath)\(activitySelected!.schedule_id!).mp4"
        if !FileManager.default.fileExists(atPath: filePathMP4) {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Download in progress.."
            myPopup.informativeText = "Download in progress. Please try after some time"
            myPopup.alertStyle = .warning
            myPopup.addButton(withTitle: "OK")
            myPopup.runModal()
            NSLog("Download in progress. Please try after some time")
        } else {
            videoFinished = false
            
            let url:URL = URL(fileURLWithPath: filePathMP4)
            playerItem = AVPlayerItem(url: url)
            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
            playerView.player = AVPlayer(playerItem: playerItem)
            playerView.player?.status
            playerView.player?.addObserver(self, forKeyPath:"rate", options: NSKeyValueObservingOptions(rawValue: UInt(0)), context: nil)
            
            if activitySelected?.activity_types == "Posture Improvement" {
                NSLog("Posture Improvement")
                var ballPath: BallPathRecord? = nil
                let fileName = appDirPath + "\(activitySelected!.schedule_id!).mndr"
                if FileManager.default.fileExists(atPath: fileName) {
                    if let _ = NSKeyedUnarchiver.unarchiveObject(withFile: fileName) as? BallPathRecord {
                        ballPath = (NSKeyedUnarchiver.unarchiveObject(withFile: fileName) as? BallPathRecord)!
                        if ballPath!.gyroPaths.count > 0 && ballPath!.posturePaths.count > 0 {
                            labelIdealPathExists!.stringValue = "Ideal path exists"
                        } else {
                           // labelIdealPathExists!.stringValue = "Wrong ideal path exists" ankush change
                        }
                        NSLog("Ideal path: \(fileName) PosturePathCount:\(ballPath?.posturePaths.count) GyroPathCount:\(ballPath?.gyroPaths.count)")
                    } else {
                        labelIdealPathExists!.stringValue = "Ideal path not found"
                        NSLog("Ideal path not found")
                    }
                } else {
                    labelIdealPathExists!.stringValue = "Ideal path not found"
                    NSLog("Ideal path not found")
                }
                if ballPath != nil {
                    DispatchQueue.main.async(execute: {
                        MinderManager.sharedInstance.isActivityPlaying = true
                        self.delegateGuidance?.Replay(ballPath!.posturePaths, gyroData: ballPath!.gyroPaths, isReplay: false)
                    })
                }
            } else if activitySelected?.activity_types == "Guided Meditation" {
                NSLog("Guided Meditation")
                let mediatationBreak = MeditationBreak()
                mediatationBreak.numberOfBreath = (activitySelected?.breathCount?.intValue)!
                mediatationBreak.inhaleLength = (activitySelected?.inhaleLen?.intValue)!
                mediatationBreak.retailInhaleLength = (activitySelected?.inhaleRetail?.intValue)!
                mediatationBreak.exhaleLength = (activitySelected?.exhaleLen?.intValue)!
                mediatationBreak.retailExhaleLength = (activitySelected?.exhaleRetail?.intValue)!
                
                DispatchQueue.main.async(execute: {
                    self.delegateGuidance?.AnimateMeditationGudeRing(mediatationBreak)
                })
               
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                self.buttonPlayerClose.isHidden = false
            })
            self.playerView.player?.play()
            
            started_time = Date.GetDateString(Date(), format: "yyyy-MM-dd hh:mm:ss")
            
            labelTitle.isHidden = false
            labelDescription.isHidden = false
            buttonReplay.isHidden = true
            buttonSaveRecord.isHidden = true
            buttonRecordsDelete.isHidden = true
            /* progressReplay.isHidden = true*/
            playerView.isHidden = false
            
            labelTitle.stringValue = activitySelected!.media_title!
            labelDateTime.stringValue = "Schedule: \(activitySelected!.schedule_date!) \(activitySelected!.start_time!)"
            let duration = "Duration: \(MinderUtility.SecondsToHMSString(activitySelected!.duration!.intValue))"
            labelDuration.stringValue = duration
            labelDescription.stringValue = "Description: " + activitySelected!.media_description!
            
            playerContainer.isHidden = false
            calendarView.isSelectable = false
            calendarView.enclosingScrollView?.hasVerticalScroller = false
                /*buttonRecord.isEnabled = false
                buttonShowRecords.isEnabled = false
                popButtonCollectionStyle.isEnabled = false
                buttonTabRecords.isEnabled = false
                buttonTabActivities.isEnabled = false*/
            }
    }
    
    func videoButtonAction(_ button: NSButton) {
        print(button.tag)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        playerView.player?.removeObserver(self, forKeyPath: "rate")
        PerformCollectionClick(currentScheduledVideos[button.tag - 10])
        for i in 0...currentScheduledVideos.count - 1{
            if let theButton = self.view.viewWithTag(i + 10) as? NSButton {
                theButton.isEnabled = true
            }
        }
        button.isEnabled = false
    }
    
    // MARK:playerDidFinishPlaying
    func playerDidFinishPlaying(_ note: Notification) {
        //self.perform(#selector(CloseTapped), with: nil, afterDelay: 1)
        NSLog("Playing Video Finished")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "actionAtItemEnd"{
            NSLog("Playing Video Finished")
        } else if keyPath == "rate" {
            if !videoFinished {
                if playerView.player?.rate == 0 {
                    DispatchQueue.main.async(execute: {
                        MinderManager.sharedInstance.replayState = .pause
                        NSLog("Playing Video Paused")
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        MinderManager.sharedInstance.replayState = .play
                        NSLog("Playing Video")
                    })
                }
            }
        }
    }
}

extension NSDate {
    struct Gregorian {
        static let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
    }
    var startOfWeek: NSDate {
        return Gregorian.calendar.date(from: Gregorian.calendar.components([.yearForWeekOfYear, .weekOfYear ], from: self as Date))! as NSDate
    }
}

