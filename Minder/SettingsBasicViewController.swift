//
//  SettingsBasicViewController.swift
//  Minder
//
//  Created by Abdul on 10/11/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa
import CoreAudioKit

class SettingsBasicViewController: NSViewController {
    
    var delegateSettings: DelegateSettings? = nil
    var opp: OPP = OPP()
    
    @IBOutlet weak var textDeviceName: NSTextField!
    @IBOutlet weak var buttonConnection: NSButton!
    @IBOutlet weak var buttonEnableFeatures: NSButton!
    @IBOutlet weak var buttonPauseProcessing: NSButton!
    @IBOutlet weak var buttonToggleGyroRotation: NSButton!
    @IBOutlet weak var buttonSkipCalibration: NSButton!
    @IBOutlet weak var buttonVolumeMute: NSButton!
    
    @IBOutlet weak var sliderTransperancy: NSSlider!
    @IBOutlet weak var sliderSensitivity: NSSlider!
    @IBOutlet weak var labelSensitivity: NSTextField!
    @IBOutlet weak var sliderSensitivityRotation: NSSlider!
    @IBOutlet weak var labelSensitivityRotation: NSTextField!
    @IBOutlet weak var sliderVolume: NSSlider!
    @IBOutlet weak var sliderDifficulty: NSSlider!
    @IBOutlet weak var popButtonVibration: NSPopUpButton!
    @IBOutlet weak var popButtonOPP: NSPopUpButton!
    @IBOutlet weak var popButtonPreferedDevice: NSPopUpButton!
    
    @IBOutlet weak var viewEditOPP: NSView!
    @IBOutlet weak var txtEditOPP: NSTextField!
    @IBOutlet weak var labelNotCalibrated: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonSkipCalibration.attributedTitle = NSAttributedString(string:"Skip calibration on startup", attributes: [ NSForegroundColorAttributeName : NSColor.white ])
        opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        
        viewEditOPP.wantsLayer = true
        viewEditOPP.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        
        RefreshControls()
        showHideOPPEditView(true)
    }
    //  refreshing the view
    func RefreshControls() {
        if MinderManager.sharedInstance.deviceStatus == .connected {
            textDeviceName.stringValue = MinderManager.sharedInstance.deviceName
            buttonConnection.title = "Disconnect"
            if MinderManager.sharedInstance.deviceFeaturesEnabled == true {
                buttonEnableFeatures.isEnabled = false
                buttonPauseProcessing.isEnabled = true
                if MinderManager.sharedInstance.dataProcessingPaused == true {
                    buttonPauseProcessing.title = "Resume Posture"
                } else {
                    buttonPauseProcessing.title = "Pause Posture"
                }
            } else {
                buttonEnableFeatures.isEnabled = true
                buttonPauseProcessing.isEnabled = false
            }
        } else {
            buttonConnection.title = "Connect"
        }
        buttonSkipCalibration.state = (MinderManager.sharedInstance.settings?.skipCalibration)!
        
        sliderTransperancy.floatValue = opp.transperancy
        sliderSensitivity.floatValue = opp.sensitivityPosture
        labelSensitivity.stringValue = "\(round(opp.sensitivityPosture * 100) / 100)"
        sliderSensitivityRotation.floatValue = opp.sensitivityRotation
        labelSensitivityRotation.stringValue = "\(round(opp.sensitivityRotation * 100) / 100)"
        sliderDifficulty.floatValue = 100 - opp.difficultyYellow
        if opp.volumeYellowMute {
            sliderVolume.floatValue = 0
            buttonVolumeMute.toolTip = "Unmute all volume"
            buttonVolumeMute.image = NSImage(named: "VolumeMute")
        } else {
            sliderVolume.floatValue = opp.volumeYellow
            buttonVolumeMute.toolTip = "Mute all volume"
            if opp.volumeYellow > 0.65 {
                buttonVolumeMute.image = NSImage(named: "VolumeMax")
            } else if opp.volumeYellow > 0.35 {
                buttonVolumeMute.image = NSImage(named: "VolumeMid")
            } else {
                buttonVolumeMute.image = NSImage(named: "VolumeMin")
            }
        }
        popButtonVibration.selectItem(at: opp.vibrateYellow)
        
        popButtonOPP.removeAllItems()
        var index: Int = 0
        for opp in MinderManager.sharedInstance.settings!.OPPs {
            let menuItem: NSMenuItem = NSMenuItem()
            menuItem.title = opp.title
            menuItem.tag = index
            popButtonOPP.menu?.addItem(menuItem)
            index += 1
        }
        popButtonOPP.selectItem(withTag: (MinderManager.sharedInstance.settings?.OPPSelected)!)
        
        if opp.offsetPoint.x == 0 && opp.offsetPoint.y == 0 {
            labelNotCalibrated.isHidden = false
        } else {
            labelNotCalibrated.isHidden = true
        }
        
        if MinderManager.sharedInstance.isGyroOn {
            buttonToggleGyroRotation.title = "Turn off rotation"
        } else {
            buttonToggleGyroRotation.title = "Turn on rotation"
        }
        popButtonPreferedDevice.selectItem(withTag: opp.notificationDevice)
        
    }
    
    @IBAction func saveEditOPPClicked(_ sender: AnyObject) {
        opp.title = txtEditOPP.stringValue
        RefreshControls()
        MinderManager.sharedInstance.SaveSettings()
        showHideOPPEditView(true)
    }
    
    @IBAction func cancelEditOPPClicked(_ sender: AnyObject) {
        showHideOPPEditView(true)
    }
    
    @IBAction func editNameOPPClicked(_ sender: AnyObject) {
        showHideOPPEditView(false)
        txtEditOPP.stringValue = opp.title
    }
    
    func showHideOPPEditView(_ sender: Bool){
        if sender {
            viewEditOPP.isHidden = true
        }
        else{
            viewEditOPP.isHidden = false
        }
    }
    
    // OPP Changing functionality
    @IBAction func OPPChanged(_ sender: NSPopUpButton) {
        MinderManager.sharedInstance.settings?.OPPSelected = sender.selectedTag()
        opp = MinderManager.sharedInstance.settings!.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!]
        
        sliderSensitivity.floatValue = opp.sensitivityPosture
        labelSensitivity.stringValue = "\(round(opp.sensitivityPosture * 100) / 100)"
        sliderSensitivityRotation.floatValue = opp.sensitivityRotation
        labelSensitivityRotation.stringValue = "\(round(opp.sensitivityRotation * 100) / 100)"
        delegateSettings?.UpdateDifficulty(.yellow, value: opp.difficultyYellow)
        sliderDifficulty.floatValue = 100 - opp.difficultyYellow
        delegateSettings?.UpdateDifficulty(.red, value: opp.difficultyRed)
        delegateSettings?.SetSeatback(opp.isSeatBack)
        delegateSettings?.UpdateDifficulty(.seatback, value: opp.difficultySeatback)
        sliderVolume.floatValue = opp.volumeYellow
        sliderTransperancy.floatValue = opp.transperancy
        popButtonPreferedDevice.selectItem(withTag: opp.notificationDevice)
        if opp.offsetPoint.x == 0 && opp.offsetPoint.y == 0 {
            labelNotCalibrated.isHidden = false
        } else {
            labelNotCalibrated.isHidden = true
        }
        
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings!.OPPChanged()
    }
    // Opp managing functionality
    @IBAction func ManageOPPClicked(_ sender: AnyObject) {
        delegateSettings?.OPPClicked(popButtonOPP.selectedTag())
    }
    // NewOPPClicked
    @IBAction func NewOPPClicked(_ sender: AnyObject) {
        delegateSettings?.OPPClicked(-1)
    }
    // ConnectionClicked
    @IBAction func ConnectionClicked(_ sender: AnyObject) {
        if buttonConnection.title == "Disconnect" {
            MinderManager.sharedInstance.forceDisconnect = true
            MinderManager.sharedInstance.DeviceDisconnect()
            sleep(2)
            MinderManager.sharedInstance.deviceName = ""
            textDeviceName.stringValue = "Device disconnected"
            buttonConnection.title = "Connect"
            buttonEnableFeatures.isEnabled = false
            buttonPauseProcessing.isEnabled = false
            MinderManager.sharedInstance.dataProcessingPaused = false
            MinderManager.sharedInstance.deviceFeaturesEnabled = false
        } else {
            MinderManager.sharedInstance.DeviceConnect()
            sleep(2)
        }
    }
    // EnableFeatures
    @IBAction func EnableFeaturesClicked(_ sender: AnyObject) {
        MinderManager.sharedInstance.DeviceEnableFeatures()
        
        buttonEnableFeatures.isEnabled = false
        buttonPauseProcessing.isEnabled = true
    }
    // PauseProcessing
    @IBAction func PauseProcessingClicked(_ sender: AnyObject) {
        delegateSettings?.UpdateProcessingPaused()
    }
    // SkipCalibration
    @IBAction func SkipCalibrationClicked(_ sender: AnyObject) {
        MinderManager.sharedInstance.settings?.skipCalibration = buttonSkipCalibration.state
        MinderManager.sharedInstance.SaveSettings()
    }
    // ToggleGyroRotation
    @IBAction func ToggleGyroRotation(_ sender: AnyObject) {
        delegateSettings?.ToggleGyroRotation()
    }
    // TransperancyChanged
    @IBAction func TransperancyChanged(_ sender: AnyObject) {
        opp.transperancy = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
    }
    // SensitivityChanged
    @IBAction func SensitivityChanged(_ sender: NSSlider) {
        opp.sensitivityPosture = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        labelSensitivity.stringValue = "\(round(opp.sensitivityPosture * 100) / 100)"
    }
    @IBAction func SensitivityRotationChanged(_ sender: AnyObject) {
        opp.sensitivityRotation = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        labelSensitivityRotation.stringValue = "\(round(opp.sensitivityRotation * 100) / 100)"
    }
    // VolumeChanged
    @IBAction func VolumeChanged(_ sender: NSSlider) {
        if sender.floatValue > 0 {
            opp.volumeYellowMute = false
            opp.volumeRedMute = false
        }
        opp.volumeYellow = sender.floatValue
        opp.volumeRed = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    // MuteClicked
    @IBAction func MuteClicked(_ sender: AnyObject) {
        if opp.volumeYellowMute {
            opp.volumeRedMute = false
            opp.volumeYellowMute = false
        } else {
            opp.volumeRedMute = true
            opp.volumeYellowMute = true
        }
        
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    // difficultyChanged
    @IBAction func DifficultyChanged(_ sender: NSSlider) {
        let difficulty = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.yellow, value: difficulty)
        opp.difficultyYellow = difficulty
        
        delegateSettings?.UpdateDifficulty(.red, value: difficulty)
        opp.difficultyRed = difficulty
        
        MinderManager.sharedInstance.SaveSettings()
    }
    // VibrationChanged
    @IBAction func VibrationChanged(_ sender: NSPopUpButton) {
        opp.vibrateYellow = (sender.selectedItem?.tag)!
        opp.vibrateRed = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    @IBAction func PreferedDeviceChanged(_ sender: AnyObject) {
        opp.notificationDevice = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
}
