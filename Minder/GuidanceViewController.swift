//
//  GuidanceViewController.swift
//  Minder
//
//  Created by Abdul on 8/9/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa
import AVKit
import AVFoundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum CollectionStyleType: Int {
    case grid = 0
    case list = 1
}

enum GuidanceViewType {
    case activity
    case record
}

protocol DelegateGuidance: class {
    func Replay(_ replayData: [PostureData], gyroData: [GyroData], isReplay: Bool)
    func ReplayClosed()
    func switchMeditationMode(_ flag:Bool)
    func AnimateMeditationGudeRing(_ meditationBreak: MeditationBreak)
    func EditRecordName(currentRecordName: String, currentRecordSystemName: String)
}

class GuidanceViewController: NSViewController {
    
    weak var delegateGuidance: DelegateGuidance?
    var started_time: String = ""
    var activitySelected: Activity?
    var recordingSelected: BallPathRecord?
    var currentViewType: GuidanceViewType = .activity
    
    @IBOutlet weak var collectionView: NSCollectionView!
    var collectionStyle: CollectionStyleType = .grid
    var dateSections: Array<String> = Array<String>()
    var sectionItems: Array<[AnyObject]> = Array<[AnyObject]>()
    
    @IBOutlet weak var buttonShowRecords: NSButton!
    @IBOutlet weak var buttonRecord: NSButton!
    @IBOutlet weak var buttonReplay: NSButton!
    @IBOutlet weak var buttonSaveRecord: NSButton!
    @IBOutlet weak var buttonRecordsDelete: NSButton!
    @IBOutlet weak var progressReplay: NSProgressIndicator!
    @IBOutlet weak var popButtonCollectionStyle: NSPopUpButton!
    @IBOutlet weak var buttonTabActivities: NSButton!
    @IBOutlet weak var buttonTabRecords: NSButton!
    @IBOutlet weak var buttonTabMeditation: NSButton!
    
    @IBOutlet weak var playerContainer: NSView!
    @IBOutlet var playerView: AVPlayerView!
    
    @IBOutlet weak var labelDescription: NSTextField!
    @IBOutlet weak var labelDuration: NSTextField!
    @IBOutlet weak var labelDateTime: NSTextField!
    @IBOutlet weak var labelTitle: NSTextField!
    @IBOutlet weak var labelIdealPathExists: NSTextField!
    
    var videoFinished: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.wantsLayer = true
        
        playerContainer.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        buttonRecord.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).cgColor
        buttonReplay.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).cgColor
        buttonShowRecords.attributedTitle = NSAttributedString(string: "Show Records", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : NSColor.white ])
        
        buttonTabActivities.layer?.backgroundColor = NSColor(calibratedWhite: 0.8 , alpha: 0.8).cgColor
        buttonTabActivities.attributedTitle = NSAttributedString(string: "Activities", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13) ])
        buttonTabRecords.layer?.backgroundColor = NSColor(calibratedWhite: 0.8 , alpha: 0.8).cgColor
        buttonTabMeditation.layer?.backgroundColor = NSColor(calibratedWhite: 0.8 , alpha: 0.8).cgColor // fbn
        
        GetGuidanceActivity()
        configureCollectionView(collectionStyle)
    }
    // MARK:GetRecords
    func GetRecords() {
        dateSections.removeAll()
        sectionItems.removeAll()
        
        var appDirPath = MinderManager.sharedInstance.appDirectory
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            appDirPath += (MinderManager.sharedInstance.userLogged?.userName)! + "/Records/"
        } else {
            appDirPath += "_/Records/"
        }
        
        if let enumerator = FileManager.default.enumerator(atPath: appDirPath) {
            var dicRecord: [BallPathRecord] = [BallPathRecord]()
            while let fileName = enumerator.nextObject() as? String {
                if fileName != ".DS_Store" && fileName.range(of: " ") != nil {
                    let date = fileName.substring(to: (fileName.range(of: " ")?.lowerBound)!)
                    if !dateSections.contains(date) {
                        if dateSections.count > 0 {
                            sectionItems.append(dicRecord)
                            dicRecord.removeAll()
                        }
                        dateSections.append(date)
                    }
                    let ballPath: BallPathRecord = (NSKeyedUnarchiver.unarchiveObject(withFile: appDirPath + fileName) as? BallPathRecord)!
                    if ballPath.gyroPaths.count > 0 && ballPath.posturePaths.count > 0 {
                        dicRecord.append(ballPath)
                    }
                    NSLog("Record found: \(fileName) ballPathCount: \(ballPath.posturePaths.count) gyroPathsCount: \(ballPath.gyroPaths.count)")
                }
            }
            if dicRecord.count > 0 {
                dicRecord = dicRecord.reversed()
                sectionItems.append(dicRecord)
            }
        }
        if dateSections.count > 0 {
            dateSections = dateSections.reversed()
            sectionItems = sectionItems.reversed()
        }
        collectionView.reloadData()
    }
    // MARK:GetGuidanceActivity
    func GetGuidanceActivity() {
        dateSections.removeAll()
        sectionItems.removeAll()
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            
            let appDelegate = NSApplication.shared().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            
            var arrScheduleDates: NSArray = NSArray()
            let fetchRequestActivitySchedule = NSFetchRequest<NSFetchRequestResult>(entityName: "Activity")
            
            fetchRequestActivitySchedule.propertiesToFetch = ["schedule_date"]
            fetchRequestActivitySchedule.resultType = .dictionaryResultType
            fetchRequestActivitySchedule.sortDescriptors = [NSSortDescriptor(key: "schedule_date", ascending: false)]
            fetchRequestActivitySchedule.predicate = NSPredicate(format: "user_email == %@", (MinderManager.sharedInstance.userLogged?.email)!)
            do {
                arrScheduleDates = try managedContext.fetch(fetchRequestActivitySchedule) as NSArray
            } catch {
                NSLog("Failed to fetch employees: \(error)")
            }
            print("arrScheduleDates: \(arrScheduleDates.count)")
            
            for _date in arrScheduleDates as! Array<NSDictionary> {
                let date = _date.value(forKey: "schedule_date") as! String
                if !dateSections.contains(date) {
                    let fetchRequestActivity = NSFetchRequest<NSFetchRequestResult>(entityName: "Activity")
                    fetchRequestActivity.predicate = NSPredicate(format: "schedule_date == %@ && user_email == %@", date, (MinderManager.sharedInstance.userLogged?.email)!)
                    fetchRequestActivity.sortDescriptors = [NSSortDescriptor(key: "schedule_id", ascending: false), NSSortDescriptor(key: "schedule_date_id", ascending: false)]
                    do {
                        var activities: [Activity] = try managedContext.fetch(fetchRequestActivity) as! [Activity]
                        print("Activity: \(activities) ")
                        // orders according to schedule time
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
                        activities = activities.sorted(by: {
                            let dateTime1 = "\($0.schedule_date!) \($0.start_time!)"
                            let date1 = dateFormatter.date(from: dateTime1)
                            
                            let dateTime2 = "\($1.schedule_date!) \($1.start_time!)"
                            let date2 = dateFormatter.date(from: dateTime2)
                            
                            return date1?.compare(date2!) == .orderedAscending
                        })
                        
                        let currentdateTime = "\(activities[0].schedule_date!) \(activities[0].start_time!)"
                        let currentDate = dateFormatter.date(from: currentdateTime)
                        
                        if (Calendar.current.isDateInToday(currentDate!)) {
                            var activitiesFilteredArray = activities.filter() {
                                if ($0.schedule_date as String!) != nil {
                                    let dateTime = "\($0.schedule_date!) \($0.start_time!)"
                                    let date = dateFormatter.date(from: dateTime)
                                    
                                    let compareResult = date?.timeIntervalSinceNow
                                    if ( compareResult > 0 ) {
                                        return true
                                    }else{
                                        return false
                                    }
                                } else {
                                    return false
                                }
                            }
                            let activitiesPastFilteredArray = activities.filter() {
                                if ($0.schedule_date as String!) != nil {
                                    let dateTime = "\($0.schedule_date!) \($0.start_time!)"
                                    let date = dateFormatter.date(from: dateTime)
                                    
                                    let compareResult = date?.timeIntervalSinceNow
                                    if ( compareResult < 0 ) {
                                        return true
                                    }else{
                                        return false
                                    }
                                } else {
                                    return false
                                }
                            }
                            activitiesFilteredArray.append(contentsOf: activitiesPastFilteredArray)
                            activities = activitiesFilteredArray
                        }
                        //
                        sectionItems.append(activities)
                        dateSections.append(date)
                    } catch {
                        NSLog("Failed to fetch employees: \(error)")
                    }
                }
            }
        }
        collectionView.reloadData()
    }
    // MARK:TabActivityTapped
    @IBAction func TabActivityTapped(_ sender: AnyObject) {
        delegateGuidance?.switchMeditationMode(false)
        buttonTabActivities.attributedTitle = NSAttributedString(string: "Activities", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13) ])
        buttonTabRecords.title = "Records"
        currentViewType = .activity
        GetGuidanceActivity()
    }
    // MARK:TabRecordTapped
    @IBAction func TabRecordTapped(_ sender: AnyObject) {
        delegateGuidance?.switchMeditationMode(false)
        buttonTabActivities.title = "Activities"
        buttonTabRecords.attributedTitle = NSAttributedString(string: "Records", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13) ])
        currentViewType = .record
        GetRecords()
    }
    @IBAction func TabMeditationTapped(_ sender: AnyObject) {
        delegateGuidance?.switchMeditationMode(true)
    }
   
    // MARK:RecordTapped
    @IBAction func RecordTapped(_ sender: NSButton) {
        
    }
    // MARK:ShowRecordsTapped
    @IBAction func ShowRecordsTapped(_ sender: NSButton) {
        if sender.state == 1 {
            currentViewType = .record
            GetRecords()
        } else {
            currentViewType = .activity
            GetGuidanceActivity()
        }
    }
    // MARK:configureCollectionView
    fileprivate func configureCollectionView(_ type: CollectionStyleType) {
        let flowLayout = NSCollectionViewFlowLayout()
        switch type {
        case .grid:
            flowLayout.itemSize = NSSize(width: 132.0, height: 140.0)
        case .list:
            flowLayout.itemSize = NSSize(width: 920.0, height: 140.0)
        }
        flowLayout.sectionInset = EdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        flowLayout.minimumInteritemSpacing = 10.0
        flowLayout.minimumLineSpacing = 10.0
        collectionView.collectionViewLayout = flowLayout
        collectionView.layer?.backgroundColor = NSColor.black.cgColor
        collectionView.reloadData()
    }
    // MARK:CollectionStyleChanged
    @IBAction func CollectionStyleChanged(_ sender: NSPopUpButton) {
        collectionStyle = CollectionStyleType(rawValue: (sender.selectedItem?.tag)!)!
        configureCollectionView(collectionStyle)
    }
    // MARK:ReplayFinished
    func ReplayFinished() {
        //dispatch_async(dispatch_get_main_queue(), {
        MinderManager.sharedInstance.replayState = .none
        self.buttonReplay.title = "PLAY"
        self.buttonReplay.image = NSImage(named: "Play")
        //})
    }
    // MARK:ReplayTapped
    @IBAction func ReplayTapped(_ sender: NSButton) {
        switch MinderManager.sharedInstance.replayState {
        case .none:
            progressReplay.maxValue = Double(recordingSelected!.posturePaths.count)
            delegateGuidance?.Replay(recordingSelected!.posturePaths, gyroData: (recordingSelected?.gyroPaths)!, isReplay: true)
            buttonReplay.title = "PAUSE"
            buttonReplay.image = NSImage(named: "Pause")
        case .play:
            MinderManager.sharedInstance.replayState = .pause
            buttonReplay.title = "PLAY"
            buttonReplay.image = NSImage(named: "Play")
        case .pause:
            MinderManager.sharedInstance.replayState = .play
            buttonReplay.title = "PAUSE"
            buttonReplay.image = NSImage(named: "Pause")
        }
    }
    // MARK:SaveRecordTapped
    @IBAction func SaveRecordTapped(_ sender: AnyObject) {
        let savePanel = NSSavePanel()
        //savePanel.setTitleWithRepresentedFilename(recordingSelected.)
        savePanel.title = "Save Recording"
        savePanel.canCreateDirectories = true
        savePanel.showsHiddenFiles = true
        savePanel.level = Int(CGShieldingWindowLevel())
        savePanel.nameFieldStringValue = "Record \(recordingSelected!.name).mndr"
        savePanel.begin { (result) -> Void in
            if result == NSFileHandlingPanelOKButton
            {
                var appDirPath = MinderManager.sharedInstance.appDirectory
                if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
                    appDirPath += (MinderManager.sharedInstance.userLogged?.userName)! + "/Records/"
                } else {
                    appDirPath += "_/Records/"
                }
                appDirPath += self.recordingSelected!.name + ".mndr"
                do {
                    try FileManager.default.copyItem(at: URL(fileURLWithPath: appDirPath), to: savePanel.url!)
                } catch let error as NSError {
                    let myPopup: NSAlert = NSAlert()
                    myPopup.messageText = "Record saving failed"
                    myPopup.informativeText = "Ooops! Something went wrong:\r\n\(error)"
                    myPopup.alertStyle = .warning
                    myPopup.addButton(withTitle: "OK")
                    myPopup.runModal()
                }
            }
        }
    }
    // MARK:RecordsDeleteClicked
    @IBAction func RecordsDeleteClicked(_ sender: AnyObject) {
        let confirmPopup: NSAlert = NSAlert()
        confirmPopup.messageText = "Record delete confirmation"
        confirmPopup.informativeText = "Are you sure you want to delete this recording ? Recording will be no longer available."
        confirmPopup.alertStyle = .informational
        confirmPopup.addButton(withTitle: "OK")
        confirmPopup.addButton(withTitle: "Cancel")
        let res = confirmPopup.runModal()
        if res == NSAlertFirstButtonReturn {
            var appDirPath = MinderManager.sharedInstance.appDirectory
            if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
                appDirPath += (MinderManager.sharedInstance.userLogged?.userName)! + "/Records/"
            } else {
                appDirPath += "_/Records/"
            }
            appDirPath += self.recordingSelected!.name + ".mndr"
            do {
                try FileManager.default.removeItem(at: URL(fileURLWithPath: appDirPath))
                let myPopup: NSAlert = NSAlert()
                myPopup.messageText = "Record deleted"
                myPopup.informativeText = "You have successfully deleted this recording"
                myPopup.alertStyle = .warning
                myPopup.addButton(withTitle: "OK")
                myPopup.runModal()
                GetRecords()
                CloseTapped(NSButton())
            } catch let error as NSError {
                let myPopup: NSAlert = NSAlert()
                myPopup.messageText = "Record delete failed"
                myPopup.informativeText = "Ooops! Something went wrong:\r\n\(error)"
                myPopup.alertStyle = .warning
                myPopup.addButton(withTitle: "OK")
                myPopup.runModal()
            }
        }
    }
    // MARK:UpdateReplayProgress
    func UpdateReplayProgress(_ progress: Int) {
        progressReplay.doubleValue = Double(progress)
    }
    // MARK:CloseTapped
    @IBAction func CloseTapped(_ sender: NSButton) {
        videoFinished = true
        if currentViewType == .record {
            buttonReplay.title = "PLAY"
            buttonReplay.image = NSImage(named: "Play")
            recordingSelected = nil
        } else {
            /*dispatch_async(dispatch_get_main_queue(), {
                NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: self.playerView.player?.currentItem)
                self.playerView.player?.removeObserver(self, forKeyPath:"rate", context: nil)
            })
            playerView.player = nil*/
            let result: String = ""
            let completed_time: String = Date.GetDateString(Date(), format: "yyyy-MM-dd hh:mm:ss")
            MinderManager.sharedInstance.SetExerciseStatus((activitySelected?.program_id?.intValue)!, schedule_date_id: (activitySelected?.schedule_date_id?.intValue)!, schedule_id: (activitySelected?.schedule_id?.intValue)!, modified_id: (activitySelected?.modified_id?.intValue)!, result: result, started_time: started_time, completed_time: completed_time, completionHandler: {_ in})
            activitySelected = nil
            delegateGuidance!.ReplayClosed()
        }
        MinderManager.sharedInstance.isActivityPlaying = false
        MinderManager.sharedInstance.replayState = .none
        playerContainer.isHidden = true
        collectionView.isSelectable = true
        buttonRecord.isEnabled = true
        buttonShowRecords.isEnabled = true
        popButtonCollectionStyle.isEnabled = true
        buttonTabRecords.isEnabled = true
        buttonTabActivities.isEnabled = true
    }
}

extension GuidanceViewController : NSCollectionViewDataSource {
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        //return imageDirectoryLoader.numberOfSections
        return dateSections.count
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return sectionItems[section].count
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        
        var item: NSCollectionViewItem?
        
        var appDirPath = MinderManager.sharedInstance.appDirectory
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            appDirPath += (MinderManager.sharedInstance.userLogged?.userName)!
        } else {
            appDirPath += "_"
        }
        
        switch collectionStyle {
        case .grid:
            item = collectionView.makeItem(withIdentifier: "CollectionViewItem", for: indexPath)
            guard let collectionViewItem = item as? CollectionViewItem else { return item! }
            
            if currentViewType == .record {
                let recording = sectionItems[indexPath.section][indexPath.item] as! BallPathRecord
                collectionViewItem.textField?.stringValue = "Duration: \(Int(recording.posturePaths.count / 10)) s \(Int(recording.posturePaths.count % 10)) ms"
                collectionViewItem.imageView?.image = recording.image
                if let textField: NSTextField = collectionViewItem.view.viewWithTag(1) as? NSTextField {
                    textField.stringValue = ""
                }
            } else {
                let activity = sectionItems[indexPath.section][indexPath.item] as! Activity
                let fileName = appDirPath + "/Activities/\(activity.schedule_id!).png"
                if FileManager.default.fileExists(atPath: fileName) {
                    collectionViewItem.imageView?.image = NSImage(contentsOfFile: fileName)
                } else {
                    collectionViewItem.imageView?.DownloadFrom(link: activity.additional_file)
                }
                collectionViewItem.textField?.stringValue = activity.media_title!
                
                var labelSchedule: NSTextField? = NSTextField()
                if let textField: NSTextField = collectionViewItem.view.viewWithTag(1) as? NSTextField {
                    labelSchedule = textField
                } else {
                    labelSchedule = NSTextField(frame: NSMakeRect(1, 120, 130, 17))
                    labelSchedule!.tag = 1
                    labelSchedule!.isEditable = false
                    labelSchedule!.isBezeled = false
                    labelSchedule!.drawsBackground = false
                    labelSchedule!.isSelectable = false
                    collectionViewItem.view.addSubview(labelSchedule!)
                }
                labelSchedule!.stringValue = "\(activity.schedule_date!) \(activity.start_time!)"
            }
            
        case .list:
            item = collectionView.makeItem(withIdentifier: "CollectionViewItemList", for: indexPath)
            guard let collectionViewItem = item as? CollectionViewItemList else { return item! }
            
            if currentViewType == .record {
                let recording = sectionItems[indexPath.section][indexPath.item] as! BallPathRecord
                collectionViewItem.textField?.stringValue = "Duration: \(Int(recording.posturePaths.count / 10)) s \(Int(recording.posturePaths.count % 10)) ms"
                collectionViewItem.imageView?.image = recording.image
                if let textField: NSTextField = collectionViewItem.view.viewWithTag(1) as? NSTextField {
                    textField.stringValue = ""
                }
                if let textField: NSTextField = collectionViewItem.view.viewWithTag(2) as? NSTextField {
                    textField.stringValue = ""
                }
                if let textField: NSTextField = collectionViewItem.view.viewWithTag(3) as? NSTextField {
                    textField.stringValue = ""
                }
            } else {
                let activity = sectionItems[indexPath.section][indexPath.item] as! Activity
                
                let fileNamePNG = appDirPath + "/Activities/\(activity.schedule_id!).png"
                if FileManager.default.fileExists(atPath: fileNamePNG) {
                    collectionViewItem.imageView?.image = NSImage(contentsOfFile: fileNamePNG)
                } else {
                    collectionViewItem.imageView?.DownloadFrom(link: activity.additional_file)
                }
                collectionViewItem.textField?.stringValue = activity.media_title!
                
                var labelSchedule: NSTextField? = NSTextField()
                if let textField: NSTextField = collectionViewItem.view.viewWithTag(1) as? NSTextField {
                    labelSchedule = textField
                } else {
                    labelSchedule = NSTextField(frame: NSMakeRect(144, 86, 273, 17))
                    labelSchedule!.tag = 1
                    labelSchedule!.isEditable = false
                    labelSchedule!.isBezeled = false
                    labelSchedule!.drawsBackground = false
                    labelSchedule!.isSelectable = false
                    collectionViewItem.view.addSubview(labelSchedule!)
                }
                labelSchedule!.stringValue = "Schedule: \(activity.schedule_date!) \(activity.start_time!)"
                
                let duration = "Duration: \(MinderUtility.SecondsToHMSString(activity.duration!.intValue))"
                
                var labelDuration: NSTextField? = NSTextField()
                if let textField: NSTextField = collectionViewItem.view.viewWithTag(2) as? NSTextField {
                    labelDuration = textField
                } else {
                    labelDuration = NSTextField(frame: NSMakeRect(144, 61, 273, 17))
                    labelDuration!.tag = 2
                    labelDuration!.isEditable = false
                    labelDuration!.isBezeled = false
                    labelDuration!.drawsBackground = false
                    labelDuration!.isSelectable = false
                    collectionViewItem.view.addSubview(labelDuration!)
                }
                labelDuration!.stringValue = duration
                
                var labelDescription: NSTextField? = NSTextField()
                if let textField: NSTextField = collectionViewItem.view.viewWithTag(3) as? NSTextField {
                    labelDescription = textField
                } else {
                    labelDescription = NSTextField(frame: NSMakeRect(144, 6, 273, 47))
                    labelDescription!.tag = 3
                    labelDescription!.isEditable = false
                    labelDescription!.isBezeled = false
                    labelDescription!.drawsBackground = false
                    labelDescription!.isSelectable = false
                    collectionViewItem.view.addSubview(labelDescription!)
                }
                labelDescription!.stringValue = "Description: \(activity.media_description!)"
            }
        }
        
        
        return item!
    }
    
    func collectionView(_ collectionView: NSCollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> NSView {
        let view = collectionView.makeSupplementaryView(ofKind: NSCollectionElementKindSectionHeader, withIdentifier: "HeaderView", for: indexPath) as! HeaderView
        view.sectionTitle.stringValue = dateSections[indexPath.section]
        if buttonShowRecords.state == 1 {
            view.imageCount.stringValue = "\(sectionItems[indexPath.section].count) recordings"
        } else {
            view.imageCount.stringValue = "\(sectionItems[indexPath.section].count) activities"
        }
        return view
    }
}

extension GuidanceViewController : NSCollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> NSSize {
        //return imageDirectoryLoader.singleSectionMode ? NSZeroSize : NSSize(width: 1000, height: 40)
        return NSSize(width: 456, height: 30)
    }
}

extension GuidanceViewController : NSCollectionViewDelegate {
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        PerformCollectionClick(indexPaths)
        collectionView.deselectItems(at: indexPaths)
    }
    func PerformCollectionClick(_ indexPaths: Set<IndexPath>) {
        guard let indexPath = indexPaths.first else { return }
        if currentViewType == .record {
            recordingSelected = sectionItems[indexPath.section][indexPath.item] as? BallPathRecord
            labelTitle.isHidden = true
            labelDateTime.stringValue = "Date: \(dateSections[indexPath.section])"
            labelDuration.stringValue = "Duration: \(Int(recordingSelected!.posturePaths.count / 10)) s \(Int(recordingSelected!.posturePaths.count % 10)) ms"
            labelDescription.isHidden = true
            buttonReplay.isHidden = false
            buttonSaveRecord.isHidden = false
            buttonRecordsDelete.isHidden = false
            progressReplay.isHidden = false
            playerView.isHidden = true
            
            playerContainer.isHidden = false
            collectionView.isSelectable = false
            buttonRecord.isEnabled = false
            buttonShowRecords.isEnabled = false
            popButtonCollectionStyle.isEnabled = false
            buttonTabRecords.isEnabled = false
            buttonTabActivities.isEnabled = false
        } else {
            activitySelected = sectionItems[indexPath.section][indexPath.item] as? Activity
            var appDirPath = MinderManager.sharedInstance.appDirectory
            if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
                appDirPath += (MinderManager.sharedInstance.userLogged?.userName)! + "/Activities/"
            } else {
                appDirPath += "_/Activities/"
            }
            let filePathMP4 = "\(appDirPath)\(activitySelected!.schedule_id!).mp4"
            if !FileManager.default.fileExists(atPath: filePathMP4) {
                let myPopup: NSAlert = NSAlert()
                myPopup.messageText = "Download in progress.."
                myPopup.informativeText = "Download in progress. Please try after some time"
                myPopup.alertStyle = .warning
                myPopup.addButton(withTitle: "OK")
                myPopup.runModal()
            } else {
                videoFinished = false
                var ballPath: BallPathRecord? = nil
                let fileName = appDirPath + "\(activitySelected!.schedule_id!).mndr"
                if FileManager.default.fileExists(atPath: fileName) {
                    ballPath = (NSKeyedUnarchiver.unarchiveObject(withFile: fileName) as? BallPathRecord)!
                    if ballPath!.gyroPaths.count > 0 && ballPath!.posturePaths.count > 0 {
                        labelIdealPathExists!.stringValue = "Ideal path exists"
                    } else {
                        labelIdealPathExists!.stringValue = "Wrong ideal path exists"
                    }
                    NSLog("Ideal path: \(fileName) PosturePathCount:\(ballPath?.posturePaths.count) GyroPathCount:\(ballPath?.gyroPaths.count)")
                } else {
                    labelIdealPathExists!.stringValue = "Ideal path not found"
                }
                
                let url:URL = URL(fileURLWithPath: filePathMP4)
                let playerItem = AVPlayerItem(url: url)
                NotificationCenter.default.addObserver(self, selector: #selector(GuidanceViewController.playerDidFinishPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                playerView.player = AVPlayer(playerItem: playerItem)
                playerView.player?.addObserver(self, forKeyPath:"rate", options: NSKeyValueObservingOptions(rawValue: UInt(0)), context: nil)
                
                self.playerView.player?.play()
                if ballPath != nil {
                    DispatchQueue.main.async(execute: {
                        MinderManager.sharedInstance.isActivityPlaying = true
                        self.delegateGuidance?.Replay(ballPath!.posturePaths, gyroData: ballPath!.gyroPaths, isReplay: false)
                    })
                }
                
                started_time = Date.GetDateString(Date(), format: "yyyy-MM-dd hh:mm:ss")
                
                labelTitle.isHidden = false
                labelDescription.isHidden = false
                buttonReplay.isHidden = true
                buttonSaveRecord.isHidden = true
                buttonRecordsDelete.isHidden = true
                progressReplay.isHidden = true
                playerView.isHidden = false
                labelTitle.stringValue = activitySelected!.media_title!
                labelDateTime.stringValue = "Schedule: \(activitySelected!.schedule_date!) \(activitySelected!.start_time!)"
                let duration = "Duration: \(MinderUtility.SecondsToHMSString(activitySelected!.duration!.intValue))"
                labelDuration.stringValue = duration
                labelDescription.stringValue = "Description: " + activitySelected!.media_description!
                
                playerContainer.isHidden = false
                collectionView.isSelectable = false
                buttonRecord.isEnabled = false
                buttonShowRecords.isEnabled = false
                popButtonCollectionStyle.isEnabled = false
                buttonTabRecords.isEnabled = false
                buttonTabActivities.isEnabled = false
            }
        }
    }
    
    // MARK:playerDidFinishPlaying
    func playerDidFinishPlaying(_ note: Notification) {
        self.perform(#selector(CloseTapped), with: nil, afterDelay: 1)
        print("playerDidFinishPlaying Video Finished")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "actionAtItemEnd"{
            print("observeValueForKeyPath Finished")
        } else if keyPath == "rate" {
            if !videoFinished {
                if playerView.player?.rate == 0 {
                    DispatchQueue.main.async(execute: {
                        MinderManager.sharedInstance.replayState = .pause
                        print("observeValueForKeyPath Paused")
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        MinderManager.sharedInstance.replayState = .play
                        print("observeValueForKeyPath Play")
                    })
                }
            }
        }
    }
}
