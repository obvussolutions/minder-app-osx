//
//  MeditationReportCard.swift
//  Minder
//
//  Created by Febin on 5/11/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Foundation

struct MeditationReportCard {
    var totalCycles:Int
    var totalTime:Int
    var perfectInhalePersentage:[CGFloat]
    var perfectExcalePersentage:[CGFloat]
    var BPM:CGFloat = 60
    
    init(totalCycles:Int, totalTime:Int, perfectInhalePersentage:[CGFloat], perfectExcalePersentage:[CGFloat]) {
        self.totalCycles = totalCycles
        self.totalTime = totalTime
        self.perfectInhalePersentage = perfectInhalePersentage
        self.perfectExcalePersentage = perfectExcalePersentage
    }
}
