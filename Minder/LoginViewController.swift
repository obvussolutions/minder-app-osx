//
//  LoginViewController.swift
//  Minder
//
//  Created by Abdul on 8/24/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa
import CoreBluetooth

enum EnterType {
    case withLogin
    case withOutLogin
}

class LoginViewController: NSViewController, NSTextFieldDelegate  {
    
    var centralManager: CBCentralManager?
    var peripherals = Array<String>()
    
    weak var delegateLogin: DelegateMainView? = nil
    
    @IBOutlet weak var progressLogin: NSProgressIndicator!
    @IBOutlet var loginBaseView: NSView!
    @IBOutlet var InnerLoginPageView: NSView!
    
    @IBOutlet var userName: NSTextField!
    @IBOutlet var password: NSTextField!
//    @IBOutlet var deviceName: NSTextField!
    @IBOutlet var deviceListField: NSComboBox!
    @IBOutlet var userNameErrorMessage: NSTextField!
    @IBOutlet var passwordErrorMessage: NSTextField!
    @IBOutlet var deviceNameErrorMessage: NSTextField!
    @IBOutlet var invalidUserCredentials: NSTextField!
    
    @IBOutlet var loginButtonClick: NSButton!
    @IBOutlet var enterWithoutLoginButton: NSButton!
    
    var _deviceName : String? =  nil
//    NSString *_deviceName;
    //@IBOutlet weak var deviceNamePopUp: NSPopUpButton!
    
    var initialLocation: NSPoint = NSPoint()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.wantsLayer = true
        loginBaseView.wantsLayer = true
        InnerLoginPageView.wantsLayer = true
        
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
        
        userName.delegate = self
        password.delegate = self
        
        userNameErrorMessage.isHidden = true
        passwordErrorMessage.isHidden = true
        deviceNameErrorMessage.isHidden = true
        invalidUserCredentials.isHidden = true
       // deviceName.stringValue = MinderManager.sharedInstance.updatedDeviceName
//        deviceNamePopUp.removeAllItems()
//        deviceNamePopUp.isHidden = true
    }
    override func viewWillAppear() {
        self.view.window?.title = "Minder"
        loginBaseView.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1).cgColor
        
        InnerLoginPageView.wantsLayer = true
        InnerLoginPageView.layer?.backgroundColor = NSColor.white.cgColor
        MinderUtility.ShapeView(InnerLoginPageView, type: .rectTopCurvedHalf)
        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        loginButtonClick.attributedTitle = NSAttributedString(string: "Login", attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style])
        enterWithoutLoginButton.attributedTitle = NSAttributedString(string: "Enter without Login", attributes: [ NSForegroundColorAttributeName : NSColor.red, NSParagraphStyleAttributeName : style,NSUnderlineStyleAttributeName : 1])
        
        NSLog("Setup Login view done")
    }
    override func viewDidAppear() {
        if let screenSize = NSScreen.main()!.frame.size as? CGSize {
            view.window!.setFrameOrigin(NSPoint(x: screenSize.width/2 - view.window!.frame.size.width/2, y: (screenSize.height - view.window!.frame.size.height)/2))
        }
    }
    //MouseDraggedEvent
    override func mouseDown(with theEvent: NSEvent) {
        let windowFrame = self.view.window!.frame
        initialLocation = NSEvent.mouseLocation()
        initialLocation.x -= windowFrame.origin.x
        initialLocation.y -= windowFrame.origin.y
    }
    //MouseDownEvent
    override func mouseDragged(with theEvent: NSEvent) {
        var newOrigin: NSPoint = NSPoint()
        let screenFrame = NSScreen.main()!.frame
        let windowFrame = self.view.window!.frame
        let currentLocation: NSPoint = NSEvent.mouseLocation()
        newOrigin.x = currentLocation.x - initialLocation.x
        newOrigin.y = currentLocation.y - initialLocation.y
        if (newOrigin.y + windowFrame.size.height) > (screenFrame.origin.y + screenFrame.size.height) {
            newOrigin.y = screenFrame.origin.y + (screenFrame.size.height - windowFrame.size.height)
        }
        self.view.window!.setFrameOrigin(newOrigin)
    }
    
    //  Validating before data submition
    override func controlTextDidChange(_ obj: Notification) {
        if (obj.object! as AnyObject)  .isEqual(userName) {
            userNameErrorMessage.isHidden = true
            invalidUserCredentials.isHidden = true
        }
        if (obj.object! as AnyObject)  .isEqual(password) {
            passwordErrorMessage.isHidden = true
            invalidUserCredentials.isHidden = true
        }
        if (obj.object! as AnyObject)  .isEqual(deviceListField) {
            deviceNameErrorMessage.isHidden = true
            invalidUserCredentials.isHidden = true
        }
    }
    
    // Normal Login Actions
    @IBAction func loginButtonClick(_ sender: AnyObject) {
        if userName.stringValue == "" || password.stringValue == "" || deviceListField.stringValue == "" {
            if userName.stringValue == "" {
                userNameErrorMessage.isHidden = false
            }
            if password.stringValue == "" {
                passwordErrorMessage.isHidden = false
            }
            if deviceListField.stringValue == "" {
                deviceNameErrorMessage.isHidden = false
            }
        } else {
            progressLogin.isHidden = false
            progressLogin.startAnimation(nil)
            MinderManager.sharedInstance.Login(userName.stringValue, password: password.stringValue, deviceName: deviceListField.stringValue, completionHandler: { data -> Void in
                if data {
                    // Create user folders - Activities, Records
                    let fileManager = FileManager.default
                    var isDir : ObjCBool = true
                    var path: String = MinderManager.sharedInstance.appDirectory + (MinderManager.sharedInstance.userLogged?.userName)! + "/Activities/"
                    if !fileManager.fileExists(atPath: path, isDirectory: &isDir) {
                        do {
                            try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                        } catch let error as NSError {
                            print("Error while creating Activities folder. \(error)")
                            
                        }
                    }
                    path = MinderManager.sharedInstance.appDirectory + (MinderManager.sharedInstance.userLogged?.userName)! + "/Records/"
                    if !fileManager.fileExists(atPath: path, isDirectory: &isDir) {
                        do {
                            try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                        } catch let error as NSError {
                            print("Error while creating Records folder. \(error)")
                        }
                    }
                    MinderManager.sharedInstance.LoadSettings()
                    self.centralManager?.stopScan()
                    DispatchQueue.main.async {
                        MinderManager.sharedInstance.deviceName = self.deviceListField.stringValue
                    }
                    
                    self.delegateLogin?.updateMainView(.home, typeLogin: .withLogin)
                } else {
                    DispatchQueue.main.async(execute: {
                        self.invalidUserCredentials.isHidden = false
                    })
                }
                DispatchQueue.main.async(execute: {
                    self.progressLogin.stopAnimation(nil)
                    self.progressLogin.isHidden = true
                })
            })
        }
    }
    
    // Button action to enter without Login
    @IBAction func enterWithoutLogin(_ sender: AnyObject) {
        if deviceListField.stringValue == "" {
            deviceNameErrorMessage.isHidden = false
        } else {
            MinderManager.sharedInstance.deviceName = deviceListField.stringValue
            // Create user folders - Activities, Records
            let fileManager = FileManager.default
            var isDir : ObjCBool = true
            var path: String = MinderManager.sharedInstance.appDirectory + "_/Activities/"
            if !fileManager.fileExists(atPath: path, isDirectory: &isDir) {
                do {
                    try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError {
                    print("Error while creating Activities folder. \(error)")
                }
            }
            path = MinderManager.sharedInstance.appDirectory + "_/Records/"
            if !fileManager.fileExists(atPath: path, isDirectory: &isDir) {
                do {
                    try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError {
                    print("Error while creating Records folder. \(error)")
                }
            }
            self.delegateLogin?.updateMainView(.home, typeLogin: .withOutLogin)
        }
    }
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    // Interval2RedChanged
    @IBAction func deviceNameChanged(_ sender: NSPopUpButton) {
        
        deviceListField.stringValue = (sender.selectedItem?.title)!
        MinderManager.sharedInstance.deviceName = deviceListField.stringValue
    }
//    func getDeviceNames() {
//        if peripherals == nil {
//            peripherals = [Any]() as! Array<String>
//        }
//        else {
//            peripherals.removeAll()
//        }
//        _deviceName = nil
//        if centralManager?.state == .poweredOn {
//            self.centralManager?.scanForPeripherals(withServices: nil, options: nil)
//        }
//    }
}
extension LoginViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if (central.state == .poweredOn){
            // self.centralManager?.scanForPeripherals(withServices: [NSArray ob], options: [CBCentralManagerScanOptionAllowDuplicatesKey : false])
            var scanOptions = [ CBCentralManagerScanOptionAllowDuplicatesKey : (false ? 1 : 0) ]
            self.centralManager?.scanForPeripherals(withServices: [CBUUID(string: DEVICE_SERVICE_UUID)], options: scanOptions)
        }
        else {
            // do something like alert the user that ble is not on
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
  //      deviceNamePopUp.isHidden = false
        NSLog("11111 peripherals name  \(peripheral)")
        print("11111 peripherals name  \(peripheral)")
        if peripheral.name != nil
        {
            print("222222 peripherals name  \(peripheral.name)")
            if !(peripherals.contains(peripheral.name!))
            {
                    print("(In if condition) peripheral selected name \(peripheral.name)")
                    peripherals.append(peripheral.name!)
                    deviceListField.addItem(withObjectValue: peripheral.name!)
                    deviceListField.stringValue = deviceListField.itemObjectValue(at: 0) as! String
                
            }
            else
            {
                central.stopScan()
                deviceListField.stringValue = deviceListField.itemObjectValue(at: 0) as! String
//                deviceListField.stringValue = deviceNamePopUp.itemTitle(at: 0)
                MinderManager.sharedInstance.deviceName = deviceListField.stringValue
                print("(In else condition) peripherals are \(peripheral)")
            }
        }

        
    }
}
