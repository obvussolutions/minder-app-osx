//
//  DeviceImitationView.swift
//  Minder
//
//  Created by Abdul on 8/26/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

protocol DelegateImitationView: class {
    func curveMenuClick(_ show: Int)
}

class PostureView: NSView {
    var originBall: CGPoint = CGPoint(x: 0, y: 0)
    var hideCircleBall: Bool = false
    var isRecordView: Bool = false
    var isReplay: Bool = false
    var idealPath: NSBezierPath? = nil
    var recordPath: NSBezierPath? = nil
    
    var difficultyRed: CGFloat = 0.0
    var difficultyYellow: CGFloat = 0.0
    var isSeatback: Int = 0
    var difficultySeatback: CGFloat = 0.0
    
    weak var idelegate: DelegateImitationView?
    var showProgressCircle: Bool = true
    var isInBadPosture: Bool = true
    var textAlert: String = ""
    
    var percentRed: CGFloat = 0.0
    var percentYellow: CGFloat = 0.0
    var percentGreen: CGFloat = 0.0
    
    var pitch: CGFloat = 0.0
    var roll: CGFloat = 0.0
    var yaw: CGFloat = 0.0
    
    var overZoneCircle: ZoneType = .none
//    let gradientLayer:CAGradientLayer = CAGradientLayer()
//    let myMainLayer:CAShapeLayer = CAShapeLayer()

    
//    let myString = "Swift Attributed String"
//    let myAttribute = [ NSForegroundColorAttributeName: NSColor.blue ]
//    let currentText = NSAttributedString(string: "", attributes: myAttribute as? [String : AnyObject])
    //NSAttributedString(string: myString, attributes: myAttribute)
    
    override func viewWillDraw() {
        
//        gradientLayer.frame.size = self.frame.size
//        gradientLayer.colors = [NSColor.black.cgColor,NSColor.green.withAlphaComponent(1).cgColor]
//        self.layer?.insertSublayer(gradientLayer, at: 0)
//        
//        myMainLayer.frame.size = self.frame.size
//        myMainLayer.backgroundColor = NSColor.clear.cgColor
//        self.layer?.insertSublayer(myMainLayer, at: 1)
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        let center = CGPoint(x: dirtyRect.size.width/2, y: dirtyRect.size.height/2)
        if isRecordView {
            let lineWidth = (dirtyRect.size.height * 2) / 100
            if recordPath != nil {
                NSColor.green.set()
                recordPath!.lineWidth = lineWidth
                recordPath!.stroke()
            }
            if idealPath != nil {
                NSColor.white.set()
                idealPath!.lineWidth = lineWidth
                idealPath!.stroke()
            }
        } else {
            if showProgressCircle {
                let lineWidth = (dirtyRect.size.height * 15) / 100
                let gPath: NSBezierPath = NSBezierPath(ovalIn: NSMakeRect(0, 0, dirtyRect.size.height, dirtyRect.size.height))
                gPath.close()
                NSColor.green.set()
                gPath.lineWidth = lineWidth
                gPath.stroke()
                /*if percentYellow != 0 {
                 let yAngle = (percentYellow * 360) / 100
                 let yPath: NSBezierPath = NSBezierPath()
                 yPath.appendArc(withCenter: center, radius: dirtyRect.size.height/2, startAngle: 0, endAngle: yAngle)
                 NSColor.yellow.set()
                 yPath.lineWidth = lineWidth
                 yPath.stroke()
                 }
                 if percentRed != 0 {
                 let rPath: NSBezierPath = NSBezierPath()
                 let rAngle: CGFloat = 360 - (percentRed * 360) / 100
                 rPath.appendArc(withCenter: center, radius: dirtyRect.size.height/2, startAngle: rAngle, endAngle: 360)
                 NSColor.red.set()
                 rPath.lineWidth = lineWidth
                 rPath.stroke()
                 }*/
            }
            // Seatback used to show resize indication on outer circle to resize view entirely.
            if overZoneCircle == .green {
                //                let pattern: [CGFloat] = [5.0, 5.0]
                //                //NSColor.blueColor().set()
                //                let gPath: NSBezierPath = NSBezierPath(ovalInRect: NSMakeRect(0, 0, dirtyRect.size.height, dirtyRect.size.height))
                //                gPath.setLineDash(pattern, count: 2, phase: 0.0)
                //                gPath.lineWidth = (dirtyRect.size.height * 2) / 100
                //                gPath.stroke()
            }
            if textAlert != "" {
                let attributes: NSDictionary = [
                    NSFontAttributeName : NSFont.boldSystemFont(ofSize: dirtyRect.size.height * 0.04),
                    NSForegroundColorAttributeName : NSColor.black
                ]
                let currentText = NSAttributedString(string: textAlert, attributes: attributes as? [String : AnyObject])
                let attrSize = currentText.size()
                currentText.draw(at: NSMakePoint((dirtyRect.size.width / 2) - (attrSize.width / 2), (dirtyRect.size.height / 2) - (attrSize.height / 2)))
    
            } else {
                if !hideCircleBall {
                    drawCircleAndBall(dirtyRect.size.height, centerPoint: center)
                }
                
            }
            
        }
        if showProgressCircle {
            let lineWidthSize = (dirtyRect.size.height * 8) / 100
            let lineWidth2 = (dirtyRect.size.height * 7.5) / 100
            let lineWidthPadding = (dirtyRect.size.height * 0.5) / 100
            let imageCalibration = NSImage.init(imageLiteralResourceName: "CMCalibration")
            imageCalibration.draw(in: NSMakeRect(center.x - (lineWidthSize/2), dirtyRect.size.height - lineWidth2, lineWidthSize, lineWidthSize))
            let imageDashboard = NSImage.init(imageLiteralResourceName: "CMDashboard")
            imageDashboard.draw(in: NSMakeRect(dirtyRect.size.height - lineWidth2, (dirtyRect.size.height/2) - (lineWidthSize/2), lineWidthSize, lineWidthSize))
            let imageSettings = NSImage.init(imageLiteralResourceName: "CMSettings")
            imageSettings.draw(in: NSMakeRect(center.x - (lineWidthSize/2), (lineWidthPadding * -1), lineWidthSize, lineWidthSize))
            if (MinderManager.sharedInstance.dataProcessingPaused) {
                let imageResume = NSImage.init(imageLiteralResourceName: "CMPosturePlay")
                imageResume.draw(in: NSMakeRect((lineWidthPadding * -1), (dirtyRect.size.height/2) - (lineWidthSize/2), lineWidthSize, lineWidthSize))
            } else {
                let imagePause = NSImage.init(imageLiteralResourceName: "CMPosturePause")
                imagePause.draw(in: NSMakeRect((lineWidthPadding * -1), (dirtyRect.size.height/2) - (lineWidthSize/2), lineWidthSize, lineWidthSize))
            }
        }
    }
    
    func drawCircleAndBall(_ height: CGFloat, centerPoint: CGPoint) {
        let pattern: [CGFloat] = [5.0, 5.0]
        let lineWidth = (height * 0.50 ) / 100
        
        // Finding ball space and radius for calculation
        let radiusBall = (height * MinderManager.percentageBall) / 100
        
        let margin = (height * MinderManager.percentageOffset) / 100
        // Finding yellow radius. Yellow zone is MinderManager.percentageZoneYellow percent of height but start after ball space and an offset
        let percentYellow = (difficultyYellow * MinderManager.percentageZoneYellow) / 100
        let radiusYellow = ((height * percentYellow) / 100) + radiusBall + margin
        
        // Finding red radius. Red zone is MinderManager.percentageZoneRed percent of height and start after ball space, yellow zone space and two offset spaces
        let radiusYellow100 = (height * (MinderManager.percentageBall + MinderManager.percentageOffset + MinderManager.percentageZoneYellow)) / 100
        
        let percentRed = (difficultyRed * MinderManager.percentageZoneRed) / 100
        let radiusRed = ((height * percentRed) / 100) + radiusYellow100 + (2 * margin)
        
        if isSeatback == 1 {
            // draw half yellow zone circle (inner)
            if overZoneCircle == .yellow {
                NSColor.blue.set()
            } else {
                NSColor.white.set()
            }
            let cPath: NSBezierPath = NSBezierPath()
            cPath.appendArc(withCenter: centerPoint, radius: radiusYellow, startAngle: 0, endAngle: 180)
            cPath.setLineDash(pattern, count: 2, phase: 0.0)
            cPath.lineWidth = lineWidth
            cPath.stroke()
            
            if overZoneCircle == .seatback {
                NSColor.blue.set()
            } else {
                NSColor.white.set()
            }
            
            // draw seatback representation
            let sPath: NSBezierPath = NSBezierPath()
            let sYOffset: CGFloat = ((height / 3) * difficultySeatback) / 100
            let sPoint: CGPoint = CGPoint(x: centerPoint.x, y: centerPoint.y - sYOffset)
            // radian of angle 180 = 3.14159 and 360 = 6.28319
            sPath.move(to: CGPoint(x: centerPoint.x + (radiusYellow * cos(3.14159)), y: centerPoint.y + (radiusYellow * sin(3.14159))))
            sPath.line(to: CGPoint(x: sPoint.x + (radiusYellow * cos(3.14159)), y: sPoint.y + (radiusYellow * sin(3.14159))))
            sPath.appendArc(withCenter: sPoint, radius: radiusYellow, startAngle: 180, endAngle: 360)
            sPath.line(to: CGPoint(x: centerPoint.x + (radiusYellow * cos(6.28319)), y: centerPoint.y + (radiusYellow * sin(6.28319))))
            sPath.lineWidth = lineWidth
            sPath.stroke()
            
            /* zoneYellow -> zoneBad if overZoneCircle == .red {
             NSColor.blue.set()
             } else {
             NSColor.white.set()
             }
             
             // draw red zone circle (outer)
             // Find if red zone circle cross seatback area to clip intersecting area
             if (sYOffset + radiusYellow) > radiusRed  {
             // Find the red zone - seatback hit point to find the angle difference from 270
             let alti = sqrt((radiusRed * radiusRed) - (radiusYellow * radiusYellow))
             let p1 = CGPoint(x: centerPoint.x, y: centerPoint.y - alti)
             let p2 = CGPoint(x: centerPoint.x + (radiusYellow * cos(3.14159)), y: centerPoint.y - alti)
             let v1 = CGVector(dx: p1.x - centerPoint.x, dy: p1.y - centerPoint.y)
             let v2 = CGVector(dx: p2.x - centerPoint.x, dy: p2.y - centerPoint.y)
             let diffAngle: CGFloat = (atan2(v2.dy, v2.dx) - atan2(v1.dy, v1.dx)) * CGFloat(180.0 / M_PI)
             
             let cOPath: NSBezierPath = NSBezierPath()
             cOPath.appendArc(withCenter: centerPoint, radius: radiusRed, startAngle: 270 + diffAngle, endAngle: 270 - diffAngle, clockwise: true)
             cOPath.setLineDash(pattern, count: 2, phase: 0.0)
             cOPath.lineWidth = lineWidth
             cOPath.stroke()
             } else {
             
             let cORect = NSMakeRect(centerPoint.x - radiusRed, centerPoint.y - radiusRed, radiusRed * 2, radiusRed * 2)
             let cOPath: NSBezierPath = NSBezierPath(ovalIn: cORect)
             cOPath.setLineDash(pattern, count: 2, phase: 0.0)
             cOPath.lineWidth = lineWidth
             cOPath.stroke()
             //}*/
            
        } else {
            if overZoneCircle == .yellow {
                NSColor.blue.set()
            } else {
                NSColor.white.set()
            }
            // draw yellow zone circle (inner)
            let cIRect = NSMakeRect(centerPoint.x - radiusYellow, centerPoint.y - radiusYellow, radiusYellow * 2, radiusYellow * 2)
            let cIPath: NSBezierPath = NSBezierPath(ovalIn: cIRect)
            cIPath.setLineDash(pattern, count: 2, phase: 0.0)
            cIPath.lineWidth = lineWidth
            cIPath.stroke()
            //myMainLayer.path = cIPath.CGPath(forceClose: false)
            
            /* zoneYellow -> zoneBadif overZoneCircle == .red {
             NSColor.blue.set()
             } else {
             NSColor.white.set()
             }
             // draw red zone circle (outer)
             let cORect = NSMakeRect(centerPoint.x - radiusRed, centerPoint.y - radiusRed, radiusRed * 2, radiusRed * 2)
             let cOPath: NSBezierPath = NSBezierPath(ovalIn: cORect)
             cOPath.setLineDash(pattern, count: 2, phase: 0.0)
             cOPath.lineWidth = lineWidth
             cOPath.stroke()*/
        }
        
        if MinderManager.sharedInstance.isCursorOnGadget
        {
            if overZoneCircle == .green || overZoneCircle == .yellow || overZoneCircle == .red || overZoneCircle == .seatback
            {
                NSCursor.resizeLeftRight().set()
            }
            else
            {
                NSCursor.openHand().set()
            }
        }
        else
        {
            NSCursor.arrow().set()
        }
        
        /*NSColor.whiteColor().set()
         // Circle representing ball
         let cRect = NSMakeRect(originBall.x - radiusBall, originBall.y - radiusBall, radiusBall * 2, radiusBall * 2)
         let cPath: NSBezierPath = NSBezierPath(ovalInRect: cRect)
         NSColor.whiteColor().set()
         cPath.fill()*/
    }
    
    // function for updating alert text
    func UpdateAlertText(_ text: String) {
        textAlert = text
        needsDisplay = true
    }
    
    // function for updating zone percentage
    func UpdateZonePercentage(_ green: CGFloat, yellow: CGFloat, red: CGFloat) {
        percentRed = red
        percentYellow = yellow
        percentGreen = green
        needsDisplay = true
    }
    
    func SetSeatback(_ value: Int) {
        isSeatback = value
        needsDisplay = true
    }
    
    // function to update zone difficulty
    func UpdateDifficulty(_ type: ZoneType, difficulty: Float) {
        switch type {
        case .red:
            difficultyRed = CGFloat(difficulty)
        case .yellow:
            difficultyYellow = CGFloat(difficulty)
        case .seatback:
            difficultySeatback = CGFloat(difficulty)
        default:
            break
        }
        needsDisplay = true
    }
    // function updating record cordinates
    func UpdateRecordCordinates(_ x: CGFloat, y: CGFloat) {
        if recordPath == nil {
            recordPath = NSBezierPath()
            recordPath!.move(to: NSPoint(x: x, y: y))
        } else {
            recordPath!.line(to: NSPoint(x: x, y: y))
        }
        needsDisplay = true
    }
    
    // function for moving ball circle by updating the co-ordinates
    func UpdateBallCordinates(_ x: CGFloat, y: CGFloat) {
        if !isReplay {
            if !isRecordView {
                originBall = CGPoint(x: x, y: y)
            } else {
                if idealPath == nil {
                    idealPath = NSBezierPath()
                    idealPath!.move(to: NSPoint(x: x, y: y))
                } else {
                    idealPath!.line(to: NSPoint(x: x, y: y))
                }
            }
            needsDisplay = true
        }
    }
    
    // function for toggling Progress-Circle
    func ToggleShowProgressCircleFlag(_ flag: Bool) {
        showProgressCircle = flag
        needsDisplay = true
    }
    
    // function for updating Gyro
    func UpdateGyro(_ _pitch: CGFloat, _roll: CGFloat, _yaw: CGFloat) {
        pitch = _pitch
        roll = _roll
        yaw = _yaw
        needsDisplay = true
    }
    //to identify window after mouse over
    override func acceptsFirstMouse(for event: NSEvent?) -> Bool // ankush change
    {
        return true
    }
}
