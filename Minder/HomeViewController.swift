//
//  ViewController.swift
//  Minder
//
//  Created by Abdul on 8/9/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa
import SceneKit

// To distiguish different appication modes
enum AppMode {
    case normal
    case gadget
}

// To distiguish different operations on gadget
enum GadgetOperationMode {
    case none
    case click
    case move
    case resize
    case resizeYellow
    case resizeRed
    case resizeSeatback
}

// To distiguish different sub view type
enum ViewType {
    case dashboard
    case guidance
    case settings
    case message
    case community
    case testnlog
    case debugminderlib
}

// To distiguish different zone types
enum ZoneType {
    case none
    case green
    case yellow
    case red
    case seatback
}

enum WaveProgressState {
    case horizontal
    case up
    case down
}

enum BreathType {
    case onhold
    case inhale
    case exhale
}


protocol DelegateDeviceHandler : class{
    func UpdateAlertText(_ text: String)
    func StatusUpdated()
    func UpdateCharge(_ charge: Float)
    func UpdatePosture(_ x: CGFloat, withY y: CGFloat)
    func UpdateColor(_ type: ZoneType)
    func UpdateDataGraphs()
    func UpdateUpdateTime(_ elapsed: Int)
    func UpdateStepCount()
    func UpdateGyroWithRoll(_ roll: CGFloat, withPitch pitch: CGFloat, withYaw yaw: CGFloat, withT t: Double)
    func EnableSeatback()
    func UpdateAccel(_ x: CGFloat, withY y: CGFloat, withZ z: CGFloat, withT t: Double)
    func UpdateHeartRate(_ heartRate: Float)
    func UpdateTemparature(_ temp: Double)
}

class HomeViewController: NSViewController {
    
    //ankush change
    //    @IBOutlet weak var buttonSkipMeditation: NSButton!
    @IBOutlet weak var meditationViewAlertView: NSView!
    @IBOutlet weak var backgroundForMeditationViewAlertView: NSView!
    //    @IBOutlet weak var buttonStartMeditaionGadget: NSButton!
    @IBOutlet weak var buttonDunny: NSButton!
    @IBOutlet weak var buttonDibugMinderlib: NSButton!
    @IBOutlet weak var meditatiobMsglabel: NSTextField!
    @IBOutlet weak var skipLabel: NSTextField!
    @IBOutlet weak var startMeditaionGadgetLabel: NSTextField!
    
    @IBOutlet weak var batteryIndicatorLabel: NSTextField!
    @IBOutlet weak var errorPopUpLabel: NSTextField!
    @IBOutlet weak var recordingNameTextField: NSTextField!
    @IBOutlet weak var heartRateTextField: NSTextField!
    @IBOutlet weak var textHeartRateAverage: NSTextField!
    @IBOutlet weak var temparatureTextField: NSTextField!
    @IBOutlet weak var recordNameInnerPopUpView: NSView!
    @IBOutlet weak var recordNamePopUpView: NSView!
    @IBOutlet weak var buttonDashboard: NSButton!
    @IBOutlet weak var buttonGuidance: NSButton!
    @IBOutlet weak var buttonSettings: NSButton!
    @IBOutlet weak var buttonWebsite: NSButton!
    @IBOutlet weak var killApp: NSButton! //temporary button to kill app and erase all persistent data
    //  @IBOutlet weak var buttonSync: NSButton!//ankush change
    //  @IBOutlet weak var buttonNotification: NSButton!//ankush change
    @IBOutlet weak var buttonCommunity: NSButton!
    @IBOutlet weak var buttonMessage: NSButton!
    
    @IBOutlet weak var buttonCalibrate: NSButton!
    @IBOutlet weak var buttonRecord: NSButton!
    @IBOutlet weak var buttonTestNLog: NSButton!
    
    @IBOutlet weak var buttonClose: NSButton!
    
    @IBOutlet weak var buttonMenuDashboard: NSButton!
    @IBOutlet weak var buttonMenuGuidance: NSButton!
    @IBOutlet weak var buttonMenuNotification: NSButton!
    @IBOutlet weak var buttonMenuProcessPause: NSButton!
    @IBOutlet weak var buttonMenuSettings: NSButton!
    
    @IBOutlet weak var labelTrackingBall: NSTextField!
    @IBOutlet weak var viewTrackball: NSView!
    @IBOutlet weak var viewPosture: PostureView!
    @IBOutlet weak var viewBattery: NSView!
    @IBOutlet weak var viewStatusBar: NSView!
    @IBOutlet weak var imageBattery: NSImageView!
    @IBOutlet weak var hrArrowUp: NSImageView!
    @IBOutlet weak var hrArrowDown: NSImageView!
    @IBOutlet weak var shoulderRegionImage: NSImageView!
    @IBOutlet weak var labelBattery: NSTextField!
    @IBOutlet weak var viewCurveMenuHolder: NSView!
    @IBOutlet weak var viewAnnulus: AnnulusView!
    @IBOutlet weak var viewBall: NSView!
    @IBOutlet weak var viewGyroControls: NSView!
    
    @IBOutlet weak var viewLogin: NSView!
    @IBOutlet weak var buttonLogin: NSButton!
    @IBOutlet var btnLogoutViewToggle: NSButton!
    @IBOutlet var logOutView: NSView!
    @IBOutlet var logoutLineView: NSView!
    @IBOutlet var userImageRoundView: NSImageView!
    @IBOutlet weak var CurveimageView: NSImageView!
    @IBOutlet var editProfileButon: NSButton!
    @IBOutlet var logoutButon: NSButton!
    @IBOutlet var updownArrow: NSImageView!
    
    var viewControllerCurrent: NSViewController!
    @IBOutlet weak var viewPlaceHolder: NSView!
    
    @IBOutlet var userNameLabel: NSTextField!
    ///
    
    @IBOutlet weak var sceneBall: SCNView!
    @IBOutlet weak var topOpaqueView: NSView!
    
    @IBOutlet weak var leftOpaqueView: NSView!
    @IBOutlet weak var rightOpaqueView: NSView!
    @IBOutlet weak var bottomOpaqueView: NSView!
    
    @IBOutlet weak var currentTimeWidgetLabel: NSTextField!
    @IBOutlet weak var heartRateWidgetLabel: NSTextField!
    @IBOutlet weak var heartRateUnitWidgetLabel: NSTextField!
    ///
    var initialLocation: NSPoint = NSPoint()
    var curveMenu: CurveMenuViewController?
    var viewControllerEditProfile: EditProfileViewcontoller?
    var meditationReportViewController: MeditationReportViewController?
    
    var originNormalView: NSPoint? = nil   // holds normal view origin
    var originGadgetView: NSPoint? = nil   // holds gadget view origin
    var sizeNormalView: NSSize? = nil   // holds normal view size
    var sizeGadgetView: CGFloat? = nil   // holds gadget view size
    
    weak var delegateLogin: DelegateMainView? = nil
    var trackingDeviceResizeLocation:NSTrackingArea?
    var gadgetOperation: GadgetOperationMode = .none
    
    var isResizeReset: Bool = false
    var isSwitchingView: Bool = false
    var isInBadPosture: Bool = false
    
    var finalSizeOfPosture : CGFloat = 0
    var currentHeightPosture: CGFloat = 0
    
    var boxNode:SCNNode?
    var previousZ: CGFloat = 0
    var previousZHigh: CGFloat = 0
    var previousZLow: CGFloat = 0
    var currentProgressStateZ: WaveProgressState?
    
    var previousTimeAccel:Int32 = 0
    var previousZTimeHigh:Int32 = 0
    var previousZTimeLow:Int32 = 0
    var previousProgressStateZ:WaveProgressState?
    var animationFrame:NSRect = NSRect()
    var timeAnimation:Int = 0
    var currentTimeAccel:Int32 = 0
    var previousMeditationSize:CGFloat = 25
    var lastCapturedHeartRate:Int = 0
    var perviousTouchLocation:CGPoint = CGPoint(x: 0,y:0)
    var newInitialLoc: CGPoint? = CGPoint(x: 0,y:0)
    //var resizeOffset:CGFloat = 0
    var currentRecordingFileName:String = ""
    var editDoneFromPlayer:Bool = false
    var currentRecordingUserSetName:String = ""
    var userMeditationFullyCompleted:Bool = false
    var userMeditationProgressBarCompleted:Bool = false
    var meditationReportCard:MeditationReportCard?
    
    @IBOutlet weak var buttonEscForGadget: NSButton! //ankush addition
    @IBOutlet weak var meditationViewPosture: MeditationView!
    @IBOutlet weak var meditationViewBall: NSView!
    @IBOutlet weak var meditationImageBall: NSImageView!
    
    //timer variable
    @IBOutlet weak var buttonTimer: NSButton!
    @IBOutlet weak var mainContainerView: NSView!
    @IBOutlet weak var timerContainerView: NSView!
    @IBOutlet var timerStartLabel: NSTextField!
    @IBOutlet var timerEndLabel: NSTextField!
    @IBOutlet weak var sliderTimer: NSSlider!
    @IBOutlet weak var progressTimer: NSProgressIndicator!
    @IBOutlet weak var heartWidget: NSImageView!
    var  mytimer : Timer?
    var  badPostureTimer : Timer?
    var  reversing : Bool?
    
    var lastMonitorTime: Date? = nil
    var lastMonitorSwitch: Bool = false // false if not started true if started, if started monitor only for 15sec
    
    @IBAction func recordNameEditDone(_ sender: NSButton) {
        
        processingRecordName(fromPlayer: editDoneFromPlayer)
    }
    @IBAction func recordNameEditClose(_ sender: NSButton) {
        errorPopUpLabel.isHidden = true
        recordNamePopUpView.isHidden = true
        recordNameInnerPopUpView.isHidden = true
        recordNamePopUpView.layer?.backgroundColor = CGColor.init(gray: 0, alpha: 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        toggleLaunchAtStartup()
        
        self.view.wantsLayer = true
        viewPosture.hideCircleBall = true
        currentHeightPosture = viewPosture.frame.size.height
        sceneBall.isHidden = true //ankush temporary change
        // Assign "Escape" as close button key equivalent
        buttonClose.keyEquivalent = "\u{1b}"
        
        //ankush change , to clear all seat back values when app starts
        for opp in MinderManager.sharedInstance.settings!.OPPs {
            opp.isSeatBack = 0;
        }
        
        let trackingProfileImageHover:NSTrackingArea = NSTrackingArea(rect: userImageRoundView.bounds, options: [NSTrackingAreaOptions.mouseEnteredAndExited,NSTrackingAreaOptions.activeAlways], owner: self, userInfo: ["section": "ProfileImage"])
        userImageRoundView.addTrackingArea(trackingProfileImageHover)
        
        let appManager = MinderManager.sharedInstance
        let userDefaults = UserDefaults.standard
        
        MinderManager.sharedInstance.dLogPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/obVusMinderLog/" + MinderManager.sharedInstance.deviceName
        var isDir : ObjCBool = true
        if !FileManager.default.fileExists(atPath: MinderManager.sharedInstance.dLogPath, isDirectory: &isDir) {
            do {
                try FileManager.default.createDirectory(atPath: MinderManager.sharedInstance.dLogPath, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        MinderManager.sharedInstance.dLogPath += "/\(Date.GetDateString(Date(), format: "MM-dd-yyyy")).log"
        MinderManager.sharedInstance.deviceLog(_log: MinderManager.sharedInstance.dLogPath)
        
        
        var logString: String = "Position-Data not found"
        if let data = userDefaults.object(forKey: "pDataArray") as? Data{
            appManager.pDataArray = (NSKeyedUnarchiver.unarchiveObject(with: data) as? [PositionData])!
            logString = "Position-Data loaded"
        }
        NSLog(logString)
        
        logString = "GYR values not found"
        if let data = userDefaults.object(forKey: "GYR") as? String {
            let gyr = data.characters.split{$0 == ":"}.map(String.init)
            appManager.green = Int(gyr[0])!
            appManager.yellow = Int(gyr[1])!
            appManager.red = Int(gyr[2])!
            logString = "GYR values loaded"
        }
        NSLog(logString)
        
        logString = "Steps not found"
        if let data = userDefaults.object(forKey: "Step") as? String {
            appManager.dStepCount = Int(data)!
            logString = "Steps loaded"
        }
        NSLog(logString)
        
        logString = "originGadgetView not found"
        if let data = userDefaults.object(forKey: "originGadgetView") as? NSString {
            originGadgetView = NSPointFromString(data as String)
            /*if MinderManager.sharedInstance.appMode == .Gadget {
             view.window?.setFrameOrigin(originGadgetView!)
             }*/
            logString = "originGadgetView loaded"
        }
        NSLog(logString)
        
        logString = "originNormalView not found"
        if let data = userDefaults.object(forKey: "originNormalView") as? NSString {
            originNormalView = NSPointFromString(data as String)
            /*if MinderManager.sharedInstance.appMode == .Normal {
             view.window?.setFrameOrigin(originNormalView!)
             }*/
            logString = "originNormalView loaded"
            //            originNormalView = nil;//ankush temporary condition
        }
        NSLog(logString)
        
        logString = "sizeGadgetView not found"
        if let data = userDefaults.object(forKey: "sizeGadgetView") as? CGFloat {
            sizeGadgetView = data
            if MinderManager.sharedInstance.appMode == .gadget {
                //GadgetResize(sizeGadgetView!)
            }
            logString = "sizeGadgetView loaded"
        }
        NSLog(logString)
        
        logString = "sizeNormalView not found"
        if let data = userDefaults.object(forKey: "sizeNormalView") as? NSString {
            sizeNormalView = NSSizeFromString(data as String)
            if MinderManager.sharedInstance.appMode == .normal {
                view.window?.setFrame(NSMakeRect(originNormalView!.x, originNormalView!.y, sizeNormalView!.width, sizeNormalView!.height), display: true)
            }
            logString = "sizeNormalView loaded"
        }
        NSLog(logString)
        
        var appDirPath = appManager.appDirectory
        if !(appManager.userLogged?.userName ?? "").isEmpty {
            appDirPath += (appManager.userLogged?.userName)! + "/"
            appManager.GetZoneData({ _ in })
            print("homeviewcontroller getactivity ")
            GetActivities(false)
            print("UserToken: \(appManager.userLogged?.token)")
        } else {
            appDirPath += "_/"
        }
        
        if appManager.settings?.breaks.count == 0 {
            
            let breakDefault: MeditationBreak = MeditationBreak()
            breakDefault.title = "Default"
            breakDefault.numberOfBreath = 4
            breakDefault.inhaleLength = 4
            breakDefault.retailInhaleLength = 8
            breakDefault.exhaleLength = 7
            breakDefault.retailExhaleLength = 1
            appManager.settings?.breaks.append(breakDefault)
            
            let break1: MeditationBreak = MeditationBreak()
            break1.title = "Beginner"
            break1.numberOfBreath = 1
            break1.inhaleLength = 1
            break1.retailInhaleLength = 1
            break1.exhaleLength = 1
            break1.retailExhaleLength = 1
            appManager.settings?.breaks.append(break1)
            
            let break2: MeditationBreak = MeditationBreak()
            break2.title = "Intermediate"
            break2.numberOfBreath = 10
            break2.inhaleLength = 10
            break2.retailInhaleLength = 10
            break2.exhaleLength = 10
            break2.retailExhaleLength = 1
            appManager.settings?.breaks.append(break2)
            
            let break3: MeditationBreak = MeditationBreak()
            break3.title = "Expert"
            break3.numberOfBreath = 20
            break3.inhaleLength = 20
            break3.retailInhaleLength = 20
            break3.exhaleLength = 20
            break3.retailExhaleLength = 1
            appManager.settings?.breaks.append(break3)
            
            appManager.SaveSettings()
        }
        if appManager.settings?.OPPs.count == 0 {
            let _oppT1: OPP = OPP()
            _oppT1.title = "T1"
            /*_opp.sensitivityPosture = (opp.sensitivityPosture)
            _opp.difficultyRed = (opp.difficultyRed)
            _opp.difficultyYellow = (opp.difficultyYellow)
            _opp.isSeatBack = (opp.isSeatBack)
            _opp.difficultySeatback = (opp.difficultySeatback)*/
            appManager.settings?.OPPs.append(_oppT1)
            
            let _oppFontChest: OPP = OPP()
            _oppFontChest.title = "Front of the body"
            /*_opp.sensitivityPosture = (opp.sensitivityPosture)
             _opp.difficultyRed = (opp.difficultyRed)
             _opp.difficultyYellow = (opp.difficultyYellow)
             _opp.isSeatBack = (opp.isSeatBack)
             _opp.difficultySeatback = (opp.difficultySeatback)*/
            appManager.settings?.OPPs.append(_oppFontChest)
            
            appManager.SaveSettings()
        }
        
        logString = "Heatmap not found"
        if FileManager.default.fileExists(atPath: appDirPath + "hm.png") {
            appManager.imageHeatmap = NSImage(contentsOfFile: appDirPath + "hm.png")
            logString = "Heatmap loaded"
        }
        NSLog(logString)
        
        // To get padding of device view
        appManager.centerDeviceView = NSPoint(x: viewPosture.frame.size.width / 2, y: viewPosture.frame.size.height / 2)
        appManager.centerDeviceViewHeight = viewPosture.frame.size.height
        
        // Setup
        let mainStoryboard: NSStoryboard = NSStoryboard(name: "Main", bundle: nil)
        viewCurveMenuHolder.alphaValue = 0
        
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        viewPosture.idelegate = self
        viewPosture.UpdateDifficulty(.red, difficulty: (opp.difficultyRed))
        viewPosture.UpdateDifficulty(.yellow, difficulty: (opp.difficultyYellow))
        SetSeatback(opp.isSeatBack)
        viewPosture.UpdateDifficulty(.seatback, difficulty: (opp.difficultySeatback))
        
        viewControllerEditProfile = (mainStoryboard.instantiateController(withIdentifier: "ViewcontollerEditProfile") as? EditProfileViewcontoller)!
        viewControllerEditProfile?.delegateProfileEdit = self
        if MinderManager.sharedInstance.isLogEnabled {
            NSLog("Views Setup done")
        }
        
        meditationReportViewController = (mainStoryboard.instantiateController(withIdentifier: "ViewControllerMeditationReport") as? MeditationReportViewController)!
        
        // Shaping n Coloring
        self.view.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1).cgColor
        
        viewGyroControls.layer?.backgroundColor = NSColor.brown.cgColor
        viewBall.layer?.backgroundColor = NSColor.white.cgColor
        
        viewAnnulus.wantsLayer = true
        MinderUtility.ShapeView(viewAnnulus, type: .circle)
        viewBall.wantsLayer = true
        MinderUtility.ShapeView(viewBall, type: .circle)
        
        viewTrackball.wantsLayer = true
        viewTrackball.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        MinderUtility.ShapeView(viewTrackball, type: .rectTopCurved1By3)
        
        viewPosture.wantsLayer = true
        viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 46/255, green: 204/255, blue: 114/255, alpha: CGFloat(opp.transperancy)).cgColor
        MinderUtility.ShapeView(viewPosture, type: .circle)
        
        viewPlaceHolder.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        //        MinderUtility.ShapeView(viewPlaceHolder, type: .RectTopCurved1By3) //ankush change
        //        viewPlaceHolder.layer?.backgroundColor = NSColor.init(calibratedRed: 1.0/255, green: 45.0/255, blue: 62/255, alpha: 1).CGColor
        
        viewBattery.wantsLayer = true
        viewBattery.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        MinderUtility.ShapeView(viewBattery, type: .rectTopCurved1By3)
        
        viewStatusBar.wantsLayer = true
        viewStatusBar.layer?.backgroundColor = NSColor.init(calibratedRed: 27/255, green: 36/255, blue: 42/255, alpha: 1).cgColor
        MinderUtility.ShapeView(viewStatusBar, type: .rectTopCurved1By3Reverse)
        
        viewLogin.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        logOutView.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        
        viewCurveMenuHolder.layer?.backgroundColor = NSColor.clear.cgColor
        
        buttonDashboard.wantsLayer = true
        buttonDashboard.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).cgColor    //ankush change
        buttonGuidance.wantsLayer = true
        buttonGuidance.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        buttonSettings.wantsLayer = true
        buttonSettings.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        buttonWebsite.wantsLayer = true
        buttonWebsite.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        buttonMessage.wantsLayer = true
        buttonMessage.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        buttonCommunity.wantsLayer = true
        buttonCommunity.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        buttonTestNLog.wantsLayer = true
        buttonTestNLog.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
        //buttonSync.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor//ankush change
        //buttonNotification.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor//ankush change
        //        buttonCalibrate.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).CGColor//ankush change
        //        buttonRecord.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).CGColor//ankush change
        
        userImageRoundView.wantsLayer = true
        userImageRoundView.layer!.cornerRadius = self.userImageRoundView.frame.size.width / 2
        userImageRoundView.layer!.borderWidth = 1.0
        userImageRoundView.layer!.cornerRadius = 45/2
        userImageRoundView.layer!.masksToBounds = true
        
        viewLogin.isHidden = true
        
        NSLog("Views Shaped and Coloured")
        
        // Naming
        let styleParagraph = NSMutableParagraphStyle()
        styleParagraph.alignment = .center
        let attributesButton: [String : AnyObject] = [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : styleParagraph ]
        
        //ankush change
        let attributesButtonNew: [String : AnyObject] = [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 8), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : styleParagraph ]
        
        editProfileButon.attributedTitle = NSAttributedString(string: "EDIT PROFILE", attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : styleParagraph])
        logoutButon.attributedTitle = NSAttributedString(string: "LOGOUT", attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : styleParagraph])
        logoutLineView.layer?.backgroundColor = NSColor.white.cgColor
        buttonLogin.attributedTitle = NSAttributedString(string: "LOGIN", attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : styleParagraph])
        
        buttonDashboard.attributedTitle = NSAttributedString(string: "DASHBOARD", attributes: attributesButton)
        buttonGuidance.attributedTitle = NSAttributedString(string: "SCHEDULE", attributes: attributesButton)
        buttonSettings.attributedTitle = NSAttributedString(string: "SETTINGS", attributes: attributesButton)
        buttonWebsite.attributedTitle = NSAttributedString(string: "WEB", attributes: attributesButton)
        buttonMessage.attributedTitle = NSAttributedString(string: "MESSAGE", attributes: attributesButton)
        buttonCommunity.attributedTitle = NSAttributedString(string: "COMMUNITY", attributes: attributesButton)
        buttonTestNLog.attributedTitle = NSAttributedString(string: "Show Log", attributes: attributesButton)
        //  buttonSync.attributedTitle = NSAttributedString(string: "SYNC", attributes: attributesButton)//ankush change
        //  buttonNotification.attributedTitle = NSAttributedString(string: "RING", attributes: attributesButton)//ankush change
        buttonCalibrate.attributedTitle = NSAttributedString(string: "CALIBRATE", attributes: attributesButtonNew)//ankush change
        buttonRecord.attributedTitle = NSAttributedString(string: "RECORD START", attributes: attributesButtonNew)//ankush change
        UpdateNotification()
        
        NSLog("Views Named")
        
        NSWorkspace.shared().notificationCenter.addObserver(self, selector: #selector(self.appDeactivated), name: NSNotification.Name.NSWorkspaceDidDeactivateApplication, object: nil)
        
//        if appManager.minderlib == nil {
//            appManager.initDevice(self)
//
//            //appManager.DeviceConnect()
//            //self.perform(#selector(DeviceConnect), with: nil, afterDelay: 5.0)
//        }
        
        if MinderManager.sharedInstance.minderlib == nil {
            MinderManager.sharedInstance.initDevice(self)
            
            //appManager.DeviceConnect()
            //self.perform(#selector(DeviceConnect), with: nil, afterDelay: 5.0)
        }
        
        meditationViewBall.isHidden = true
        self.mainContainerView.isHidden = true
        
        OPPChanged()
        recordingNameTextField.delegate = self
        
        //ankush change
        let gestureSkip = NSClickGestureRecognizer()
        gestureSkip.buttonMask = 0x1 // left mouse
        gestureSkip.numberOfClicksRequired = 1
        gestureSkip.target = self
        gestureSkip.action = #selector(self.skipLabelClicked)
        self.skipLabel.addGestureRecognizer(gestureSkip)
        
        // opp.offsetPoint = CGPoint.zero ankush change
        
        let gestureStart = NSClickGestureRecognizer()
        gestureStart.buttonMask = 0x1 // left mouse
        gestureStart.numberOfClicksRequired = 1
        gestureStart.target = self
        gestureStart.action = #selector(self.stratMeditaionGadgetTapped)
        self.startMeditaionGadgetLabel.addGestureRecognizer(gestureStart)
        
        heartWidget.isHidden = opp.isWidgetHRHidden
        hrArrowDown.isHidden = opp.isWidgetHRHidden
        hrArrowUp.isHidden = opp.isWidgetHRHidden
        heartRateUnitWidgetLabel.isHidden = opp.isWidgetHRHidden
        heartRateWidgetLabel.isHidden = opp.isWidgetHRHidden        
        currentTimeWidgetLabel.isHidden = opp.isWidgetTimerHidden        
        shoulderRegionImage.isHidden = opp.isWidgetShoulderHidden        
        displayCurrentTime()
    }
    
    func DeviceConnect() {
        DispatchQueue.main.async {
            MinderManager.sharedInstance.DeviceConnect()
        }
    }
    
    override func viewWillAppear() {
        SwitchContainerViewController()
        UpdateDataGraphs()
        setUpSceneView()
        
        //        self.view.window?.styleMask.insert(NSWindowStyleMask.Resizable)
    }
    
    deinit {
        exitFromTheQueue = true
        print("home controller deinitialized")
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        if originNormalView == nil {
            originNormalView = view.window!.frame.origin
            if let screenSize = NSScreen.main()!.frame.size as? CGSize {
                originGadgetView = NSMakePoint((screenSize.width/2) - (viewTrackball.frame.size.width/2) , (screenSize.height/2) - (viewTrackball.frame.size.height/2))
            }
            sizeNormalView = view.window?.frame.size //Ankush change NSSize (width: 1000,height: 800)//
            sizeGadgetView = viewTrackball.frame.size.height
            viewBall.frame.origin = NSMakePoint(-10000, -10000)
            sceneBall.frame.origin = NSMakePoint(-10000, -10000) // fbn
            saveDimensions()
        }
        setupWindow()
        //ResetDrawByResize()
    }
    
    //update temprature and heart rate
    
    //  AppDeactivation-While clicking OutWindow
    func appDeactivated(_ notification: Notification) {
        // print("app deactivated")
        if MinderManager.sharedInstance.appMode == .normal {
            if MinderManager.sharedInstance.isRecording {
                RecordTapped(NSButton())
            }
            SwitchToGadget()
            ResetDrawByResize()
        }
    }
    
    //OnOffGyroControl
    func OnOffGyroControl() {
        if MinderManager.sharedInstance.isGyroOn {
            MinderManager.sharedInstance.isGyroOn = false
            (NSApp.delegate as! AppDelegate).SetMenuNotificationTitle("Rotation Rendering On")
            viewAnnulus.isHidden = true
        } else {
            MinderManager.sharedInstance.isGyroOn = true
            (NSApp.delegate as! AppDelegate).SetMenuGyroOnOffTitle("Rotation Rendering Off")
            viewAnnulus.isHidden = false
        }
        if MinderManager.sharedInstance.currentView == .settings {
            if let viewControllerCurrent = viewControllerCurrent {
                let vcSettingsVC = (viewControllerCurrent as! SettingUpdatedViewController)//ankush change
                vcSettingsVC.RefreshControls()
            }
        }
    }
    //ToggleGyroControl
    func ToggleGyroControl() {
        if MinderManager.sharedInstance.isGyroOn {
            if MinderManager.sharedInstance.isGyroFromDevice {
                MinderManager.sharedInstance.isGyroFromDevice = false
                viewGyroControls.isHidden = false
            } else {
                MinderManager.sharedInstance.isGyroFromDevice = true
                viewGyroControls.isHidden = true
            }
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Please On rotation rendering"
            myPopup.informativeText = "You need to switch on rotation rendering for this feature."
            myPopup.alertStyle = .warning
            myPopup.addButton(withTitle: "OK")
            myPopup.runModal()
        }
    }
    
    override func keyUp(with event: NSEvent) {
        
    }
    
    // MARK:MouseClickFunctions
    override func mouseEntered(with theEvent: NSEvent) {
        if let userData = theEvent.trackingArea?.userInfo as? [String : String] {
            let section = userData["section"]!
            if section == "ProfileImage" {
                userImageRoundView.frame = CGRect(x: userImageRoundView.frame.origin.x-10, y: userImageRoundView.frame.origin.y-10, width: userImageRoundView.frame.width+15, height: userImageRoundView.frame.height+15)
            } else if section == "DeviceView" {
                if MinderManager.sharedInstance.appMode == .gadget {
                    //viewPosture.showTriangle = true
                }
            }
        }
    }
    override func mouseExited(with theEvent: NSEvent) {
        if let userData = theEvent.trackingArea?.userInfo as? [String : String] {
            let section = userData["section"]!
            if section == "ProfileImage" {
                userImageRoundView.frame = CGRect(x: userImageRoundView.frame.origin.x + 10, y: userImageRoundView.frame.origin.y + 10, width: userImageRoundView.frame.width - 15, height: userImageRoundView.frame.height - 15)
            } else if section == "DeviceView" {
                NSCursor.arrow().set()
                MinderManager.sharedInstance.isCursorOnGadget = false
                /*if gadgetOperation != .Resize {
                 NSCursor.arrowCursor().set()
                 }*/
            }
        }
    }
    
    override func mouseMoved(with theEvent: NSEvent) {
        
        let windowFrame = self.view.window!.frame
        var location = NSEvent.mouseLocation()
        //print ("location is \(location)")
        
        MinderManager.sharedInstance.isCursorOnGadget = true
        
        location.x -= windowFrame.origin.x
        location.y -= windowFrame.origin.y
        
        if MinderManager.sharedInstance.appMode == .normal {
            location.x -= (viewTrackball.frame.origin.x + viewPosture.frame.origin.x)
            location.y -= (viewTrackball.frame.origin.y + viewPosture.frame.origin.y)
        }
        viewPosture.overZoneCircle = .none
        let marginResize = (viewPosture.frame.size.height * 4) / 100
        let radiusMove = (viewPosture.frame.size.height / 2) - marginResize
        let xDist = Float( abs(location.x - MinderManager.sharedInstance.centerDeviceView.x))
        let yDist = Float( abs(location.y - MinderManager.sharedInstance.centerDeviceView.x))
        let distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
        
        let lineWidth = viewPosture.frame.size.height / 100
        let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
        let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
        let percentYellow = (CGFloat(viewPosture.difficultyYellow) * MinderManager.percentageZoneYellow) / 100
        let radiusYellow = ((viewPosture.frame.size.height * percentYellow) / 100) + radiusBall + margin
        let radiusYellow100 = (viewPosture.frame.size.height * (MinderManager.percentageBall + MinderManager.percentageOffset + MinderManager.percentageZoneYellow)) / 100
        
        let percentRed = (viewPosture.difficultyRed * MinderManager.percentageZoneRed) / 100
        let radiusRed = ((viewPosture.frame.size.height * percentRed) / 100) + radiusYellow100 + (2 * margin)
        if CGFloat(distance) > (viewPosture.frame.size.height / 2) {
            //NSCursor.arrowCursor().set()
        } else if CGFloat(distance) >= radiusMove && CGFloat(distance) <= (viewPosture.frame.size.height / 2) && MinderManager.sharedInstance.appMode == .gadget && location.x > MinderManager.sharedInstance.centerDeviceView.x && location.y < MinderManager.sharedInstance.centerDeviceView.y{
            //if (location.x > MinderManager.sharedInstance.centerDeviceView.x) && (location.y < MinderManager.sharedInstance.centerDeviceView.y) {
            //  NSCursor.resizeLeftRightCursor().set()
            viewPosture.overZoneCircle = .green
            /*} else {
             NSCursor.openHandCursor().set()
             }*/
        } else if CGFloat(distance) >= (radiusYellow - lineWidth) && CGFloat(distance) <= (radiusYellow + lineWidth) {
            if MinderManager.sharedInstance.isSeatbackEnabled {
                if location.y >= MinderManager.sharedInstance.centerDeviceView.y {
                    // NSCursor.resizeLeftRightCursor().set()
                    viewPosture.overZoneCircle = .yellow
                }
            } else {
                //NSCursor.resizeLeftRightCursor().set()
                viewPosture.overZoneCircle = .yellow
            }
        } else if CGFloat(distance) >= (radiusRed - lineWidth) && CGFloat(distance) <= (radiusRed + lineWidth) {
            // NSCursor.resizeLeftRightCursor().set()
            viewPosture.overZoneCircle = .red
        } else {
            let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
            
            let sPath: NSBezierPath = NSBezierPath()
            let sYOffset: CGFloat = ((viewPosture.frame.size.height / 3) * CGFloat(opp.difficultySeatback)) / 100
            let sPoint: CGPoint = CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x, y: MinderManager.sharedInstance.centerDeviceView.y - sYOffset)
            // radian of angle 180 = 3.14159 and 360 = 6.28319
            sPath.move(to: CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x + (radiusYellow * cos(3.14159)), y: MinderManager.sharedInstance.centerDeviceView.y + (radiusYellow * sin(3.14159))))
            sPath.line(to: CGPoint(x: sPoint.x + (radiusYellow * cos(3.14159)), y: sPoint.y + (radiusYellow * sin(3.14159))))
            sPath.appendArc(withCenter: sPoint, radius: radiusYellow, startAngle: 180, endAngle: 360)
            sPath.line(to: CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x + (radiusYellow * cos(6.28319)), y: MinderManager.sharedInstance.centerDeviceView.y + (radiusYellow * sin(6.28319))))
            sPath.lineWidth = lineWidth
            
            
            
            let allPath: NSBezierPath = sPath.flattened
            if allPath.contains(location) {
                // NSCursor.resizeLeftRightCursor().set()
                viewPosture.overZoneCircle = .seatback
            } else  if gadgetOperation != .resize {
                NSCursor.openHand().set()
            }
        }
        
        //        if viewPosture.overZoneCircle == .Green || viewPosture.overZoneCircle == .Yellow || viewPosture.overZoneCircle == .Red || viewPosture.overZoneCircle == .Seatback
        //        {
        //            NSCursor.resizeLeftRightCursor().set()
        //        }
        //        else
        //        {
        //            NSCursor.openHandCursor().set()
        //        }
        viewPosture.needsDisplay = true
        
        
    }
    
    var heightPosture: CGFloat? = nil
    var pointMouseDown: CGPoint? = nil
    var centerP: CGPoint? = nil
    override func mouseDown(with theEvent: NSEvent) {
        gadgetOperation = .click
        pointMouseDown = NSEvent.mouseLocation()
        let windowFrame = view.window!.frame
        initialLocation = NSEvent.mouseLocation()
        initialLocation.x -= windowFrame.origin.x
        initialLocation.y -= windowFrame.origin.y
        newInitialLoc = initialLocation
        centerP = NSMakePoint(viewTrackball.frame.origin.x + (viewPosture.frame.size.width / 2), viewTrackball.frame.origin.y + (viewPosture.frame.size.height / 2))
        if MinderManager.sharedInstance.deviceActionMode == .broadcasting {
            if MinderManager.sharedInstance.appMode == .normal {
                //ankush change
                // initialLocation.x -= (viewTrackball.frame.origin.x + viewPosture.frame.origin.x)
                // initialLocation.y -= (viewTrackball.frame.origin.y + viewPosture.frame.origin.y)
            }
            
            //ankush change
            var initialLocationTemp: NSPoint = NSPoint()
            initialLocationTemp = initialLocation
            if MinderManager.sharedInstance.appMode == .normal {
                //ankush change
                initialLocationTemp.x -= (viewTrackball.frame.origin.x + viewPosture.frame.origin.x)
                initialLocationTemp.y -= (viewTrackball.frame.origin.y + viewPosture.frame.origin.y)
            }
            
            let marginResize = (viewPosture.frame.size.height * 4) / 100
            let radiusMove = (viewPosture.frame.size.height / 2) - marginResize
            let xDist = Float( abs(initialLocationTemp.x - MinderManager.sharedInstance.centerDeviceView.x)) //ankush change
            let yDist = Float( abs(initialLocationTemp.y - MinderManager.sharedInstance.centerDeviceView.x)) //ankush change
            let distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
            
            let lineWidth = viewPosture.frame.size.height / 100
            let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
            let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
            
            let percentYellow = (CGFloat(viewPosture.difficultyYellow) * MinderManager.percentageZoneYellow) / 100
            let radiusYellow = ((viewPosture.frame.size.height * percentYellow) / 100) + radiusBall + margin
            
            let radiusYellow100 = (viewPosture.frame.size.height * (MinderManager.percentageBall + MinderManager.percentageOffset + MinderManager.percentageZoneYellow)) / 100
            let percentRed = (viewPosture.difficultyRed * MinderManager.percentageZoneRed) / 100
            let radiusRed = ((viewPosture.frame.size.height * percentRed) / 100) + radiusYellow100 + (2 * margin)
            let iconWidth = (viewPosture.frame.size.height * 3) / 100
            
            if CGFloat(distance) >= radiusMove && CGFloat(distance) <= (viewPosture.frame.size.height / 2)  && MinderManager.sharedInstance.appMode == .gadget && initialLocationTemp.x > (MinderManager.sharedInstance.centerDeviceView.x + iconWidth) && initialLocationTemp.y < (MinderManager.sharedInstance.centerDeviceView.y - iconWidth) {
                //if (initialLocation.x > MinderManager.sharedInstance.centerDeviceView.x) && (initialLocation.y < MinderManager.sharedInstance.centerDeviceView.y) {
                gadgetOperation = .resize
                heightPosture = viewPosture.frame.size.height
                viewCurveMenuHolder.alphaValue = 0
                viewPosture.ToggleShowProgressCircleFlag(true)
                
                viewPosture.UpdateAlertText("")
                ResetDrawByResize()
                //NSCursor.resizeLeftRightCursor().set()
                /*} else {
                 NSCursor.closedHandCursor().set()
                 gadgetOperation = .Move
                 }*/
            } else if CGFloat(distance) >= (radiusYellow - lineWidth) && CGFloat(distance) <= (radiusYellow + lineWidth) {
                if MinderManager.sharedInstance.isSeatbackEnabled {
                    if initialLocation.y >= MinderManager.sharedInstance.centerDeviceView.y {
                        gadgetOperation = .resizeYellow
                        viewBall.isHidden = true
                        sceneBall.isHidden = true // fbn
                       // hideUnhideWidgetElements(hiddenStatusTimer: true, hiddenStatusShoulder: true, hiddenStatusHR: true)
                        heightPosture = radiusYellow
                        viewCurveMenuHolder.alphaValue = 0
                        viewPosture.ToggleShowProgressCircleFlag(true)
                        //NSCursor.resizeLeftRightCursor().set()
                    }
                } else {
                    gadgetOperation = .resizeYellow
                    viewBall.isHidden = true
                    sceneBall.isHidden = true // fbn
                   // hideUnhideWidgetElements(hiddenStatusTimer: true, hiddenStatusShoulder: true, hiddenStatusHR: true)
                    heightPosture = radiusYellow
                    viewCurveMenuHolder.alphaValue = 0
                    viewPosture.ToggleShowProgressCircleFlag(true)
                    //NSCursor.resizeLeftRightCursor().set()
                }
            } /* zoneYellow -> zoneBad else if CGFloat(distance) >= (radiusRed - lineWidth) && CGFloat(distance) <= (radiusRed + lineWidth) {
                gadgetOperation = .resizeRed
                viewBall.isHidden = true
                sceneBall.isHidden = true // fbn
                hideUnhideWidgetElements(isHiddenStatus: true)
                heightPosture = radiusRed
                viewCurveMenuHolder.alphaValue = 0
                viewPosture.ToggleShowProgressCircleFlag(true)
                //NSCursor.resizeLeftRightCursor().set()
            }*/ else {
                
                let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                
                let sPath: NSBezierPath = NSBezierPath()
                let sYOffset: CGFloat = ((viewPosture.frame.size.height / 3) * CGFloat(opp.difficultySeatback)) / 100
                let sPoint: CGPoint = CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x, y: MinderManager.sharedInstance.centerDeviceView.y - sYOffset)
                // radian of angle 180 = 3.14159 and 360 = 6.28319
                sPath.move(to: CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x + (radiusYellow * cos(3.14159)), y: MinderManager.sharedInstance.centerDeviceView.y + (radiusYellow * sin(3.14159))))
                sPath.line(to: CGPoint(x: sPoint.x + (radiusYellow * cos(3.14159)), y: sPoint.y + (radiusYellow * sin(3.14159))))
                sPath.appendArc(withCenter: sPoint, radius: radiusYellow, startAngle: 180, endAngle: 360)
                sPath.line(to: CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x + (radiusYellow * cos(6.28319)), y: MinderManager.sharedInstance.centerDeviceView.y + (radiusYellow * sin(6.28319))))
                sPath.lineWidth = lineWidth
                
                let allPath: NSBezierPath = sPath.flattened
                //                 let newPath: NSBezierPath
                //                var count = allPath.elementCount
                //                var prev : NSPoint
                //                var curr : NSPoint
                //                var i = 0
                //                for i in 0..<count {
                //                    // Since we are using a flattened path, no element will contain more than one point
                //
                //                    curr = allPath.currentPoint
                //                    var type : NSBezierPathElement = allPath.elementAtIndex(i, associatedPoints: &curr)
                //                    if type == NSBezierPathElement.LineToBezierPathElement {
                //                        newPath.appendBezierPathWithArcFromPoint(<#T##point1: NSPoint##NSPoint#>, toPoint: <#T##NSPoint#>, radius: <#T##CGFloat#>)
                //                        print("Line from \(NSStringFromPoint(curr)) to \(NSStringFromPoint(curr))")
                //
                //                    }
                //                    else if type == NSBezierPathElement.ClosePathBezierPathElement {
                //                        // Get the first point in the path as the line's end. The first element in a path is a move to operation
                //                        allPath.elementAtIndex(0, associatedPoints: &curr)
                //                        print("Close line from \(NSStringFromPoint(curr)) to \(NSStringFromPoint(curr))")
                //                    }
                //              }
                if allPath.contains(initialLocationTemp) {//ankush change
                    gadgetOperation = .resizeSeatback
                    viewBall.isHidden = true
                    sceneBall.isHidden = true // fbn
                   // hideUnhideWidgetElements(hiddenStatusTimer: true, hiddenStatusShoulder: true, hiddenStatusHR: true)
//                    currentTimeWidgetLabel.isHidden = true
//                    heartRateWidgetLabel.isHidden = true
//                    heartWidget.isHidden = true
                    heightPosture = sYOffset
                    viewCurveMenuHolder.alphaValue = 0
                    viewPosture.ToggleShowProgressCircleFlag(true)
                    //NSCursor.resizeLeftRightCursor().set()
                } else {
                    NSCursor.closedHand().set()
                    gadgetOperation = .move
                }
            }
            
            
            /*} else {
             NSCursor.closedHandCursor().set()
             gadgetOperation = .Move
             }*/
            print("mouseDown \(gadgetOperation)")
            
            logOutView.isHidden = true
            viewLogin.isHidden = true
        }
    }
    
    func GadgetResize(_ sizePosture : CGFloat) {
        //ankush notes
        //0.41 ratio of gadget width and viewCurveMenuHolder width
        //0.85 ration of gadget height and viewCurveMenuHolder height
        
        viewPosture.setFrameSize(NSMakeSize(sizePosture, sizePosture))
        viewCurveMenuHolder?.setFrameSize(NSMakeSize(viewPosture.frame.size.width * 0.41, viewPosture.frame.size.height * 0.86))
        viewTrackball.setFrameSize(NSMakeSize(viewPosture.frame.origin.x + viewPosture.frame.size.width + (viewCurveMenuHolder.frame.size.width / 2), viewPosture.frame.size.height))
        view.window!.setFrame(NSMakeRect(view.window!.frame.origin.x, view.window!.frame.origin.y, viewTrackball.frame.origin.x + viewTrackball.frame.size.width, viewPosture.frame.size.height), display: true)
        print("GadgetResize \(viewTrackball.frame.origin.x + viewTrackball.frame.size.width)X\(viewPosture.frame.size.height)")
        viewCurveMenuHolder?.setFrameOrigin(NSMakePoint(viewTrackball.frame.size.width - viewCurveMenuHolder.frame.size.width - ((5.5 * viewCurveMenuHolder.frame.size.width) / 100), (viewPosture.frame.size.height / 2) - (viewCurveMenuHolder.frame.size.height / 2) + ((0.4 * viewCurveMenuHolder.frame.size.height) / 100)))
        
        currentHeightPosture = viewPosture.frame.size.height
        let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
//        viewBall.frame.size = NSMakeSize(radiusBall * 2.0, radiusBall * 2.0)
//        sceneBall.frame.size = NSMakeSize(radiusBall * 2.5, radiusBall * 2.5) // fbn
        
        let ballWidth = 40 + (20 * (viewPosture.frame.size.height/160) - 20)
        
        viewBall.frame.size = NSMakeSize(ballWidth, ballWidth)
        sceneBall.frame.size = NSMakeSize(ballWidth, ballWidth) // fbn
        
        MinderUtility.ShapeView(viewPosture, type: .circle)
        MinderUtility.ShapeView(viewAnnulus, type: .circle)
        MinderUtility.ShapeView(viewBall, type: .circle)
        
        MinderManager.sharedInstance.centerDeviceViewHeight = sizePosture
        MinderManager.sharedInstance.centerDeviceView = NSPoint(x: viewPosture.frame.size.width / 2, y: viewPosture.frame.size.height / 2)
        
        if MinderManager.sharedInstance.meditationModeFlag == true {
            setUpMeditationView(.gadget, frame: viewPosture.frame)
        }
        backgroundForMeditationViewAlertView.layer?.backgroundColor = NSColor.green.cgColor
        backgroundForMeditationViewAlertView.frame = self.viewPosture.bounds
        resizeAlertMeditationMessageView(sizePosture)
        resizeTimeAndHrLabel(sizePosture)
        
    }
    func resizeTimeAndHrLabel(_ sizePosture : CGFloat)
    {
        currentTimeWidgetLabel.setFrameOrigin(NSMakePoint(sizePosture/2 - currentTimeWidgetLabel.frame.size.width/2,  (340/450) * sizePosture))
        currentTimeWidgetLabel.setFrameSize(NSMakeSize(150/450 * sizePosture, 30/450 * sizePosture))
        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        currentTimeWidgetLabel.alignment = NSTextAlignment.center
        currentTimeWidgetLabel.attributedStringValue =  NSAttributedString(string: currentTimeWidgetLabel.stringValue, attributes: [NSFontAttributeName : NSFont.boldSystemFont(ofSize: (sizePosture * 22 / 450) + 4), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName :style,NSStrokeColorAttributeName :NSColor.black,NSStrokeWidthAttributeName : -1.0])
        
        
        
        heartRateWidgetLabel.setFrameOrigin(NSMakePoint(sizePosture/2 - heartRateWidgetLabel.frame.size.width/2,  (72/450) * sizePosture))
        heartRateWidgetLabel.setFrameSize(NSMakeSize(70/450 * sizePosture, 60/450 * sizePosture))
        
        style.alignment = .center
        heartRateWidgetLabel.alignment = NSTextAlignment.center
        heartRateWidgetLabel.attributedStringValue =  NSAttributedString(string: heartRateWidgetLabel.stringValue, attributes: [NSFontAttributeName : NSFont.boldSystemFont(ofSize: (sizePosture * 35 / 450) + 4), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName :style,NSStrokeColorAttributeName :NSColor.black,NSStrokeWidthAttributeName : -1.0])
        
        heartRateUnitWidgetLabel.setFrameOrigin(NSMakePoint(sizePosture/2 + heartRateWidgetLabel.frame.size.width/2,  (55/450) * sizePosture))
        heartRateUnitWidgetLabel.setFrameSize(NSMakeSize(60/450 * sizePosture, 60/450 * sizePosture))
        
        style.alignment = .center
        heartRateUnitWidgetLabel.alignment = NSTextAlignment.center
        heartRateUnitWidgetLabel.attributedStringValue =  NSAttributedString(string: "bpm", attributes: [NSFontAttributeName : NSFont.boldSystemFont(ofSize: (sizePosture * 18 / 450) + 4), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName :style,NSStrokeColorAttributeName :NSColor.black,NSStrokeWidthAttributeName : -1.0])
        
        heartWidget.setFrameOrigin(NSMakePoint(heartRateWidgetLabel.frame.origin.x - heartWidget.frame.size.width,  (75/450) * sizePosture))
        heartWidget.setFrameSize(NSMakeSize(50/450 * sizePosture, 50/450 * sizePosture))
        
        
        hrArrowUp.setFrameOrigin(NSMakePoint(sizePosture/2 - hrArrowUp.frame.size.width/2,  (124/450) * sizePosture))
        hrArrowUp.setFrameSize(NSMakeSize(40/450 * sizePosture, 40/450 * sizePosture))
        
        hrArrowDown.setFrameOrigin(NSMakePoint(sizePosture/2 - hrArrowDown.frame.size.width/2,  (44/450) * sizePosture))
        hrArrowDown.setFrameSize(NSMakeSize(40/450 * sizePosture, 40/450 * sizePosture))
        
        shoulderRegionImage.setFrameOrigin(NSMakePoint(sizePosture/2 - shoulderRegionImage.frame.size.width/2,  sizePosture/2 - shoulderRegionImage.frame.size.height/2))
        shoulderRegionImage.setFrameSize(NSMakeSize(200/450 * sizePosture, 200/450 * sizePosture))
        
    }
    func setUpAndDownArrow(heartRate : Int)
    {
        if MinderManager.sharedInstance.appMode == .gadget && viewPosture.textAlert == ""
        {
            if lastCapturedHeartRate > heartRate
            {
                hrArrowDown.isHidden = false
                hrArrowUp.isHidden = true
            }
            else if lastCapturedHeartRate < heartRate
            {
                hrArrowDown.isHidden = true
                hrArrowUp.isHidden = false
            }
            else if lastCapturedHeartRate == heartRate
            {
                hrArrowDown.isHidden = true
                hrArrowUp.isHidden = true
            }
        }
        lastCapturedHeartRate = heartRate
            
    }
    func hideUnhideWidgetElements (hiddenStatusTimer : Bool , hiddenStatusShoulder : Bool , hiddenStatusHR : Bool)
    {
        currentTimeWidgetLabel.isHidden = hiddenStatusTimer
        heartRateWidgetLabel.isHidden = hiddenStatusHR
        heartWidget.isHidden = hiddenStatusHR
        hrArrowUp.isHidden = hiddenStatusHR
        hrArrowDown.isHidden = hiddenStatusHR
        heartRateUnitWidgetLabel.isHidden = hiddenStatusHR
        shoulderRegionImage.isHidden = hiddenStatusShoulder
    }
    func resizeAlertMeditationMessageView (_ sizePosture : CGFloat)
    {
        print ("new change value is ", 260/450 * sizePosture)
        
        let messageViewWidth = (260/450) * sizePosture
        
        meditationViewAlertView.setFrameSize(NSMakeSize(260/450 * sizePosture, 260/450 * sizePosture))
        meditationViewAlertView.setFrameOrigin(NSMakePoint(sizePosture/2 - messageViewWidth/2, sizePosture/2 - messageViewWidth/2))
        
        skipLabel.setFrameOrigin(NSMakePoint(messageViewWidth/2 - skipLabel.frame.size.width/2,  (31/450) * sizePosture))
        skipLabel.setFrameSize(NSMakeSize(81/450 * sizePosture, 32/450 * sizePosture))
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        
        let stringAttributed = NSMutableAttributedString.init(string: "Skip")
        let font = NSFont.boldSystemFont(ofSize: (sizePosture * 12 / 450) + 4)
        stringAttributed.addAttribute(NSFontAttributeName, value:font, range: NSRange.init(location: 0, length: 4))
        stringAttributed.addAttribute(NSForegroundColorAttributeName, value: NSColor.white, range: NSRange.init(location: 0, length: 4))
        stringAttributed.addAttribute(NSUnderlineStyleAttributeName, value: 1.0, range: NSRange.init(location: 0, length: 4))
        stringAttributed.setAlignment(NSTextAlignment.center, range: NSRange.init(location: 0, length: 4))
        
        skipLabel.attributedStringValue = stringAttributed
        //NSAttributedString(string: "Skip", attributes: [NSFontAttributeName : NSFont.boldSystemFont(ofSize: (sizePosture * 12 / 450) + 4), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName :style])
        skipLabel.layer?.cornerRadius = skipLabel.frame.size.height/2
        skipLabel.layer?.masksToBounds = true
        skipLabel.alignment = NSTextAlignment.center
        
        
        startMeditaionGadgetLabel.setFrameOrigin(NSMakePoint(messageViewWidth/2 - startMeditaionGadgetLabel.frame.size.width/2,  (80/450) * sizePosture))
        startMeditaionGadgetLabel.setFrameSize(NSMakeSize(81/450 * sizePosture, 32/450 * sizePosture))
        startMeditaionGadgetLabel.layer?.backgroundColor = NSColor.orange.cgColor
        startMeditaionGadgetLabel.layer?.cornerRadius = skipLabel.frame.size.height/2
        startMeditaionGadgetLabel.layer?.masksToBounds = true
        startMeditaionGadgetLabel.alignment = NSTextAlignment.center
        startMeditaionGadgetLabel.attributedStringValue =  NSAttributedString(string: "Start", attributes: [NSFontAttributeName : NSFont.boldSystemFont(ofSize: (sizePosture * 12 / 450) + 4), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName :style])
        
        meditatiobMsglabel.setFrameSize(NSMakeSize(260/450 * sizePosture, 66/260 * sizePosture))
        meditatiobMsglabel.setFrameOrigin(NSMakePoint(0, meditationViewAlertView.frame.size.height - 66/260 * sizePosture))
        meditatiobMsglabel.attributedStringValue =  NSAttributedString(string: "Time to take a breath.", attributes: [NSFontAttributeName : NSFont.boldSystemFont(ofSize: (sizePosture * 24 / 450) + 4), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style])
        
    }
    override func mouseDragged(with theEvent: NSEvent) {
        
        let windowFrame = view.window!.frame
        var currentLocation: NSPoint = NSEvent.mouseLocation()
        if gadgetOperation == .move {
            var newOrigin: NSPoint = NSPoint()
            let screenFrame = NSScreen.main()!.frame
            newOrigin.x = currentLocation.x - initialLocation.x
            newOrigin.y = currentLocation.y - initialLocation.y
            if (newOrigin.y + windowFrame.size.height) > (screenFrame.origin.y + screenFrame.size.height) {
                newOrigin.y = screenFrame.origin.y + (screenFrame.size.height - windowFrame.size.height)
            }
            if MinderManager.sharedInstance.appMode == .gadget {
                originGadgetView = newOrigin
            } else {
                originNormalView = newOrigin
            }
            self.view.window!.setFrameOrigin(newOrigin)
        } else if gadgetOperation != .none && gadgetOperation != .click {
            currentLocation.x -= windowFrame.origin.x
            currentLocation.y -= windowFrame.origin.y
            let xDist = Float( abs(currentLocation.x - (newInitialLoc?.x)!))
            let yDist = Float( abs(currentLocation.y - (newInitialLoc?.y)!))
            let maxValue = max(xDist,yDist)
            var distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
            if distance > 0 {
                // distance /= 2 //ankush change
                var _sizePosture: CGFloat = 0
                
                
                let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
                let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
                let radiusYellow100 = (viewPosture.frame.size.height * (MinderManager.percentageBall + MinderManager.percentageOffset + MinderManager.percentageZoneYellow)) / 100
                
                if gadgetOperation == .resize {
                    viewBall.isHidden = true
                    sceneBall.isHidden = true // fbn
                    let rectTemp = viewPosture.frame
                    let circlePath = NSBezierPath.init(ovalIn: NSMakeRect(rectTemp.origin.x, rectTemp.origin.y, currentHeightPosture, currentHeightPosture ))
                    // print ("redius  \(radiusBall) and maxvlaue \(maxValue)")
                    if initialLocation.x > centerP!.x && initialLocation.y < centerP!.y
                    {
                        if circlePath.contains(currentLocation) {
                            _sizePosture = currentHeightPosture - CGFloat(maxValue)
                            //                        perviousTouchLocation = currentLocation
                            //  resizeOffset = 10
                        }
                        else
                        {
                            _sizePosture = currentHeightPosture + CGFloat(maxValue)
                            //                        perviousTouchLocation = currentLocation
                            //   resizeOffset = 10
                            
                        }
                    }
                    //                    if initialLocation.x > centerP!.x && initialLocation.y > centerP!.y {
                    //                        if currentLocation.x >= initialLocation.x && currentLocation.y > initialLocation.y {
                    //                            _sizePosture = heightPosture! + CGFloat(distance)
                    //                        } else if currentLocation.x > initialLocation.x && currentLocation.y < initialLocation.y{
                    //                            _sizePosture = heightPosture! + CGFloat(distance)
                    //                        }
                    //                        else if currentLocation.x < initialLocation.x && currentLocation.y > initialLocation.y{
                    //                            _sizePosture = heightPosture! - CGFloat(distance)
                    //                        }
                    //                        else {
                    //                            _sizePosture = heightPosture! - CGFloat(distance)
                    //                        }
                    //                    } else if initialLocation.x < centerP!.x && initialLocation.y > centerP!.y {
                    //                        if currentLocation.x < initialLocation.x && currentLocation.y > initialLocation.y {
                    //                            _sizePosture = heightPosture! + CGFloat(distance)
                    //                        } else if currentLocation.x < initialLocation.x && currentLocation.y < initialLocation.y{
                    //                            _sizePosture = heightPosture! + CGFloat(distance)
                    //                        }
                    //                        else if currentLocation.x < initialLocation.x && currentLocation.y > initialLocation.y{
                    //                            _sizePosture = heightPosture! - CGFloat(distance)
                    //                        }
                    //                        else {
                    //                            _sizePosture = heightPosture! - CGFloat(distance)
                    //                        }
                    //                    } else if initialLocation.x < centerP!.x && initialLocation.y < centerP!.y {
                    //                        if currentLocation.x < initialLocation.x && currentLocation.y < initialLocation.y {
                    //                            _sizePosture = heightPosture! + CGFloat(distance)
                    //                        } else if currentLocation.x < initialLocation.x && currentLocation.y > initialLocation.y {
                    //                            _sizePosture = heightPosture! + CGFloat(distance)
                    //                        }
                    //                        else if currentLocation.x < initialLocation.x && currentLocation.y < initialLocation.y {
                    //                            _sizePosture = heightPosture! - CGFloat(distance)
                    //                        }
                    //                        else {
                    //                            _sizePosture = heightPosture! - CGFloat(distance)
                    //                        }
                    //                    } else {
                    //                        if currentLocation.x < initialLocation.x && currentLocation.y > initialLocation.y {
                    //                            _sizePosture = heightPosture! - CGFloat(distance)
                    //                        } else if currentLocation.x < initialLocation.x && currentLocation.y < initialLocation.y{
                    //                            _sizePosture = heightPosture! - CGFloat(distance)
                    //                        }
                    //                        else if currentLocation.x < initialLocation.x && currentLocation.y > initialLocation.y{
                    //                            _sizePosture = heightPosture! + CGFloat(distance)
                    //                        }
                    //                        else {
                    //                            _sizePosture = heightPosture! + CGFloat(distance)
                    //                        }
                    //                    }
                    
                    finalSizeOfPosture = _sizePosture //ankush change
                    //  print("heightPosture : \(currentHeightPosture), distance : \(distance)")
                    let screenFrame = NSScreen.main()!.frame
                    // Restrict minimum and maximum scaling
                    if _sizePosture > (screenFrame.size.height / 5.5) && _sizePosture < sizeNormalView!.height {
                        
                        currentHeightPosture = _sizePosture
                        GadgetResize(currentHeightPosture)
                        newInitialLoc = currentLocation;
                    }
                } else if gadgetOperation == .resizeYellow {
                    
                    //ankush change
                    if(MinderManager.sharedInstance.appMode == .normal )
                    {
                        centerP = NSMakePoint(viewTrackball.frame.origin.x + (viewTrackball.frame.size.width / 2), viewTrackball.frame.origin.y + (viewTrackball.frame.size.height / 2))
                    }
                    
                    let dx = abs(currentLocation.x - (centerP?.x)!)
                    let dy = abs(currentLocation.y - (centerP?.y)!)
                    _sizePosture =  sqrt((dx * dx) + (dy * dy))
                    
                    let yellowActualRadius = _sizePosture - (radiusBall + margin)
                    let yellowPercent = (yellowActualRadius * 100) / viewPosture.frame.size.height
                    let yellowDifficulty = (yellowPercent * 100) / MinderManager.percentageZoneYellow
                    if yellowDifficulty < 100 && yellowDifficulty >= 0 {
                        let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                        opp.difficultyYellow = Float(yellowDifficulty)
                        UpdateDifficulty(.yellow, value: Float(yellowDifficulty))
                        MinderManager.sharedInstance.SaveSettings()
                    }
                    //ResetDrawByResize()
                } else if gadgetOperation == .resizeRed {
                    
                    //ankush change
                    if(MinderManager.sharedInstance.appMode == .normal )
                    {
                        centerP = NSMakePoint(viewTrackball.frame.origin.x + (viewTrackball.frame.size.width / 2), viewTrackball.frame.origin.y + (viewTrackball.frame.size.height / 2))
                    }
                    
                    
                    let dx = abs(currentLocation.x - (centerP?.x)!)
                    let dy = abs(currentLocation.y - (centerP?.y)!)
                    _sizePosture =  sqrt((dx * dx) + (dy * dy))
                    
                    let redActualRadius = _sizePosture - (radiusYellow100 + (4 * margin))
                    let redPercent = (redActualRadius * 100) / viewPosture.frame.size.height
                    let redDifficulty = (redPercent * 100) / MinderManager.percentageZoneYellow
                    if redDifficulty < 100 && redDifficulty >= 0 {
                        let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                        opp.difficultyRed = Float(redDifficulty)
                        UpdateDifficulty(.red, value: Float(redDifficulty))
                        MinderManager.sharedInstance.SaveSettings()
                    }
                    //ResetDrawByResize()
                } else if gadgetOperation == .resizeSeatback {
                    
                    //ankush change
                    if(MinderManager.sharedInstance.appMode == .normal )
                    {
                        centerP = NSMakePoint(viewTrackball.frame.origin.x + (viewTrackball.frame.size.width / 2), viewTrackball.frame.origin.y + (viewTrackball.frame.size.height / 2))
                    }
                    
                    let dx = abs(currentLocation.x - (centerP?.x)!)
                    let dy = currentLocation.y - (centerP?.y)!
                    let radius = sqrt((dx * dx) + (abs(dy) * abs(dy)))
                    _sizePosture =  dy
                    if _sizePosture < 0 {
                        let yellowPercent = (abs(_sizePosture) * 100) / viewPosture.frame.size.height
                        var seatBackDifficulty = (yellowPercent * 100) / MinderManager.percentageZoneYellow
                        seatBackDifficulty = seatBackDifficulty - radius
                        //print("seatBackDifficulty : \(seatBackDifficulty), _sizePosture : \(_sizePosture)")
                        if seatBackDifficulty < 100 && seatBackDifficulty >= 0 {
                            let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                            opp.difficultySeatback = Float(seatBackDifficulty)
                            UpdateDifficulty(.seatback, value: Float(seatBackDifficulty))
                            MinderManager.sharedInstance.SaveSettings()
                        }
                    }
                }
                //
            }
            
            //ankush change
            if MinderManager.sharedInstance.currentView == .settings {
                if let viewControllerCurrent = self.viewControllerCurrent {
                    let vcSettingsVC = (viewControllerCurrent as! SettingUpdatedViewController) //ankush change
                    vcSettingsVC.RefreshControls()
                }
            }
            
            //            if viewPosture.overZoneCircle == .Green || viewPosture.overZoneCircle == .Yellow || viewPosture.overZoneCircle == .Red || viewPosture.overZoneCircle == .Seatback
            //            {
            //                NSCursor.resizeLeftRightCursor().set()
            //            }
            //            else
            //            {
            //                NSCursor.closedHandCursor().set()
            //            }
        }
    }
    
    func ResetDrawByResize() {
        // if background then return
        if NSApp.activationPolicy() == .accessory {
            return
        }
        if ((MinderManager.sharedInstance.appMode == .gadget) && (MinderManager.sharedInstance.replayState == .none)) {
            isResizeReset = true
            let time: DispatchTime = DispatchTime.now() + Double(Int64(NSEC_PER_SEC / 8)) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                let height = self.viewPosture.frame.size.height + 1
                self.GadgetResize(height)
            })
            DispatchQueue.main.asyncAfter(deadline: time, execute:  {
                let height = self.viewPosture.frame.size.height - 1
                self.GadgetResize(height)
            })
            DispatchQueue.main.asyncAfter(deadline: time, execute:  {
                self.viewPosture.needsDisplay = true
                self.viewAnnulus.needsDisplay = true
                if  MinderManager.sharedInstance.deviceStatus == .connected //ankush change
                {
                    // self.viewBall.hidden = true
                    self.sceneBall.isHidden = false // fbn ankush change
                }
                else{
                    self.viewBall.isHidden = true
                    self.sceneBall.isHidden = true
                }
                self.isResizeReset = false
            })
        }
        
    }
    
    override func mouseUp(with theEvent: NSEvent) {
        
        
        let windowFrame = view.window!.frame
        var currlocation: NSPoint = NSEvent.mouseLocation()
        if theEvent.clickCount == 2 {
            if MinderManager.sharedInstance.isRecording {
                RecordTapped(NSButton())
            } else if theEvent.type == NSLeftMouseUp { //ankush change (loop shuffle to check app mode first)
                
                if MinderManager.sharedInstance.appMode == .gadget {
                    
                    buttonClose.keyEquivalent = "\u{1b}" //ankush change
                    currlocation.x -= windowFrame.origin.x// + 28
                    currlocation.y -= windowFrame.origin.y// + 51
                    if viewPosture.frame.contains(currlocation) {
                        menuButtonTapped(MinderManager.sharedInstance.currentView)
                    }
                    //ankush change
                    let screenFrame = NSScreen.main()!.frame
                    if finalSizeOfPosture > (screenFrame.size.height / 5.5) && finalSizeOfPosture < sizeNormalView!.height {
                        GadgetResize(finalSizeOfPosture)
                    }
                    //skipLabelClicked(sender:self)
                    //ankush chnage
                    if MinderManager.sharedInstance.appMode == .gadget
                    {
                        meditationViewAlertView.isHidden = true
                        backgroundForMeditationViewAlertView.isHidden = true
                    }
                    
                } else {
                    
                    currlocation.x -= windowFrame.origin.x// + 28  //ankush change
                    currlocation.y -= windowFrame.origin.y// + 51//ankush change
                    
                    print("currlocation  position is \(currlocation.x)")
                    
                    //                    NSLog("mouseDown \(gadgetOperation)")
                    if viewTrackball.frame.contains(currlocation) {//ankush change
                        SwitchToGadgetOnDoubleClick(theEvent)
                    }
                }
                
            } else {
            }
        } else {
            if MinderManager.sharedInstance.deviceActionMode != .broadcasting {
                MinderManager.sharedInstance.deviceActionMode = .broadcasting
            }
            else {
                switch gadgetOperation {
                case .move:
                    let xD = Float( abs(currlocation.x - pointMouseDown!.x))
                    let yD = Float( abs(currlocation.y - pointMouseDown!.y))
                    let dist: Float = sqrt((xD * xD) + (yD * yD))
                    if dist <= 3 {
                        // Disabling Curvemenu - abdu
                        // curveMenuClick(abs(Int(viewCurveMenuHolder.alphaValue) - 1))
                        currlocation.x -= windowFrame.origin.x
                        currlocation.y -= windowFrame.origin.y
                        handleMenuClick(point: currlocation)
                    }
                case .resize:
                    currlocation.x -= windowFrame.origin.x// + 28
                    currlocation.y -= windowFrame.origin.y// + 51
                    let xDist = Float( abs(currlocation.x - initialLocation.x))
                    let yDist = Float( abs(currlocation.y - initialLocation.y))
                    var distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
                    if distance > 0 {
                        distance = distance / 2
                        viewPosture.removeTrackingArea(trackingDeviceResizeLocation!)
                        trackingDeviceResizeLocation = NSTrackingArea(rect: viewPosture.bounds, options: [NSTrackingAreaOptions.mouseMoved, NSTrackingAreaOptions.mouseEnteredAndExited, NSTrackingAreaOptions.activeAlways], owner: self, userInfo: ["section": "DeviceView"])
                        viewPosture.addTrackingArea(trackingDeviceResizeLocation!)
                    }
                    sizeGadgetView = viewPosture.frame.size.height
                    if  MinderManager.sharedInstance.deviceStatus == .connected //ankush change
                    {
                        viewBall.isHidden = true
                        sceneBall.isHidden = false // fbn ankush change
                    } else {
                        self.viewBall.isHidden = true
                        self.sceneBall.isHidden = true
                    }
                    viewAnnulus.needsDisplay = true
                    //viewPosture.hideCircleBall = false
                    viewPosture.needsDisplay = true
                    
                case .resizeRed, .resizeYellow, .resizeSeatback:
                    if MinderManager.sharedInstance.appMode == .gadget {
                        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                        //self.hideUnhideWidgetElements(hiddenStatusTimer: opp.isWidgetTimerHidden, hiddenStatusShoulder: opp.isWidgetShoulderHidden, hiddenStatusHR: opp.isWidgetHRHidden)
                        ResetDrawByResize()
                    }
                    if  MinderManager.sharedInstance.deviceStatus == .connected //ankush change
                    {
                        self.viewBall.isHidden = true
                        self.sceneBall.isHidden = false // fbn ankush change
                    }
                    else{
                        self.viewBall.isHidden = true
                        self.sceneBall.isHidden = true
                    }
                    
                    break
                default:
                    // Disabling curvemenu - abdu
                    /*viewPosture.needsDisplay = true
                     curveMenuClick(abs(Int(viewCurveMenuHolder.alphaValue) - 1))*/
                    currlocation.x -= windowFrame.origin.x
                    currlocation.y -= windowFrame.origin.y
                    handleMenuClick(point: currlocation)
                    break
                }
                saveDimensions()
            }
            
        }
        let delayTime = DispatchTime.now() + Double(Int64(NSEC_PER_SEC / 8)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.gadgetOperation = .none
        }
        //        if MinderManager.sharedInstance.appMode == .gadget {
        //            NSCursor.openHand().set()
        //        } else {
        //            NSCursor.arrow().set()
        //        }
    }
    
    func handleMenuClick(point: NSPoint) {
        let lineWidth = (viewPosture.frame.size.height * 8) / 100
        let lineWidth2 = (viewPosture.frame.size.height * 7.5) / 100
        let lineWidth3 = (viewPosture.frame.size.height * 0.5) / 100
        let rectCalibration = NSMakeRect((centerP?.x)! - (lineWidth/2), viewPosture.frame.size.height - lineWidth2, lineWidth, lineWidth)
        let rectDashboard = NSMakeRect(viewPosture.frame.size.height - lineWidth2, (viewPosture.frame.size.height/2) - (lineWidth/2), lineWidth, lineWidth)
        let rectSettings = NSMakeRect((centerP?.x)! - (lineWidth/2), (lineWidth3 * -1), lineWidth, lineWidth)
        let rectPauseResume = NSMakeRect((lineWidth3 * -1), (viewPosture.frame.size.height/2) - (lineWidth/2), lineWidth, lineWidth)
        
       // print("\(rectDashboard) point \(point)")
        
        if rectCalibration.contains(point) {
            CalibrateTapped(NSButton())
        } else if rectDashboard.contains(point) {
            menuButtonTapped(.dashboard)
        } else if rectSettings.contains(point) {
            menuButtonTapped(.settings)
        } else if rectPauseResume.contains(point) {
            UpdateProcessingPaused()
        }
    }
    
    func saveDimensions() {
        let sharedPreference = UserDefaults.standard
        
        sharedPreference.set(NSStringFromPoint(originNormalView!), forKey: "originNormalView")
        sharedPreference.set(NSStringFromPoint(originGadgetView!), forKey: "originGadgetView")
        sharedPreference.set(NSStringFromSize(sizeNormalView!), forKey: "sizeNormalView")
        sharedPreference.set(sizeGadgetView!, forKey: "sizeGadgetView")
        
        sharedPreference.synchronize()
    }
    
    // MARK: setupWindow
    func setupWindow() {
        let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        NSLog("setupWindow for \(MinderManager.sharedInstance.appMode)")
        isSwitchingView = true
        viewBall.isHidden = true
        sceneBall.isHidden = true // fbn
        viewBall.frame.origin = NSMakePoint(-10000, -10000)
        //  sceneBall.frame.origin = NSMakePoint(viewPosture.frame.size.width/2, viewPosture.frame.size.height/2)   // fbn
        sceneBall.frame.origin = NSMakePoint(-10000, -10000)  // fbn
        viewPosture.UpdateAlertText("")
        ResetDrawByResize()

        
        switch MinderManager.sharedInstance.appMode {
        case .gadget:
            if MinderManager.sharedInstance.isRecording {
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor
            } else {
                viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 46/255, green: 204/255, blue: 114/255, alpha: CGFloat(opp.transperancy)).cgColor
            }
            resizeTimeAndHrLabel(self.sizeGadgetView!)
            view.subviews.forEach({
                if $0 != viewTrackball {
                    $0.isHidden = true
                }
            })   
            
            view.window?.backgroundColor = NSColor.clear
            view.layer?.backgroundColor = NSColor.clear.cgColor
            
            viewTrackball.setFrameOrigin(NSMakePoint(0, 0))
            viewTrackball.layer?.backgroundColor = NSColor.clear.cgColor
            viewTrackball.layer?.mask = nil
            
            /*
             NSAnimationContext.runAnimationGroup({(_ context: NSAnimationContext) -> Void in
             context.duration = 0.5
             self.view.window!.animator().setFrameOrigin(NSMakePoint(self.originGadgetView!.x, self.originGadgetView!.y))
             }, completionHandler: {() -> Void in
             })*/
            self.viewPosture.setFrameOrigin(NSMakePoint(0, 0))
            self.viewPosture.setFrameSize(NSMakeSize(self.sizeGadgetView!, self.sizeGadgetView!))
            self.viewCurveMenuHolder?.setFrameSize(NSMakeSize(self.viewPosture.frame.size.width * 0.41, self.viewPosture.frame.size.height * 0.86))
            self.viewTrackball.setFrameSize(NSMakeSize(self.viewPosture.frame.origin.x + self.viewPosture.frame.size.width + (self.viewCurveMenuHolder.frame.size.width / 2), self.viewPosture.frame.size.height))
            self.view.window!.setFrame(NSMakeRect(self.originGadgetView!.x, self.originGadgetView!.y, self.viewTrackball.frame.origin.x + self.viewTrackball.frame.size.width, self.viewPosture.frame.size.height), display: true)
            
            self.viewCurveMenuHolder?.setFrameOrigin(NSMakePoint(self.viewTrackball.frame.size.width - self.viewCurveMenuHolder.frame.size.width - ((5.5 * self.viewCurveMenuHolder.frame.size.width) / 100), (self.viewPosture.frame.size.height / 2) - (self.viewCurveMenuHolder.frame.size.height / 2) + ((0.4 * self.viewCurveMenuHolder.frame.size.height) / 100)))
            
            
            labelTrackingBall.isHidden = true
            buttonCalibrate.isHidden = true
            buttonRecord.isHidden = true
            viewCurveMenuHolder.isHidden = false
            viewCurveMenuHolder.alphaValue = 0
            // Making application window level to floating - top of all other applications
            view.window!.level = Int(CGWindowLevelForKey(.maximumWindow))
            
            meditationViewPosture.isHidden = true // fbn
            if MinderManager.sharedInstance.meditationModeFlag == true {
                setUpMeditationView(.gadget, frame: self.viewPosture.frame)
            }
            
            viewPosture.ToggleShowProgressCircleFlag(true)
            backgroundForMeditationViewAlertView.layer?.backgroundColor = NSColor.green.cgColor
            backgroundForMeditationViewAlertView.frame = self.viewPosture.bounds
            resizeAlertMeditationMessageView(self.sizeGadgetView!)
            resizeTimeAndHrLabel(self.sizeGadgetView!)
            
            DispatchQueue.main.async(execute: {
               let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
               self.hideUnhideWidgetElements(hiddenStatusTimer: opp.isWidgetTimerHidden, hiddenStatusShoulder: opp.isWidgetShoulderHidden, hiddenStatusHR: opp.isWidgetHRHidden)
            })
            
        //GadgetResize(viewPosture.frame.size.height)
        case .normal:
            //NSCursor.arrowCursor().set()
            viewCurveMenuHolder.isHidden = true
            
            viewPosture.ToggleShowProgressCircleFlag(false)
            if MinderManager.sharedInstance.isRecording {
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor    //CGFloat(opp.transperancy)
            } else {
                viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 46/255, green: 204/255, blue: 114/255, alpha: CGFloat(opp.transperancy)).cgColor
            }
            view.subviews.forEach({
                if $0 != viewTrackball && $0 != mainContainerView {
                    $0.isHidden = false
                }
            })
            //          viewTrackball.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor //ankush change
            
            /*NSAnimationContext.runAnimationGroup({(_ context: NSAnimationContext) -> Void in
             context.duration = 0.5
             view.window?.animator().setFrame(NSMakeRect(originNormalView!.x, originNormalView!.y, sizeNormalView!.width, sizeNormalView!.height), display: true)
             }, completionHandler: {() -> Void in
             
             })*/
            
            //print("NormalView size: \(sizeNormalView?.width)*\(sizeNormalView?.height) origin: \(originNormalView?.x)*\(originNormalView?.y)")
            
            self.view.window?.setFrame(NSMakeRect(self.originNormalView!.x, self.originNormalView!.y, self.sizeNormalView!.width, self.sizeNormalView!.height), display: true)
            //            viewTrackball.frame = NSMakeRect(7, 35, 557, 520)
            //            viewPosture.frame = NSMakeRect(20, 16, 450, 450)
            viewTrackball.frame = NSMakeRect(300, 540 - (800 - 598), 400, 220) //ankush changes, fbn change
            viewPosture.frame = NSMakeRect(100, 0, 200, 200) //ankush changes
            buttonCalibrate.frame = NSMakeRect(310, 0, 80, 40)
            buttonRecord.frame = NSMakeRect(310, 200 - 40, 80, 40)
            view.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1).cgColor
            MinderUtility.ShapeView(viewTrackball, type: .rectTopCurved1By3)
            MinderUtility.ShapeView(viewPosture, type: .circle)
            UpdateLoggedUserDetails()
            labelTrackingBall.isHidden = true//ankush change false
            buttonCalibrate.isHidden = false
            buttonRecord.isHidden = false
            // Set window level to normal
            view.window!.level = Int(CGWindowLevelForKey(.normalWindow))
            //ResetDrawByResize()
            if MinderManager.sharedInstance.meditationModeFlag == true {
                setUpMeditationView(.normal, frame: self.viewPosture.frame)
            }
            hideUnhideWidgetElements(hiddenStatusTimer: true, hiddenStatusShoulder: true, hiddenStatusHR: true)
        }
        setUpWindowTransperency() // fbn
        
       // print("\(MinderManager.sharedInstance.appMode)  sizeNormalView: \(sizeNormalView) viewTrackball: \(viewTrackball.frame.size) viewPosture: \(viewPosture.frame.size) sizeGadgetView: \(sizeGadgetView!)")
        
        if trackingDeviceResizeLocation != nil {
            viewPosture.removeTrackingArea(trackingDeviceResizeLocation!)
        }
        trackingDeviceResizeLocation = NSTrackingArea(rect: viewPosture.bounds, options: [NSTrackingAreaOptions.mouseMoved, NSTrackingAreaOptions.mouseEnteredAndExited, NSTrackingAreaOptions.activeAlways], owner: self, userInfo: ["section": "DeviceView"])
        viewPosture.addTrackingArea(trackingDeviceResizeLocation!)
        
        if  MinderManager.sharedInstance.deviceStatus == .connected //ankush change
        {
            viewBall.isHidden = true
            sceneBall.isHidden = false // fbn ankush change
        }
        else{
            self.viewBall.isHidden = true
            self.sceneBall.isHidden = true
        }
        //viewAnnulus.hidden = false
        viewAnnulus.needsDisplay = true
        
        MinderUtility.ShapeView(viewPosture, type: .circle)
        viewPosture.needsDisplay = true
        
        let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
        let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
        // Finding yellow radius. Yellow zone is MinderManager.percentageZoneYellow percent of height but start after ball space and an offset
        let percentYellow = (CGFloat(viewPosture.difficultyYellow) * MinderManager.percentageZoneYellow) / 100
        let radiusYellow = ((viewPosture.frame.size.height * percentYellow) / 100) + radiusBall + margin
        
//        viewBall.frame.size = NSMakeSize(radiusBall * 2.0, radiusBall * 2.0)
//        sceneBall.frame.size = NSMakeSize(radiusBall * 2.5, radiusBall * 2.5) // fbn
        
        let ballWidth = 40 + (20 * (viewPosture.frame.size.height/160) - 20)
        
        viewBall.frame.size = NSMakeSize(ballWidth, ballWidth)
        sceneBall.frame.size = NSMakeSize(ballWidth, ballWidth) // fbn

        
        MinderUtility.ShapeView(viewAnnulus, type: .circle)
        MinderUtility.ShapeView(viewBall, type: .circle)
        
        MinderManager.sharedInstance.centerDeviceViewHeight = viewPosture.frame.size.height
        MinderManager.sharedInstance.centerDeviceView = NSPoint(x: viewPosture.frame.size.width / 2, y: viewPosture.frame.size.height / 2)
        
        isSwitchingView = false
        
        //if MinderManager.sharedInstance.deviceStatus == .Connected {
        if MinderManager.sharedInstance.dataProcessingPaused {
            viewPosture.UpdateAlertText("Posture is paused.")
        }
        
        
//        if(HRMonitorType(rawValue: (MinderManager.sharedInstance.settings?.hrMonitorSelected)!) == Minder.HRMonitorType.Off) {
//            MinderManager.sharedInstance.DeviceHeartRateDisable()
//        } else {
//            MinderManager.sharedInstance.DeviceHeartRateEnable()
//        }

        /*} else {
         viewPosture.UpdateAlertText("No device connected.")
         }*/
    }
    
    @IBAction func CalibrateTapped(_ sender: AnyObject) {
        
//        MinderManager.sharedInstance.sendNotification((MinderManager.sharedInstance.userLogged?.userName)!, messageBody: "I am sending demo message", vibration: "Yes", sound: "No", title: "Push Notification", completionHandler: {data -> Void in
//            if data {
//                
//            } else {
//
//            }
//
//        })
//        MinderManager.sharedInstance.sendNotificationToken((MinderManager.sharedInstance.userLogged?.userName)!, password: "ser_123", token: "123456789123456789123456789", deviceType: "iOS", completionHandler: {data -> Void in
//            if data {
//                
//            } else {
//                
//            }
//            
//        })
        
        if MinderManager.sharedInstance.deviceActionMode == .broadcasting {
            MinderManager.sharedInstance.calibratedFlag = true
            viewBall.isHidden = true
            sceneBall.isHidden = true // fbn
            hideUnhideWidgetElements(hiddenStatusTimer: true, hiddenStatusShoulder: true, hiddenStatusHR: true)
            let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
            opp.offsetPoint.x = 0
            opp.offsetPoint.y = 0
            
            MinderManager.sharedInstance.deviceActionMode = .calibrating
            UpdateColor(.green)
            MinderManager.sharedInstance.roll = 0
            MinderManager.sharedInstance.pitch = 0
            MinderManager.sharedInstance.yaw = 0
            GyroTheView()
            if MinderManager.sharedInstance.isLogEnabled {
                MinderManager.sharedInstance.deviceLog(_log:"Calibration started")
            }
        }
    }
    func SkipCalibration() {
        MinderManager.sharedInstance.deviceActionMode = .broadcasting
        UpdateColor(.green)
        if MinderManager.sharedInstance.isLogEnabled {
            MinderManager.sharedInstance.deviceLog(_log:"Calibration Skipped")
        }
    }
    
    // MARK:UpdateLoggedUserDetails
    func UpdateLoggedUserDetails() {
        let appManager = MinderManager.sharedInstance
        if !(appManager.userLogged?.userName ?? "").isEmpty {
            userNameLabel.attributedStringValue = NSAttributedString(string: (appManager.userLogged?.name)!, attributes: [ NSFontAttributeName: NSFont.boldSystemFont(ofSize: 14), NSForegroundColorAttributeName : NSColor.white])
            if let userimage = appManager.userLogged?.image {
                userImageRoundView.image = userimage
            }
        } else {
            userNameLabel.stringValue = ""
            userImageRoundView.isHidden = true
        }
        logOutView.isHidden = true
        viewLogin.isHidden = true
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    // MARK:buttonToggleView
    @IBAction func buttonToggleView(_ sender: AnyObject) {
        if userImageRoundView.isHidden {
            if viewLogin.isHidden {
                viewLogin.isHidden = false
            } else {
                viewLogin.isHidden = true
            }
        } else {
            if logOutView.isHidden {
                logOutView.isHidden = false
            } else {
                logOutView.isHidden = true
            }
        }
    }
    // MARK:ActivitiesAlert
    func GetActivities(_ showAlert: Bool) {
        buttonGuidance.isEnabled = false
        MinderManager.sharedInstance.GetActivities({ _ in
            if MinderManager.sharedInstance.currentView == .guidance {
                //let vcGuidanceVC = (self.viewControllerCurrent as! GuidanceViewController)
                //vcGuidanceVC.GetGuidanceActivity()
                let vcScheduleVC = (self.viewControllerCurrent as! ScheduleViewController) // fbn
                vcScheduleVC.GetGuidanceActivity() // fbn
            }
            if showAlert {
                DispatchQueue.main.async(execute: {
                    let myPopup: NSAlert = NSAlert()
                    myPopup.messageText = "Activity synchronization done."
                    myPopup.informativeText = "Your activity schedules are now upto date. Videos for new schedules are downloading in background."
                    myPopup.alertStyle = .informational
                    myPopup.addButton(withTitle: "OK")
                    myPopup.runModal()
                })
            }
            //fbn
            DispatchQueue.main.async(execute: {
                self.buttonGuidance.isEnabled = true
            })
        })
    }
    // MARK:SwitchToGadget
    func SwitchToGadget() {
        if !MinderManager.sharedInstance.isRecording && MinderManager.sharedInstance.replayState == .none {
            MinderManager.sharedInstance.appMode = .gadget
            setupWindow()
            disableDeviceViewClick = false
        }
    }
    // MARK:CloseTapped
    @IBAction func CloseTapped(_ sender: AnyObject) {
        if MinderManager.sharedInstance.isRecording {
            RecordTapped(NSButton())
        }
        
        MinderManager.sharedInstance.deviceFeaturesEnabled = false
        MinderManager.sharedInstance.deviceStatus = .disConnected
        MinderManager.sharedInstance.isAppInBackground = true
        NSApplication.shared().orderedWindows[0].setIsVisible(false)//ankush change
        //        if MinderManager.sharedInstance.appMode == .normal {
        //            SwitchToGadget()
        //            //resetting button to empty key equivalenet
        //            buttonClose.keyEquivalent = ""
        //        } else {
        //            curveMenuClick(0)
        //        }
    }
    
    func SwitchToGadgetOnDoubleClick(_ sender: AnyObject) {
        if MinderManager.sharedInstance.appMode == .normal {
            if MinderManager.sharedInstance.isRecording {
                RecordTapped(NSButton())
            }
            SwitchToGadget()
            //resetting button to empty key equivalenet
            buttonClose.keyEquivalent = ""
        } else {
            hideUnhideWidgetElements(hiddenStatusTimer: true, hiddenStatusShoulder: true, hiddenStatusHR: true)
            curveMenuClick(0)
        }
        
    }
    
    // MARK:MinimiseTapped
    @IBAction func MinimiseTapped(_ sender: AnyObject) {
        self.view.window!.setIsMiniaturized(true)
    }
    // MARK:DashboardTapped
    @IBAction func DashboardTapped(_ sender: AnyObject) {
        mainButtonTap(.dashboard)
    }
    // MARK:GuidanceTapped
    @IBAction func GuidanceTapped(_ sender: AnyObject) {
        mainButtonTap(.guidance)
    }
    // MARK:SettingsTapped
    @IBAction func SettingsTapped(_ sender: AnyObject) {
        mainButtonTap(.settings)
    }
    // MARK:SettingsTapped
    @IBAction func MessageTapped(_ sender: AnyObject) {
        mainButtonTap(.message)
    }
    // MARK:SettingsTapped
    @IBAction func CommunityTapped(_ sender: AnyObject) {
        mainButtonTap(.community)
    }
    // MARK:WebsiteTapped
    @IBAction func WebsiteTapped(_ sender: AnyObject) {
        if let checkURL = URL(string: "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro") {
            NSWorkspace.shared().open(checkURL)
        }
    }
    @IBAction func TestNLogTapped(_ sender: Any) {
        mainButtonTap(.testnlog)
    }
    @IBAction func DebugMinderlibTapped(_ sender: AnyObject) {
        mainButtonTap(.debugminderlib)
    }
    // MARK:SyncTapped
    @IBAction func SyncTapped(_ sender: AnyObject) {
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            GetActivities(true)
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Please login"
            myPopup.informativeText = "You have to login with your Minder credentials to sync your activities."
            myPopup.alertStyle = .informational
            myPopup.addButton(withTitle: "OK")
            myPopup.runModal()
        }
    }
    // MARK:NotificationTapped
    @IBAction func NotificationTapped(_ sender: AnyObject) {
        let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.volumeYellowMute && opp.vibrateYellow == 0 {
            opp.volumeYellowMute = false
            opp.vibrateYellow = 1
            opp.volumeRedMute = false//ankush change
            opp.vibrateRed = 1
        } else if !opp.volumeYellowMute {
            opp.volumeYellowMute = true
            opp.vibrateYellow = 1
            opp.volumeRedMute = true
            opp.vibrateRed = 1
        } else {
            opp.volumeYellowMute = true
            opp.vibrateYellow = 0
            opp.volumeRedMute = true
            opp.vibrateRed = 0
        }
        MinderManager.sharedInstance.SaveSettings()
        UpdateNotification()
    }
    func mainButtonTap(_ type: ViewType) {
        MinderManager.sharedInstance.currentView = type
        UnselectButtons()
        switch MinderManager.sharedInstance.currentView {
        case .dashboard:
            DispatchQueue.main.async(execute: {
                self.buttonDashboard.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).cgColor
                self.buttonDashboard.image = NSImage(named: "Dashboard")
                self.buttonRecord.isHidden = true
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(false)
            })
            break
        case .guidance:
            DispatchQueue.main.async(execute: {
                self.buttonGuidance.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).cgColor
                self.buttonGuidance.image = NSImage(named: "Guidance")
                self.buttonRecord.isHidden = false
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(true)
            })
            break
        case .settings:
            DispatchQueue.main.async(execute: {
                self.buttonSettings.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).cgColor
                self.buttonSettings.image = NSImage(named: "Settings")
                self.buttonRecord.isHidden = true
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(false)
            })
            break
        case .message:
            DispatchQueue.main.async(execute: {
                self.buttonMessage.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).cgColor
                self.buttonMessage.image = NSImage(named: "MessageButton")
                self.buttonRecord.isHidden = true
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(false)
            })
            break
        case .community:
            DispatchQueue.main.async(execute: {
                self.buttonCommunity.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).cgColor
                self.buttonCommunity.image = NSImage(named: "CommunityButton")
                self.buttonRecord.isHidden = true
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(false)
            })
            break
        case .testnlog:
            DispatchQueue.main.async(execute: {
                self.buttonTestNLog.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).cgColor
                self.buttonRecord.isHidden = false
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(false)
            })
            break
        case .debugminderlib:
            DispatchQueue.main.async(execute: {
                 self.buttonRecord.isHidden = false
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(false)
            })
            break
        }
        
        SwitchContainerViewController()
    }
    func SwitchContainerViewController() {
        
        DispatchQueue.main.async(execute: {
            let subViews = self.viewPlaceHolder.subviews
            for view in subViews {
                view.removeFromSuperview()
            }
            
            let mainStoryboard: NSStoryboard = NSStoryboard(name: "Main", bundle: nil)
            switch MinderManager.sharedInstance.currentView {
            case .dashboard:
                self.switchMeditationMode(false)
                self.viewControllerCurrent = mainStoryboard.instantiateController(withIdentifier: "ViewControllerDashboard") as? DashboardViewController
                break
            case .guidance:
                //let vc = mainStoryboard.instantiateController(withIdentifier: "ViewControllerGuidance") as? GuidanceViewController
                let vc = mainStoryboard.instantiateController(withIdentifier: "ViewControllerSchedule") as? ScheduleViewController
                vc?.delegateGuidance = self
                self.viewControllerCurrent = vc
                break
            case .settings:
                self.switchMeditationMode(false)
                let vc = mainStoryboard.instantiateController(withIdentifier: "SettingUpdatedViewController") as? SettingUpdatedViewController
                vc?.delegateSettings = self
                self.viewControllerCurrent = vc
                break
            case .message:
                self.switchMeditationMode(false)
                let vc = mainStoryboard.instantiateController(withIdentifier: "MessageViewController") as? MessageViewController
                //vc?.delegateSettings = self
                self.viewControllerCurrent = vc
                break
            case .community:
                self.switchMeditationMode(false)
                let vc = mainStoryboard.instantiateController(withIdentifier: "CommunityViewController") as? CommunityViewController
                // vc?.delegateSettings = self
                self.viewControllerCurrent = vc
                break
            case .testnlog:
                //self.switchMeditationMode(false)
                let vc = mainStoryboard.instantiateController(withIdentifier: "LogViewController") as? LogViewController
                // vc?.delegateSettings = self
                self.viewControllerCurrent = vc
                break
            case .debugminderlib:
                //self.switchMeditationMode(false)
                let vc = mainStoryboard.instantiateController(withIdentifier: "MinderLibDebugController") as? MinderLibDebugController
                // vc?.delegateSettings = self
                self.viewControllerCurrent = vc
                break
            default:
                break
            }
            //self.ReplayClosed()
            self.viewPlaceHolder.addSubview(self.viewControllerCurrent.view)
            self.viewControllerCurrent.view.setFrameOrigin(NSPoint(x: 0, y: 0))
            self.viewControllerCurrent.view.setFrameSize(self.viewPlaceHolder.frame.size)
        })
    }
    func UnselectButtons() {
        DispatchQueue.main.async(execute: {
            self.buttonDashboard.image = NSImage(named: "DashboardUn")
            self.buttonDashboard.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
            self.buttonGuidance.image = NSImage(named: "GuidanceUn")
            self.buttonGuidance.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
            self.buttonSettings.image = NSImage(named: "SettingsUn")
            self.buttonSettings.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
            self.buttonMessage.image = NSImage(named: "MessageButton")
            self.buttonMessage.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
            self.buttonCommunity.image = NSImage(named: "CommunityButton")
            self.buttonCommunity.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
            
            self.buttonTestNLog.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
            
            self.logOutView.isHidden = true
            self.viewLogin.isHidden = true
        })
    }
    
    @IBAction func editProfileShowButtonAction(_ sender: AnyObject) {
        self.view.addSubview(self.viewControllerEditProfile!.view)
        self.viewControllerEditProfile!.view.setFrameOrigin(NSPoint(x: 0, y: 0))
        self.viewControllerEditProfile!.RefreshControls()
        logOutView.isHidden = true
    }
    @IBAction func logoutTapped(_ sender: AnyObject) {
        
        
        
        MinderManager.sharedInstance.userLogged = User()
        
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "UserDetails")
        userDefaults.synchronize()
        
        // Create user folders - Activities, Records
        let fileManager = FileManager.default
        var isDir : ObjCBool = true
        var path: String = MinderManager.sharedInstance.appDirectory + "_/Activities/"
        if !fileManager.fileExists(atPath: path, isDirectory: &isDir) {
            do {
                try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Error while creating Activities folder. \(error)")
            }
        }
        path = MinderManager.sharedInstance.appDirectory + "_/Records/"
        if !fileManager.fileExists(atPath: path, isDirectory: &isDir) {
            do {
                try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Error while creating Records folder. \(error)")
            }
        }
        
        MinderManager.sharedInstance.LoadSettings()
        if MinderManager.sharedInstance.currentView == .settings {
            if let viewControllerCurrent = viewControllerCurrent {
                let vcSettingsVC = (viewControllerCurrent as! SettingUpdatedViewController)//ankush change
                vcSettingsVC.RefreshControls()
            }
        }
        
        userImageRoundView.isHidden = true
        //btnLogoutViewToggle.hidden = true
        logOutView.isHidden = true
        userNameLabel.stringValue = ""
        
        let myPopup: NSAlert = NSAlert()
        myPopup.messageText = "Logged out"
        myPopup.informativeText = "You have been logged out from the Minder. From now on your activities are not analysed."
        myPopup.alertStyle = .informational
        myPopup.addButton(withTitle: "OK")
        //        myPopup.runModal()
        
        //ankush changes
        let logoutModel = myPopup.runModal()
        if logoutModel == NSAlertFirstButtonReturn {
            MinderManager.sharedInstance.DeviceDisconnect()
            sleep(2)
            delegateLogin?.updateMainView(.login, typeLogin: .withLogin)
            MinderManager.sharedInstance.appMode = .gadget
            MinderManager.sharedInstance.minderlib = nil
        }
    }
    @IBAction func loginTapped(_ sender: AnyObject) {
        MinderManager.sharedInstance.DeviceDisconnect()
        sleep(2)
        viewLogin.isHidden = true
        delegateLogin?.updateMainView(.login, typeLogin: .withOutLogin)
    }
    @IBAction func RecordTapped(_ sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        
        if MinderManager.sharedInstance.deviceStatus == .connected {
            let style = NSMutableParagraphStyle()
            style.alignment = .center
            if MinderManager.sharedInstance.isRecording {
                let systemRecordingName = MinderManager.sharedInstance.SaveRecorded(NSImage(data: viewPosture.dataWithPDF(inside: viewPosture.bounds))!)
                self.editDoneFromPlayer = false
                showPopUpAfterRecording(recordSystemName: systemRecordingName) // fbn
                MinderManager.sharedInstance.isRecording = false
                viewPosture.isRecordView = false
                viewAnnulus.viewMode = false
                //viewBall.layer?.backgroundColor = NSColor.white.cgColor
                viewPosture.idealPath = nil
                buttonRecord.attributedTitle = NSAttributedString(string: "RECORD START", attributes: [NSFontAttributeName : NSFont.boldSystemFont(ofSize: 8), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style])//ankush change fontsize 13 -> 10
                buttonRecord.image = NSImage(named: "RecordStart")
                (NSApp.delegate as! AppDelegate).SetMenuRecordTitle("Start")
                
                if MinderManager.sharedInstance.currentView == .guidance {
                    if let viewControllerCurrent = self.viewControllerCurrent {
                        let vcGuidanceVC = (viewControllerCurrent as! ScheduleViewController)
                        if vcGuidanceVC.currentViewType == .record {
                            vcGuidanceVC.GetRecords()
                        }
                    }
                }
                buttonCalibrate.isHidden = false
                //                viewAnnulus.hidden = false
                self.view.layer?.backgroundColor = NSColor.clear.cgColor
                
            } else {
                
                if MinderManager.sharedInstance.meditationModeFlag == false {
                    viewBall.isHidden = false
                }
                sceneBall.isHidden = true
                MinderManager.sharedInstance.isRecording = true
                viewPosture.isRecordView = true
                viewAnnulus.viewMode = true
                let radiusBall = (MinderManager.sharedInstance.centerDeviceViewHeight * MinderManager.percentageBall) / 100
                viewBall.frame.origin = NSMakePoint(MinderManager.sharedInstance.centerDeviceView.x - radiusBall, MinderManager.sharedInstance.centerDeviceView.y - radiusBall)
                sceneBall.frame.origin = NSMakePoint(MinderManager.sharedInstance.centerDeviceView.x - radiusBall, MinderManager.sharedInstance.centerDeviceView.y - radiusBall) // fbn
                //                sceneBall.frame.origin = NSMakePoint(-1000, -1000) // fbn
                viewBall.layer?.backgroundColor = NSColor.clear.cgColor
                //sceneBall.layer?.backgroundColor = NSColor.clearColor().CGColor // fbn
                buttonRecord.attributedTitle = NSAttributedString(string: "RECORD STOP", attributes: [NSFontAttributeName : NSFont.boldSystemFont(ofSize: 8), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style])//ankush change fontsize 13 -> 10
                buttonRecord.image = NSImage(named: "RecordStop")
                (NSApp.delegate as! AppDelegate).SetMenuRecordTitle("Stop")
                let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor
                    self.view.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor // fbn
                }
                buttonCalibrate.isHidden = true
                sceneBall.isHidden = true
                
            }
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Record can't start."
            myPopup.informativeText = "Please connect your device to start recording."
            myPopup.alertStyle = .informational
            myPopup.addButton(withTitle: "OK")
            myPopup.runModal()
        }
    }
    
    @IBAction func RedZoneIncrementTapped(_ sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        
        if opp.difficultyRed  < 100 {
            opp.difficultyRed += 1
            viewPosture.UpdateDifficulty(.red, difficulty: (opp.difficultyRed))
            
            MinderManager.sharedInstance.SaveSettings()
        }
    }
    @IBAction func RedZoneDecrementTapped(_ sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.difficultyRed > 0 {
            opp.difficultyRed -= 1
            viewPosture.UpdateDifficulty(.red, difficulty: (opp.difficultyRed))
            
            MinderManager.sharedInstance.SaveSettings()
        }
    }
    @IBAction func YellowZoneIncrementTapped(_ sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.difficultyYellow < 100 {
            opp.difficultyYellow += 1
            viewPosture.UpdateDifficulty(.yellow, difficulty: (opp.difficultyYellow))
            
            MinderManager.sharedInstance.SaveSettings()
        }
    }
    @IBAction func YellowZoneDecrementTapped(_ sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.difficultyYellow > 0 {
            opp.difficultyYellow -= 1
            viewPosture.UpdateDifficulty(.yellow, difficulty: (opp.difficultyYellow))
            
            MinderManager.sharedInstance.SaveSettings()
        }
    }
    
    var disableDeviceViewClick = false
    var prevZoneType: ZoneType = .green
    
    @IBAction func dashboardTapped(_ sender: AnyObject) {
        menuButtonTapped(.dashboard)
    }
    @IBAction func guidanceTapped(_ sender: AnyObject) {
        menuButtonTapped(.guidance)
    }
    @IBAction func notificationTapped(_ sender: AnyObject) {
        NotificationTapped(sender)
    }
    @IBAction func ProcessPauseTapped(_ sender: AnyObject) {
        UpdateProcessingPaused()
        
        if MinderManager.sharedInstance.isLogEnabled {
            MinderManager.sharedInstance.deviceLog(_log:"ProcessPauseTapped")
        }
    }
    @IBAction func settingsTapped(_ sender: AnyObject) {
        menuButtonTapped(.settings)
    }
    
    @IBAction func PitchChanged(_ sender: AnyObject) {
        MinderManager.sharedInstance.pitch = CGFloat(sender.floatValue) * CGFloat(M_PI) / 180
        GyroTheView()
    }
    @IBAction func RollChanged(_ sender: AnyObject) {
        MinderManager.sharedInstance.roll = CGFloat(sender.floatValue) * CGFloat(M_PI) / 180
        GyroTheView()
    }
    @IBAction func YawChanged(_ sender: AnyObject) {
        MinderManager.sharedInstance.yaw = CGFloat(sender.floatValue) * CGFloat(M_PI) / 180
        GyroTheView()
    }
    
    func GyroTheView() {
        var transform = CATransform3D()
        transform = CATransform3DMakeRotation(MinderManager.sharedInstance.pitch, 1, 0, 0)
        transform = CATransform3DRotate(transform, MinderManager.sharedInstance.roll, 0, 1, 0)
        transform = CATransform3DRotate(transform, MinderManager.sharedInstance.yaw, 0, 0, 1)
        let frame = viewAnnulus.layer!.frame
        let center = CGPoint(x: frame.midX, y: frame.midY)
        self.viewAnnulus.layer!.position = center
        self.viewAnnulus.layer!.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.viewAnnulus.layer!.transform = transform
        
        let xAngle = SCNMatrix4MakeRotation(MinderManager.sharedInstance.pitch, 1, 0, 0)
        let yAngle = SCNMatrix4MakeRotation(MinderManager.sharedInstance.roll, 0, 1, 0)
        let zAngle = SCNMatrix4MakeRotation(MinderManager.sharedInstance.yaw, 0, 0, 1)
        let rotationMatrix = SCNMatrix4Mult(SCNMatrix4Mult(xAngle, yAngle), zAngle)
        boxNode!.transform = rotationMatrix
        
    }
    
    // MARK: show alert with textfield after recording
    func showPopUpAfterRecording(recordSystemName: String) {
        recordingNameTextField.stringValue = MinderManager.sharedInstance.FetchRecordingName(recordSystemName: recordSystemName)
        currentRecordingFileName = recordSystemName
        currentRecordingUserSetName = recordingNameTextField.stringValue
        recordNamePopUpView.isHidden = false
        recordNamePopUpView.acceptsTouchEvents = true
        recordNameInnerPopUpView.isHidden = false
        recordNamePopUpView.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: 0.8).cgColor
        recordNameInnerPopUpView.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1.0).cgColor
    }
    
    // MARK: remove special charecters
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),:!_-".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    
    var exitFromTheQueue = false
    var maxWaitTimeExit: UInt32 = 0
    
    //timer functionality
    @IBAction func buttonTimerTapped(_ sender: AnyObject) {
        if mytimer == nil
        {
            reversing = false;
            mytimer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
        else
        {
            mytimer!.invalidate()
            mytimer = nil
        }
        
    }
    
    func timerAction()
    {
        let sliderrange =  sliderTimer.maxValue - sliderTimer.minValue;
        let increment = sliderrange/100;
        var newval = sliderTimer.doubleValue;
        
        
        if reversing!
        {
            newval = sliderTimer.doubleValue - increment;
        }
        else
        {
            newval = sliderTimer.doubleValue + increment;
        }
        if(newval >= sliderTimer.maxValue)
        {
            reversing = true;
            newval = newval - 2 * increment;
            
        }
        else  if newval <= 0
        {
            reversing = false;
        }
        sliderTimer.doubleValue = newval
        progressTimer.doubleValue = newval
    }
    func badPostureTimerCalled()
    {
        //hit api
        badPostureTimer!.invalidate()
        badPostureTimer = nil
        isInBadPosture = true
        print("ball is in bad posture for 10 seconds")
        
        if MinderManager.sharedInstance.isNotificationVibrate
        {
            MinderManager.sharedInstance.sendNotification((MinderManager.sharedInstance.userLogged?.userName)!, messageBody: "You are in bad posture.", vibration: "Yes", sound: "No", title: "Push Notification", completionHandler: {data -> Void in
                if data {
                    
                } else {
                    
                }
                
            })
        }
        else
        {
            MinderManager.sharedInstance.sendNotification((MinderManager.sharedInstance.userLogged?.userName)!, messageBody: "You are in bad posture.", vibration: "No", sound: "No", title: "Push Notification", completionHandler: {data -> Void in
                if data {
                    
                } else {
                    
                }
                
            })
        }
       
        
    }
    //    @IBAction func stratMeditaionGadgetTapped(_ sender: AnyObject) {
    //        if MinderManager.sharedInstance.appMode == .gadget
    //        {
    //            setUpMeditationView(.gadget, frame: viewPosture.frame)
    //            startMeditaionGadgetLabel.isHidden = true
    //
    //            let mediatationBreak = MeditationBreak()
    //            mediatationBreak.numberOfBreath = 2
    //            mediatationBreak.inhaleLength = 3
    //            mediatationBreak.retailInhaleLength = 3
    //            mediatationBreak.exhaleLength = 3
    //            mediatationBreak.retailExhaleLength = 3
    //            mediatationBreak.totalTimeForMeditation = UInt32(mediatationBreak.numberOfBreath) * (UInt32(mediatationBreak.inhaleLength) + UInt32(mediatationBreak.retailInhaleLength) + UInt32(mediatationBreak.exhaleLength) + UInt32(mediatationBreak.retailExhaleLength))
    //
    //            //AnimateMeditationGudeRingFromGadget(mediatationBreak)
    //        }
    //    }
    @IBAction func showMeditationButtonOnGadget(_ sender: AnyObject) {
        if MinderManager.sharedInstance.appMode == .gadget
        {
            if MinderManager.sharedInstance.meditationModeFlag == true {
                switchMeditationMode(false)
                startMeditaionGadgetLabel.isHidden = false
                
            }
            else
            {
                if meditationViewAlertView.isHidden == true
                {
                    backgroundForMeditationViewAlertView.isHidden = false
                    meditationViewAlertView.isHidden = false
                    startMeditaionGadgetLabel.isHidden = false
                    // switchBackgroundWhileMeditationPopUp()
                }
                else
                {
                    backgroundForMeditationViewAlertView.isHidden = true
                    meditationViewAlertView.isHidden = true
                }
            }
            
        }
    }
    func stratMeditaionGadgetTapped(sender: NSGestureRecognizer) {
        if MinderManager.sharedInstance.appMode == .gadget {
            setUpMeditationView(.gadget, frame: viewPosture.frame)
            startMeditaionGadgetLabel.isHidden = true
            
            let mediatationBreak = MinderManager.sharedInstance.settings?.breaks[(MinderManager.sharedInstance.settings?.breakSelected)!];
            
            AnimateMeditationGudeRingFromGadget(mediatationBreak!)
        }
    }
    func AnimateMeditationGudeRingFromGadget(_ mediatationBreak: MeditationBreak) {
        
       // print("meditation exhaleLength: \(mediatationBreak.exhaleLength) meditation inhaleLength: \(mediatationBreak.inhaleLength) meditation numberOfBreath: \(mediatationBreak.numberOfBreath) meditation retailExhaleLength: \(mediatationBreak.retailExhaleLength) meditation retailInhaleLength: \(mediatationBreak.retailInhaleLength)")
        //  MinderManager.sharedInstance.appendtoLogFile(stringToAppend: "meditation exhaleLength: \(mediatationBreak.exhaleLength) meditation inhaleLength: \(mediatationBreak.inhaleLength) meditation numberOfBreath: \(mediatationBreak.numberOfBreath) meditation retailExhaleLength: \(mediatationBreak.retailExhaleLength) meditation retailInhaleLength: \(mediatationBreak.retailInhaleLength)", isLogEnable: MinderManager.sharedInstance.isLogEnable)
        // switchMeditationMode(true)
        
        exitFromTheQueue = true
        sleep(maxWaitTimeExit)
        exitFromTheQueue = false
        
        maxWaitTimeExit = UInt32(mediatationBreak.retailInhaleLength > mediatationBreak.retailExhaleLength ? mediatationBreak.retailInhaleLength : mediatationBreak.retailExhaleLength)
        print("maxwaittime \(maxWaitTimeExit)")
        // MinderManager.sharedInstance.appendtoLogFile(stringToAppend:"maxwaittime \(maxWaitTimeExit)", isLogEnable: MinderManager.sharedInstance.isLogEnable)
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {() -> Void in
            
            var perfectInhalePersentage:[CGFloat] = []
            var perfectExhalePersentage:[CGFloat] = []
            var percentGuideRing: CGFloat = 15
            
            sleep(UInt32(mediatationBreak.retailExhaleLength))
            for i in 0..<mediatationBreak.numberOfBreath {
                let inhaleCounter: CGFloat = (35 - percentGuideRing) / CGFloat(mediatationBreak.inhaleLength * 10)
                repeat {
                    //let methodStart = Date()
                    if self.exitFromTheQueue {
                        break
                    }
                    DispatchQueue.main.async(execute: {
                        self.meditationViewPosture.guideRingRadius = percentGuideRing
                        self.meditationViewPosture.needsDisplay = true
                    })
                    percentGuideRing += inhaleCounter
                    /*let executionTime = Date().timeIntervalSince(methodStart)
                     let sleepTime = 100 - executionTime*/
                    usleep(99900)
                } while (percentGuideRing < 35)
                if self.exitFromTheQueue {
                    break
                }
                let inhalePercentage = abs(100 - (abs(self.meditationViewPosture.ballRadius - self.meditationViewPosture.guideRingRadius)/self.meditationViewPosture.guideRingRadius) * 100)
                perfectInhalePersentage.append(inhalePercentage)
                sleep(UInt32(mediatationBreak.retailInhaleLength))
                
                let exhaleCounter: CGFloat = (percentGuideRing - 15) / CGFloat(mediatationBreak.exhaleLength * 10)
                repeat {
                    //let methodStart = Date()
                    if self.exitFromTheQueue {
                        break
                    }
                    DispatchQueue.main.async(execute: {
                        self.meditationViewPosture.guideRingRadius = percentGuideRing
                        self.meditationViewPosture.needsDisplay = true
                    })
                    percentGuideRing -= exhaleCounter
                    /*let executionTime = Date().timeIntervalSince(methodStart)
                     let sleepTime = 100 - executionTime*/
                    usleep(99900)
                } while (percentGuideRing > 15)
                if self.exitFromTheQueue {
                    break
                }
                let exhalePersentage = abs(100 - (abs(self.meditationViewPosture.ballRadius - self.meditationViewPosture.guideRingRadius)/self.meditationViewPosture.guideRingRadius) * 100)
                perfectExhalePersentage.append(exhalePersentage)
                if (i + 1) == mediatationBreak.numberOfBreath {
                    self.userMeditationFullyCompleted = true
                } else {
                    sleep(UInt32(mediatationBreak.retailExhaleLength))
                }
            }
            self.meditationReportCard = MeditationReportCard(totalCycles: mediatationBreak.numberOfBreath, totalTime: Int(mediatationBreak.totalMeditationTime()!), perfectInhalePersentage: perfectInhalePersentage, perfectExcalePersentage: perfectExhalePersentage)
            sleep(1)
            DispatchQueue.main.async(execute: {
                if self.userMeditationProgressBarCompleted == true && self.userMeditationFullyCompleted == true {
                    self.showMeditationReport()
                }
            })
        })
        //updateMeditationProgressBar(mediatationBreak)
    }
    func skipLabelClicked(sender: NSGestureRecognizer) {
        
        if MinderManager.sharedInstance.appMode == .gadget
        {
            meditationViewAlertView.isHidden = true
            backgroundForMeditationViewAlertView.isHidden = true
        }

    }
    func setYellowHaptic (selectedTag : Int)
    {
        switch selectedTag {
        case 1:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(10)
            break
        case 2:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(80)
            break
        case 3:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(60)
            break
        case 4:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(40)
            break
            
        default:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(10)
        }
    }
    func setRedHaptic (selectedTag : Int)
    {
        switch selectedTag {
        case 1:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(10)
            break
        case 2:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(80)
            break
        case 3:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(60)
            break
        case 4:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(40)
            break
            
        default:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(10)
        }
    }
    
    func displayCurrentTime()
    {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
//        var hourString = "0"
//        var minString = "0"
//        var hourInteger = 0

//        if hour > 12
//        {
//            hourInteger = hour - 12
//        }
//        else
//        {
//            hourInteger = hour
//        }
//        
//        if hourInteger < 10
//        {
//            hourString = "0\(hourInteger)"
//        }
//        else
//        {
//            hourString = "\(hourInteger)"
//        }
//        if minutes < 10
//        {
//            minString = "0\(minutes)"
//        }
//        else
//        {
//            minString = "\(minutes)"
//        }
       // print("hours :\(hour)  minutes : \(minutes)   ")
        if hour > 12
        {
            if minutes < 10
            {
                self.currentTimeWidgetLabel.stringValue = "\(hour - 12):0\(minutes) PM "
            }
            else{
                self.currentTimeWidgetLabel.stringValue = "\(hour - 12):\(minutes) PM "
            }
            
        }
        else
        {
            if minutes < 10
            {
                self.currentTimeWidgetLabel.stringValue = "\(hour):0\(minutes) AM "
            }
            else{
                self.currentTimeWidgetLabel.stringValue = "\(hour):\(minutes) AM "
            }
        }
        
    }
}

extension HomeViewController : DelegateProfileEdit {
    func ProfileEdited() {
        DispatchQueue.main.async(execute: {
            self.UpdateLoggedUserDetails()
        })
    }
}

extension HomeViewController : DelegateGuidance {
    func Replay(_ postureData: [PostureData], gyroData: [GyroData], isReplay: Bool) {
        
        exitFromTheQueue = true
        switchMeditationMode(false)
        
        
        self.viewBall.isHidden = false
        self.viewBall.layer?.backgroundColor = NSColor.clear.cgColor
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {() -> Void in
            
            NSLog("Replay started.")
            
            self.viewBall.frame.origin = NSMakePoint(self.viewPosture.frame.size.height/2 - self.viewBall.frame.size.height/2, self.viewPosture.frame.size.height/2 - self.viewBall.frame.size.height/2)
            self.viewPosture.isRecordView = true
            self.viewPosture.isReplay = isReplay
            self.viewAnnulus.viewMode = true
            MinderManager.sharedInstance.BackupGyro()
            let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor
                self.buttonCalibrate.isHidden = true
                self.buttonRecord.isHidden = true
                self.viewBall.layer?.backgroundColor = NSColor.clear.cgColor
                
                self.view.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor // fbn
                //self.sceneBall.layer?.backgroundColor = NSColor.clearColor().CGColor // fbn
                //self.viewAnnulus.hidden = true
            }
            MinderManager.sharedInstance.replayState = .play
            var progress: Int = 0
            for pData in postureData {
                DispatchQueue.main.async(execute: {
                    self.viewPosture.UpdateRecordCordinates(pData.z , y: pData.y)
                    let gData = gyroData[progress]
                    self.UpdateGyroWithRoll(gData.roll, withPitch: gData.pitch, withYaw: gData.yaw, withT: 0.0)
                    progress += 1
                    if MinderManager.sharedInstance.currentView == .guidance {
                        if let viewControllerCurrent = self.viewControllerCurrent {
                            //let vcGuidanceVC = (viewControllerCurrent as! GuidanceViewController)
                            let vcGuidanceVC = (viewControllerCurrent as! ScheduleViewController) // fbn
                            DispatchQueue.main.async(execute: {
                                vcGuidanceVC.UpdateReplayProgress(progress)
                            })
                        }
                    }
                })
                usleep(100000)
                while(MinderManager.sharedInstance.replayState == .pause) {}
                if MinderManager.sharedInstance.replayState == .none {
                    break;
                }
            }
            if !MinderManager.sharedInstance.isActivityPlaying {
                self.ReplayClosed()
            }
            NSLog("Replay ballpath ended.")
            
        })
    }
    func ReplayClosed() {
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.buttonCalibrate.isHidden = false
            self.buttonRecord.isHidden = false
            //self.viewAnnulus.hidden = false
            
            self.view.layer?.backgroundColor = NSColor.clear.cgColor // fbn
        }
        
        exitFromTheQueue = true
        self.viewPosture.isRecordView = false
        self.viewPosture.isReplay = false
        self.viewAnnulus.viewMode = false
        self.viewBall.isHidden = true
        self.viewPosture.recordPath = nil
        self.viewPosture.idealPath = nil
        self.viewBall.layer?.backgroundColor = NSColor.white.cgColor
        let oppSelected = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        // self.viewPosture.layer?.backgroundColor = viewPosture.layer?.backgroundColor?.copy(alpha: CGFloat(oppSelected.transperancy))
        self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 46/255, green: 204/255, blue: 114/255, alpha: CGFloat(oppSelected.transperancy)).cgColor
        MinderManager.sharedInstance.RestoreGyro()
        MinderManager.sharedInstance.replayState = .none
        if MinderManager.sharedInstance.currentView == .guidance {
            if let viewControllerCurrent = self.viewControllerCurrent {
                //let vcGuidanceVC = (viewControllerCurrent as! GuidanceViewController)
                let vcScheduleViewController = (viewControllerCurrent as! ScheduleViewController)
                DispatchQueue.main.async(execute: {
                    vcScheduleViewController.ReplayFinished()
                })
            }
        }
        self.viewPosture.needsDisplay = true
    }
    
    func switchMeditationMode(_ flag:Bool) {
        if flag {
            //self.mainContainerView.isHidden = false
            setUpMeditationView(.normal, frame: NSRect(x: 0, y: 0, width: 0, height: 0 ))
        }else {
            //self.mainContainerView.isHidden = true
            hideMeditationView()
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    //MARK: Update the meditation progress bar
    func updateMeditationProgressBar(_ mediatationBreak: MeditationBreak) {
        progressTimer.maxValue = Double(mediatationBreak.totalMeditationTime()!)
        self.timerStartLabel.stringValue = "0:00"
        MinderManager.sharedInstance.deviceLog(_log: "totalTimeForMeditation : \(Double(mediatationBreak.totalMeditationTime()!))")
        self.mainContainerView.isHidden = false
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {() -> Void in
            let (h,m,s) = self.secondsToHoursMinutesSeconds(seconds: Int(mediatationBreak.totalMeditationTime()!))
            let minutes = abs(Int(m)) < 10 ? "0\(m)" : String(m)
            let seconds = abs(Int(s)) < 10 ? "0\(s)" : String(s)
            DispatchQueue.main.async {
                self.timerEndLabel.stringValue = "\(minutes):\(seconds)"
            }
            let tmt = mediatationBreak.totalMeditationTime()!
            for i in 0...tmt {
                if self.exitFromTheQueue {
                    DispatchQueue.main.async {
                        self.progressTimer.doubleValue = 0
                        self.timerStartLabel.stringValue = "00:00"
                        self.timerEndLabel.stringValue = "00:00"
                    }
                    break
                }
                
                let minutes = i / 60 == 0 ? 0 : (i/60)
                let showMinutes = abs(Int(minutes)) < 10 ? "0\(minutes)" : String(minutes)
                
                let seconds = i < 60 ? i : i % 60
                let showSeconds = abs(Int(seconds)) < 10 ? "0\(seconds)" : String(seconds)
                DispatchQueue.main.async {
                    self.progressTimer.doubleValue = Double(i)
                    //let showTime = abs(Int(i)) < 10 ? "0\(i)" : String(i)
                    self.timerStartLabel.stringValue = "\(showMinutes):\(showSeconds)"
                }
                usleep(1000000)
            }
            DispatchQueue.main.async {
                self.userMeditationProgressBarCompleted = true
                self.progressTimer.doubleValue = 0
                self.timerStartLabel.stringValue = "00:00"
                self.timerEndLabel.stringValue = "00:00"
                /*self.meditationReportCard = MeditationReportCard(totalCycles: mediatationBreak.numberOfBreath, totalTime: Int(mediatationBreak.totalTimeForMeditation), perfectInhalePersentage: perfectInhalePersentage, perfectExcalePersentage: perfectExhalePersentage)
                 if self.userMeditationProgressBarCompleted == true && self.userMeditationFullyCompleted == true {
                 self.showMeditationReport()
                 }*/
            }
        })
    }
    
    //MARK: show user meditation report data
    func showMeditationReport() {
        self.mainContainerView.isHidden = true
        meditationReportViewController?.view.setFrameOrigin(NSPoint(x: 0, y: 0))
        view.addSubview((meditationReportViewController?.view)!)
        self.userMeditationFullyCompleted = false
        self.userMeditationProgressBarCompleted = false
        meditationReportViewController?.populateReportData(meditationReportCard: self.meditationReportCard!)
        
        if let viewControllerCurrent = self.viewControllerCurrent {
            //let vcGuidanceVC = (viewControllerCurrent as! GuidanceViewController)
            let vcScheduleViewController = (viewControllerCurrent as! ScheduleViewController)
            DispatchQueue.main.async(execute: {
                vcScheduleViewController.postureMenuClicked(NSButton())
            })
        }
    }
    
    func AnimateMeditationGudeRing(_ mediatationBreak: MeditationBreak) {
        MinderManager.sharedInstance.deviceLog(_log: "MeditationBreak \r\n\t\t exhaleLength: \(mediatationBreak.exhaleLength)\r\n\t\t inhaleLength: \(mediatationBreak.inhaleLength)\r\n\t\t numberOfBreath: \(mediatationBreak.numberOfBreath)\r\n\t\t retailExhaleLength: \(mediatationBreak.retailExhaleLength) \r\n\t\t retailInhaleLength: \(mediatationBreak.retailInhaleLength)")
        switchMeditationMode(true)
        
        exitFromTheQueue = true
        sleep(maxWaitTimeExit)
        exitFromTheQueue = false
        
        maxWaitTimeExit = UInt32(mediatationBreak.retailInhaleLength > mediatationBreak.retailExhaleLength ? mediatationBreak.retailInhaleLength : mediatationBreak.retailExhaleLength)
        MinderManager.sharedInstance.deviceLog(_log: "maxwaittime \(maxWaitTimeExit)")
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {() -> Void in
            
            var perfectInhalePersentage:[CGFloat] = []
            var perfectExhalePersentage:[CGFloat] = []
            var percentGuideRing: CGFloat = 15
            
            sleep(UInt32(mediatationBreak.retailExhaleLength))
            for i in 0..<mediatationBreak.numberOfBreath {
                let inhaleCounter: CGFloat = (35 - percentGuideRing) / CGFloat(mediatationBreak.inhaleLength * 10)
                repeat {
                    //let methodStart = Date()
                    if self.exitFromTheQueue {
                        break
                    }
                    DispatchQueue.main.async(execute: {
                        self.meditationViewPosture.guideRingRadius = percentGuideRing
                        self.meditationViewPosture.needsDisplay = true
                    })
                    percentGuideRing += inhaleCounter
                    /*let executionTime = Date().timeIntervalSince(methodStart)
                     let sleepTime = 100 - executionTime*/
                    usleep(99900)
                } while (percentGuideRing < 35)
                if self.exitFromTheQueue {
                    break
                }
                let inhalePercentage = abs(100 - (abs(self.meditationViewPosture.ballRadius - self.meditationViewPosture.guideRingRadius)/self.meditationViewPosture.guideRingRadius) * 100)
                perfectInhalePersentage.append(inhalePercentage)
                sleep(UInt32(mediatationBreak.retailInhaleLength))
                
                let exhaleCounter: CGFloat = (percentGuideRing - 15) / CGFloat(mediatationBreak.exhaleLength * 10)
                repeat {
                    //let methodStart = Date()
                    if self.exitFromTheQueue {
                        break
                    }
                    DispatchQueue.main.async(execute: {
                        self.meditationViewPosture.guideRingRadius = percentGuideRing
                        self.meditationViewPosture.needsDisplay = true
                    })
                    percentGuideRing -= exhaleCounter
                    /*let executionTime = Date().timeIntervalSince(methodStart)
                     let sleepTime = 100 - executionTime*/
                    usleep(99900)
                } while (percentGuideRing > 15)
                if self.exitFromTheQueue {
                    break
                }
                let exhalePersentage = abs(100 - (abs(self.meditationViewPosture.ballRadius - self.meditationViewPosture.guideRingRadius)/self.meditationViewPosture.guideRingRadius) * 100)
                perfectExhalePersentage.append(exhalePersentage)
                if (i + 1) == mediatationBreak.numberOfBreath {
                    self.userMeditationFullyCompleted = true
                } else {
                    sleep(UInt32(mediatationBreak.retailExhaleLength))
                }
            }
            self.meditationReportCard = MeditationReportCard(totalCycles: mediatationBreak.numberOfBreath, totalTime: Int(mediatationBreak.totalMeditationTime()!), perfectInhalePersentage: perfectInhalePersentage, perfectExcalePersentage: perfectExhalePersentage)
            MinderManager.sharedInstance.deviceLog(_log: "MeditationBreak Report \r\n\t\t totalTimeForMeditation: \(mediatationBreak.totalMeditationTime)\r\n\t\t numberOfBreath: \(mediatationBreak.numberOfBreath)\r\n\t\t perfectInhalePersentage: \(perfectInhalePersentage) \r\n\t\t perfectExhalePersentage: \(perfectExhalePersentage)")
            sleep(1)
            DispatchQueue.main.async(execute: {
                if self.userMeditationProgressBarCompleted == true && self.userMeditationFullyCompleted == true {
                    self.showMeditationReport()
                }
            })
        })
        updateMeditationProgressBar(mediatationBreak)
    }
    
    func EditRecordName(currentRecordName: String, currentRecordSystemName: String) {
        self.editDoneFromPlayer = true
        showPopUpAfterRecording(recordSystemName: currentRecordSystemName)
        self.currentRecordingFileName = currentRecordSystemName
        currentRecordingUserSetName = currentRecordName
    }
}

extension HomeViewController : DelegateImitationView {
    func curveMenuClick(_ show: Int) {
        if !disableDeviceViewClick {
            if show == 1 {
                viewCurveMenuHolder?.setFrameSize(NSMakeSize(viewPosture.frame.size.width * 0.41, viewPosture.frame.size.height * 0.86))
                viewCurveMenuHolder?.setFrameOrigin(NSMakePoint(viewTrackball.frame.size.width - viewCurveMenuHolder.frame.size.width - ((5.5 * viewCurveMenuHolder.frame.size.width) / 100), (viewPosture.frame.size.height / 2) - (viewCurveMenuHolder.frame.size.height / 2) + ((0.4 * viewCurveMenuHolder.frame.size.height) / 100)))
            }
            NSAnimationContext.runAnimationGroup({(_ context: NSAnimationContext) -> Void in
                context.duration = 0.5
                viewCurveMenuHolder.animator().alphaValue = CGFloat(show)
            }, completionHandler: {() -> Void in
                self.viewCurveMenuHolder.alphaValue = CGFloat(show)
            })
            viewPosture.ToggleShowProgressCircleFlag(show == 1 ? false : true)
        }
    }
}

extension HomeViewController : DelegateDeviceHandler {
    func UpdateAlertText(_ text: String) {
        if MinderManager.sharedInstance.isAppInBackground
        {
            return
        }
        if !isSwitchingView {
            /*if text == "" {
             viewPosture.showTriangle = true
             viewAnnulus.hidden = false
             } else {
             viewPosture.showTriangle = false
             viewAnnulus.hidden = true
             }*/
            if text != "" {
                viewBall.frame.origin = NSPoint(x: -10000, y: -10000)
                sceneBall.frame.origin = NSPoint(x: -10000, y: -10000) // fbn
                if MinderManager.sharedInstance.meditationModeFlag == true {
                    self.meditationViewPosture.isHidden = true
                    self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
                } else {
                    self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 1/255, green: 204/255, blue: 1/255, alpha: 1).cgColor
                }
                if MinderManager.sharedInstance.appMode == .gadget
                {
                    hideUnhideWidgetElements(hiddenStatusTimer: true, hiddenStatusShoulder: true, hiddenStatusHR: true)
                }
            }
            else
            {
                if MinderManager.sharedInstance.appMode == .gadget && sceneBall.frame.origin == NSPoint(x: -10000, y: -10000)
                {
                    let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                    self.hideUnhideWidgetElements(hiddenStatusTimer: opp.isWidgetTimerHidden, hiddenStatusShoulder: opp.isWidgetShoulderHidden, hiddenStatusHR: opp.isWidgetHRHidden)
                }
            }
            viewPosture.UpdateAlertText(text)
            
        }
    }
    //Status Updation
    func UpdateHeartRate(_ heartRate: Float) {
        
        
       // print("HRMonitorType: \(HRMonitorType(rawValue: (MinderManager.sharedInstance.settings?.hrMonitorSelected)!))")
        if let type = HRMonitorType(rawValue: (MinderManager.sharedInstance.settings?.hrMonitorSelected)!) {
            switch type {
            case .Off:
                if MinderManager.sharedInstance.isHrSensorEnable
                {
                    MinderManager.sharedInstance.DeviceHeartRateDisable()
                    updateHrWhenSensorOff()
                }
                lastMonitorSwitch = false
                lastMonitorTime = nil
                
                break;
            case .AutoOn:
                if(lastMonitorTime != nil) {
                    let elapsed = Int(Date().timeIntervalSince(lastMonitorTime!))
                    if(lastMonitorSwitch && elapsed <= 25) {
                       // print ("Elspsed time 1111111   \(elapsed)")
                        if !MinderManager.sharedInstance.isHrSensorEnable
                        {
                            MinderManager.sharedInstance.DeviceHeartRateEnable()
                        }
                        saveHeartRate(heartRate)
                        self.heartRateWidgetLabel.stringValue = "\(Int(heartRate))"
                        self.heartRateTextField.stringValue = " Heart Rate :\(heartRate)"
                        
                        
                    } else if(lastMonitorSwitch && elapsed > 25 && elapsed < 300) {
                        // print ("Elspsed time 222222  \(elapsed)")
                        if MinderManager.sharedInstance.isHrSensorEnable
                        {
                            MinderManager.sharedInstance.DeviceHeartRateDisable()
                            updateHrWhenSensorOff()
                        }
                        lastMonitorSwitch = false
                        lastMonitorTime = Date()
                        
                    } else if(!lastMonitorSwitch && elapsed > 300) {
                        // print ("Elspsed time 33333333  \(elapsed)")
                        lastMonitorSwitch = true
                        lastMonitorTime = Date()
                    }
                    setUpAndDownArrow(heartRate: Int(heartRate))
                } else {
                    
                    lastMonitorSwitch = true
                    lastMonitorTime = Date()
                    
                }
                break;
            case .Live:
                
                if !MinderManager.sharedInstance.isHrSensorEnable
                {
                    MinderManager.sharedInstance.DeviceHeartRateEnable()
                }
                saveHeartRate(heartRate)
                self.heartRateWidgetLabel.stringValue = "\(Int(heartRate))"
                self.heartRateTextField.stringValue = " Heart Rate :\(heartRate)"
                
               print("heart rate is \(heartRate)")
               // self.heartRateWidgetLabel.stringValue = "\(heartRate) bpm"
                lastMonitorSwitch = false 
                lastMonitorTime = nil
                setUpAndDownArrow(heartRate: Int(heartRate))
                break;
            }
            
            
        }
        
        
        
       
        
        self.displayCurrentTime()
        resizeTimeAndHrLabel(self.viewPosture.frame.size.width)
        //self.textHeartRateAverage.stringValue = " Average Heart Rate :\((MinderManager.sharedInstance.settings?.hrTotal)! / Float((MinderManager.sharedInstance.settings?.hrCount)!))"
        
    }
    func updateHrWhenSensorOff()
    {
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let week = calendar.component(.weekOfMonth, from: date)
        let day = calendar.component(.day, from: date)
         if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
        MinderManager.sharedInstance.GetHRAverage(String(year), month: String(month), week: String(week), day: String(day), completionHandler: { data -> Void in
            if data["status"]! as? String == "Success" {
                if let result = data["result"] as? Dictionary<String, AnyObject> {
                    if let hravg = result["hr_avg"] as? String {
                        print("hravg \(Int(hravg))")
                        DispatchQueue.main.async(execute: {
                            if let myNumber = NumberFormatter().number(from: hravg) {
                                self.heartRateWidgetLabel.stringValue = "\(myNumber.intValue)"
                                self.heartRateTextField.stringValue = " Heart Rate :\(myNumber.intValue)"
                                // do what you need to do with myInt
                            }
                            
                        })
                        
                    }
                }
            }
        })
        }
    }
    func saveHeartRate(_ heartRate: Float) {
        var epochTime: String = String(Date().timeIntervalSince1970)
        epochTime = epochTime.substring(to: (epochTime.range(of: ".")?.lowerBound)!)
        MinderManager.sharedInstance.settings?.hrTotal = (MinderManager.sharedInstance.settings?.hrTotal)! + heartRate
        MinderManager.sharedInstance.settings?.hrCount = (MinderManager.sharedInstance.settings?.hrCount)! + 1
        MinderManager.sharedInstance.SaveSettings()
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty && heartRate.rounded() != 0.0 {
            MinderManager.sharedInstance.PostUserAttributes("heart_rate", value: "\(heartRate)", timestamp: epochTime, completionHandler:{_ in})
        }
    }
    func UpdateTemparature(_ temp: Double) {
        self.temparatureTextField.stringValue = "Temperature : \(temp)"
    }
    
    func StatusUpdated() {
        if MinderManager.sharedInstance.deviceStatus == .connected {
            viewPosture.hideCircleBall = false
            DispatchQueue.main.async(execute: {
                let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                self.hideUnhideWidgetElements(hiddenStatusTimer: opp.isWidgetTimerHidden, hiddenStatusShoulder: opp.isWidgetShoulderHidden, hiddenStatusHR: opp.isWidgetHRHidden)
            })
        }
        else
        {
            viewPosture.hideCircleBall = true
        }
       
        //ankush change
        DispatchQueue.main.async(execute: {
            if MinderManager.sharedInstance.currentView == .settings {
                if let viewControllerCurrent = self.viewControllerCurrent {
                    let vcSettingsVC = (viewControllerCurrent as! SettingUpdatedViewController)
                    vcSettingsVC.RefreshControls()
                }
            }
        })
    }
    
    //ChargeUpdation
    func UpdateCharge(_ charge: Float) {
        //print("updateCharge: \(charge)")
        if charge > 70 {
            imageBattery.image = NSImage(named: "Battery100")
        } else if charge > 40 {
            imageBattery.image = NSImage(named: "Battery70")
        } else if charge > 10 {
            imageBattery.image = NSImage(named: "Battery40")
        } else {
            imageBattery.image = NSImage(named: "Battery10")
        }
        MinderManager.sharedInstance.deviceCharge = charge
        labelBattery.stringValue = String(describing: Int(charge)) + "%"
        
        if charge <= 80 && !MinderManager.sharedInstance.isBatteryLowMessageAvailable
        {
            
            if MinderManager.sharedInstance.appMode == .normal
            {
                MinderManager.sharedInstance.isBatteryLowMessageAvailable = true
                dialogOKCancel(question: "Battery Low!", text: "Please charge your device")
            }
        }
        else if charge > 80{
            MinderManager.sharedInstance.isBatteryLowMessageAvailable = false
        }
        
    }
    func dialogOKCancel(question: String, text: String) -> Bool {
        let alert = NSAlert()
        alert.messageText = question
        alert.informativeText = text
        alert.alertStyle = NSAlertStyle.warning
        alert.addButton(withTitle: "OK")
        return alert.runModal() == NSAlertFirstButtonReturn
    }
    //UpdateGyroWithRoll
    func UpdateGyroWithRoll(_ roll: CGFloat, withPitch pitch: CGFloat, withYaw yaw: CGFloat, withT t: Double) {
        if MinderManager.sharedInstance.isAppInBackground
        {
            return
        }
        
        if MinderManager.sharedInstance.deviceActionMode == .broadcasting && !isSwitchingView && MinderManager.sharedInstance.isGyroOn {
            if  MinderManager.sharedInstance.deviceStatus == .connected //ankush change
            {
                //                viewBall.isHidden = false
                sceneBall.isHidden = false // fbn ankush change
                //viewAnnulus.hidden = false
            }
            else{
                self.viewBall.isHidden = true
                self.sceneBall.isHidden = true
            }
            if MinderManager.sharedInstance.appMode == .gadget && MinderManager.sharedInstance.isCalibrationDone
            {
                MinderManager.sharedInstance.isCalibrationDone = false
                let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                self.hideUnhideWidgetElements(hiddenStatusTimer: opp.isWidgetTimerHidden, hiddenStatusShoulder: opp.isWidgetShoulderHidden, hiddenStatusHR: opp.isWidgetHRHidden)
            }
        }
        if MinderManager.sharedInstance.isGyroFromDevice && !isSwitchingView && MinderManager.sharedInstance.deviceActionMode == .broadcasting {
            if roll != -1 {
                MinderManager.sharedInstance.roll += roll / 10    //    (360 / CGFloat(M_PI_2)) * CGFloat(roll)    //
            }
            if pitch != -1 {
                MinderManager.sharedInstance.pitch += pitch / 10  //    (360 / CGFloat(M_PI_2)) * CGFloat(pitch)
            }
            if yaw != -1 {
                MinderManager.sharedInstance.yaw += yaw / 10  //    (360 / CGFloat(M_PI_2)) * CGFloat(yaw)  //
            }
            //print("yaw : \(MinderManager.sharedInstance.yaw)  pitch :\(MinderManager.sharedInstance.pitch)  roll : \( MinderManager.sharedInstance.roll)")
        }
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.isWidgetShoulderHidden
        {
            self.GyroTheView()
            
        }
        else
        {
            MinderManager.sharedInstance.roll = 1.5    //    (360 / CGFloat(M_PI_2)) * CGFloat(roll)    //
            MinderManager.sharedInstance.pitch = 1.5  //    (360 / CGFloat(M_PI_2)) * CGFloat(pitch)
            MinderManager.sharedInstance.yaw = 1.5
            self.GyroTheView()
            MinderManager.sharedInstance.roll = 0    //    (360 / CGFloat(M_PI_2)) * CGFloat(roll)    //
            MinderManager.sharedInstance.pitch = 0  //    (360 / CGFloat(M_PI_2)) * CGFloat(pitch)
            MinderManager.sharedInstance.yaw = 0
        }
        
    }
    
    //PostureUpdation
    internal func UpdateAccel(_ x: CGFloat, withY y: CGFloat, withZ z: CGFloat, withT t: Double) {
        if MinderManager.sharedInstance.isAppInBackground
        {
            return
        }
        
        //MARK: pragma mark Z
        if(z > previousZ) {
            currentProgressStateZ = .up
        } else if(z < previousZ) {
            currentProgressStateZ = .down
        } else {
            currentProgressStateZ = .horizontal
        }
        
        var ratePercent:CGFloat = 0
        
        //MARK: mark ANIMATION
        //        if(currentProgressStateZ == .up) {
        //            ratePercent = previousMeditationSize - ((CGFloat(abs(z - previousZ)) / 60) * 100)
        //        } else if(currentProgressStateZ == .down) {
        //            ratePercent = previousMeditationSize + ((CGFloat(abs(previousZ - z)) / 60) * 100)
        //        }
        
        if(currentProgressStateZ == .up) {
            ratePercent = previousMeditationSize - ((CGFloat(abs(z - previousZ)) / 16000) * 100)
        } else if(currentProgressStateZ == .down) {
            ratePercent = previousMeditationSize + ((CGFloat(abs(previousZ - z)) / 16000) * 100)
        }
        
        ratePercent = ((ratePercent < 5) || (ratePercent > 100)) ? previousMeditationSize : ratePercent
        /*let newSize: CGFloat = (self.viewPosture.frame.size.width * ratePercent) / 100
         
         self.meditationViewBall.frame = NSMakeRect(MinderManager.sharedInstance.centerDeviceView.x - (newSize/2), MinderManager.sharedInstance.centerDeviceView.y - (newSize/2), newSize, newSize)
         
         MinderUtility.ShapeView(self.meditationViewBall, type: .circle)
         
         self.meditationViewBall.layer?.backgroundColor = NSColor.init(calibratedRed: 44/255, green: 255/255, blue: 255/255, alpha: 1).cgColor*/
        //print("ratePercent is \(ratePercent) ")
        meditationViewPosture.ballRadius = ratePercent / 2
        meditationViewPosture.needsDisplay = true
        
        previousZ = z
        previousProgressStateZ = currentProgressStateZ
        
        previousTimeAccel = currentTimeAccel
        currentTimeAccel += 1
        
        previousMeditationSize = ratePercent
        
    }
    
    //PostureUpdation
    func UpdatePosture(_ x: CGFloat, withY y: CGFloat) {
       // print("x:\(x) y:\(y)")
        if MinderManager.sharedInstance.isAppInBackground
        {
            return
        }
        if gadgetOperation == .none && !isResizeReset {
            if MinderManager.sharedInstance.replayState == .none && !MinderManager.sharedInstance.isRecording {
                //print("isResizeReset false")
                viewBall.frame.origin = NSPoint(x: x - (viewBall.frame.size.width/2), y: y - (viewBall.frame.size.width/2))
                sceneBall.frame.origin = NSPoint(x: x - (sceneBall.frame.size.width/2), y: y - (sceneBall.frame.size.width/2)) // fbn
            } else {
                //print("isResizeReset true")
                viewPosture.UpdateBallCordinates(x , y: y)
                sceneBall.isHidden = true
                viewBall.isHidden = false
                sceneBall.frame.origin = NSPoint(x: -10000, y: -10000)
            }
        } else {
            viewBall.frame.origin = NSPoint(x: -10000, y: -10000)
            sceneBall.frame.origin = NSPoint(x: -10000, y: -10000) //fbn
        }
        
        /*if MinderManager.sharedInstance.currentView == .testnlog {
         if let viewControllerCurrent = viewControllerCurrent {
         (viewControllerCurrent as! LogViewController).updateLog(log: "x: \(x), y: \(y)")
         }
         }*/
        
        //        if(MinderManager.sharedInstance.isRecording)
        //        {
        //            sceneBall.frame.origin = NSPoint(x: -10000, y: -10000)
        //            sceneBall.hidden = true
        //            viewBall.hidden = false
        //        }
        /*if MinderManager.sharedInstance.appMode == .Gadget {
         if (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!.transperancy != 1 {
         self.view.window?.backgroundColor = NSColor.clearColor()
         self.view.layer?.backgroundColor = NSColor.clearColor().CGColor
         self.viewTrackball.layer?.backgroundColor = NSColor.clearColor().CGColor
         MinderUtility.ShapeView(viewAnnulus, type: .Circle)
         MinderUtility.ShapeView(viewPosture, type: .Circle)
         MinderUtility.ShapeView(viewBall, type: .Circle)
         }
         }*/
    }
    func UpdateStepCount() {
        if MinderManager.sharedInstance.isAppInBackground
        {
            return
        }
        if MinderManager.sharedInstance.currentView == .dashboard {
            if let viewControllerCurrent = viewControllerCurrent {
                (viewControllerCurrent as! DashboardViewController).setStepCount()
            }
        }
    }
    func EnableSeatback() {
        if MinderManager.sharedInstance.isAppInBackground
        {
            return
        }
        MinderManager.sharedInstance.isSeatbackEnabled = true
        viewPosture.SetSeatback(1)
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        viewPosture.UpdateDifficulty(.seatback, difficulty: opp.difficultySeatback)
        opp.isSeatBack = 1 //ankush change
        UpdateAlertText("Supported Posture Enabled")
        let time: DispatchTime = DispatchTime.now() + Double(Int64(NSEC_PER_SEC * 3)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time, execute:  {
            self.UpdateAlertText("")
            self.ResetDrawByResize()
            })
        DispatchQueue.main.async(execute: {
            if MinderManager.sharedInstance.currentView == .settings {
                if let viewControllerCurrent = self.viewControllerCurrent {
                    let vcSettingsVC = (viewControllerCurrent as! SettingUpdatedViewController)//ankush change
                    vcSettingsVC.RefreshControls()
                }
            }
        })
        ResetDrawByResize()
    }
    //ColorUpdation
    func UpdateColor(_ type: ZoneType) {
        if MinderManager.sharedInstance.isAppInBackground
        {
            return
        }
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if gadgetOperation != .resize {
            switch type {
            case .green:
                if badPostureTimer != nil
                {
                    badPostureTimer!.invalidate()
                    badPostureTimer = nil
                    isInBadPosture = false
                }
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 1/255, green: 255/255, blue: 1/255, alpha: CGFloat(opp.transperancy)).cgColor
            case .yellow:
                
                if badPostureTimer == nil && !isInBadPosture
                {
//                    print("timer started")
//                    badPostureTimer =  Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(badPostureTimerCalled), userInfo: nil, repeats: false)
//                    
                }
                let blueIntensity = opp.yellowIntensity/225
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 1/255, blue: 1/255, alpha: CGFloat(opp.transperancy)).cgColor
            case .red:
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 1/255, blue: 1/255, alpha: CGFloat(opp.transperancy)).cgColor
            case .seatback:
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 1/255, green: 204/255, blue: 1/255, alpha: CGFloat(opp.transperancy)).cgColor
            //self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 1/255, green: 204/255, blue: 204/255, alpha: CGFloat(opp.transperancy)).CGColor
            default: break
            }
        }
        prevZoneType = type
    }
    //GraphUpdateswithData
    func UpdateDataGraphs() {
        if MinderManager.sharedInstance.isAppInBackground
        {
            return
        }
        if MinderManager.sharedInstance.currentView == .dashboard && !MinderManager.sharedInstance.showPreviousData {
            if let viewControllerCurrent = viewControllerCurrent {
                let vcDashboardVC = (viewControllerCurrent as! DashboardViewController)
                
                let manager = MinderManager.sharedInstance
                vcDashboardVC.viewGraphBar.positionDataList = manager.pDataArray
                
                if manager.imageHeatmap != nil {
                    vcDashboardVC.viewGraphHeat.imageHeatmap = MinderUtility.ResizeImage(manager.imageHeatmap!, destSize: vcDashboardVC.viewGraphHeat.frame.size)
                }
                
                vcDashboardVC.viewGraphCompleted.positionDataList = manager.pDataArray
                
                if manager.green > 0 || manager.yellow > 0 || manager.red > 0 {
                    let gyrTotal = CGFloat(manager.green + manager.yellow + manager.red)
                    var greenPercent: CGFloat = 0
                    if manager.green > 0 {
                        greenPercent = round(((CGFloat(manager.green) / gyrTotal) * 100) * 10) / 10
                    }
                    var yellowPercent: CGFloat = 0
                    if manager.yellow > 0 {
                        yellowPercent = round(((CGFloat(manager.yellow) / gyrTotal) * 100) * 10) / 10
                    }
                    var redPercent: CGFloat = 0
                    if manager.red > 0 {
                        redPercent = round(((CGFloat(manager.red) / gyrTotal) * 100) * 10) / 10
                    }
                    
                    vcDashboardVC.labelGreenPosition.stringValue = "Green: \(MinderUtility.SecondsToHMSString(manager.green)) (\(greenPercent)%)"
                    vcDashboardVC.labelYellowPosition.stringValue = "Yellow: \(MinderUtility.SecondsToHMSString(manager.yellow)) (\(yellowPercent)%)"
                    vcDashboardVC.labelRedPosition.stringValue = "Red: \(MinderUtility.SecondsToHMSString(manager.red)) (\(redPercent)%)"
                    
                    vcDashboardVC.viewGraphPie.zoneYellowValue = yellowPercent
                    vcDashboardVC.viewGraphPie.zoneRedValue = redPercent
                    
                    vcDashboardVC.refreshGraphs()
                    viewPosture.UpdateZonePercentage(greenPercent, yellow: yellowPercent, red: redPercent)
                }
            }
        }
    }
    //Date&Time Updation
    func UpdateUpdateTime(_ elapsed: Int) {
        if MinderManager.sharedInstance.isAppInBackground
        {
            return
        }
        if MinderManager.sharedInstance.currentView == .dashboard {
            let remaining: Int = 60 - (elapsed / 1000)
            if remaining == 0 {
                if let viewControllerCurrent = viewControllerCurrent {
                    (viewControllerCurrent as! DashboardViewController).labelRemaingIndicator.stringValue = "Update in 1 minute"
                }
            } else {
                if let viewControllerCurrent = viewControllerCurrent {
                    (viewControllerCurrent as! DashboardViewController).labelRemaingIndicator.stringValue = "Update in \(remaining) seconds"
                }
            }
            UpdateDataGraphs()
        }
    }
}

extension HomeViewController : DelegateCurveMenu {
    func menuButtonTapped(_ type: ViewType) {
        DispatchQueue.global(qos: .userInitiated).async(execute: {
            DispatchQueue.main.async(execute: {
                self.curveMenuClick(abs(Int(self.viewCurveMenuHolder.alphaValue) - 1))
                self.showViewByType(type)
            });
        });
    }
    func showViewByType(_ type: ViewType) {
        self.disableDeviceViewClick = true
        MinderManager.sharedInstance.appMode = .normal
        self.setupWindow()
        self.mainButtonTap(type)
    }
    
    func UpdateProcessingPaused() {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if MinderManager.sharedInstance.dataProcessingPaused {
            MinderManager.sharedInstance.dataProcessingPaused = false
            buttonMenuProcessPause.image = NSImage(named: "CMPosturePlay")
            viewPosture.UpdateAlertText("")
            ResetDrawByResize()
            buttonMenuProcessPause.toolTip = "Posture playing"
        } else {
            MinderManager.sharedInstance.dataProcessingPaused = true
            buttonMenuProcessPause.image = NSImage(named: "CMPosturePause")
            UpdateColor(.green)
            viewPosture.UpdateAlertText("Posture is paused")
            buttonMenuProcessPause.toolTip = "Posture paused"
        }
        if MinderManager.sharedInstance.currentView == .settings {
            if let viewControllerCurrent = self.viewControllerCurrent {
                let vcSettingsVC = (viewControllerCurrent as! SettingUpdatedViewController)//ankush change
                vcSettingsVC.RefreshControls()
            }
        }
    }
}
extension HomeViewController : DelegateSettings {
    
    func ShowHideWidgetEntity(entityName : String, isHiddenStatus : Bool) {
        if MinderManager.sharedInstance.appMode == .gadget {
            switch entityName
            {
            case "Timer" :
                self.currentTimeWidgetLabel.isHidden = isHiddenStatus
                break
            case "Shoulder" :
                self.shoulderRegionImage.isHidden = isHiddenStatus
                break
            case "HR" :
                self.heartRateWidgetLabel.isHidden = isHiddenStatus
                self.heartWidget.isHidden = isHiddenStatus
                self.heartRateUnitWidgetLabel.isHidden = isHiddenStatus
                self.hrArrowUp.isHidden = isHiddenStatus
                self.hrArrowDown.isHidden = isHiddenStatus
                break
            default :
                
                break
                
                
            }
        }
        
    }
    func ToggleGyroRotation() {
        OnOffGyroControl()
    }
    //OPP Changing
    func OPPChanged() {
        if MinderManager.sharedInstance.settings?.OPPSelected != 0 {
            self.labelTrackingBall.stringValue = "\((MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!].title)!) Posture"
        } else {
            labelTrackingBall.stringValue = "Posture"
        }
        CalibrateTapped(self)
    }
    //ConnectDevice
    func ConnectDevice() {
        DispatchQueue.main.async(execute: {
            MinderManager.sharedInstance.DeviceConnect()
        })
    }
    //UpdateSetSeatbackValue
    func SetSeatback(_ value: Int) {
        MinderManager.sharedInstance.isSeatbackEnabled = value == 0 ? false : true
        print("isSeatbackEnabled \(MinderManager.sharedInstance.isSeatbackEnabled)")
        viewPosture.SetSeatback(value)
    }
    //UpdateDifficultySettings
    func UpdateDifficulty(_ type: ZoneType, value: Float) {
        viewPosture.UpdateDifficulty(type, difficulty: value)
        if type == .yellow {
            // Finding ball space and radius for calculation
            let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
            
            let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
            // Finding yellow radius. Yellow zone is MinderManager.percentageZoneYellow percent of height but start after ball space and an offset
            let percentYellow = (CGFloat(value) * MinderManager.percentageZoneYellow) / 100
            let radiusYellow = ((viewPosture.frame.size.height * percentYellow) / 100) + radiusBall + margin
            
//            viewBall.frame.size = NSMakeSize(radiusBall * 2.0, radiusBall * 2.0)
//            sceneBall.frame.size = NSMakeSize(radiusBall * 2.5, radiusBall * 2.5) //fbn
            
            let ballWidth = 40 + (20 * (viewPosture.frame.size.height/160) - 20)
            
            viewBall.frame.size = NSMakeSize(ballWidth, ballWidth)
            sceneBall.frame.size = NSMakeSize(ballWidth, ballWidth) // fbn
            
            //viewAnnulus.frame.origin = NSMakePoint((viewPosture.frame.size.height/2) - (viewAnnulus.frame.size.height/2), (viewPosture.frame.size.height/2) - (viewAnnulus.frame.size.height/2))
            MinderUtility.ShapeView(viewAnnulus, type: .circle)
            MinderUtility.ShapeView(viewBall, type: .circle)
        }
    }
    //UpdateNotification
    func UpdateNotification() {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        let styleParagraph = NSMutableParagraphStyle()
        styleParagraph.alignment = .center
        let attributesButton: [String : AnyObject] = [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : styleParagraph ]
        DispatchQueue.main.async(execute: {
            
            //ankush change as notification button removed from home
            //            if opp.volumeYellowMute {
            //                if opp.vibrateYellow == 0 {
            //                    self.buttonNotification.attributedTitle = NSAttributedString(string: "MUTE", attributes: attributesButton)
            //                    self.buttonNotification.image = NSImage(named: "VolumeMute")
            //                    self.buttonMenuNotification.image = NSImage(named: "CMVolumeMute")
            //                } else {
            //                    self.buttonNotification.attributedTitle = NSAttributedString(string: "VIBRATE", attributes: attributesButton)
            //                    self.buttonNotification.image = NSImage(named: "Vibrate")
            //                    self.buttonMenuNotification.image = NSImage(named: "CMVibrate")
            //                }
            //            } else {
            //                self.buttonNotification.attributedTitle = NSAttributedString(string: "RING", attributes: attributesButton)
            //                if opp.volumeYellow > 0.65 {
            //                    self.buttonNotification.image = NSImage(named: "VolumeMax")
            //                    self.buttonMenuNotification.image = NSImage(named: "CMVolumeMax")
            //                } else if opp.volumeYellow > 0.35 {
            //                    self.buttonNotification.image = NSImage(named: "VolumeMid")
            //                    self.buttonMenuNotification.image = NSImage(named: "CMVolumeMid")
            //                } else if opp.volumeYellow > 0 {
            //                    self.buttonNotification.image = NSImage(named: "VolumeMin")
            //                    self.buttonMenuNotification.image = NSImage(named: "CMVolumeMin")
            //                } else {
            //                    if opp.vibrateYellow == 0 {
            //                        self.buttonNotification.attributedTitle = NSAttributedString(string: "MUTE", attributes: attributesButton)
            //                        self.buttonNotification.image = NSImage(named: "VolumeMute")
            //                        self.buttonMenuNotification.image = NSImage(named: "CMVolumeMute")
            //                    } else {
            //                        self.buttonNotification.attributedTitle = NSAttributedString(string: "VIBRATE", attributes: attributesButton)
            //                        self.buttonNotification.image = NSImage(named: "Vibrate")
            //                        self.buttonMenuNotification.image = NSImage(named: "CMVibrate")
            //                    }
            //                }
            //            }
            if MinderManager.sharedInstance.dataProcessingPaused {
                self.buttonMenuProcessPause.image = NSImage(named: "CMPosturePause")
                self.buttonMenuProcessPause.toolTip = "Posture paused"
            } else {
                self.buttonMenuProcessPause.image = NSImage(named: "CMPosturePlay")
                self.buttonMenuProcessPause.toolTip = "Posture playing"
            }
            if MinderManager.sharedInstance.currentView == .settings {
                if let viewControllerCurrent = self.viewControllerCurrent {
                    let vcSettingsVC = (viewControllerCurrent as! SettingUpdatedViewController)//ankush change
                    vcSettingsVC.RefreshControls()
                }
            }
        })
    }
    func OPPClicked(_ index: Int) {
        
    }
    //ankush addition
    @IBAction func EscForGadgetClicked(_ sender: AnyObject) {
        if MinderManager.sharedInstance.appMode == .gadget {
            viewPosture.needsDisplay = true
            if(abs(Int(viewCurveMenuHolder.alphaValue) - 1) == 0)
            {
                curveMenuClick(0)
            }
            
        }
    }
    func UpdateTransperency() {
        let oppSelected = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        self.view.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 0).cgColor
        //   viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 46/255, green: 204/255, blue: 114/255, alpha: CGFloat(oppSelected.transperancy)).cgColor
        print("oppSelected.transperancy \(oppSelected.transperancy)")
        viewPosture.layer?.backgroundColor = viewPosture.layer?.backgroundColor?.copy(alpha: CGFloat(oppSelected.transperancy))
    }
    func applicationIsInStartUpItems() -> Bool {
        return (itemReferencesInLoginItems().existingReference != nil)
    }
    func itemReferencesInLoginItems() -> (existingReference: LSSharedFileListItem?, lastReference: LSSharedFileListItem?) {
        var itemUrl : UnsafeMutablePointer<Unmanaged<CFURL>?> = UnsafeMutablePointer<Unmanaged<CFURL>?>.allocate(capacity: 1)
        if let appUrl : NSURL = NSURL.fileURL(withPath: Bundle.main.bundlePath) as NSURL? {
            let loginItemsRef = LSSharedFileListCreate(
                nil,
                kLSSharedFileListSessionLoginItems.takeRetainedValue(),
                nil
                ).takeRetainedValue() as LSSharedFileList?
            if loginItemsRef != nil {
                let loginItems: NSArray = LSSharedFileListCopySnapshot(loginItemsRef, nil).takeRetainedValue() as NSArray
                print("There are \(loginItems.count) login items")
                if(loginItems.count > 0)
                {
                    let lastItemRef: LSSharedFileListItem = loginItems.lastObject as! LSSharedFileListItem
                    for i in 0 ..< loginItems.count  {
                        let currentItemRef: LSSharedFileListItem = loginItems.object(at: i) as! LSSharedFileListItem
                        if LSSharedFileListItemResolve(currentItemRef, 0, itemUrl, nil) == noErr {
                            if let urlRef: NSURL =  itemUrl.pointee?.takeRetainedValue() {
                                print("URL Ref: \(urlRef.lastPathComponent)")
                                if urlRef.isEqual(appUrl) {
                                    return (currentItemRef, lastItemRef)
                                }
                            }
                        }
                        else {
                            print("Unknown login application")
                        }
                    }
                    //The application was not found in the startup list
                    return (nil, lastItemRef)
                }
                else
                {
                    let addatstart: LSSharedFileListItem = kLSSharedFileListItemBeforeFirst.takeRetainedValue()
                    
                    return(nil,addatstart)
                }
            }
        }
        return (nil, nil)
    }
    func toggleLaunchAtStartup() {
        let itemReferences = itemReferencesInLoginItems()
        let shouldBeToggled = (itemReferences.existingReference == nil)
        if let loginItemsRef = LSSharedFileListCreate( nil, kLSSharedFileListSessionLoginItems.takeRetainedValue(), nil).takeRetainedValue() as LSSharedFileList? {
            if shouldBeToggled {
                if let appUrl : CFURL = NSURL.fileURL(withPath: Bundle.main.bundlePath) as CFURL? {
                    LSSharedFileListInsertItemURL(loginItemsRef, itemReferences.lastReference, nil, nil, appUrl, nil, nil)
                }
            } else {
                if let itemRef = itemReferences.existingReference {
                    LSSharedFileListItemRemove(loginItemsRef,itemRef);
                }
            }
        }
    }
    
    
    //    @IBAction func killAppTapped(_ sender: AnyObject) {
    //        if let appBundle = Bundle.main.bundleIdentifier
    //        {
    //            UserDefaults.standard.removePersistentDomain(forName: appBundle)
    //            NSApplication.shared().terminate(self)
    //        }
    //    }
}

extension HomeViewController {
    func setUpSceneView() {
        let scene = SCNScene()
        let box: SCNSphere? = SCNSphere(radius: 30)
        boxNode = SCNNode(geometry: box)
        
        let aLight = SCNLight()
        aLight.type = SCNLight.LightType.directional
        let aLightNode = SCNNode()
        aLightNode.light = aLight
        aLightNode.position = SCNVector3Make(0, 60, 50)
        scene.rootNode.addChildNode(aLightNode)
        
        /*let bLightNode = SCNNode()
         bLightNode.light = aLight
         bLightNode.position = SCNVector3Make(0, -60, 100)
         scene.rootNode.addChildNode(bLightNode)
         
         let cLightNode = SCNNode()
         cLightNode.light = aLight
         cLightNode.position = SCNVector3Make(60, 0, 100)
         scene.rootNode.addChildNode(cLightNode)
         
         let dLightNode = SCNNode()
         dLightNode.light = aLight
         dLightNode.position = SCNVector3Make(-60, 0, 100)
         scene.rootNode.addChildNode(dLightNode)*/
        
        let geoScene = SCNScene(named: "minderBall.dae")
        boxNode = geoScene!.rootNode.childNode(withName: "Cube", recursively: true)!
        scene.rootNode.addChildNode(boxNode!)
        sceneBall.allowsCameraControl = false
        sceneBall.scene = scene
        //sceneBall.autoenablesDefaultLighting = true
    }
    
    func setUpWindowTransperency(){
        switch MinderManager.sharedInstance.appMode {
        case .gadget:
            self.view.layer?.isOpaque = true
            self.view.layer?.opacity = 1.0
            break
        case .normal:
            //self.view.layer?.opaque = false
            //self.view.layer?.opacity = 0.5
            // Shaping n Coloring
            
            let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
            self.view.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 0).cgColor
            viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 46/255, green: 204/255, blue: 114/255, alpha: CGFloat(opp.transperancy)).cgColor
            
            ///
            topOpaqueView.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1.0).cgColor
            leftOpaqueView.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1.0).cgColor
            rightOpaqueView.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1.0).cgColor
            bottomOpaqueView.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1.0).cgColor
            ///
            break
        }
    }
    
    
    func setUpMeditationView(_ mode:AppMode,frame:NSRect) {
        let meditationFrame:NSRect = mode == .normal ? NSMakeRect(100, 0, 200, 200) : frame
        meditationViewPosture.frame = meditationFrame
        MinderUtility.ShapeView(meditationViewPosture, type: .circle)
        meditationViewPosture.isHidden = false
        meditationViewPosture.layer?.backgroundColor = NSColor.white.cgColor
        meditationViewPosture.needsDisplay = true
        
        /*let radiusBall = mode == .normal ? CGFloat(40.0) : CGFloat(frame.size.width/2 * 0.5)
         meditationViewBall.frame.size = NSMakeSize(radiusBall * 2, radiusBall * 2)
         MinderUtility.ShapeView(meditationViewBall, type: .circle)
         meditationViewBall.isHidden = false
         //
         if mode == .normal {
         meditationViewBall.frame.origin = NSMakePoint(MinderManager.sharedInstance.centerDeviceView.x - radiusBall, MinderManager.sharedInstance.centerDeviceView.y - radiusBall)
         } else{
         meditationViewBall.frame.origin = NSMakePoint(frame.size.width/2 - radiusBall , frame.size.width/2 - radiusBall)
         }
         meditationViewBall.layer?.backgroundColor = NSColor.init(calibratedRed: 44/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
         meditationViewBall.needsDisplay = true
         
         animationFrame = meditationViewBall.frame*/
        
        MinderManager.sharedInstance.meditationModeFlag = true
    }
    
    func hideMeditationView() {
        meditationViewPosture.isHidden = true
        meditationViewBall.isHidden = true
        MinderManager.sharedInstance.meditationModeFlag = false
    }
    
    func processingRecordName(fromPlayer: Bool) {
        errorPopUpLabel.isHidden = true
        var userTypedNamed = removeSpecialCharsFromString(text: recordingNameTextField.stringValue.trimmingCharacters(in: .whitespaces))
        if userTypedNamed == ""
        {
            errorPopUpLabel.stringValue = "*File name can not be empty."
            errorPopUpLabel.isHidden = false
            return
        }
        if currentRecordingUserSetName != userTypedNamed {
            if MinderManager.sharedInstance.checkRecordingNameExistance(userTypedName: userTypedNamed) {
                // name already exists
                errorPopUpLabel.stringValue = "*File already exist, Please enter another name."
                errorPopUpLabel.isHidden = false
                return
            }else {
                MinderManager.sharedInstance.SaveRecordingToDB(systemSetName: currentRecordingFileName, userSetName: userTypedNamed, editFlag: true)
            }
        }
        recordNamePopUpView.isHidden = true
        recordNameInnerPopUpView.isHidden = true
        recordNamePopUpView.layer?.backgroundColor = CGColor.init(gray: 0, alpha: 0)
        if let scheduleVC = self.viewControllerCurrent as? ScheduleViewController {
            if scheduleVC.currentViewType == .record {
                scheduleVC.calendarView.reloadData()
            }
            if fromPlayer{
                scheduleVC.labelTitle.stringValue = userTypedNamed
            }
        }
    }
}
class TopCurvedOpaqueView: NSView {
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 0.0).setFill()
        let frame: CGRect = bounds
        let originX: CGFloat = frame.origin.x
        let originY: CGFloat = frame.origin.y
        let Width: CGFloat = frame.size.width
        let Height: CGFloat = frame.size.height
        let bPath = NSBezierPath()
        
        bPath.move(to: CGPoint(x: originX, y: originY))
        bPath.line(to: NSPoint(x: originX , y: originY + Height))
        bPath.line(to: NSPoint(x: originX + Width, y: originY + Height))
        bPath.line(to: NSPoint(x: originX + Width, y: originY))
        bPath.appendArc(withCenter: NSPoint(x: originX + (Width/2), y: originY), radius: Height, startAngle: CGFloat(0), endAngle: -CGFloat(M_PI), clockwise: false)
        
        bPath.close()
        NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1.0).setFill()
        bPath.fill()
    }
    
}

class BottomCurvedOpaqueView: NSView {
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 0.0).setFill()
        let frame: CGRect = bounds
        let originX: CGFloat = frame.origin.x
        let originY: CGFloat = frame.origin.y
        let Width: CGFloat = frame.size.width
        let Height: CGFloat = frame.size.height
        let bPath = NSBezierPath()
        
        bPath.move(to: CGPoint(x: originX, y: originY))
        bPath.line(to: NSPoint(x: originX , y: originY + Height))
        bPath.appendArc(withCenter: NSPoint(x: originX + (Width/2), y: originY + Height), radius: Height, startAngle: CGFloat(0), endAngle: -CGFloat(M_PI), clockwise: false)
        bPath.line(to: NSPoint(x: originX + Width, y: originY))
        bPath.line(to: NSPoint(x: originX, y: originY))
        
        
        bPath.close()
        NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1.0).setFill()
        bPath.fill()
    }
    
}
class VerticallyCenteredTextFieldCell: NSTextFieldCell {
    override func titleRect(forBounds rect: NSRect) -> NSRect {
        var titleRect = super.titleRect(forBounds: rect)
        
        let minimumHeight = self.cellSize(forBounds: rect).height
        titleRect.origin.y += (titleRect.height - minimumHeight) / 2
        titleRect.size.height = minimumHeight
        
        return titleRect
    }
    
    override func drawInterior(withFrame cellFrame: NSRect, in controlView: NSView) {
        super.drawInterior(withFrame: titleRect(forBounds: cellFrame), in: controlView)
    }
    
    //    override func selectWithFrame(aRect: NSRect, inView controlView: NSView, editor textObj: NSText, delegate anObject: AnyObject?, start selStart: Int, length selLength: Int) {
    //        super.selectWithFrame(titleRectForBounds(rect: aRect), inView: controlView, editor: textObj, delegate: anObject, start: selStart, length: selLength);
    //    }
}
extension HomeViewController : NSTextFieldDelegate {
    func control(_ control: NSControl, textShouldBeginEditing fieldEditor: NSText) -> Bool {
        return true
    }
    
    override func controlTextDidChange(_ obj: Notification) {
        if let _ = obj.object as? NSTextField {
            self.errorPopUpLabel.isHidden = true
        }
    }
    
}
