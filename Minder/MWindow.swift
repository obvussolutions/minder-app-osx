//
//  MWindow.swift
//  Minder
//
//  Created by Abdul on 9/6/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class MWindow: NSWindow {
    // Purpose of the custom NSWindow class with this override is to make NSTextField on inner viewcontrollers editable
    override var canBecomeKey: Bool {
        return true
    }
}
