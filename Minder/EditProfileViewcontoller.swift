//
//  EditProfileViewcontoller.swift
//  Minder
//
//  Created by Jithin AG on 8/30/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol DelegateProfileEdit : class {
    func ProfileEdited()
}

class EditProfileViewcontoller: NSViewController {
    
    @IBOutlet var basicInfoView: NSView!
    
    @IBOutlet weak var progressEditprofile: NSProgressIndicator!
    @IBOutlet var textFirstName: NSTextField!
    @IBOutlet var textLastName: NSTextField!
    @IBOutlet var textGender: NSTextField!
    @IBOutlet weak var popButtonGender: NSPopUpButton!
    @IBOutlet var textAge: NSTextField!
    @IBOutlet var textPhone: NSTextField!
    @IBOutlet var textAddress: NSTextField!
    @IBOutlet var textHeight: NSTextField!
    @IBOutlet var textWeight: NSTextField!
    @IBOutlet var textBuild: NSTextField!
    
    @IBOutlet var textUserName: NSTextField!
    @IBOutlet var textEmail: NSTextField!
    
    @IBOutlet var buttonChangePhoto: NSButton!
    @IBOutlet weak var imageViewProfile: NSImageView!
    @IBOutlet weak var buttonEditSave: NSButton!
    @IBOutlet weak var textErrorMessage: NSTextField!
    
    weak var delegateProfileEdit: DelegateProfileEdit?
    var isValidData: String = "11111111"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RefreshControls()
    }
    // MARK: refreshing the view
    func RefreshControls() {
        let appManager = MinderManager.sharedInstance
        if !(appManager.userLogged?.userName ?? "").isEmpty {
            textFirstName.stringValue = (appManager.userLogged?.firstName)!
            textFirstName.delegate = self
            textLastName.stringValue = (appManager.userLogged?.lastName)!
            textLastName.delegate = self
            textGender.stringValue = (appManager.userLogged?.gender)!
            popButtonGender.selectItem(withTitle: (appManager.userLogged?.gender)!)
            textAge.stringValue = (appManager.userLogged?.age)!
            textAge.delegate = self
            textPhone.stringValue = (appManager.userLogged?.phone)!
            textPhone.delegate = self
            textAddress.stringValue = (appManager.userLogged?.address)!
            textAddress.delegate = self
            textHeight.stringValue = (appManager.userLogged?.height)!
            textHeight.delegate = self
            textWeight.stringValue = (appManager.userLogged?.weight)!
            textWeight.delegate = self
            textBuild.stringValue = (appManager.userLogged?.build)!
            textBuild.delegate = self
            
            textUserName.stringValue = (appManager.userLogged?.userName)!
            textUserName.isEditable = false
            textEmail.stringValue = (appManager.userLogged?.email)!
            textEmail.isEditable = false
            
            imageViewProfile.image = appManager.userLogged?.image
        }
        toggleTextEdit(false)
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        self.view.layer?.backgroundColor = NSColor.init(calibratedRed: 72/255, green: 72/255, blue: 72/255, alpha: 0.8).cgColor
        basicInfoView.layer?.backgroundColor = NSColor.white.cgColor
    }
    
    func toggleTextEdit(_ toggle: Bool) {
        textFirstName.isEditable = toggle
        textLastName.isEditable = toggle
        if toggle {
            textGender.isHidden = true
            popButtonGender.isHidden = false
        } else {
            textGender.stringValue = (popButtonGender.selectedItem?.title)!
            textGender.isHidden = false
            popButtonGender.isHidden = true
        }
        textGender.isEditable = toggle
        textAge.isEditable = toggle
        textPhone.isEditable = toggle
        textAddress.isEditable = toggle
        textHeight.isEditable = toggle
        textWeight.isEditable = toggle
        textBuild.isEditable = toggle
        buttonChangePhoto.isHidden = !toggle
    }
    
    var isEdit: Bool = false
    @IBAction func cancelTapped(_ sender: AnyObject) {
        if isEdit {
            textErrorMessage.isHidden = true
            toggleTextEdit(false)
            isEdit = false
            buttonEditSave.title = "Edit"
        } else {
            textErrorMessage.isHidden = true
            self.view.removeFromSuperview()
        }
    }
    
    //Edited details saving
    @IBAction func editSaveTapped(_ sender: AnyObject) {
        if isEdit {
            if isValidData == "11111111" {
                progressEditprofile.isHidden = false
                progressEditprofile.startAnimation(nil)
                textErrorMessage.isHidden = true
                buttonChangePhoto.isHidden = true
                
                let manager = MinderManager.sharedInstance
                manager.EditProfile(textFirstName.stringValue, lname: textLastName.stringValue, gender: (popButtonGender.selectedItem?.title)!, age: textAge.stringValue, phone: textPhone.stringValue, address: textAddress.stringValue, height: textHeight.stringValue, weight: textWeight.stringValue, build: textBuild.stringValue, image: imageViewProfile.image!, completionHandler: { data -> Void in
                    if data {
                        DispatchQueue.main.async(execute: {
                            self.toggleTextEdit(false)
                            self.isEdit = false
                            self.buttonEditSave.title = "Edit"
                            
                            self.delegateProfileEdit?.ProfileEdited()
                            
                            let myPopup: NSAlert = NSAlert()
                            myPopup.messageText = "Profile updated"
                            myPopup.informativeText = "Your profile has been updated successfully."
                            myPopup.alertStyle = .informational
                            myPopup.addButton(withTitle: "OK")
                            myPopup.runModal()
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.textErrorMessage.isHidden = false
                        })
                    }
                    self.progressEditprofile.isHidden = true
                    self.progressEditprofile.stopAnimation(nil)
                })
            }
        } else {
            toggleTextEdit(true)
            isEdit = true
            buttonEditSave.title = "Save"
        }
    }
    //Profile Photo changing
    @IBAction func ChangePhotoTapped(_ sender: NSButton)
    {
        let openPanel = NSOpenPanel()
        openPanel.allowsMultipleSelection = false
        openPanel.canChooseDirectories = false
        openPanel.canCreateDirectories = false
        openPanel.canChooseFiles = true
        openPanel.level = Int(CGShieldingWindowLevel())
        openPanel.allowedFileTypes = ["png", "jpg", "jpeg"]
        openPanel.begin { (result) -> Void in
            if result == NSFileHandlingPanelOKButton
            {
                self.imageViewProfile.image = NSImage(contentsOf: openPanel.url!)
            }
        }
    }
    
    @IBAction func closePopUp(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
}
//Textfiled Value
extension EditProfileViewcontoller : NSTextFieldDelegate {
    override func controlTextDidChange (_ notification: Notification) {
        guard let textField = notification.object as? NSTextField else { return }
        if textField == textFirstName {
           
        } else if textField == textLastName {
            
        } else if textField == textAge {
            if String.isNumerical(textField.stringValue) {
                if Int(textField.stringValue) > 0 {
                    
                    isValidData = String.replace(isValidData, 2, "1")
                    textField.backgroundColor = NSColor.clear
                } else {
                    isValidData = String.replace(isValidData, 2, "0")
                    textField.backgroundColor = NSColor.red
                }
            } else {
                isValidData = String.replace(isValidData, 2, "0")
                textField.backgroundColor = NSColor.red
            }
        } else if textField == textPhone {
            if String.isValidPhone(textField.stringValue) {
                
                isValidData = String.replace(isValidData, 5, "1")
                textField.backgroundColor = NSColor.clear
            } else {
                isValidData = String.replace(isValidData, 5, "0")
                textField.backgroundColor = NSColor.red
            }
        } else if textField == textHeight {
            if String.isNumerical(textField.stringValue) {
                
                isValidData = String.replace(isValidData, 3, "1")
                textField.backgroundColor = NSColor.clear
            } else {
                isValidData = String.replace(isValidData, 3, "0")
                textField.backgroundColor = NSColor.red
            }
        } else if textField == textWeight {
            if String.isNumerical(textField.stringValue) {
                
                isValidData = String.replace(isValidData, 4, "1")
                textField.backgroundColor = NSColor.clear
            } else {
                isValidData = String.replace(isValidData, 4, "0")
                textField.backgroundColor = NSColor.red
            }
        } else if textField == textBuild {
            if String.isNumerical(textField.stringValue) {
                
                isValidData = String.replace(isValidData, 4, "1")
                textField.backgroundColor = NSColor.clear
            } else {
                isValidData = String.replace(isValidData, 4, "0")
                textField.backgroundColor = NSColor.red
            }
        }
    }
}
