//
//  CurveMenuViewController.swift
//  Minder
//
//  Created by Abdul on 8/29/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

protocol DelegateCurveMenu {
    func menuButtonTapped(_ type: ViewType)
    func UpdateNotification()
    func UpdateProcessingPaused()
}

class CurveMenuViewController: NSViewController {
    
    var idelegate: DelegateCurveMenu?
    @IBOutlet weak var buttonNotification: NSButton!
    @IBOutlet weak var buttonProcessPause: NSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //DashboardButtonAction
    @IBAction func dashboardTapped(_ sender: AnyObject) {
        idelegate?.menuButtonTapped(.dashboard)
    }
    //GuidenceButtonAction
    @IBAction func guidanceTapped(_ sender: AnyObject) {
        idelegate?.menuButtonTapped(.guidance)
    }
    //NotificationButtonAction
    @IBAction func notificationTapped(_ sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.volumeYellow == 0 && opp.vibrateYellow == 0 {
            opp.volumeYellow = 1
            opp.vibrateYellow = 1
            opp.volumeRed = 1
            opp.vibrateRed = 1
        } else if opp.volumeYellow > 0 {
            opp.volumeYellow = 0
            opp.vibrateYellow = 1
            opp.volumeRed = 0
            opp.vibrateRed = 1
        } else {
            opp.volumeYellow = 0
            opp.vibrateYellow = 0
            opp.volumeRed = 0
            opp.vibrateRed = 0        }
        MinderManager.sharedInstance.SaveSettings()
        idelegate?.UpdateNotification()
    }
    //PauseButton Action
    @IBAction func ProcessPauseTapped(_ sender: AnyObject) {
        idelegate?.UpdateProcessingPaused()
    }
    //SettingButtonAction
    @IBAction func settingsTapped(_ sender: AnyObject) {
        idelegate?.menuButtonTapped(.settings)
    }
}
