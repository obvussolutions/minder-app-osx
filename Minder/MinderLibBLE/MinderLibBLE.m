
//  MinderLibBLE.m
//  MinderLibBLE
//
//  Created by Eric Yuen on 10/26/16.
//  Copyright © 2016 WHI. All rights reserved.
//

#import "MinderLibBLE.h"

@implementation MinderLibBLE {
    int formerByte;
    BOOL packetFlag;
    int x, xFormer;
    int y, yFormer;
    int z, zFormer;
    int packetByteCounter;
    float dy, dz;
    float dyyFormer, dzzFormer;
    float displayX, displayY;
    int packetType;
    unsigned long int lastTS;
    int stepCount;
    int stepOffset;
    uint8_t vibrationPattern;
    uint8_t ledColor;
    NSString *_deviceName;
    int temperature;
    NSString *characteristicValue;
    NSMutableArray *peripherals;
    BOOL isLogging;
    NSString *filePath;

    double lastHRTime;

    BOOL isConnected;    
}

- (id)init {
    self = [super init];
    if (self) {
        formerByte = 0;
        packetFlag = false;
        packetByteCounter = 0;
        xFormer = 0;
        yFormer = 0;
        zFormer = 0;
        dyyFormer = 0;
        dzzFormer = 0;
        dy = 0;
        dz = 0;
        packetType = 7;
        _status = 0;
        lastTS = 0;
        stepCount = 0;
        stepOffset = -1;
        CBCentralManager *centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        self.centralManager = centralManager;
    }
    

    return self;
}

- (id)initWithDelegate:(id<MinderLibDelegate>)delegate {
    _dataDelegate = delegate;
    [_dataDelegate updateStatus:INITIALIZING];
    

    return [self init];
}

- (BOOL)connect:(NSString *)deviceName {
    NSLog((@"connect received, scan"));
    _deviceName = deviceName;
    
    [self.centralManager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:DEVICE_SERVICE_UUID]] options:nil];
    [_dataDelegate updateStatus:CONNECTING];

    return TRUE;
}

- (void)getDeviceNames{
    if(!peripherals){
        peripherals = [[NSMutableArray alloc] init];
    }
    else{
        [peripherals removeAllObjects];
    }
    _deviceName = nil;

    if(self.centralManager.state == CBCentralManagerStatePoweredOn){
        NSDictionary *scanOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
        [self.centralManager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:DEVICE_SERVICE_UUID]] options:scanOptions];
    }
}

- (void)getRSSI{
    [self.posturePeripheral readRSSI];
}

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error{
    [_dataDelegate updateRSSI:RSSI];
}
- (void)disconnect {
    [self.centralManager cancelPeripheralConnection:self.posturePeripheral];

}

- (BOOL)enableAccel {
    NSLog(@"Enable Accel Received\n");
    return [self writeCmdToDevice:ENABLE_ACCEL];
}
- (BOOL)disableAccel {
    NSLog(@"Disable Accel Received\n");
    return [self writeCmdToDevice:DISABLE_ACCEL];
}

- (void)setLEDColor:(uint8_t)color withIntensity:(uint8_t)intensity
{
    float i = (float)intensity/10.0;
    uint8_t j = (int)(i + 0.5);
    
    ledColor = (color << 6) | j;
}

- (BOOL)blinkLEDwithColor:(uint8_t)color withIntensity:(uint8_t)intensity {
    [self setLEDColor:color withIntensity:intensity];
    float i = (float)intensity/10.0;
    uint8_t j = (int)(i + 0.5);
    uint8_t ledcolor = (color << 6) | j;
    return [self writeCmdDataToDevice:BLINK_LED withData:ledcolor];
}
- (BOOL)blinkLED:(uint8_t)color withPeriod:(short)period withDutyCycle:(short)dutyCycle {
    char *data = malloc(sizeof(uint8_t) + 2*(sizeof(short)));
    data[0] = BLINK_LED;
    data[1] = color;
    data[2] = (char)(dutyCycle & 0xff);
    data[3] = (char)((dutyCycle << 8) & 0xff);
    data[4] = (char)(period & 0xff);
    data[5] = (char)((period << 8) & 0xff);

    NSData* nsdata =  [NSData dataWithBytes:data length:sizeof(data)];
    return [self writeDataToDevice:nsdata];
}

- (BOOL)vibrateDevice {
    NSLog(@"vibrate Received\n");
    //return [self writeCommandToDevice:VIBRATE_DEVICE];
    return [self writeCmdDataToDevice:VIBRATE_DEVICE withData:vibrationPattern];
}

- (BOOL)stopDevice {
    NSLog(@"stop device Received\n");
    return [self writeCmdToDevice:STOP_COMMAND];
}
- (void)setVibrationPattern:(uint8_t)pattern{
    vibrationPattern = pattern;
}

- (BOOL)enableHeartRate {
    return [self writeCmdToDevice:ENABLE_HEARTRATE];
}

- (BOOL)disableHeartRate {
    return [self writeCmdToDevice:DISABLE_HEARTRATE];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    if(peripheral)
    {
        [peripheral setDelegate:nil];
        peripheral = nil;
    }
}

// method called whenever you have successfully connected to the BLE peripheral
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    [peripheral setDelegate:self];
    [peripheral discoverServices:nil];
    self.connected = [NSString stringWithFormat:@"Connected: %@", peripheral.state == CBPeripheralStateConnected ? @"YES" : @"NO"];
    NSLog(@"%@", self.connected);
    stepOffset = -1;
    stepCount = 0;
    isConnected = YES;
    [_dataDelegate updateStatus:CONNECTED];
}

// CBCentralManagerDelegate - This is called with the CBPeripheral class as its main input parameter. This contains most of the information there is to know about a BLE peripheral.
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSLog(@"discoverd peripheral");
    //NSLog(@"UUID = %@", peripheral.identifier);
    NSLog(@"Name = %@", peripheral.name);

    if(_deviceName.length == 0){
        [peripherals addObject:peripheral.name];
        [_dataDelegate updateDeviceList:peripherals];
    }
    else {
        if([_deviceName isEqualToString:peripheral.name]){
            [self.centralManager stopScan];
    
            peripheral.delegate = self;
            self.posturePeripheral = peripheral;
            [self.centralManager connectPeripheral:peripheral options:nil];
        }
    }
}

// method called whenever the device state changes.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    // Determine the state of the peripheral
    if ([central state] == CBCentralManagerStatePoweredOff) {
        NSLog(@"CoreBluetooth BLE hardware is powered off");
        [_dataDelegate updateBLEStatus:POWEREDOFF];
    }
    else if ([central state] == CBCentralManagerStatePoweredOn) {
        NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
         [self.centralManager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:DEVICE_SERVICE_UUID]] options:nil];
        [_dataDelegate updateBLEStatus:POWEREDON];
    }
    else if ([central state] == CBCentralManagerStateUnauthorized) {
        NSLog(@"CoreBluetooth BLE state is unauthorized");
        [_dataDelegate updateBLEStatus:UNAUTHORIZED];
    }
    else if ([central state] == CBCentralManagerStateUnknown) {
        NSLog(@"CoreBluetooth BLE state is unknown");
        [_dataDelegate updateBLEStatus:UNKNOWNSTATE];
    }
    else if ([central state] == CBCentralManagerStateUnsupported) {
        NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
        [_dataDelegate updateBLEStatus:UNSUPPORTED];
    }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(nullable NSError *)error {
    [_dataDelegate updateStatus:DISCONNECTED];
    //NSLog(@"didDisconnectPeripheral event occured \(error)");
    isConnected = NO;
}

#pragma mark - CBPeripheralDelegate

// CBPeripheralDelegate - Invoked when you discover the peripheral's available services.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    for (CBService *service in peripheral.services) {
        NSLog(@"Discovered service: %@", service.UUID);
        [peripheral discoverCharacteristics:nil forService:service];
    }
}


// Invoked when you discover the characteristics of a specified service.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if ([service.UUID isEqual:[CBUUID UUIDWithString:DEVICE_SERVICE_UUID]])  {  // 1
        for (CBCharacteristic *aChar in service.characteristics)
        {
            // Request notifications
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_NOTICE_UUID]]) { // 2
                [self.posturePeripheral setNotifyValue:YES forCharacteristic:aChar];
                NSLog(@"Found heart rate measurement characteristic");
            }
            // enable angle
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_WRITE_UUID]]) { // 3
                NSLog(@"Found a DSPS writeWithoutResponse characteristic");
                self.writeCharacteristic = aChar;
            }
        }
    }
    
}

// Invoked when you retrieve a specified characteristic's value, or when the peripheral device notifies your app that the characteristic's value has changed.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_NOTICE_UUID]]) { // 1
        characteristicValue = [NSString stringWithFormat: @"%@\n", [characteristic value]];
        NSData *data = [characteristic value];
        if(isLogging){
            const uint8_t *hd = [data bytes];
            if(hd[0]==DATAGRAM_HEADER){
                [self WriteToFile:characteristicValue];
                NSString *debugData = [[[characteristicValue stringByReplacingOccurrencesOfString:@"<" withString:@""]
                                        stringByReplacingOccurrencesOfString:@">" withString:@""]
                                       stringByReplacingOccurrencesOfString:@" " withString:@","];
                [_dataDelegate updateDebug:debugData];
            }
            if(hd[0]==PIC_VERSION_HEADER){
                NSString *picData = [[[characteristicValue stringByReplacingOccurrencesOfString:@"<" withString:@""]
                                        stringByReplacingOccurrencesOfString:@">" withString:@""]
                                       stringByReplacingOccurrencesOfString:@" " withString:@","];
                NSLog(@"PIC Data: %@",picData);
            }
        }
        unsigned long length = [data length];
        const uint8_t *reportData = [data bytes];

        [self parsePacket:reportData withLength:length];
    }
}
#pragma DLI Protocol
-(void)parsePacket:(const uint8_t *)data withLength:(unsigned long)length
{
    if(length == 20)
    {
        uint16 seqNumber = ((data[2] << 8) + data[1]);
        //lastTS = (data[3] << 8) + data[4];
        switch (data[0]) {
            case ACCEL_HEADER:
                [self handleAccelData:data withSeqNumber:seqNumber];
                [self handleGyroData:data withSeqNumber:seqNumber];
                [self handleAngleData:data withSeqNumber:seqNumber];
                [self handleButtonStatusData:data];
                break;
            case DATAGRAM_HEADER:
                [self handleDatagramData:data];
                break;
            case PIC_VERSION_HEADER:
                [self handlePicVersion:data];
                break;
            default:
                break;
        }
    }
}

- (void)handlePicVersion:(const uint8_t *)data{
    NSString *pcbVersion = [NSString stringWithFormat:@"%d", (short)data[1]];
    NSString *majorVersion = [NSString stringWithFormat:@"%d", (short)data[2]];
    NSString *minorVersion = [NSString stringWithFormat:@"%d", (short)data[3]];
    NSString *versionYear = [NSString stringWithFormat:@"%d", (short)(data[4])];
    NSString *versionMonth = [NSString stringWithFormat:@"%d", (short)(data[5])];
    NSString *versionDay = [NSString stringWithFormat:@"%d", (short)(data[6])];
    NSString *versionHour = [NSString stringWithFormat:@"%d", (short)(data[7])];
    NSString *versionMinute = [NSString stringWithFormat:@"%d", (short)(data[8])];
    
    NSString *fwPICVersion = [NSString stringWithFormat:@"%@.%@.%@.%@.%@.%@.%@.%@",pcbVersion,majorVersion,minorVersion,versionYear,versionMonth,versionDay,versionHour,versionMinute];
    [_dataDelegate updatePicVersion:fwPICVersion];
    
    // FW Version
    NSString *fwVersionDay = [NSString stringWithFormat:@"%d", (short)(data[9])];
    NSString *fwVersionMonth = [NSString stringWithFormat:@"%d", (short)(data[10])];
    NSString *fwVersionYear = [NSString stringWithFormat:@"%d", (short)(data[11])];
    NSString *fwVersionHour = [NSString stringWithFormat:@"%d", (short)(data[12])];
    NSString *fwVersionMinute = [NSString stringWithFormat:@"%d", (short)(data[13])];
    
    NSString *fwVersion = [NSString stringWithFormat:@"%@.%@.%@.%@.%@", fwVersionYear, fwVersionMonth, fwVersionDay,fwVersionHour,fwVersionMinute];
    [_dataDelegate updateFWVersion:fwVersion];
    
    int picStatus = data[14];
    [_dataDelegate updatePICStatus:picStatus];
}
- (void)handleAccelData:(const uint8_t *)data withSeqNumber:(uint16) seqNumber {
    
    x = (short)((data[3] << 8) + data[4]);
    y = (short)((data[5] << 8) + data[6]);
    z = (short)((data[7] << 8) + data[8]);
    
    [_dataDelegate updateAccelWithX:x withY:y withZ:z withT:seqNumber];
}
    
- (void)handleGyroData:(const uint8_t *)data withSeqNumber:(uint16) seqNumber {
    x = (short)((data[9] << 8) + data[10]);
    y = (short)((data[11] << 8) + data[12]);
    z = (short)((data[13] << 8) + data[14]);
    
    int x1 = x >> 8;
    int y1 = x >> 8;
    int z1 = x >> 8;

    [_dataDelegate updateGyroWithRoll:x1 withPitch:y1 withYaw:z1 withT:seqNumber];
   // [_dataDelegate updateGyroWithRoll:x withPitch:y withYaw:z withT:seqNumber];
}

- (void)handleAngleData:(const uint8_t *)data withSeqNumber:(uint16) seqNumber {
    x = data[15];
    if(x > 127) {
        x = x - 256;
    }
    y = data[16];
    if(y > 127) {
        y = y - 256;
    }
    z = data[17];
    if(z > 127) {
        z = z -256;
    }
    //NSLog(@"x,y,z: %d,%d,%d",x,y,z);
  
    if (abs(x)>3000 || abs(y)>3000 || abs(z)>3000){
        x=xFormer;
        y=yFormer;
        z=zFormer;
    }
    else{
        xFormer=x;
        yFormer=y;
        zFormer=z;
    }
     
    dy = y;
    dz = z;
    
    // Smoothing
    dy=0.8*dyyFormer+0.2*dy;
    dz=0.8*dzzFormer+0.2*dz;
    dyyFormer=dy;
    dzzFormer=dz;
    
    displayX=150+dy;
    displayY=150+dz;
    
    [self.dataDelegate updateRawAngle:(int)x withY:(int)y withZ:(int)z];
    [self.dataDelegate updatePosture:displayX withY:displayY withT:seqNumber];
}

- (void)handleButtonStatusData:(const uint8_t *)data{
    uint8_t buttonPressed = data[19];
    [_dataDelegate updateButtonStatus:buttonPressed];
}

- (void)handleDatagramData:(const uint8_t *)data {
    //Step count
    stepCount = (short)((data[4] << 8) + data[3]);
    [self.dataDelegate updateStepCount:(stepCount)];
    
    //Heart rate
    int heartRate = (ushort)(((data[6] << 8) + data[5])/100);
    [_dataDelegate updateHeartRate:heartRate];
    
    //Temperature
    temperature = data[7];
    NSString *t = [NSString stringWithFormat:@"%d.%d", temperature, data[8]];
    NSNumberFormatter *f = [[NSNumberFormatter alloc]init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *tempVal = [f numberFromString: t];
    [_dataDelegate updateTemperature:tempVal.doubleValue];
    
    //Wireless charging status
    uint8_t wireless = ((data[9] & 0x02) >> 1) ;
    [_dataDelegate updateWirelessChargerStatus:wireless];
    
    //Charging status - 0 = not charging, 1 = charging, 2 = completed, 3 = fault
    uint8_t charging = (data[9] >> 2) & 0x03;
    [_dataDelegate updateChargingStatus:charging];
    
    //Battery level status
    UInt8 a = (UInt8)data[11];
    UInt8 b = (a >> 2);
    UInt8 batteryLevel = 0;
    //UInt8 b = (((a >> 5) & 0x03) * 10 + 60) + ((a >> 2) & 0x07);
    switch (b) {
        case 31:
            batteryLevel = 100;
            break;
        case 30:
            batteryLevel = 90;
            break;
        case 27:
            batteryLevel = 80;
            break;
        case 26:
            batteryLevel = 70;
            break;
        case 25:
            batteryLevel = 60;
            break;
        case 23:
            batteryLevel = 50;
            break;
        case 22:
            batteryLevel = 40;
            break;
        case 19:
            batteryLevel = 30;
            break;
        case 18:
            batteryLevel = 20;
            break;
        case 17:
            batteryLevel = 10;
            break;
        case 15:
        default:
            batteryLevel = 0;
            break;
    }
    //NSLog(@"Battery Level: %d", data[11]);
    [_dataDelegate updateCharge:batteryLevel];
    
    //LRA status
    if(data[12] != 0){
        [_dataDelegate updateLRA:data[12]];
    }
    
    

    //LED status
    
    int redLED = data[13];
    int greenLED = data[14];
    int blueLED = data[15];
    [_dataDelegate updateLEDStatus:redLED withGreen:greenLED withBlue:blueLED];
    
    
    //VSYS
    UInt8 vsys = data[16];
    float fvsys = vsys / 10.0;
    [_dataDelegate updateVSysCode:fvsys];
    
    // FW Version
    //NSString *fwVersionDay = [NSString stringWithFormat:@"%d", (short)(data[17])];
    //NSString *fwVersionMonth = [NSString stringWithFormat:@"%d", (short)(data[18])];
    //NSString *fwVersionYear = [NSString stringWithFormat:@"%d", (short)(data[19])];
    
    //NSString *fwVersion = [NSString stringWithFormat:@"%@.%@.%@", fwVersionYear, fwVersionMonth, fwVersionDay];
    //[_dataDelegate updateFWVersion:fwVersion];
    
    [self.posturePeripheral readRSSI];
}

- (void)handleStepData:(const uint8_t *)data {
    stepCount = data[3] + (data[4] << 8);
    [self.dataDelegate updateStepCount:(stepCount)];
}

#pragma mark - CBCharacteristic helpers
-(BOOL)writeCmdDataToDevice:(uint8_t)command withData:(uint8_t)data {
    NSMutableData *message = [NSMutableData dataWithBytes:(void*)&command length:sizeof(command)];
    NSData *d = [NSData dataWithBytes:(void*)&data length:sizeof(data)];
    [message appendData:d];
    if(message.length < MESSAGE_LEN){
        [message increaseLengthBy:MESSAGE_LEN - message.length];
    }
    [self.posturePeripheral writeValue:message forCharacteristic:self.writeCharacteristic type:CBCharacteristicWriteWithoutResponse];
    [_dataDelegate updateDataSent:message];
    return TRUE;
}

-(BOOL)writeCmdToDevice:(uint8_t)command {
    NSMutableData *message = [NSMutableData dataWithBytes:(void*)&command length:sizeof(command)];
    [message increaseLengthBy:MESSAGE_LEN - message.length];
    [self.posturePeripheral writeValue:message forCharacteristic:self.writeCharacteristic type:CBCharacteristicWriteWithoutResponse];
    [_dataDelegate updateDataSent:message];
    return TRUE;
}

-(BOOL)writeDataToDevice:(NSData*)data {
    NSMutableData *message = [NSMutableData dataWithData:data];
    [message increaseLengthBy:MESSAGE_LEN - message.length];
    [self.posturePeripheral writeValue:data forCharacteristic:self.writeCharacteristic type:CBCharacteristicWriteWithoutResponse];
    [_dataDelegate updateDataSent:message];
    return TRUE;
}

- (void)WriteToFile:(NSString *)str {
    NSString *csvString = [[[str stringByReplacingOccurrencesOfString:@"<" withString:@""]
                           stringByReplacingOccurrencesOfString:@">" withString:@""]
                            stringByReplacingOccurrencesOfString:@" " withString:@","];
    
    NSData *data = [csvString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        // Add the text at the end of the file.
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
        [fileHandler seekToEndOfFile];
        [fileHandler writeData:data];
        [fileHandler closeFile];
    } else {
        [data writeToFile:filePath atomically:YES];
    }
    
}

- (void)logData:(BOOL)enable{
    isLogging = enable;
    if(isLogging){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        if (0 < [paths count]) {
            NSString *documentsDirPath = [paths objectAtIndex:0];
            NSDate *currentDate = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"YYMMddHHmmss"];
            NSString *dateString = [dateFormatter stringFromDate:currentDate];
            NSString *fileName = [NSString stringWithFormat:@"bledata%@.csv",dateString];
            filePath = [documentsDirPath stringByAppendingPathComponent:fileName];
        }
    }
}

@end
