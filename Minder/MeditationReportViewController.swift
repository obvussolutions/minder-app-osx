//
//  MeditationReportViewController.swift
//  Minder
//
//  Created by Febin on 5/11/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa

class MeditationReportViewController: NSViewController {

    @IBOutlet weak var congratulation: NSTextField!
    @IBOutlet weak var bpmButton: NSButton!
    @IBOutlet weak var timedButton: NSButton!
    @IBOutlet weak var focusButtton: NSButton!
    @IBOutlet weak var cycleButton: NSButton!
    @IBAction func closeReportClicked(_ sender: NSButton) {
        self.view.removeFromSuperview()
    }
    @IBOutlet weak var meditationContainerView: NSView!
    @IBOutlet weak var meditationReportView: NSView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    var cycles:Int = 0{
        didSet {
            cycleButton.title = "\(cycles) Mindful \n Cycles"
        }
    }
    var focusTime:Int = 0{
        didSet {
            focusButtton.title = "\(focusTime) seconds \n of Focus"
        }
    }
    var perfectTiming:Int = 0{
        didSet {
            timedButton.title = "\(perfectTiming)% Perfectly \n Timed"
        }
    }
    var BPM:Int = 0{
        didSet {
            bpmButton.title = "Lowest HR \n \(BPM) BPM"
        }
    }
    
    
    
    override func viewWillAppear() {
        meditationReportView.layer?.backgroundColor = NSColor.init(calibratedRed: 242/255, green: 242/255, blue: 242/255, alpha: 1.0).cgColor
        meditationContainerView.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1.0).cgColor
        meditationReportView.layer?.cornerRadius = 5.0
        let buttonCornerRadius:CGFloat = 10.0
        let buttonBorderWidth:CGFloat = 0.5
        cycleButton.layer?.backgroundColor = NSColor.white.cgColor
        focusButtton.layer?.backgroundColor = NSColor.white.cgColor
        timedButton.layer?.backgroundColor = NSColor.white.cgColor
        bpmButton.layer?.backgroundColor = NSColor.white.cgColor
        cycleButton.layer?.cornerRadius = buttonCornerRadius
        focusButtton.layer?.cornerRadius = buttonCornerRadius
        timedButton.layer?.cornerRadius = buttonCornerRadius
        bpmButton.layer?.cornerRadius = buttonCornerRadius
        cycleButton.layer?.borderWidth = buttonBorderWidth
        cycleButton.layer?.borderColor = NSColor.black.cgColor
        focusButtton.layer?.borderWidth = buttonBorderWidth
        focusButtton.layer?.borderColor = NSColor.black.cgColor
        timedButton.layer?.borderWidth = buttonBorderWidth
        timedButton.layer?.borderColor = NSColor.black.cgColor
        bpmButton.layer?.borderWidth = buttonBorderWidth
        bpmButton.layer?.borderColor = NSColor.black.cgColor
        
        congratulation.underlined()
        
    }
    
    func populateReportData(meditationReportCard:MeditationReportCard) {
        self.cycles = meditationReportCard.totalCycles
        self.focusTime = meditationReportCard.totalTime
        var inhaleSum:CGFloat = 0.0
        var exhaleSum:CGFloat = 0.0
        for (mi, me) in zip(meditationReportCard.perfectInhalePersentage, meditationReportCard.perfectExcalePersentage) {
            inhaleSum += mi
            exhaleSum += me
        }
        self.perfectTiming = (Int(inhaleSum) + Int(exhaleSum))/(meditationReportCard.perfectInhalePersentage.count * 2)
        self.BPM = Int(meditationReportCard.BPM)
    }
    
}

extension NSTextField {
    
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.5)
        border.borderColor = NSColor.black.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer?.addSublayer(border)
        self.layer?.masksToBounds = true
    }
}
