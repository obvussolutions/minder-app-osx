//
//  AnnulusView.swift
//  Minder
//
//  Created by Abdul on 12/29/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class AnnulusView: NSView {
    
    var viewMode: Bool = false
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        let centerCircle: NSPoint = NSMakePoint(dirtyRect.size.height / 2, dirtyRect.size.height / 2)
        let attributes: NSDictionary = [
            NSFontAttributeName : NSFont.boldSystemFont(ofSize: dirtyRect.size.height * 0.3),
            NSForegroundColorAttributeName : (viewMode ? NSColor.white : NSColor.black)
        ]
        let bigLine = NSAttributedString(string: "|", attributes: attributes as? [String : AnyObject])
        let smallLine = NSAttributedString(string: "-", attributes: attributes as? [String : AnyObject])
        
        // 12 O-Clock
        var xString = centerCircle.x + (dirtyRect.size.height/2.2 * cos(90 * CGFloat(M_PI) / 180))
        var yString = centerCircle.y + (dirtyRect.size.height/2.2 * sin(90 * CGFloat(M_PI) / 180))
        let attributes12: NSDictionary = [
            NSFontAttributeName : NSFont.boldSystemFont(ofSize: dirtyRect.size.height * 0.4),
            NSForegroundColorAttributeName : (viewMode ? NSColor.white : NSColor.black)
        ]
        let bigLine12 = NSAttributedString(string: "||", attributes: attributes12 as? [String : AnyObject])
        bigLine12.draw(at: NSMakePoint(xString-(bigLine12.size().width/2), yString-(bigLine12.size().height/2)))
                
        // 3 O-Clock
        xString = centerCircle.x + (dirtyRect.size.height/2.2 * cos(270 * CGFloat(M_PI) / 180))
        yString = centerCircle.y + (dirtyRect.size.height/2.2 * sin(270 * CGFloat(M_PI) / 180))
        NSGraphicsContext.saveGraphicsState()
        var transf : AffineTransform = AffineTransform.identity
        transf.translate(x: dirtyRect.size.height/2, y: dirtyRect.size.height/2)
        transf.rotate(byDegrees: 90)
        transf.translate(x: -dirtyRect.size.height/2, y: -dirtyRect.size.height/2)
        (transf as NSAffineTransform).concat()
        bigLine.draw(at: NSMakePoint(xString-(bigLine.size().width/2), yString-(bigLine.size().height/2)))
        NSGraphicsContext.restoreGraphicsState()
        
        // L from 12 to 3 O-Cloack
        let pathLine : NSBezierPath = NSBezierPath()
        pathLine.move(to: NSMakePoint(dirtyRect.size.height/2, dirtyRect.size.height))
        pathLine.line(to: centerCircle)
        pathLine.line(to: NSMakePoint(dirtyRect.size.height, dirtyRect.size.height/2))
        viewMode ? NSColor.white.set() : NSColor.black.set()
        pathLine.lineWidth = (dirtyRect.size.height * 8) / 100
        pathLine.stroke()
        
        // 6 O-Clock
        xString = centerCircle.x + (dirtyRect.size.height/2.2 * cos(270 * CGFloat(M_PI) / 180))
        yString = centerCircle.y + (dirtyRect.size.height/2.2 * sin(270 * CGFloat(M_PI) / 180))
        bigLine.draw(at: NSMakePoint(xString-(bigLine.size().width/2), yString-(bigLine.size().height/2)))
        
        // 9 O-Clock
        xString = centerCircle.x + (dirtyRect.size.height/2.2 * cos(90 * CGFloat(M_PI) / 180))
        yString = centerCircle.y + (dirtyRect.size.height/2.2 * sin(90 * CGFloat(M_PI) / 180))
        NSGraphicsContext.saveGraphicsState()
        transf = AffineTransform.identity
        transf.translate(x: dirtyRect.size.height/2, y: dirtyRect.size.height/2)
        transf.rotate(byDegrees: 90)
        transf.translate(x: -dirtyRect.size.height/2, y: -dirtyRect.size.height/2)
        (transf as NSAffineTransform).concat()
        bigLine.draw(at: NSMakePoint(xString-(bigLine.size().width/2), yString-(bigLine.size().height/2)))
        NSGraphicsContext.restoreGraphicsState()
        
        // 1.30 O-Clock
        xString = centerCircle.x + (dirtyRect.size.height/2.2 * cos(0))
        yString = centerCircle.y + (dirtyRect.size.height/2.2 * sin(0))
        NSGraphicsContext.saveGraphicsState()
        transf = AffineTransform.identity
        transf.translate(x: dirtyRect.size.height/2, y: dirtyRect.size.height/2)
        transf.rotate(byDegrees: 45)
        transf.translate(x: -dirtyRect.size.height/2, y: -dirtyRect.size.height/2)
        (transf as NSAffineTransform).concat()
        smallLine.draw(at: NSMakePoint(xString-(smallLine.size().width/2), yString-(smallLine.size().height/2)))
        NSGraphicsContext.restoreGraphicsState()
        
        //  4.5 O-Clock
        xString = centerCircle.x + (dirtyRect.size.height/2.2 * cos(0))
        yString = centerCircle.y + (dirtyRect.size.height/2.2 * sin(0))
        NSGraphicsContext.saveGraphicsState()
        transf = AffineTransform.identity
        transf.translate(x: dirtyRect.size.height/2, y: dirtyRect.size.height/2)
        transf.rotate(byDegrees: -45)
        transf.translate(x: -dirtyRect.size.height/2, y: -dirtyRect.size.height/2)
        (transf as NSAffineTransform).concat()
        smallLine.draw(at: NSMakePoint(xString-(smallLine.size().width/2), yString-(smallLine.size().height/2)))
        NSGraphicsContext.restoreGraphicsState()
        
        //  7.5 O-Clock
        xString = centerCircle.x + (dirtyRect.size.height/2.2 * cos(0))
        yString = centerCircle.y + (dirtyRect.size.height/2.2 * sin(0))
        NSGraphicsContext.saveGraphicsState()
        transf = AffineTransform.identity
        transf.translate(x: dirtyRect.size.height/2, y: dirtyRect.size.height/2)
        transf.rotate(byDegrees: -135)
        transf.translate(x: -dirtyRect.size.height/2, y: -dirtyRect.size.height/2)
        (transf as NSAffineTransform).concat()
        smallLine.draw(at: NSMakePoint(xString-(smallLine.size().width/2), yString-(smallLine.size().height/2)))
        NSGraphicsContext.restoreGraphicsState()
        
        // 10:30 O-Clock
        xString = centerCircle.x + (dirtyRect.size.height/2.2 * cos(0))
        yString = centerCircle.y + (dirtyRect.size.height/2.2 * sin(0))
        NSGraphicsContext.saveGraphicsState()
        transf = AffineTransform.identity
        transf.translate(x: dirtyRect.size.height/2, y: dirtyRect.size.height/2)
        transf.rotate(byDegrees: 135)
        transf.translate(x: -dirtyRect.size.height/2, y: -dirtyRect.size.height/2)
        (transf as NSAffineTransform).concat()
        smallLine.draw(at: NSMakePoint(xString-(smallLine.size().width/2), yString-(smallLine.size().height/2)))
        NSGraphicsContext.restoreGraphicsState()
    }
}
