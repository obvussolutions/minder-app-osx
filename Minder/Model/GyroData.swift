//
//  GyroData.swift
//  Minder
//
//  Created by Abdul on 2/8/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa

class GyroData: NSObject {
    var roll: CGFloat = 0
    var pitch: CGFloat = 0
    var yaw: CGFloat = 0
    var state: Int = 0
    var time: String = ""
    
    override init() {
        
    }
    required init(coder aDecoder: NSCoder) {
        if let _roll = aDecoder.decodeObject(forKey: "roll") as? CGFloat {
            self.roll = _roll
        }
        if let _pitch = aDecoder.decodeObject(forKey: "pitch") as? CGFloat {
            self.pitch = _pitch
        }
        if let _yaw = aDecoder.decodeObject(forKey: "yaw") as? CGFloat {
            self.yaw = _yaw
        }
        if let _state = aDecoder.decodeObject(forKey: "state") as? Int {
            self.state = _state
        }
        if let _time = aDecoder.decodeObject(forKey: "time") as? String {
            self.time = _time
        }
    }
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let _roll = self.roll as? CGFloat {
            aCoder.encode(_roll, forKey: "roll")
        }
        if let _pitch = self.pitch as? CGFloat {
            aCoder.encode(_pitch, forKey: "pitch")
        }
        if let _yaw = self.yaw as? CGFloat {
            aCoder.encode(_yaw, forKey: "yaw")
        }
        if let _state = self.state as? Int {
            aCoder.encode(_state, forKey: "state")
        }
        if let _time = self.time as? String {
            aCoder.encode(_time, forKey: "time")
        }
    }
    
    static func GetDictionaryFromDeviceData(_ epochTimeInterval:String, positionData: GyroData) -> Dictionary<String, String> {
        var epochTimeInterval = epochTimeInterval
        if epochTimeInterval == "" {
            epochTimeInterval = String(Date().timeIntervalSince1970)
            epochTimeInterval = epochTimeInterval.substring(to: (epochTimeInterval.range(of: ".")?.lowerBound)!)
        }
        let params: Dictionary<String, String> = ["t": String(positionData.time), "oauth_token": MinderManager.sharedInstance.userLogged!.token!, "roll": String(describing: positionData.roll), "pitch": String(describing: positionData.pitch), "yaw": String(describing: positionData.yaw), "state": String(positionData.state), "epoch_time": epochTimeInterval]
        return params
    }
    static func GetDictionaryArrayFromDeviceDataArray(_ postureDatas: [GyroData]) -> Array<Dictionary<String, String>> {
        var epochTimeInterval: String = String(Date().timeIntervalSince1970)
        epochTimeInterval = epochTimeInterval.substring(to: (epochTimeInterval.range(of: ".")?.lowerBound)!)
        var array: Array<Dictionary<String, String>> = Array<Dictionary<String, String>>()
        for dData in postureDatas {
            array.append(GetDictionaryFromDeviceData(epochTimeInterval, positionData: dData))
        }
        return array
    }
    static func GetDeviceDataFromJsonData(_ json: Dictionary<String, String>) -> GyroData {
        let dData: GyroData = GyroData()
        if let n = NumberFormatter().number(from: json["roll"]!) {
            dData.roll = CGFloat(n)
        }
        if let n = NumberFormatter().number(from: json["pitch"]!) {
            dData.pitch = CGFloat(n)
        }
        if let n = NumberFormatter().number(from: json["yaw"]!) {
            dData.yaw = CGFloat(n)
        }
        dData.state = Int(json["state"]!)!
        dData.time = json["t"]!
        return dData
    }
    static func GetDeviceDataArrayFromJsonArray(_ jsonArray: Array<Dictionary<String, String>>) -> [GyroData] {
        var dDatas: [GyroData] = [GyroData]()
        for json in jsonArray {
            dDatas.append(GetDeviceDataFromJsonData(json))
        }
        return dDatas
    }
}
