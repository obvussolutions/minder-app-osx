//
//  Zone.swift
//  Minder
//
//  Created by Abdul on 10/4/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class MeditationBreak: NSObject {
    var title: String = ""
    
    var numberOfBreath: Int = 2
    var inhaleLength: Int = 3
    var retailInhaleLength: Int = 3
    var exhaleLength: Int = 3
    var retailExhaleLength: Int = 3
    
    override init() {}
    
    required init(coder aDecoder: NSCoder) {
        if let _title = aDecoder.decodeObject(forKey: "title") as? String {
            self.title = _title
        }
        if let _numberOfBreath = aDecoder.decodeInteger(forKey: "numberOfBreath") as? Int {
            self.numberOfBreath = _numberOfBreath
        }
        if let _inhaleLength = aDecoder.decodeInteger(forKey: "inhaleLength") as? Int {
            self.inhaleLength = _inhaleLength
        }
        if let _retailInhaleLength = aDecoder.decodeInteger(forKey: "retailInhaleLength") as? Int {
            self.retailInhaleLength = _retailInhaleLength
        }
        if let _exhaleLength = aDecoder.decodeInteger(forKey: "exhaleLength") as? Int {
            self.exhaleLength = _exhaleLength
        }
        if let _retailExhaleLength = aDecoder.decodeInteger(forKey: "retailExhaleLength") as? Int {
            self.retailExhaleLength = _retailExhaleLength
        }
    }
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let _title = self.title as? String {
            aCoder.encode(_title, forKey: "title")
        }
        
        if let _numberOfBreath = self.numberOfBreath as? Int {
            aCoder.encode(_numberOfBreath, forKey: "numberOfBreath")
        }
        if let _inhaleLength = self.inhaleLength as? Int {
            aCoder.encode(_inhaleLength, forKey: "inhaleLength")
        }
        if let _retailInhaleLength = self.retailInhaleLength as? Int {
            aCoder.encode(_retailInhaleLength, forKey: "retailInhaleLength")
        }
        if let _exhaleLength = self.exhaleLength as? Int {
            aCoder.encode(_exhaleLength, forKey: "exhaleLength")
        }
        if let _retailExhaleLength = self.retailExhaleLength as? Int {
            aCoder.encode(_retailExhaleLength, forKey: "retailExhaleLength")
        }
    }
    
    
    public func totalMeditationTime() -> Int? {
        return (self.numberOfBreath * (self.inhaleLength + self.retailInhaleLength + self.exhaleLength + self.retailExhaleLength))
    }
}
