//
//  Activity+CoreDataProperties.swift
//  Minder
//
//  Created by Abdul on 5/2/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Foundation
import CoreData


extension Activity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Activity> {
        return NSFetchRequest<Activity>(entityName: "Activity");
    }

    @NSManaged public var acess: String?
    @NSManaged public var activity_for: String?
    @NSManaged public var activity_types: String?
    @NSManaged public var additional_file: String?
    @NSManaged public var background_music: String?
    @NSManaged public var closed_caption: String?
    @NSManaged public var company_logo: String?
    @NSManaged public var display_timer: NSNumber?
    @NSManaged public var duration: NSNumber?
    @NSManaged public var end_time: String?
    @NSManaged public var exercise_status: NSNumber?
    @NSManaged public var marker_annotation: String?
    @NSManaged public var media_description: String?
    @NSManaged public var media_id: NSNumber?
    @NSManaged public var media_keywords: String?
    @NSManaged public var media_title: String?
    @NSManaged public var media_type: String?
    @NSManaged public var media_url: String?
    @NSManaged public var modified_id: NSNumber?
    @NSManaged public var program_id: NSNumber?
    @NSManaged public var schedule_date: String?
    @NSManaged public var schedule_date_id: NSNumber?
    @NSManaged public var schedule_id: NSNumber?
    @NSManaged public var sensitivity_level: NSNumber?
    @NSManaged public var start_time: String?
    @NSManaged public var user_email: String?
    @NSManaged public var views: NSNumber?
    @NSManaged public var breathCount: NSNumber?
    @NSManaged public var exhaleLen: NSNumber?
    @NSManaged public var exhaleRetail: NSNumber?
    @NSManaged public var inhaleLen: NSNumber?
    @NSManaged public var inhaleRetail: NSNumber?

}
