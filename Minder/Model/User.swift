//
//  User.swift
//  Minder
//
//  Created by Abdul on 8/25/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class User: NSObject {
    
    var name: String?
    var image: NSImage?
    var token: String?
    var firstName: String?
    var lastName: String?
    var age: String?
    var phone: String?
    var gender: String?
    var address: String?
    var userName: String?
    var email: String?
    var height: String?
    var weight: String?
    var build: String?
    var deviceName: String?
    
    override init() {}
    
    required init(coder aDecoder: NSCoder) {
        if let _name = aDecoder.decodeObject(forKey: "name") as? String {
            self.name = _name
        }
        if let _image = aDecoder.decodeObject(forKey: "image") as? NSImage {
            self.image = _image
        }
        if let _token = aDecoder.decodeObject(forKey: "token") as? String {
            self.token = _token
        }
        if let _firstName = aDecoder.decodeObject(forKey: "firstName") as? String {
            self.firstName = _firstName
        }
        if let _lastName = aDecoder.decodeObject(forKey: "lastName") as? String {
            self.lastName = _lastName
        }
        if let _age = aDecoder.decodeObject(forKey: "age") as? String {
            self.age = _age
        }
        if let _phone = aDecoder.decodeObject(forKey: "phone") as? String {
            self.phone = _phone
        }
        if let _address = aDecoder.decodeObject(forKey: "address") as? String {
            self.address = _address
        }
        if let _gender = aDecoder.decodeObject(forKey: "gender") as? String {
            self.gender = _gender
        }
        if let _userName = aDecoder.decodeObject(forKey: "userName") as? String {
            self.userName = _userName
        }
        if let _email = aDecoder.decodeObject(forKey: "email") as? String {
            self.email = _email
        }
        if let _height = aDecoder.decodeObject(forKey: "height") as? String {
            self.height = _height
        }
        if let _weight = aDecoder.decodeObject(forKey: "weight") as? String {
            self.weight = _weight
        }
        if let _build = aDecoder.decodeObject(forKey: "build") as? String {
            self.build = _build
        }
        if let _deviceName = aDecoder.decodeObject(forKey: "deviceName") as? String {
            self.deviceName = _deviceName
        }
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let _name = self.name {
            aCoder.encode(_name, forKey: "name")
        }
        if let _image = self.image {
            aCoder.encode(_image, forKey: "image")
        }
        if let _token = self.token {
            aCoder.encode(_token, forKey: "token")
        }
        if let _firstName = self.firstName {
            aCoder.encode(_firstName, forKey: "firstName")
        }
        if let _lastName = self.lastName {
            aCoder.encode(_lastName, forKey: "lastName")
        }
        if let _age = self.age {
            aCoder.encode(_age, forKey: "age")
        }
        if let _phone = self.phone {
            aCoder.encode(_phone, forKey: "phone")
        }
        if let _address = self.address {
            aCoder.encode(_address, forKey: "address")
        }
        if let _gender = self.gender {
            aCoder.encode(_gender, forKey: "gender")
        }
        if let _userName = self.userName {
            aCoder.encode(_userName, forKey: "userName")
        }
        if let _email = self.email {
            aCoder.encode(_email, forKey: "email")
        }
        if let _height = self.height {
            aCoder.encode(_height, forKey: "height")
        }
        if let _weight = self.weight {
            aCoder.encode(_weight, forKey: "weight")
        }
        if let _build = self.build {
            aCoder.encode(_build, forKey: "build")
        }
        if let _deviceName = self.deviceName {
            aCoder.encode(_deviceName, forKey: "deviceName")
        }
    }
    
    /*
     
     defaults write com.apple.finder AppleShowAllFiles NO
     
     */
    
    static func GetUserFromDictionary(_ userDic: [String: AnyObject]) -> User {
        let user: User = User()
        user.name = (userDic["firstname"]! as! String) + " " + (userDic["lastname"]! as! String)
        
        user.firstName = userDic["firstname"]! as? String
        user.lastName = userDic["lastname"]! as? String
        user.age = userDic["age"]! as? String
        user.phone = userDic["phone"]! as? String
        user.address = userDic["address"]! as? String
        user.gender = userDic["gender"]! as? String
        user.token = userDic["oauth_token"]! as? String
        user.userName = userDic["username"]! as? String
        user.email = userDic["email"]! as? String
        user.height = userDic["height"]! as? String
        user.weight = userDic["weight"]! as? String
        user.build = userDic["build"]! as? String
        
        if let url = URL(string: userDic["profile_pic"]! as! String) {
            do {
                var response: URLResponse?
                let data = try NSURLConnection.sendSynchronousRequest(URLRequest(url: url), returning: &response)
                if let imageData = data as Data? {
                    user.image = NSImage(data: imageData)
                }
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
        
        return user
    }
}
