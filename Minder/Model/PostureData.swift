//
//  DeviceData.swift
//  Minder
//
//  Created by Abdul on 9/21/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Foundation

class PostureData: NSObject {
    var dataType: Int = 0
    var y: CGFloat = 0
    var z: CGFloat = 0
    var state: Int = 0
    var time: String = ""
    
    override init() {
        
    }    
    required init(coder aDecoder: NSCoder) {
        if let _dataType = aDecoder.decodeObject(forKey: "dataType") as? Int {
            self.dataType = _dataType
        }
        if let _y = aDecoder.decodeObject(forKey: "y") as? CGFloat {
            self.y = _y
        }
        if let _z = aDecoder.decodeObject(forKey: "z") as? CGFloat {
            self.z = _z
        }
        if let _state = aDecoder.decodeObject(forKey: "state") as? Int {
            self.state = _state
        }
        if let _time = aDecoder.decodeObject(forKey: "time") as? String {
            self.time = _time
        }
    }
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let _dataType = self.dataType as? Int {
            aCoder.encode(_dataType, forKey: "dataType")
        }
        if let _y = self.y as? CGFloat {
            aCoder.encode(_y, forKey: "y")
        }
        if let _z = self.z as? CGFloat {
            aCoder.encode(_z, forKey: "z")
        }
        if let _state = self.state as? Int {
            aCoder.encode(_state, forKey: "state")
        }
        if let _time = self.time as? String {
            aCoder.encode(_time, forKey: "time")
        }
    }
    
    static func GetDictionaryFromDeviceData(_ epochTimeInterval:String, positionData: PostureData) -> Dictionary<String, String> {
        var epochTimeInterval = epochTimeInterval
        if epochTimeInterval == "" {
            epochTimeInterval = String(Date().timeIntervalSince1970)
            epochTimeInterval = epochTimeInterval.substring(to: (epochTimeInterval.range(of: ".")?.lowerBound)!)
        }
        let params: Dictionary<String, String> = ["t": String(positionData.time), "oauth_token": MinderManager.sharedInstance.userLogged!.token!, "is_meditation": String(positionData.dataType.hashValue), "y": String(describing: positionData.y), "z": String(describing: positionData.z), "state": String(positionData.state), "epoch_time": epochTimeInterval]
        return params
    }
    static func GetDictionaryArrayFromDeviceDataArray(_ postureDatas: [PostureData]) -> Array<Dictionary<String, String>> {
        var epochTimeInterval: String = String(Date().timeIntervalSince1970)
        epochTimeInterval = epochTimeInterval.substring(to: (epochTimeInterval.range(of: ".")?.lowerBound)!)
        var array: Array<Dictionary<String, String>> = Array<Dictionary<String, String>>()
        for dData in postureDatas {
            array.append(GetDictionaryFromDeviceData(epochTimeInterval, positionData: dData))
        }
        return array
    }
    static func GetDeviceDataFromJsonData(_ json: Dictionary<String, String>) -> PostureData {
        let dData: PostureData = PostureData()
        if let n = NumberFormatter().number(from: json["y"]!) {
            dData.y = CGFloat(n)
        }
        if let n = NumberFormatter().number(from: json["z"]!) {
            dData.z = CGFloat(n)
        }
        dData.state = Int(json["state"]!)!
        dData.time = json["t"]!
        return dData
    }
    static func GetDeviceDataArrayFromJsonArray(_ jsonArray: Array<Dictionary<String, String>>) -> [PostureData] {
        var dDatas: [PostureData] = [PostureData]()
        for json in jsonArray {
            dDatas.append(GetDeviceDataFromJsonData(json))
        }
        return dDatas
    }
}
