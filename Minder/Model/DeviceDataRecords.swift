//
//  DeviceDataRecords.swift
//  Minder
//
//  Created by Abdul on 10/25/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Foundation

class DeviceDataRecords: NSObject {
    var deviceData: [DeviceData] = [DeviceData]()
    var duration: String = ""
}
