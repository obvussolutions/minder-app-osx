//
//  AccelData.swift
//  Minder
//
//  Created by Abdul on 8/11/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa

class AccelData: NSObject {
    var x: CGFloat = 0
    var y: CGFloat = 0
    var z: CGFloat = 0
    var state: Int = 0
    var time: String = ""
    
    override init() {
        
    }
    required init(coder aDecoder: NSCoder) {
        if let _x = aDecoder.decodeObject(forKey: "x") as? CGFloat {
            self.x = _x
        }
        if let _y = aDecoder.decodeObject(forKey: "y") as? CGFloat {
            self.y = _y
        }
        if let _z = aDecoder.decodeObject(forKey: "z") as? CGFloat {
            self.z = _z
        }
        if let _state = aDecoder.decodeObject(forKey: "state") as? Int {
            self.state = _state
        }
        if let _time = aDecoder.decodeObject(forKey: "time") as? String {
            self.time = _time
        }
    }
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let _x = self.x as? CGFloat {
            aCoder.encode(_x, forKey: "x")
        }
        if let _y = self.y as? CGFloat {
            aCoder.encode(_y, forKey: "y")
        }
        if let _z = self.z as? CGFloat {
            aCoder.encode(_z, forKey: "z")
        }
        if let _state = self.state as? Int {
            aCoder.encode(_state, forKey: "state")
        }
        if let _time = self.time as? String {
            aCoder.encode(_time, forKey: "time")
        }
    }

}
