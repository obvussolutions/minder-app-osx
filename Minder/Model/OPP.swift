//
//  Zone.swift
//  Minder
//
//  Created by Abdul on 10/4/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class OPP: NSObject {
    var title: String = ""
    
    var transperancy: Float = 0.33
    var sensitivityPosture: Float = 5
    var sensitivityRotation: Float = 1
    var isSeatBack: Int = 0
    var difficultySeatback: Float = 50
    var notificationDevice: Int = 0
    
    var interval1YellowVibrate: Int = 3000
    var interval2YellowVibrate: Int = 3000
    var vibrateYellow: Int = 1
    var volumeYellowMute: Bool = false
    var interval1YellowVolume: Int = 3000
    var interval2YellowVolume: Int = 3000
    var volumeYellow: Float = 0.33
    var difficultyYellow: Float = 50
    
    var interval1RedVibrate: Int = 1000
    var interval2RedVibrate: Int = 1000
    var vibrateRed:Int = 1
    var volumeRedMute: Bool = false
    var interval1RedVolume: Int = 1000
    var interval2RedVolume: Int = 1000
    var volumeRed:Float = 0.33
    var difficultyRed: Float = 50
    var yellowIntensity:Float = 1
    var yellowHapticValue : Int = 1
    var redHapticValue : Int = 1
    var yellowSoundValue : Int = 1
    var redSoundValue : Int = 1
    
    var offsetPoint: CGPoint = CGPoint.zero
    //var offsetData: AccelData = AccelData()
    var offsetDeviceCordinate: Coordinate = .pp
    
    var isWidgetTimerHidden: Bool = false
    var isWidgetShoulderHidden: Bool = false
    var isWidgetHRHidden: Bool = false
    
    override init() {}
    
    required init(coder aDecoder: NSCoder) {
        if let _title = aDecoder.decodeObject(forKey: "title") as? String {
            self.title = _title
        }
        
        if let _transperancy = aDecoder.decodeFloat(forKey: "transperancy") as? Float {
            self.transperancy = _transperancy
        }
        if let _sensitivityPosture = aDecoder.decodeFloat(forKey: "sensitivityPosture") as? Float {
            self.sensitivityPosture = _sensitivityPosture
        }
        if let _sensitivityRotation = aDecoder.decodeFloat(forKey: "sensitivityRotation") as? Float {
            self.sensitivityRotation = _sensitivityRotation
        }
        if let _isSeatBack = aDecoder.decodeInteger(forKey: "isSeatBack") as? Int {
            self.isSeatBack = _isSeatBack//_isSeatBack ankush change (it should always remain off while starting)
        }
        if let _difficultySeatback = aDecoder.decodeFloat(forKey: "difficultySeatback") as? Float {
            self.difficultySeatback = _difficultySeatback
        }
        if let _notificationDevice = aDecoder.decodeInteger(forKey: "notificationDevice") as? Int {
            self.notificationDevice = _notificationDevice
        }
        
        if let _interval1YellowVibrate = aDecoder.decodeInteger(forKey: "interval1YellowVibrate") as? Int {
            self.interval1YellowVibrate = _interval1YellowVibrate
        }
        if let _interval2YellowVibrate = aDecoder.decodeInteger(forKey: "interval2YellowVibrate") as? Int {
            self.interval2YellowVibrate = _interval2YellowVibrate
        }
        if let _vibrateYellow = aDecoder.decodeInteger(forKey: "vibrateYellow") as? Int {
            self.vibrateYellow = _vibrateYellow
        }
        if let _volumeYellowMute = aDecoder.decodeBool(forKey: "volumeYellowMute") as? Bool {
            self.volumeYellowMute = _volumeYellowMute
        }
        if let _interval1YellowVolume = aDecoder.decodeInteger(forKey: "interval1YellowVolume") as? Int {
            self.interval1YellowVolume = _interval1YellowVolume
        }
        if let _interval2YellowVolume = aDecoder.decodeInteger(forKey: "interval2YellowVolume") as? Int {
            self.interval2YellowVolume = _interval2YellowVolume
        }
        if let _volumeYellow = aDecoder.decodeFloat(forKey: "volumeYellow") as? Float {
            self.volumeYellow = _volumeYellow
        }
        if let _difficultyYellow = aDecoder.decodeFloat(forKey: "difficultyYellow") as? Float {
            self.difficultyYellow = _difficultyYellow
        }
        
        if let _interval1RedVibrate = aDecoder.decodeInteger(forKey: "interval1RedVibrate") as? Int {
            self.interval1RedVibrate = _interval1RedVibrate
        }
        if let _interval2RedVibrate = aDecoder.decodeInteger(forKey: "interval2RedVibrate") as? Int {
            self.interval2RedVibrate = _interval2RedVibrate
        }
        if let _vibrateRed = aDecoder.decodeInteger(forKey: "vibrateRed") as? Int {
            self.vibrateRed = _vibrateRed
        }
        if let _volumeRedMute = aDecoder.decodeBool(forKey: "volumeRedMute") as? Bool {
            self.volumeRedMute = _volumeRedMute
        }
        if let _interval1RedVolume = aDecoder.decodeInteger(forKey: "interval1RedVolume") as? Int {
            self.interval1RedVolume = _interval1RedVolume
        }
        if let _interval2RedVolume = aDecoder.decodeInteger(forKey: "interval2RedVolume") as? Int {
            self.interval2RedVolume = _interval2RedVolume
        }
        if let _volumeRed = aDecoder.decodeFloat(forKey: "volumeRed") as? Float {
            self.volumeRed = _volumeRed
        }
        if let _difficultyRed = aDecoder.decodeFloat(forKey: "difficultyRed") as? Float {
            self.difficultyRed = _difficultyRed
        }
        
        if let _offsetPoint = aDecoder.decodePoint(forKey: "offsetPoint") as? CGPoint {
            self.offsetPoint = _offsetPoint
        }
        
        if let _yellowIntensity = aDecoder.decodeFloat(forKey: "yellowIntensity") as? Float {
            self.yellowIntensity = _yellowIntensity
        }
        if let _yellowHapticValue = aDecoder.decodeInteger(forKey: "yellowHapticValue") as? Int {
            self.yellowHapticValue = _yellowHapticValue
        }
        if let _redHapticValue = aDecoder.decodeInteger(forKey: "redHapticValue") as? Int {
            self.redHapticValue = _redHapticValue
        }
        if let _yellowSoundValue = aDecoder.decodeInteger(forKey: "yellowSoundValue") as? Int {
            self.yellowSoundValue = _yellowSoundValue
        }
        if let _redSoundValue = aDecoder.decodeInteger(forKey: "redSoundValue") as? Int {
            self.redSoundValue = _redSoundValue
        }
        if let _isWidgetTimerHidden = aDecoder.decodeBool(forKey: "isWidgetTimerHidden") as? Bool {
            self.isWidgetTimerHidden = _isWidgetTimerHidden
        }
        if let _isWidgetShoulderHidden = aDecoder.decodeBool(forKey: "isWidgetShoulderHidden") as? Bool {
            self.isWidgetShoulderHidden = _isWidgetShoulderHidden
        }
        if let _isWidgetHRHidden = aDecoder.decodeBool(forKey: "isWidgetHRHidden") as? Bool {
            self.isWidgetHRHidden = _isWidgetHRHidden
        }

    }
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let _title = self.title as? String {
            aCoder.encode(_title, forKey: "title")
        }
        
        if let _transperancy = self.transperancy as? Float {
            aCoder.encode(_transperancy, forKey: "transperancy")
        }
        if let _sensitivityPosture = self.sensitivityPosture as? Float {
            aCoder.encode(_sensitivityPosture, forKey: "sensitivityPosture")
        }
        if let _sensitivityRotation = self.sensitivityRotation as? Float {
            aCoder.encode(_sensitivityRotation, forKey: "sensitivityRotation")
        }
        if let _isSeatBack = self.isSeatBack as? Int {
            aCoder.encode(_isSeatBack, forKey: "isSeatBack")
        }
        if let _difficultySeatback = self.difficultySeatback as? Float {
            aCoder.encode(_difficultySeatback, forKey: "difficultySeatback")
        }
        if let _notificationDevice = self.notificationDevice as? Int {
            aCoder.encode(_notificationDevice, forKey: "notificationDevice")
        }
        
        if let _interval1YellowVibrate = self.interval1YellowVibrate as? Int {
            aCoder.encode(_interval1YellowVibrate, forKey: "interval1YellowVibrate")
        }
        if let _interval2YellowVibrate = self.interval2YellowVibrate as? Int {
            aCoder.encode(_interval2YellowVibrate, forKey: "interval2YellowVibrate")
        }
        if let _vibrateYellow = self.vibrateYellow as? Int {
            aCoder.encode(_vibrateYellow, forKey: "vibrateYellow")
        }
        if let _volumeYellowMute = self.volumeYellowMute as? Bool {
            aCoder.encode(_volumeYellowMute, forKey: "volumeYellowMute")
        }
        if let _interval1YellowVolume = self.interval1YellowVolume as? Int {
            aCoder.encode(_interval1YellowVolume, forKey: "interval1YellowVolume")
        }
        if let _interval2YellowVolume = self.interval2YellowVolume as? Int {
            aCoder.encode(_interval2YellowVolume, forKey: "interval2YellowVolume")
        }
        if let _volumeYellow = self.volumeYellow as? Float {
            aCoder.encode(_volumeYellow, forKey: "volumeYellow")
        }
        if let _difficultyYellow = self.difficultyYellow as? Float {
            aCoder.encode(_difficultyYellow, forKey: "difficultyYellow")
        }
        
        if let _interval1RedVibrate = self.interval1RedVibrate as? Int {
            aCoder.encode(_interval1RedVibrate, forKey: "interval1RedVibrate")
        }
        if let _interval2RedVibrate = self.interval2RedVibrate as? Int {
            aCoder.encode(_interval2RedVibrate, forKey: "interval2RedVibrate")
        }
        if let _vibrateRed = self.vibrateRed as? Int {
            aCoder.encode(_vibrateRed, forKey: "vibrateRed")
        }
        if let _volumeRedMute = self.volumeRedMute as? Bool {
            aCoder.encode(_volumeRedMute, forKey: "volumeRedMute")
        }
        if let _interval1RedVolume = self.interval1RedVolume as? Int {
            aCoder.encode(_interval1RedVolume, forKey: "interval1RedVolume")
        }
        if let _interval2RedVolume = self.interval2RedVolume as? Int {
            aCoder.encode(_interval2RedVolume, forKey: "interval2RedVolume")
        }
        if let _volumeRed = self.volumeRed as? Float {
            aCoder.encode(_volumeRed, forKey: "volumeRed")
        }
        if let _difficultyRed = self.difficultyRed as? Float {
            aCoder.encode(_difficultyRed, forKey: "difficultyRed")
        }
        
        if let _offsetPoint = self.offsetPoint as? CGPoint {
            aCoder.encode(_offsetPoint, forKey: "offsetPoint")
        }
        
        if let _yellowIntensity = self.yellowIntensity as? Float {
            aCoder.encode(_yellowIntensity, forKey: "yellowIntensity")
        }
        if let _yellowHapticValue = self.yellowHapticValue as? Int {
            aCoder.encode(_yellowHapticValue, forKey: "yellowHapticValue")
        }
        if let _redHapticValue = self.redHapticValue as? Int {
            aCoder.encode(_redHapticValue, forKey: "redHapticValue")
        }
        if let _yellowSoundValue = self.yellowSoundValue as? Int {
            aCoder.encode(_yellowSoundValue, forKey: "yellowSoundValue")
        }
        if let _redSoundValue = self.redSoundValue as? Int {
            aCoder.encode(_redSoundValue, forKey: "redSoundValue")
        }
        if let _isWidgetTimerHidden = self.isWidgetTimerHidden as? Bool {
            aCoder.encode(_isWidgetTimerHidden, forKey: "isWidgetTimerHidden")
        }
        if let _isWidgetShoulderHidden = self.isWidgetShoulderHidden as? Bool {
            aCoder.encode(_isWidgetShoulderHidden, forKey: "isWidgetShoulderHidden")
        }
        if let _isWidgetHRHidden = self.isWidgetHRHidden as? Bool {
            aCoder.encode(_isWidgetHRHidden, forKey: "isWidgetHRHidden")
        }
    }
}
