//
//  Recording+CoreDataProperties.swift
//  Minder
//
//  Created by Febin on 4/27/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Foundation
import CoreData


extension Recording {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Recording> {
        return NSFetchRequest<Recording>(entityName: "Recording");
    }

    @NSManaged public var user_email: String?
    @NSManaged public var record_system_name: String?
    @NSManaged public var record_user_set_name: String?
    @NSManaged public var record_type_name: String?

}
