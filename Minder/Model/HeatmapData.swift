//
//  HeatmapData.swift
//  Minder
//
//  Created by Abdul on 9/24/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class HeatmapData: NSObject {
    var point: String = ""
    var rect: NSRect = NSRect()
    var weight: CGFloat = 0
}
