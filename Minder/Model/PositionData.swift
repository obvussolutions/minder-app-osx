//
//  PositionData.swift
//  Minder
//
//  Created by Abdul on 9/21/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Foundation
import CoreData

class PositionData: NSObject {
    var green: Int = 0
    var yellow: Int = 0
    var red: Int = 0
    var time: String = ""
    
    override init(){}
    
    required init(coder aDecoder: NSCoder) {
        if let _green = aDecoder.decodeObject(forKey: "green") as? Int {
            self.green = _green
        }
        if let _yellow = aDecoder.decodeObject(forKey: "yellow") as? Int {
            self.yellow = _yellow
        }
        if let _red = aDecoder.decodeObject(forKey: "red") as? Int {
            self.red = _red
        }
        if let _time = aDecoder.decodeObject(forKey: "time") as? String {
            self.time = _time
        }
    }
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let _green = self.green as? Int {
            aCoder.encode(_green, forKey: "green")
        }
        if let _yellow = self.yellow as? Int {
            aCoder.encode(_yellow, forKey: "yellow")
        }
        if let _red = self.red as? Int {
            aCoder.encode(_red, forKey: "red")
        }
        if let _time = self.time as? String {
            aCoder.encode(_time, forKey: "time")
        }
    }
    
    static func GetDictionaryFromPositionData(_ positionData: PositionData) -> Dictionary<String, String> {
        let params: Dictionary<String, String> = ["t": "", "oauth_token": "", "y": "", "z": "", "state": "", "epoch_time": ""]
        return params
    }
    static func GetDictionaryArrayFromPositionDataArray(_ positionDatas: [PositionData]) -> NSMutableArray {
        let array: NSMutableArray = NSMutableArray()
        for pData in positionDatas {
            array.add(GetDictionaryFromPositionData(pData))
        }
        return array
    }
}
