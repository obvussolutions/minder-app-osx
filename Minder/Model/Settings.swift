//
//  Settings.swift
//  Minder
//
//  Created by Abdul on 9/22/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class Settings: NSObject {
    var lastUpdateDeviceData: String? = "1970-01-01"
    var lastUpdateActivities: String? = "1970-01-01"
    var currentWeekFirstDate: String? = Date.GetDateString(NSDate().startOfWeek as Date, format: "yyyy-MM-dd")
    var skipCalibration: Int? = 0
    
    var OPPs: [OPP] = [OPP]()
    var OPPSelected: Int? = 0
    
    var breaks: [MeditationBreak] = [MeditationBreak]()
    var breakSelected: Int? = 0
    
    var hrMonitorSelected: Int? = 1
    var hrTotal: Float? = 0
    var hrCount: Int? = 0
    
    override init() {
        lastUpdateActivities = Date.GetDateString(Date(), format: "yyyy-MM-dd")
    }
    
    required init(coder aDecoder: NSCoder) {
        if let _lastUpdateDeviceData = aDecoder.decodeObject(forKey: "lastUpdateDeviceData") {
            self.lastUpdateDeviceData = _lastUpdateDeviceData as? String
        }
        if let _lastUpdateActivities = aDecoder.decodeObject(forKey: "lastUpdateActivities") {
            self.lastUpdateActivities = _lastUpdateActivities as? String
        }
        if let _skipCalibration = aDecoder.decodeInteger(forKey: "skipCalibration") as? Int {
            self.skipCalibration = _skipCalibration as? Int
        }
        if let _OPPs = aDecoder.decodeObject(forKey: "OPPs") {
            self.OPPs = (_OPPs as? [OPP])!
        }
        if let _OPPSelected = aDecoder.decodeInteger(forKey: "OPPSelected") as? Int {
            self.OPPSelected =  _OPPSelected as? Int
        }
        if let _breaks = aDecoder.decodeObject(forKey: "breaks") {
            self.breaks = (_breaks as? [MeditationBreak])!
        }
        if let _breakSelected = aDecoder.decodeInteger(forKey: "breakSelected") as? Int {
            self.breakSelected =  _breakSelected as? Int
        }
        if let _hrMonitorSelected = aDecoder.decodeInteger(forKey: "hrMonitorSelected") as? Int {
            self.hrMonitorSelected =  _hrMonitorSelected as? Int
        }
        if let _hrTotal = aDecoder.decodeFloat(forKey: "hrTotal") as? Float {
            self.hrTotal =  _hrTotal as? Float
        }
        if let _hrCount = aDecoder.decodeInteger(forKey: "hrCount") as? Int {
            self.hrCount =  _hrCount as? Int
        }
    }
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let _lastUpdateDeviceData = self.lastUpdateDeviceData {
            aCoder.encode(_lastUpdateDeviceData, forKey: "lastUpdateDeviceData")
        }
        if let _lastUpdateActivities = self.lastUpdateActivities {
            aCoder.encode(_lastUpdateActivities, forKey: "lastUpdateActivities")
        }
        if let _skipCalibration = self.skipCalibration {
            aCoder.encode(_skipCalibration, forKey: "skipCalibration")
        }
       
        aCoder.encode(self.OPPs, forKey: "OPPs")
       
        if let _OPPSelected = self.OPPSelected {
            aCoder.encode(_OPPSelected, forKey: "OPPSelected")
        }
        
        aCoder.encode(self.breaks, forKey: "breaks")
        
        if let _breakSelected = self.breakSelected {
            aCoder.encode(_breakSelected, forKey: "breakSelected")
        }
        if let _hrMonitorSelected = self.hrMonitorSelected {
            aCoder.encode(_hrMonitorSelected, forKey: "hrMonitorSelected")
        }
        if let _hrTotal = self.hrTotal {
            aCoder.encode(_hrTotal, forKey: "hrTotal")
        }
        if let _hrCount = self.hrCount {
            aCoder.encode(_hrCount, forKey: "hrCount")
        }
    }
}
