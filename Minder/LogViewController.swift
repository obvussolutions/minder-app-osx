//
//  LogViewController.swift
//  Minder
//
//  Created by Abdul on 7/4/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa

class LogViewController: NSViewController {
    @IBOutlet var textLog: NSTextView!
    @IBOutlet weak var buttonEnableLog: NSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            let logString = try NSString(contentsOf: URL.init(fileURLWithPath: MinderManager.sharedInstance.dLogPath), encoding: String.Encoding.utf8.rawValue) as String
            
            textLog.string = logString
            
            textLog.scrollToEndOfDocument(self)
        }
        catch { /* error handling here */ }
        
        if MinderManager.sharedInstance.isLogEnabled {
            buttonEnableLog.title = "Disable Log"
        } else {
            buttonEnableLog.title = "Enable Log"
        }
        
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.UpdateTextView), userInfo: nil, repeats: true)
    }
    
    func UpdateTextView() {
        if MinderManager.sharedInstance.isLogEnabled {
        do {
            let logString = try NSString(contentsOf: URL.init(fileURLWithPath: MinderManager.sharedInstance.dLogPath), encoding: String.Encoding.utf8.rawValue) as String
            
            textLog.string = logString
        }
        catch { /* error handling here */ }
        }
    }
    
    func updateLog(log: String) {
        var scrollToEnd = false
        if (textLog.enclosingScrollView?.verticalScroller?.floatValue == 1.0) {
            scrollToEnd = true
        }
        
        textLog.string?.append("\r\n" + log)
        
        if (scrollToEnd) {
            //textLog.scrollToEndOfDocument(self)
        }
    }
    @IBAction func EnableLogTapped(_ sender: Any) {
        if MinderManager.sharedInstance.isLogEnabled {
            buttonEnableLog.title = "Enable Log"
            MinderManager.sharedInstance.isLogEnabled = false
        } else {
            buttonEnableLog.title = "Disable Log"
            MinderManager.sharedInstance.isLogEnabled = true
        }
    }
    @IBAction func SaveLogTapped(_ sender: Any) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let fileName = formatter.string(from: date)
        
        let savePanel = NSSavePanel()
        //savePanel.setTitleWithRepresentedFilename(recordingSelected.)
        savePanel.title = "Save Log"
        savePanel.canCreateDirectories = true
        savePanel.showsHiddenFiles = true
        savePanel.level = Int(CGShieldingWindowLevel())
        savePanel.nameFieldStringValue = "Minder Log \(fileName).txt"
        savePanel.begin { (result) -> Void in
            if result == NSFileHandlingPanelOKButton
            {
                do {
                    try FileManager.default.copyItem(at: URL(fileURLWithPath: MinderManager.sharedInstance.dLogPath), to: savePanel.url!)
                } catch let error as NSError {
                    let myPopup: NSAlert = NSAlert()
                    myPopup.messageText = "Log saving failed"
                    myPopup.informativeText = "Ooops! Something went wrong:\r\n\(error)"
                    myPopup.alertStyle = .warning
                    myPopup.addButton(withTitle: "OK")
                    myPopup.runModal()
                    NSLog("Ooops! Something went wrong while saving log:\r\n\(error)")
                }
            }
        }
    }
    
}
