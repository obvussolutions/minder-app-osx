//
//  MinderManager.swift
//  Minder
//
//  Created by Abdul on 8/16/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa
import IOBluetoothUI
import AVFoundation

enum DeviceStatusType: Int32 {
    case initializing = 0
    case connecting = 1
    case connected = 2
    case disConnected = 3
}

enum DeviceActionMode {
    case initializinig
    case calibrating
    case broadcasting
}

enum ReplayState {
    case none
    case play
    case pause
}

enum Coordinate: Int32 {
    case pp = 1
    case pn = 2
    case nn = 3
    case np = 4
}

enum HRMonitorType: Int {
    case Off = 0
    case AutoOn = 1
    case Live = 2
}
/*
protocol HRMonitorTypeString  {
    var description: String { get }
}
enum HRMonitorType: Int32, HRMonitorTypeString {
    case Off = 0
    case AutoOn = 1
    case Live = 2
    
    var description: String {
        switch self {
        case .Off:
            return "Off"
        case .AutoOn:
            return "Auto On"
        case .Live:
            return "Live"
        }
    }
}
*/
 
enum RecordType: String {
    case posture = "posture"
    case meditation = "meditation"
}

class MinderManager: NSObject {
    
    var appMode: AppMode = .gadget
    var currentView: ViewType = .dashboard
    
    // Ball size - in percentage of view dimension
    static let percentageBall: CGFloat = 7
    // From where the yellow zone start - in percentage of view size
    static let percentageZoneYellow: CGFloat = 14//36 //14   // 15 zoneYellow -> zoneBad
    // From where the red zone start - in percentage of view size
    static let percentageZoneRed: CGFloat = 21
    // Offset between ball-zone and two zones
    static let percentageOffset: CGFloat = 1
    
    let ballInSeatbackThreshold: Float = 60
    let noOfTimeOutofZoneThreshold: Float = 10 //ankush change
    let timerAfterCalibartionThreshold: Float = 300 //5 min ankush change
    var isTimerStartForCalibrationThreshold : Bool = false
    var isBallGoneOutOfZone : Bool = false
    
    fileprivate var timerAfterCalibartionValue : Float = 0
    fileprivate var noOfTimeOutofZoneValue : Float = 0
    
    var deviceName: String = ""     // ObVus_3CEC1054439A
    var appDirectory: String = ""
    var nsLogPath: String = ""
    var dLogPath: String = ""
    var isLogEnabled:Bool = true
    
    var deviceStatus: DeviceStatusType = DeviceStatusType.initializing
    
    var deviceCharge: Float = 0
    var dataProcessingPaused: Bool = false
    var forceDisconnect: Bool = false
    var isCursorOnGadget : Bool = false
    var isBatteryLowMessageAvailable : Bool = false
    var isNotificationVibrate : Bool = true
    
    var centerDeviceView: NSPoint = NSPoint()
    var centerDeviceViewHeight: CGFloat = CGFloat()
    
    var settings: Settings? = nil
    var userLogged: User? = nil
    var sensorState: SensorStatus? = nil
    
    var isSeatbackEnabled: Bool = false
    
    var calibratedFlag:Bool = false
    var meditationModeFlag:Bool = false // fbn
    var isAppInBackground:Bool = false // Ankush
    var isHrSensorEnable:Bool = true // Ankush
    var isCalibrationDone:Bool = false // Ankush
    
    var isGyroOn = true
    var isGyroFromDevice = true
    
    var isRecording: Bool = false
    var replayState: ReplayState = .none
    var isActivityPlaying: Bool = false
    
    var showPreviousData: Bool = false
    var showPreviousHR: Bool = false
    
    let manager: WebManager = WebManager()
    var imageHeatmap: NSImage? = NSImage(size: NSMakeSize(900, 900))
    
    var prevPitch: CGFloat = 0
    var prevRoll: CGFloat = 0
    var prevYaw: CGFloat = 0
    var pitch: CGFloat = 0
    var roll: CGFloat = 0
    var yaw: CGFloat = 0
    
    var deviceCordinate: Coordinate = .pp
    var deviceUpwards: Bool = true
    
    
    //var blockCancelShowText: dispatch_block_t? = nil
    
    func BackupGyro() {
        prevPitch = pitch
        pitch = 0
        prevRoll = roll
        roll = 0
        prevYaw = yaw
        yaw = 0
    }
    func RestoreGyro() {
        pitch = prevPitch
        roll = prevRoll
        yaw = prevYaw
    }
    
    static let sharedInstance: MinderManager = {
        let instance = MinderManager()
        return instance
    }()
    fileprivate override init() {
        userLogged = User()
        settings = Settings()
        sensorState = SensorStatus()
    }
    
    func LoadSettings() {
        let userDefaults = UserDefaults.standard
        var logString: String = "Settings not found"
        var username: String = "_"
        if !(userLogged?.userName ?? "").isEmpty {
            username = (userLogged?.userName)!
        }
        if let data = userDefaults.object(forKey: "Settings_" + username) as? Data {
            settings = NSKeyedUnarchiver.unarchiveObject(with: data) as? Settings
            logString = "Settings loaded"
        }
        if let data = userDefaults.object(forKey: "sensorState_" + username) as? Data {
            sensorState = NSKeyedUnarchiver.unarchiveObject(with: data) as? SensorStatus
            logString = "Settings loaded"
        }
        NSLog(logString)
    }
    func SaveSettings() {
        let userDefaults = UserDefaults.standard
        var username: String = "_"
        if !(userLogged?.userName ?? "").isEmpty {
            username = (userLogged?.userName)!
            //SetSensorSettings()
        }
        do {
            try userDefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: settings!), forKey: "Settings_" + username)
        } catch {
            NSLog("Settings not saved as exception caught: \(error)")
        }
        
        do {
            try userDefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: sensorState!), forKey: "sensorState_" + username)
        } catch {
            NSLog("Settings not saved as exception caught: \(error)")
        }
        userDefaults.synchronize()
        NSLog("Settings saved")
    }
    func deviceLog(_log: String) {
        if !isLogEnabled {
            return
        }
        
        if MinderManager.sharedInstance.dLogPath != "" {
            var log = ""
            do {
                log = try String(contentsOf: URL(fileURLWithPath: MinderManager.sharedInstance.dLogPath) , encoding: String.Encoding.utf8)
            } catch {}
            
            log += "\r\n[\(Date.GetDateString(Date(), format: "yyyy-MM-dd hh:mm:ss"))] " + _log
            
            do {
                try log.write(to: URL(fileURLWithPath: MinderManager.sharedInstance.dLogPath), atomically: false, encoding: String.Encoding.utf8)
            } catch {}
        }
    }
    
    // MARK: - MinderLib Methods
    var deviceFeaturesEnabled: Bool = false
    weak var delegateDevice: DelegateDeviceHandler? // fbn change
    weak var delegateDebug: DelegateDebugMinder? // ankush change
    var minderlib: MinderLibBLE? = nil
    func initDevice(_ delegate: AnyObject) {
        delegateDevice = delegate as? DelegateDeviceHandler
        delegateDebug = delegate as? DelegateDebugMinder
        minderlib = MinderLibBLE(delegate: self)
        
        if MinderManager.sharedInstance.isLogEnabled {
            MinderManager.sharedInstance.deviceLog(_log:"Device library initialised")
        }
        
        DeviceConnect()
    }
    func initDeviceDebug(_ delegate: AnyObject) {
       delegateDebug = delegate as? DelegateDebugMinder
    }
    func DeviceConnect() {
        if self.minderlib != nil && deviceStatus != .connected {
            print("device name is \(deviceName)")
            minderlib?.connect(deviceName)
            
            deviceFeaturesEnabled = false
            
            if MinderManager.sharedInstance.isLogEnabled {
                MinderManager.sharedInstance.deviceLog(_log:"Device connect request send")
            }
        }
    }
    func DeviceEnableFeatures() {
        if self.minderlib != nil && deviceStatus == .connected {
            
            if (MinderManager.sharedInstance.sensorState?.isHeartRateEnable)!
            {
                self.minderlib?.enableHeartRate()
            }
            else{
                self.minderlib?.disableHeartRate()
            }
            
            if (MinderManager.sharedInstance.sensorState?.isAccelEnable)!
            {
                self.minderlib?.enableAccel()
            }
            else{
                self.minderlib?.disableAccel()
            }
            
            
            deviceFeaturesEnabled = true
            if MinderManager.sharedInstance.isLogEnabled {
                MinderManager.sharedInstance.deviceLog(_log:"Enable feature request send")
            }
        }
    }
    
    func DeviceHeartRateEnable() {
        if self.minderlib != nil && deviceStatus == .connected {
            self.minderlib?.enableHeartRate()
            isHrSensorEnable = true
        }
    }
    func DeviceHeartRateDisable() {
        if self.minderlib != nil && deviceStatus == .connected {
            self.minderlib?.disableHeartRate()
            isHrSensorEnable = false
        }
    }
    func DeviceVibrate() {
        if self.minderlib != nil && deviceStatus == .connected {
            if !(MinderManager.sharedInstance.sensorState?.isLraMotorEnable)!
            {
                return
            }
            DeviceHeartRateDisable()
            self.minderlib?.vibrateDevice()
            let delaySeconds = 2.0
            DispatchQueue.main.asyncAfter(deadline: .now() + delaySeconds) {
                self.DeviceHeartRateEnable()
            }
            if MinderManager.sharedInstance.isLogEnabled {
                MinderManager.sharedInstance.deviceLog(_log:"Device vibrate request send")
            }
        }
    }
    func DeviceDisconnect() {
        if self.minderlib != nil && deviceStatus == .connected {
            if MinderManager.sharedInstance.isLogEnabled {
                MinderManager.sharedInstance.deviceLog(_log:"Device disconnect requested")
            }
            self.minderlib?.disconnect()
        }
    }
    
    // MARK: - API Section
    
    var mainUrl: String = "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/"
    
    // Login information is passed to server and the responce details are saved to local db
    internal func Login(_ username: String, password: String, deviceName: String, completionHandler: @escaping (Bool) -> Void) {
        
        let params: Dictionary<String, String> = ["username": username, "password": password]
        let jsonParam: Array = [params]
        NSLog("Login requested \(jsonParam)")
        
        WebManager.Post(jsonParam, url: mainUrl + "authenticate", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                print("Login \(self.mainUrl)authenticate\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            if data["status"]! as! String == "Success" {
                
                self.userLogged = User.GetUserFromDictionary(data as [String : AnyObject])
                self.userLogged?.deviceName = deviceName
                self.SetDeviceID({_ in})
                self.GetZoneData({ _ in })
                
                let userDefaults = UserDefaults.standard
                userDefaults.set(NSKeyedArchiver.archivedData(withRootObject: self.userLogged!), forKey: "UserDetails")
                userDefaults.synchronize()
                
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        })
    }
    
    // Edit Profile information is passed to server and the responce details are updated on local db
    internal func EditProfile(_ fname: String, lname: String, gender: String, age: String, phone: String, address: String, height: String, weight: String, build: String, image: NSImage, completionHandler: @escaping (Bool) -> Void) {
        
        let cgImgRef = image.cgImage(forProposedRect: nil, context: nil, hints: nil)
        let bmpImgRef = NSBitmapImageRep(cgImage: cgImgRef!)
        let pngData = bmpImgRef.representation(using: NSBitmapImageFileType.PNG, properties: [:])
        let imageString = "data:image/png;base64," + pngData!.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithLineFeed)
        
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "firstname": fname, "lastname": lname, "gender": gender, "age": age, "phone": phone, "address": address, "height": height, "weight": weight, "build": build, "profile_pic": imageString]
        let jsonParam: Array = [params]
        NSLog("EditProfile requested \(jsonParam)")
        
        WebManager.Post(jsonParam, url: mainUrl + "updateprofile", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("EditProfile \(self.mainUrl)updateprofile\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
                
            }
            // */
            if data["status"]! as! String == "Success" {
                self.userLogged?.name = fname + " " + lname
                self.userLogged?.firstName = fname
                self.userLogged?.lastName = lname
                self.userLogged?.gender = gender
                self.userLogged?.age = age
                self.userLogged?.phone = phone
                self.userLogged?.address = address
                self.userLogged?.height = height
                self.userLogged?.weight = weight
                self.userLogged?.build = build
                self.userLogged?.image = image
                
                let userDefaults = UserDefaults.standard
                userDefaults.set(NSKeyedArchiver.archivedData(withRootObject: self.userLogged!), forKey: "UserDetails")
                userDefaults.synchronize()
                
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        })
    }
    
    // Machine user name is send to server to set as Device ID
    internal func SetDeviceID(_ completionHandler: @escaping (Bool) -> Void) {
        let deviceId: String = NSUserName()
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "device_id": deviceId]
        let jsonParam: Array = [params]
        
        WebManager.Post(jsonParam, url: mainUrl + "setdeviceid", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("SetDeviceID \(self.mainUrl)setdeviceid\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            if data["status"]! as! String == "Success" {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        })
    }
    
    // To get user position details
    internal func GetZoneData(_ completionHandler: @escaping (Bool) -> Void) {
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "start_datetime": (settings?.lastUpdateDeviceData)!, "end_datetime": Date.GetDateString(Date(), format: "yyyy-MM-dd")]
        let jsonParam: Array = [params]
        
        WebManager.Post(jsonParam, url: mainUrl + "getdevicelog", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("GetDeviceData \(self.mainUrl)getdevicelog\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            if data["status"]! as! String == "Success" {
                self.green = (data["green"]! as? Int)!
                self.yellow = (data["yellow"]! as? Int)!
                self.red = (data["red"]! as? Int)!
                
                // Commented to show over-all percentage
                //self.settings?.lastUpdateDeviceData = endDate
                //self.SaveSettings()
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        })
    }
    internal func GetZoneData(_ sdate: String, edate: String, completionHandler: @escaping (Dictionary<String, AnyObject>) -> Void) {
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "start_datetime": sdate, "end_datetime": edate]
        let jsonParam: Array = [params]
        
        WebManager.Post(jsonParam, url: mainUrl + "getdevicelog", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("GetDeviceData \(self.mainUrl)getdevicelog\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            completionHandler(data as Dictionary<String, AnyObject>)
        })
    }
    internal func GetHRAverage(_ year: String, month: String, week: String, day: String, completionHandler: @escaping (Dictionary<String, AnyObject>) -> Void) {
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "year": year, "month": month, "day": day]
        let jsonParam: Array = [params]
        
        WebManager.Post(jsonParam, url: mainUrl + "getUserHeartRates", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                print("GetHRAverage \(self.mainUrl)getUserHeartRates\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
                
            }
            // */
            completionHandler(data as Dictionary<String, AnyObject>)
        })
    }
    
    // To get user position details
    internal func GetDeviceData(_ sdate: String, edate: String, completionHandler: @escaping (Dictionary<String, AnyObject>) -> Void) {
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "start": sdate, "end": edate]
        let jsonParam: Array = [params]
        
        WebManager.Post(jsonParam, url: mainUrl + "getrawdata", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("GetDeviceData \(self.mainUrl)getdevicelog\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            
            completionHandler(data as Dictionary<String, AnyObject>)
        })
    }
    internal func sendNotification(_ username: String, messageBody: String, vibration: String, sound: String, title: String, completionHandler: @escaping (Bool) -> Void) {
        
        let params: Dictionary<String, String> = ["username":username,"messageBody":messageBody,"vibration":vibration,"sound":sound,"title":title,"type" : "n"]//["username": username, "password": password]
        let jsonParam: Array = [params]
        print("send not \(jsonParam)")
        
        WebManager.Post(jsonParam, url: "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/sendNotification", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                print("Login \(self.mainUrl)authenticate\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
//            if data["status"]! as! String == "Success" {
//                
//                completionHandler(true)
//            } else {
//                completionHandler(false)
//            }
        })
    }
    internal func sendNotificationToken(_ username: String, password: String, token: String, deviceType: String, completionHandler: @escaping (Bool) -> Void) {
        
        let params: Dictionary<String, String> = ["username":username,"password":password,"token":token,"deviceType":deviceType]//["username": username, "password": password]
        let jsonParam: Array = [params]
        print("send not token \(jsonParam)")
        
        WebManager.Post(jsonParam, url: "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/authenticateToken", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                print("Login \(self.mainUrl)authenticate\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            if data["status"]! as! String == "Success" {
                
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        })
    }

    // To update server with information
    internal func SetDeviceData(_ deviceDatas: [PostureData], completionHandler: @escaping (Bool) -> Void) {
        let jsonParam: Array = PostureData.GetDictionaryArrayFromDeviceDataArray(deviceDatas)
        WebManager.Post(jsonParam, url: mainUrl + "add", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("SetDeviceData \(self.mainUrl)add\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            if data["status"]! as! String == "Success" {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        })
    }
    
    // To update server with attributes
    internal func PostUserAttributes(_ type: String, value: String, timestamp: String, completionHandler: @escaping (Bool) -> Void) {
        var epochTime: String = String(Date().timeIntervalSince1970)
        epochTime = epochTime.substring(to: (epochTime.range(of: ".")?.lowerBound)!)
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "attribute_type": type, "attribute_value": value, "timestamp": timestamp, "epoch_time": epochTime]
        let jsonParam: Array = [params]
        
        WebManager.Post(jsonParam, url: mainUrl + "saveUserAttributes", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("SaveUserAttributes \(self.mainUrl)saveUserAttributes\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            if data["status"]! as! String == "Success" {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        })
    }
    
    // To update exersice status on server
    internal func SetExerciseStatus(_ program_id: Int, schedule_date_id: Int, schedule_id: Int, modified_id: Int, result: String, started_time: String, completed_time: String, completionHandler: (Bool) -> Void) {
        
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "program_id": "\(program_id)", "schedule_date_id": "\(schedule_date_id)", "schedule_id": "\(schedule_id)", "modified_id": "\(modified_id)", "result": result, "started_time": started_time, "completed_time": completed_time]
        let jsonParam: Array = [params]
        
        WebManager.Post(jsonParam, url: mainUrl + "setexercisestatus", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("SetExerciseStatus \(self.mainUrl)setexercisestatus\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            
            if data["status"]! as! String == "Success" {
                let appDelegate = NSApplication.shared().delegate as! AppDelegate
                let managedContext = appDelegate.managedObjectContext
                
                let fetchRequestActivity = NSFetchRequest<NSFetchRequestResult>(entityName: "Activity")
                fetchRequestActivity.predicate = NSPredicate(format: "schedule_id == %d AND schedule_date_id == %d", schedule_id, schedule_date_id)
                
                var activity: NSManagedObject?
                do {
                    let _activity = try managedContext.fetch(fetchRequestActivity)
                    if let _activity = _activity as? [NSManagedObject] {
                        if _activity.count > 0 {
                            activity = _activity[0]
                            activity!.setValue(1, forKey:"exercise_status")
                            
                            try managedContext.save()
                        }
                    }
                } catch {
                    NSLog("Failed to fetch activity: \(error)")
                }
            }
        })
    }
    
    // To update difficulty on server
    internal func SetSensorSettings() {
        
        var timeInterval: String = String(Date().timeIntervalSince1970)
        timeInterval = timeInterval.substring(to: (timeInterval.range(of: ".")?.lowerBound)!)
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "sensor_sensitivity": "\(opp.sensitivityPosture)", "sensor_threshold1": "\(opp.difficultyYellow)", "sensor_threshold2": "\(opp.difficultyRed)", "sensor_leanback": "0", "timestamp": timeInterval]
        let jsonParam: Array = [params]
        
        WebManager.Post(jsonParam, url: mainUrl + "setsensorlevel", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("SetSensorSettings \(self.mainUrl)setsensorlevel\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
        })
    }
    
    // Login information is passed to server and the responce details are saved to local db
    internal func GetActivities(_ completionHandler: @escaping (Bool) -> Void) {
        let tillDate = Date.GetDateString(NSCalendar.current.date(byAdding: .day, value: 7, to: NSDate() as Date)!, format: "yyyy-MM-dd")
        // let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "from_date": (settings?.lastUpdateActivities)!, "till_date": tillDate, "format":"flat"]
        let params: Dictionary<String, String> = ["oauth_token": (userLogged?.token)!, "from_date": (settings?.currentWeekFirstDate)!, "till_date": tillDate, "format":"flat"]
        let jsonParam: Array = [params]
        
        WebManager.Post(jsonParam, url: mainUrl + "activitiesbetweendates", completionHandler:  { data -> Void in
            //*
            do {
                let dataParams: Data = try JSONSerialization.data(withJSONObject: jsonParam, options: .prettyPrinted)
                let dataResponse: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                NSLog("GetActivities \(self.mainUrl)activitiesbetweendates\r\n*****Params*****\r\n\(NSString(data: dataParams, encoding: String.Encoding.utf8.rawValue)! as String)\r\n*****Response*****\r\n\(NSString(data: dataResponse, encoding: String.Encoding.utf8.rawValue)! as String)")
            } catch {
            }
            // */
            
            DispatchQueue.main.async(execute: {
                
                if data["status"]! as! String == "Success" {
                    
                    let appDelegate = NSApplication.shared().delegate as! AppDelegate
                    let managedContext = appDelegate.managedObjectContext
                    
                    if let resultDic = data["result"] as? [AnyObject] {
                        for resultItem in resultDic {
                            let entityActivity =  NSEntityDescription.entity(forEntityName: "Activity", in:managedContext)
                            let fetchRequestActivity = NSFetchRequest<NSFetchRequestResult>(entityName: "Activity")
                            let scheduleId = Int(resultItem["schedule_id"] as! String)
                            let scheduleDateId = Int(resultItem["schedule_date_id"] as! String)
                            fetchRequestActivity.predicate = NSPredicate(format: "schedule_id == %d AND schedule_date_id == %d", scheduleId!, scheduleDateId!)
                            NSLog("Processing: \(scheduleId!):\(scheduleDateId!)")
                            var activity: NSManagedObject?
                            do {
                                let _activity = try managedContext.fetch(fetchRequestActivity)
                                if let _activity = _activity as? [NSManagedObject] {
                                    if _activity.count > 0 {
                                        activity = _activity[0]
                                    }
                                }
                            } catch {
                                NSLog("Failed to fetch activity: \(error)")
                            }
                            
                            if activity == nil {
                                activity = NSManagedObject(entity: entityActivity!, insertInto: managedContext)
                                activity!.setValue(0, forKey:"exercise_status")
                                activity!.setValue(self.userLogged?.email, forKey:"user_email")
                            }
                            
                            activity!.setValue(Int(resultItem["duration"] as! String), forKey:"duration")
                            activity!.setValue(resultItem["end_time"] as! String, forKey:"end_time") // fbn
                            activity!.setValue(resultItem["media_description"] as! String, forKey:"media_description") // fbn
                            activity!.setValue(resultItem["media_title"] as! String, forKey:"media_title") //fbn
                            activity!.setValue(resultItem["media_url"] as! String, forKey:"media_url") // fbn
                            activity!.setValue(0, forKey:"modified_id")
                            activity!.setValue(Int(resultItem["program_id"] as! String), forKey:"program_id")
                            let date = Date.GetDateStringInFormat(resultItem["schedule_date"] as! String, withFormat: "yyyy-MM-dd", toFormat: "MM-dd-yyyy")
                            activity!.setValue(date, forKey:"schedule_date")
                            activity!.setValue(Int(resultItem["schedule_date_id"] as! String), forKey:"schedule_date_id")
                            activity!.setValue(scheduleId, forKey:"schedule_id")
                            //activity!.setValue(resultItem["sensitivity_level"], forKey:"sensitivity_level")
                            activity!.setValue(resultItem["start_time"] as! String, forKey:"start_time")
                            let activityType = resultItem["activity_types"] as! String
                            activity!.setValue(activityType, forKey:"activity_types")
                            
                            if activityType == "Guided Meditation" {
                                activity!.setValue(Int(resultItem["nbreaths"] as! String), forKey:"breathCount")
                                activity!.setValue(Int(resultItem["inhlen"] as! String), forKey:"inhaleLen")
                                activity!.setValue(Int(resultItem["inhret"] as! String), forKey:"inhaleRetail")
                                activity!.setValue(Int(resultItem["exlen"] as! String), forKey:"exhaleLen")
                                activity!.setValue(Int(resultItem["exret"] as! String), forKey:"exhaleRetail")
                                
                                print("\(activityType) :: \(scheduleId!) :: \(resultItem["nbreaths"])")
                            }
                            
                            /*activity!.setValue(resultItem["media_id"] as? Int, forKey:"media_id")
                             activity!.setValue(resultItem["media_type"] as! String, forKey:"media_type")
                             activity!.setValue(resultItem["media_keywords"] as! String, forKey:"media_keywords")
                             activity!.setValue(resultItem["acess"] as! String, forKey:"acess")
                             activity!.setValue(resultItem["additional_file"] as! String, forKey:"additional_file")
                             //activity!.setValue(resultItem["activity_for"], forKey:"activity_for")
                             activity!.setValue(resultItem["display_timer"] as! String, forKey:"display_timer")
                             activity!.setValue(resultItem["views"] as? Int, forKey:"views")*/
                            
                            var appDirPath = self.appDirectory
                            if !(self.userLogged?.userName ?? "").isEmpty {
                                appDirPath += (self.userLogged?.userName)! + "/Activities/"
                            } else {
                                appDirPath += "_/Activities/"
                            }
                            
                            let filePathMndr = "\(appDirPath)\(scheduleId!).mndr"
                            if !FileManager.default.fileExists(atPath: filePathMndr) {
                                self.manager.DownloadFile(resultItem["additional_file"] as! String, fileName: "\(scheduleId!).mndr")
                            }
                            var filePathMP4 = "\(appDirPath)\(scheduleId!).mp4"
                            if !FileManager.default.fileExists(atPath: filePathMP4) {
                                self.manager.DownloadFile(resultItem["media_url"] as! String, fileName: "\(scheduleId!).mp4")
                            }
                            filePathMP4 = resultItem["media_url"] as! String
                            var filePNGURL = filePathMP4.substring(from: (filePathMP4.range(of: "/", options: .backwards)?.upperBound)!)
                            filePNGURL = "https://d387sn51gsgv97.cloudfront.net/thumbnails/" + filePNGURL.substring(to: (filePNGURL.range(of: ".")?.lowerBound)!) + ".jpg"
                            let filePathPNG = "\(appDirPath)\(scheduleId!).jpg"
                            if !FileManager.default.fileExists(atPath: filePathPNG) {
                                self.manager.DownloadFile(filePNGURL, fileName: "\(scheduleId!).png")
                            }
                        }
                    }
                    
                    do {
                        try managedContext.save()
                        self.settings?.lastUpdateActivities = tillDate
                        //self.SaveSettings()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                    
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            })
        })
    }
    
    var elapsedM: Int = 0     // to check 1000 milli-second
    var elapsedS: Int = 0     // to check 3 second
    
    var dDataArray: [PostureData] = [PostureData]()
    var pDataArray: [PositionData] = [PositionData]()
    var hDataArray: [HeatmapData] = [HeatmapData]()
    var tempStep: Int = 0
    var dStepCount: Int = 0
    
    var postureRecords: [PostureData] = [PostureData]()
    var gyroRecords: [GyroData] = [GyroData]()
    func SaveRecorded(_ image: NSImage) -> String {
        var recordFilename = self.appDirectory
        if !(self.userLogged?.userName ?? "").isEmpty {
            recordFilename += (self.userLogged?.userName)! + "/Records/"
        } else {
            recordFilename += "_/Records/"
        }
        let recordName = Date.GetDateString(Date(), format: "MM-dd-yyyy hh-mm-ss-SSS")
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        recordFilename += recordName + ".mndr"
        let recordData: BallPathRecord = BallPathRecord()
        recordData.name = recordName
        recordData.posturePaths = postureRecords
        recordData.gyroPaths = gyroRecords
        recordData.sensitivity = (opp.sensitivityPosture)
        recordData.difficultyYellow = (opp.difficultyYellow)
        recordData.difficultyRed = (opp.volumeRed)
        recordData.image = image
        NSKeyedArchiver.archiveRootObject(recordData, toFile: recordFilename)
        NSLog("Record saved to \(recordFilename) withBallPathCount: \(postureRecords.count)")
        
        SaveRecordingToDB(systemSetName: recordName, userSetName: recordName, editFlag: false)
        postureRecords.removeAll()
        gyroRecords.removeAll()
        return recordName
    }
    
    func FetchRecordingName(recordSystemName: String) -> String {
        var returnString:String = recordSystemName
        let appDelegate = NSApplication.shared().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequestRecording = NSFetchRequest<NSFetchRequestResult>(entityName: "Recording")
        fetchRequestRecording.predicate = NSPredicate(format: "record_system_name == %@ AND record_type_name == %@ AND user_email == %@", argumentArray: [ recordSystemName, "\(RecordType.posture)", (userLogged?.email)!])
        do {
            let _activity = try managedContext.fetch(fetchRequestRecording)
            if let _activity = _activity as? [NSManagedObject] {
                if _activity.count > 0 {
                    returnString = (_activity[0].value(forKeyPath: "record_user_set_name") as? String)!
                }
            }
        } catch {
            NSLog("Failed to fetch Recording: \(error)")
        }
        return returnString
    }
    
    func checkRecordingNameExistance(userTypedName: String) -> Bool {
        let appDelegate = NSApplication.shared().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequestRecording = NSFetchRequest<NSFetchRequestResult>(entityName: "Recording")
        fetchRequestRecording.predicate = NSPredicate(format: "record_user_set_name == %@ AND record_type_name == %@ AND user_email == %@", argumentArray: [ userTypedName, "\(RecordType.posture)", (userLogged?.email)!])
        do {
            let _activity = try managedContext.fetch(fetchRequestRecording)
            if let _activity = _activity as? [NSManagedObject] {
                if _activity.count > 0 {
                    return true
                }
            }
        } catch {
            NSLog("Failed to fetch Recording: \(error)")
        }
        return false
    }
    
    func SaveRecordingToDB(systemSetName: String, userSetName: String, editFlag: Bool) {
        let appDelegate = NSApplication.shared().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        if editFlag {
            let fetchRequestRecording = NSFetchRequest<NSFetchRequestResult>(entityName: "Recording")
            fetchRequestRecording.predicate = NSPredicate(format: "record_system_name == %@ AND record_type_name == %@ AND user_email == %@", argumentArray: [systemSetName, "\(RecordType.posture)", (userLogged?.email)!])
            //var activity: NSManagedObject?
            do {
                let _activity = try managedContext.fetch(fetchRequestRecording)
                if let _activity = _activity as? [NSManagedObject] {
                    if _activity.count > 0 {
                        _activity[0].setValue(userSetName, forKey: "record_user_set_name")
                    }
                }
            } catch {
                NSLog("Failed to fetch Recording: \(error)")
            }
        }else{
            let entity = NSEntityDescription.entity(forEntityName: "Recording",in: managedContext)!
            let recording = NSManagedObject(entity: entity,insertInto: managedContext)
            recording.setValue((userLogged?.email)!, forKeyPath: "user_email")
            recording.setValue(systemSetName, forKeyPath: "record_system_name")
            recording.setValue(userSetName, forKeyPath: "record_user_set_name")
            recording.setValue("\(RecordType.posture)", forKeyPath: "record_type_name")
        }
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func saveFirstEntry() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(1, forKey: "logedInLaterFlag")
        userDefaults.synchronize()
    }
    
    var green: Int = 0
    var yellow: Int = 0
    var red: Int = 0
    
    fileprivate var _green: Int = 0
    fileprivate var _yellow: Int = 0
    fileprivate var _red: Int = 0
    
    fileprivate var _intervalYellowVolume: Int = 0
    fileprivate var _intervalRedVolume: Int = 0
    fileprivate var _alertYellowVolume: Bool = true
    fileprivate var _alertRedVolume: Bool = true
    
    fileprivate var _intervalYellowVibrate: Int = 0
    fileprivate var _intervalRedVibrate: Int = 0
    fileprivate var _alertYellowVibrate: Bool = true
    fileprivate var _alertRedVibrate: Bool = true
    
    fileprivate var ballInSeatback: Float = 0
    /*private var _intervalAlertYellowTimer: Float = 0
     private var _intervalAlertRedTimer: Float = 0*/
    
    var deviceActionMode: DeviceActionMode = .initializinig
    var calibrateArray: [AccelData] = [AccelData]()
    
    var prevTheta: CGFloat = 0
    var prevOmega: CGFloat = 0
    var prevPhy: CGFloat = 0
    
    var angleSampleCount :Int = 0
    var accelSampleCount : Int = 0
    var gyroSampleCount : Int = 0
    var timerFrequencyLog : Timer? = nil
    
    func startTimer() {
        if timerFrequencyLog == nil {
            let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateDataFrequencyStatus), userInfo: nil, repeats: true)
            timerFrequencyLog = timer
            deviceLog(_log: "FrequencyStatus timer started")
        }
    }
    func stopTimer() {
        if timerFrequencyLog != nil {
            timerFrequencyLog?.invalidate()
            timerFrequencyLog = nil
            deviceLog(_log: "FrequencyStatus timer stoped")
        }
        angleSampleCount = 0
        accelSampleCount = 0
        gyroSampleCount = 0
    }
    func updateDataFrequencyStatus(_ theTimer: Timer) {
        deviceLog(_log: "FrequencyStatus angle:\(angleSampleCount), accel:\(accelSampleCount), gyro:\(gyroSampleCount)")
        delegateDebug?.updateDataFrequencyStatus1(Int(angleSampleCount), withY: Int(accelSampleCount), withZ: Int(gyroSampleCount))
        angleSampleCount = 0
        accelSampleCount = 0
        gyroSampleCount = 0
    }
}


// MinderLib delegates
extension MinderManager: MinderLibDelegate {
    public func updatePICStatus(_ status: Int32) {
    }

    public func updateDebug(_ data: String!) {
    }

    public func updateRSSI(_ rssi: NSNumber!) {
    }

    public func updatePicVersion(_ version: String!) {
    }
    public func updateHeartRate2(_ heartRate: Int32) {
        delegateDebug?.updateHeartRate2_1(Int(heartRate))
    }
    public func updateRawAngle(_ x: Int32, withY y: Int32, withZ z: Int32) {
        delegateDebug?.updateRawAngle1(x, withY: y, withZ: z)//ankush temp
    }
    public func updateVSysCode(_ vsysCode: Float) {
         delegateDebug?.updateVSysCode(vsysCode: vsysCode)
    }
    public func updateFWVersion(_ version: String!) {
         delegateDebug?.updateFWVersion(version: version)
    }
    public func updateDataSent(_ data: NSMutableData!) {
    }
    public func updateDeviceList(_ devices: NSMutableArray!) {
    }
    public func updateLEDStatus(_ red: Int32, withGreen green: Int32, withBlue blue: Int32) {
        delegateDebug?.updateLEDStatus1(Int(red), withGreen: Int(green), withBlue: Int(blue))
    }
    public func updateWirelessChargerStatus(_ status: Int32) {
        delegateDebug?.updateWirelessChargerStatus1(Int(status))
    }
    public func updateButtonStatus(_ status: Int32) {
        delegateDebug?.updateButtonStatus(Int(status))
    }
    public func updateBLEStatus(_ status: Int32) {
        delegateDebug?.updateBLEStatus1(Int(status))
    }
    public func updateChargingStatus(_ status: Int32) {
        delegateDebug?.updateChargingStatus1(Int(status))
    }
    public func updateWirelessChargeStatus(_ status: Int32) {
    }
    
    public func updateTemperature(_ temperature: Double) {
        delegateDevice?.UpdateTemparature(temperature)
        delegateDebug?.updateTemperature1(temperature)
    }
    public func updateHeartRate(_ heartRate: Int32) {
        delegateDevice?.UpdateHeartRate(Float(heartRate))
        delegateDebug?.updateHeartRate_1(Int(Float(heartRate)))
    }
    public func updateLRA(_ wave: Int32) {
        delegateDebug?.updateLRA1(Int(wave))
    }
    
    func IsInSeatbackZone(_ pointBall: NSPoint, radiusBall: CGFloat, radiusYellow: CGFloat) -> ZoneType {
        
        var retZone: ZoneType = .green
        
        // Checking if the point is in seatback zone
        // ball.y should be less than center.y
        // ball.x should be between yellow difficulty range
        if pointBall.y < centerDeviceView.y && (pointBall.x - radiusBall) > (centerDeviceView.x - radiusYellow) && (pointBall.x + radiusBall) < (centerDeviceView.x + radiusYellow) {
            let sYOffset: CGFloat = ((centerDeviceViewHeight / 3) * CGFloat((settings?.OPPs[(settings?.OPPSelected)!].difficultySeatback)!)) / 100
            let sPoint: CGPoint = CGPoint(x: centerDeviceView.x, y: centerDeviceView.y - sYOffset)
            
            // Fine tune checking on bottom side
            // If ball is at bottom check with distance
            if pointBall.y < sPoint.y {
                let xDist = abs(pointBall.x - sPoint.x)
                let yDist = abs(pointBall.y - sPoint.y)
                let distance = sqrt((xDist * xDist) + (yDist * yDist))
                if (distance + radiusBall) <= radiusYellow {
                    retZone = .seatback
                }
            } else {
                retZone = .seatback
            }
        }
        return retZone
    }
    
    func updateStatus(_ status: Int32) {
        if MinderManager.sharedInstance.isLogEnabled {
            MinderManager.sharedInstance.deviceLog(_log:"Device status \(status)")
        }
        deviceStatus = DeviceStatusType(rawValue: status)!
        if deviceStatus == .initializing {
            delegateDevice?.UpdateAlertText("Device initializing.")
            if MinderManager.sharedInstance.isLogEnabled {
                MinderManager.sharedInstance.deviceLog(_log:"Device initializing.")
            }
            //removeAlertText()
        } else if deviceStatus == .connected {
            if !NSApplication.shared().orderedWindows[0].isVisible
            {
                MinderManager.sharedInstance.isAppInBackground = false
                MinderManager.sharedInstance.appMode = .gadget//ankush change
            }
            NSApplication.shared().orderedWindows[0].setIsVisible(true)//ankush change
            tempStep = 0
            MinderManager.sharedInstance.forceDisconnect = false
            self.perform(#selector(DeviceEnableFeatures), with: nil, afterDelay: 5)
            startTimer()
            delegateDevice?.UpdateAlertText("Device connected.")
            if MinderManager.sharedInstance.isLogEnabled {
                MinderManager.sharedInstance.deviceLog(_log:"Device connected.")
            }
            //removeAlertText()
        } else if deviceStatus == .disConnected {
            if !MinderManager.sharedInstance.forceDisconnect {
                if MinderManager.sharedInstance.isLogEnabled {
                    MinderManager.sharedInstance.deviceLog(_log:"Not forcefully disconnected. Trying to reconnect...")
                }
                DispatchQueue.main.async(execute: {
                     self.perform(#selector(self.DeviceConnect), with: nil, afterDelay: 1.0)
                })
                delegateDevice?.UpdateAlertText("Device disconnected. Trying to reconnect.")
                if MinderManager.sharedInstance.isLogEnabled {
                    MinderManager.sharedInstance.deviceLog(_log:"Device disconnected.")
                }
                //removeAlertText()
            } else {
                delegateDevice?.UpdateAlertText("You disconnected the device.")
                if MinderManager.sharedInstance.isLogEnabled {
                    MinderManager.sharedInstance.deviceLog(_log:"You disconnected the device.")
                }
                //removeAlertText()
            }
            stopTimer()
        }
        delegateDevice?.StatusUpdated()
        delegateDebug?.updateStatus1(Int(status))
    }
    func updateCharge(_ charge: Float) {
        if MinderManager.sharedInstance.isLogEnabled {
            MinderManager.sharedInstance.deviceLog(_log:"Device charge \(charge)")
        }
//        let oldMax: Float = 100.0
//        let oldMin: Float = 60.0
//        let newMax: Float = 100.0
//        let newMin: Float = 1.0
//        let oldRange: Float = (oldMax - oldMin)
//        let newRange: Float = (newMax - newMin)
//        let newCharge: Float = (((charge - oldMin) * newRange) / oldRange) + newMin
        deviceCharge = Float(charge)
        delegateDevice?.UpdateCharge(Float(charge))
        delegateDebug?.updateCharge1(Float(charge))
       // print("new range is \(newCharge)")
    }
    /*func removeAlertText() {
     if blockCancelShowText == nil {
     blockCancelShowText = dispatch_block_create(DISPATCH_BLOCK_INHERIT_QOS_CLASS) {
     self.delegateDevice?.UpdateAlertText("")
     }
     print("blockCancelShowText initialised")
     } else {
     dispatch_block_cancel(blockCancelShowText!)
     print("blockCancelShowText cancelled")
     }
     let time: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC * 3))
     dispatch_after(time, dispatch_get_main_queue(), blockCancelShowText!)
     print("blockCancelShowText called")
     }*/
    
    func sensititvityMultiplier(_ sensitivity: Float) -> CGFloat {
        let percent: Float = (sensitivity / 11.0) * 100
        let divider: Float = 5.0 - ((4 * percent) / 100)
        //print("sensitivity: \(sensitivity) percent: \(percent) divider:\(divider) sensitivity:\(CGFloat(sensitivity / divider))")
        return CGFloat(sensitivity / divider)
    }
    
    func updatePosture(_ _x: Float, withY _y: Float, withT t: Double) {
        angleSampleCount += 1
        delegateDebug?.updatePosture1(_x, withY: _y, withT: t)
        /*
         if !dataProcessingPaused && !meditationModeFlag {
         // The posture value changes in a range 60 - 240. The middle can be considered as 150.0 if the device is kept symmetrically vertical, with variation of 90 both direction making a sum 180.
         
         //changing range is now 60 - 240
         let postureValueMiddle: Float = 150 //ankush change 150 -> 160
         let postureValueVariation: Float = 90 //ankush change 90 -> 80
         let postureValueLower: Float = 60
         let postureValueHigh: Float = 240
         
         let opp = settings?.OPPs[(settings?.OPPSelected)!]
         var log: String = "x:\(_x) y:\(_y)"// \(deviceCordinate)"// offset:\((opp?.offsetData.x)!):\((opp?.offsetData.y)!))"
         let x = _x + Float((opp?.offsetData.x)!)
         var y = _y + Float((opp?.offsetData.y)!)
         
         if (opp?.offsetData.y != 0) {
         if (opp!.offsetDeviceCordinate == .pp || opp!.offsetDeviceCordinate == .pn) {
         if !deviceUpwards {
         y = (postureValueHigh - _y) + postureValueHigh + Float((opp?.offsetData.y)!)
         //log += " yIh:\(y) calc:\((postureValueHigh - _y) + postureValueHigh)"
         if y > postureValueHigh {
         y = postureValueHigh
         }
         log += "   yFh:\(y)" //-52.4005877685547
         }
         } else {
         if !deviceUpwards {
         y = postureValueLower - (_y - postureValueLower) + Float((opp?.offsetData.y)!)
         //log += "   yIl:\(y) calc:\(postureValueLower - (_y - postureValueLower))"
         if y < postureValueLower {
         y = postureValueLower
         }
         log += "   yFl:\(y)"
         }
         }
         }
         
         /*var xV = Float(0)
         var yV = Float(0)
         if (opp?.offsetData.x != 0) {
         switch deviceCordinate {
         case .pp, .pn:
         break;
         case .nn:
         //x = postureValueLower - _x
         y = (postureValueLower - _y) + Float((opp?.offsetData.y)!)
         break;
         case .np:
         //xV = postureValueHigh + _x
         y = (postureValueLower - _y) + Float((opp?.offsetData.y)!)
         break;
         }
         }*/
         
         //let x = _x + xV + Float((opp?.offsetData.x)!)
         //let y = _y + yV + Float((opp?.offsetData.y)!)
         log += /*"   xV:\(xV) yV:\(yV)*/"   after calculation x:\(x) y:\(y)"
         //print("posture: \(log)")
         
         var pointPercent: NSPoint = NSPoint(x: imageHeatmap!.size.width/2, y: imageHeatmap!.size.width/2)
         if prevX == 0 && prevY == 0 {
         let xDiff = abs(postureValueMiddle - x)
         let yDiff = abs(postureValueMiddle - y)
         let xDiffPercent = Double((xDiff / postureValueVariation) * 100)
         let yDiffPercent = Double((yDiff / postureValueVariation) * 100)
         
         if x > postureValueMiddle {
         pointPercent.x += ((((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
         } else {
         pointPercent.x -= ((((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
         }
         if y > postureValueMiddle {
         pointPercent.y += ((((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
         } else {
         pointPercent.y -= ((((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
         }
         print("x: \(xDiff) y:\(yDiff)")
         log += "_x:_y \(xDiff):\(yDiff)"
         } else {
         let xDiff = abs(postureValueMiddle - prevX)
         let yDiff = abs(postureValueMiddle - prevY)
         let xDiffPercent = Double((xDiff / postureValueVariation) * 100)
         let yDiffPercent = Double((yDiff / postureValueVariation) * 100)
         if prevX > postureValueMiddle {
         pointPercent.x += (((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100)
         } else {
         pointPercent.x -= (((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100)
         }
         
         if prevY > postureValueMiddle {
         pointPercent.y += (((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100)
         } else {
         pointPercent.y -= (((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100)
         }
         
         let dX = abs(prevX - x)
         let dY = abs(prevY - y)
         let xDP = Double((dX / postureValueVariation) * 100)
         let yDP = Double((dY / postureValueVariation) * 100)
         if x > prevX {
         pointPercent.x += (((imageHeatmap!.size.width/2) * CGFloat(xDP)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)
         if pointPercent.x > (imageHeatmap?.size.width)! {
         pointPercent.x = (imageHeatmap?.size.width)!
         }
         } else if x < prevX {
         pointPercent.x -= (((imageHeatmap!.size.width/2) * CGFloat(xDP)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)
         if pointPercent.x < 0 {
         pointPercent.x = 0
         }
         }
         
         if y > prevY {
         pointPercent.y += (((imageHeatmap!.size.width/2) * CGFloat(yDP)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)
         if pointPercent.y > (imageHeatmap?.size.width)! {
         pointPercent.y = (imageHeatmap?.size.width)!
         }
         } else if y < prevY {
         pointPercent.y -= (((imageHeatmap!.size.width/2) * CGFloat(yDP)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)
         if pointPercent.y < 0 {
         pointPercent.y = 0
         }
         }
         log += "_x:_y \(xDiff):\(yDiff)"
         }
         
         prevX = x
         prevY = y
         
         let date = Date()
         var greeting = "Good morning "
         let hour = (Calendar.current as NSCalendar).component(NSCalendar.Unit.hour, from: date)
         if hour >= 12 && hour < 16 {
         greeting = "Good afternoon "
         } else if hour >= 16 && hour <= 23 {
         greeting = "Good evening "
         }
         
         switch deviceActionMode {
         case .initializinig:
         if settings?.skipCalibration == 1 {
         deviceActionMode = .broadcasting
         } else {
         var displayText = "\(greeting)"
         if !(userLogged?.userName ?? "").isEmpty {
         displayText += (userLogged?.firstName)!
         }
         delegateDevice?.UpdateAlertText("\(displayText)\r\nWelcome back")
         //removeAlertText()
         dCalibrateArray.append(PostureData())
         if dCalibrateArray.count >= 20 {
         dCalibrateArray.removeAll()
         delegateDevice?.UpdateAlertText("")
         deviceActionMode = .calibrating
         }
         }
         break;
         case .calibrating:
         if opp!.offsetPoint.x == -10000.0 && opp!.offsetPoint.y == -10000.0 {
         if dCalibrateArray.count % 10 == 0 {
         var displayText = ""
         if self.calibratedFlag == false {
         displayText = "Hello, \(greeting)"
         let userDefaults = UserDefaults.standard
         if let flag = userDefaults.object(forKey: "logedInLaterFlag") as? Int, flag == 1 {
         displayText = "\(greeting)\r\nWelcome back "
         }
         }
         
         date.timeIntervalSinceNow
         if !(userLogged?.userName ?? "").isEmpty {
         displayText += (userLogged?.firstName)!
         }
         delegateDevice?.UpdateAlertText(displayText + "\r\nLet's take some mindful breaths\r\nto get you going.\r\n\(MinderUtility.SecondsToHMSString((10000 - (dCalibrateArray.count * 100)) / 1000))\r\n\r\nClick anywhere to skip")
         }
         let dData: PostureData = PostureData()
         dData.y = pointPercent.y
         dData.z = pointPercent.x
         dCalibrateArray.append(dData)
         calibrateArray.append(CGPoint(x: CGFloat(_x), y: CGFloat(_y)))
         
         pitch = 0
         roll = 0
         yaw = 0
         
         if dCalibrateArray.count >= 100 {
         var sumX: CGFloat = 0
         var sumY: CGFloat = 0
         for data in dCalibrateArray {
         sumX += data.z
         sumY += data.y
         }
         opp!.offsetPoint.x = (imageHeatmap!.size.width/2) - (sumX / CGFloat(dCalibrateArray.count))
         opp!.offsetPoint.y = (imageHeatmap!.size.width/2) - (sumY / CGFloat(dCalibrateArray.count))
         sumX = 0
         sumY = 0
         print("sumX: \(sumX)  sumY:\(sumY)")
         for data in calibrateArray {
         sumX += data.x
         sumY += data.y
         }
         opp?.offsetData.x = CGFloat(postureValueMiddle) - (sumX / CGFloat(dCalibrateArray.count))
         opp?.offsetData.y = CGFloat(postureValueMiddle) - (sumY / CGFloat(dCalibrateArray.count))
         opp?.offsetDeviceCordinate = deviceCordinate
         
         if MinderManager.sharedInstance.isLogEnabled {
         MinderManager.sharedInstance.deviceLog(_log:"Calibration done. centerPoint: \(centerDeviceView) offsetPoint: \(opp!.offsetPoint) offsetData: \(opp!.offsetData)")
         }
         print("Calibration done. postureValueMiddle:\(postureValueMiddle) sumX:\(sumX)  sumY:\(sumY) count:\(dCalibrateArray.count) offsetData:\(opp!.offsetData)")
         dCalibrateArray.removeAll()
         calibrateArray.removeAll()
         deviceActionMode = .broadcasting
         
         // start timer after calibartion to check seatback threshold
         isTimerStartForCalibrationThreshold = true
         ballInSeatback = 0
         SaveSettings()
         }
         } else {
         deviceActionMode = .broadcasting
         
         }
         break;
         case .broadcasting:
         saveFirstEntry()
         if dCalibrateArray.count > 0
         {
         dCalibrateArray.removeAll() //ankushç change
         }
         //ankush change
         if(isTimerStartForCalibrationThreshold == true)
         {
         timerAfterCalibartionValue += 0.1
         }
         if(timerAfterCalibartionValue >= timerAfterCalibartionThreshold)
         {
         isTimerStartForCalibrationThreshold = false
         timerAfterCalibartionValue = 0
         }
         //=================
         let hPercentage = (centerDeviceViewHeight * 100) / imageHeatmap!.size.width
         //let pointPosture = NSMakePoint((hPercentage * (pointPercent.x + opp!.offsetPoint.x)) / 100, (hPercentage * (pointPercent.y + opp!.offsetPoint.y)) / 100)
         let pointPosture = NSMakePoint((hPercentage * pointPercent.x) / 100, (hPercentage * pointPercent.y) / 100)
         //delegateDevice?.UpdatePosture(pointPosture.x, withY: pointPosture.y)
         if replayState == .none {
         let xDist: Float = Float(abs(pointPosture.x - centerDeviceView.x))
         let yDist: Float = Float(abs(pointPosture.y - centerDeviceView.y))
         let distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
         
         log += "   Center: \(centerDeviceView)   offsetPoint: \(opp!.offsetPoint)   distance: \(distance)"
         
         // Finding ball space and radius for calculation
         let radiusBall = (centerDeviceViewHeight * MinderManager.percentageBall) / 100
         
         let margin = (centerDeviceViewHeight * MinderManager.percentageOffset) / 100
         // Finding yellow radius. Yellow zone is MinderManager.percentageZoneYellow percent of height but start after ball space and an offset
         let percentYellow = (CGFloat(opp!.difficultyYellow) * MinderManager.percentageZoneYellow) / 100
         let radiusYellow = ((centerDeviceViewHeight * percentYellow) / 100) + radiusBall + margin
         
         // Finding red radius. Red zone is MinderManager.percentageZoneRed percent of height and start after ball space, yellow zone space and two offset spaces
         
         let radiusYellow100 = (centerDeviceViewHeight * (MinderManager.percentageBall + MinderManager.percentageOffset + MinderManager.percentageZoneYellow)) / 100
         
         let percentRed = (CGFloat(opp!.difficultyRed) * MinderManager.percentageZoneRed) / 100
         let radiusRed = ((centerDeviceViewHeight * percentRed) / 100) + radiusYellow100 + (2 * margin)
         
         if !isSeatbackEnabled {
         if pointPosture.y < centerDeviceView.y && (pointPosture.x - radiusBall) > (centerDeviceView.x - radiusYellow)  {
         let sYOffset: CGFloat = ((centerDeviceViewHeight / 3) * 50) / 100
         let sPoint: CGPoint = CGPoint(x: centerDeviceView.x, y: centerDeviceView.y - sYOffset)
         
         // Fine tune checking on bottom side
         // If ball is at bottom check with distance
         //                            if pointPosture.y > sPoint.y {
         //                                let xDist = abs(pointPosture.x - sPoint.x)
         //                                let yDist = abs(pointPosture.y - sPoint.y)
         //                                let distance = sqrt((xDist * xDist) + (yDist * yDist))
         if (CGFloat(distance) + radiusBall) < radiusYellow {
         isBallGoneOutOfZone = false
         } else if isBallGoneOutOfZone == false {
         ballInSeatback += 1
         isBallGoneOutOfZone = true
         }
         //                            }
         //                            } else {
         //                                isBallGoneOutOfZone = false
         //                              //  print("ball in 22222")
         //
         //                            }
         } else {
         // isBallGoneOutOfZone = false
         // print("ball in 3333")
         
         }
         
         if ballInSeatback >= noOfTimeOutofZoneThreshold && isTimerStartForCalibrationThreshold == true { //ankush change
         delegateDevice!.EnableSeatback()
         //delegateDevice?.UpdateAlertText("Supported Posture Enabled")
         //removeAlertText()
         ballInSeatback = 0
         
         //as seatback wont start automatically, it will start on manual calibration
         isTimerStartForCalibrationThreshold = false
         timerAfterCalibartionValue = 0
         }
         }
         
         delegateDevice?.UpdateAlertText("")
         var zType: ZoneType = .green
         if (CGFloat(distance) + radiusBall) > radiusRed  {
         if isSeatbackEnabled {
         zType = IsInSeatbackZone(pointPosture, radiusBall: radiusBall, radiusYellow: radiusYellow)
         }
         if zType != .seatback {
         zType = .red
         _red += 1
         _intervalYellowVolume = 0
         _intervalYellowVibrate = 0
         _alertYellowVolume = true
         _alertYellowVibrate = true
         
         //-174 -175 -176 0 176 175 174 173
         
         if _intervalRedVibrate <= 0 && !isRecording {
         if _alertRedVibrate {
         _intervalRedVibrate = (opp?.interval1RedVibrate)!
         _alertRedVibrate = false
         } else {
         if opp?.vibrateRed != 0 {
         self.performSelector(inBackground: #selector(MinderManager.DeviceVibrate), with: nil)
         }
         _intervalRedVibrate = (opp?.interval2RedVibrate)!
         }
         } else if _intervalRedVibrate > 0 {
         _intervalRedVibrate -= 100
         }
         if _intervalRedVolume <= 0 && !isRecording {
         if _alertRedVolume {
         _intervalRedVolume = (opp?.interval1RedVolume)!
         _alertRedVolume = false
         } else {
         DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {() -> Void in
         if !(opp?.volumeRedMute)! {
         if let sound = NSSound(named:"red") {
         sound.volume = Float((opp?.volumeRed)!)
         sound.play()
         }
         }
         })
         _intervalRedVolume = (opp?.interval2RedVolume)!
         }
         } else if _intervalRedVolume > 0 {
         _intervalRedVolume -= 100
         }
         }
         } else if (CGFloat(distance) + radiusBall) > radiusYellow {
         if isSeatbackEnabled {
         zType = IsInSeatbackZone(pointPosture, radiusBall: radiusBall, radiusYellow: radiusYellow)
         }
         if zType != .seatback {
         zType = .yellow
         _yellow += 1
         _intervalRedVolume = 0
         _intervalRedVibrate = 0
         _alertRedVibrate = true
         _alertRedVolume = true
         
         if _intervalYellowVibrate <= 0 && !isRecording {
         if _alertYellowVibrate {
         _intervalYellowVibrate = (opp?.interval1YellowVibrate)!
         _alertYellowVibrate = false
         } else {
         if opp?.vibrateYellow != 0 {
         self.performSelector(inBackground: #selector(MinderManager.DeviceVibrate), with: nil)
         }
         _intervalYellowVibrate = (opp?.interval2YellowVibrate)!
         }
         } else if _intervalYellowVibrate > 0 {
         _intervalYellowVibrate -= 100
         }
         if _intervalYellowVolume <= 0 && !isRecording {
         if _alertYellowVolume {
         _intervalYellowVolume = (opp?.interval1YellowVolume)!
         _alertYellowVolume = false
         } else {
         DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {() -> Void in
         if !(opp?.volumeYellowMute)! {
         if let sound = NSSound(named:"yellow") {
         sound.volume = Float( (opp?.volumeYellow)!)
         sound.play()
         }
         }
         })
         _intervalYellowVolume = (opp?.interval2YellowVolume)!
         }
         } else if _intervalYellowVolume > 0 {
         _intervalYellowVolume -= 100
         }
         }
         }
         if zType != .red && zType != .yellow {
         _green += 1
         _intervalYellowVolume = 0
         _intervalYellowVibrate = 0
         _alertYellowVolume = true
         _alertYellowVibrate = true
         
         _intervalRedVolume = 0
         _intervalRedVibrate = 0
         _alertRedVibrate = true
         _alertRedVolume = true
         }
         if replayState == .none && !isRecording  {
         self.delegateDevice?.UpdateColor(zType)
         }
         
         var timeInterval: String = String(date.timeIntervalSince1970)
         timeInterval = timeInterval.substring(to: (timeInterval.range(of: ".")?.lowerBound)!)
         
         let dData: PostureData = PostureData()
         dData.y = pointPosture.y
         dData.z = pointPosture.x
         dData.state = (zType == .seatback ? ZoneType.green.hashValue : zType.hashValue)
         dData.time = timeInterval
         dDataArray.append(dData)
         
         if isRecording {
         postureRecords.append(dData)
         } else {
         let heatPoint: NSPoint = NSPoint(x: pointPercent.x + opp!.offsetPoint.x, y: pointPercent.y + opp!.offsetPoint.y)
         var heatData: HeatmapData = HeatmapData()
         if hDataArray.contains(where: { $0.rect.contains(heatPoint) }) {
         heatData = hDataArray[hDataArray.index(where: { $0.rect.contains(heatPoint) })!]
         heatData.weight += 0.1
         } else {
         heatData.rect = NSMakeRect(heatPoint.x - 3, heatPoint.y - 3, 6, 6)
         heatData.weight = 0.1
         }
         hDataArray.append(heatData)
         
         let bezPath: NSBezierPath = NSBezierPath(ovalIn: NSMakeRect(heatPoint.x, heatPoint.y, 10, 10))
         imageHeatmap!.lockFocus()
         MinderUtility.ColorForValue(heatData.weight).set()
         bezPath.fill()
         imageHeatmap!.unlockFocus()
         
         if elapsedS == 3000 {
         if !(userLogged?.userName ?? "").isEmpty {
         SetDeviceData(dDataArray, completionHandler: { _ in })
         }
         elapsedS = 0
         dDataArray.removeAll()
         elapsedM += 3000
         if delegateDevice != nil {
         delegateDevice!.UpdateUpdateTime(elapsedM)
         }
         } else {
         elapsedS += 100
         }
         
         if elapsedM == 60000 {
         _green /= 10
         _yellow /= 10
         _red /= 10
         
         var offset: Int = _red + _yellow + _green   // used same variable to hold total and find offset
         if offset > 60 {
         offset = offset - 60
         if _yellow == 0 && _red == 0 {
         _green = 60
         } else if _green == 0 && _red == 0 {
         _yellow = 60
         } else if _green == 0 && _yellow == 0 {
         _red = 60
         } else if _green > _yellow && _green > _red {
         _green -= offset - 1
         } else if _yellow > _green && _yellow > _red {
         _yellow -= offset - 1
         } else {
         _red -= offset - 1
         }
         }
         
         green += _green
         yellow += _yellow
         red += _red
         
         let pData: PositionData = PositionData()
         pData.green = _green
         pData.yellow = _yellow
         pData.red = _red
         pData.time = timeInterval
         pDataArray.insert(pData, at: 0)
         if pDataArray.count > 30 {
         pDataArray.removeLast()
         }
         if delegateDevice != nil {
         delegateDevice?.UpdateDataGraphs()
         }
         let sharedPreference = UserDefaults.standard
         sharedPreference.setValue(NSKeyedArchiver.archivedData(withRootObject: pDataArray), forKey: "pDataArray")
         sharedPreference.setValue("\(green):\(yellow):\(red)", forKey: "GYR")
         sharedPreference.synchronize()
         
         var appDirPath = self.appDirectory
         if !(self.userLogged?.userName ?? "").isEmpty {
         appDirPath += (self.userLogged?.userName)! + "/"
         } else {
         appDirPath += "_/"
         }
         
         self.imageHeatmap?.SavePNG(appDirPath + "hm.png")
         
         if MinderManager.sharedInstance.isLogEnabled {
         MinderManager.sharedInstance.deviceLog(_log:"Elapsed 1min. Green: \(_green), Yellow: \(_yellow), Red: \(_red)\tHeatmap updated")
         }
         
         _green = 0
         _yellow = 0
         _red = 0
         elapsedM = 0
         
         }
         break;
         }
         }
         }
         }
         // */
        
        /*accelSampleCount += 1
        if !dataProcessingPaused && !meditationModeFlag {
            print("\(x), \(y), \(z)")
            
            if y >= 0 && z >= 0  {
                deviceCordinate = .pp
            } else if y < 0 && z >= 0 {
                deviceCordinate = .pn
            } else if y < 0 && z < 0 {
                deviceCordinate = .nn
            } else {
                deviceCordinate = .np
            }
            
            var theta = atan(Double(x) / sqrt(pow(Double(y), 2) + pow(Double(z), 2))) * 180 / M_PI;
            if(x > 0){
                let sign = (theta<0) ? -1: (theta>0) ? 1 :0;
                theta = Double(sign) * ((90.0 - fabs(theta)) + 90.0);
            }
            var omega = atan(Double(y) / sqrt(pow(Double(x), 2) + pow(Double(z), 2))) * 180 / M_PI;
            if(y > 0){
                let sign = (omega<0) ? -1: (omega>0) ? 1 :0;
                omega = Double(sign) * ((90.0 - fabs(omega)) + 90.0);
            }
            var phy = atan(sqrt(pow(Double(x), 2) + pow(Double(y), 2)) / Double(z)) * 180 / M_PI;
            if(z > 0){
                let sign = (phy<0) ? -1: (phy>0) ? 1 :0;
                phy = Double(sign) * ((90.0 - fabs(phy)) + 90.0);
            }
            //print("theta:\(theta)  omega:\(omega)  phy:\(phy)")
            var omegaDiff = CGFloat(180 - abs(90 - omega))
            var phyDiff = CGFloat(180 - (180 - abs(phy)))
            //var log = "omegaDiff:\(omegaDiff) phyDiff:\(phyDiff)"
            let opp = settings?.OPPs[(settings?.OPPSelected)!]
            /*if opp!.offsetPoint.x != -10000.0 && opp!.offsetPoint.y != -10000.0 {
             omegaDiff -= opp!.offsetPoint.x
             phyDiff -= opp!.offsetPoint.y
             //log += "   omegaDiff:\(omegaDiff) phyDiff:\(phyDiff)"
             }*/
            //print(log)
            
            //let roll = atan2(Double(x), Double(y)) * 180 / M_PI
            var pitch = atan2(Double(-x), sqrt(pow(Double(y), 2) + pow(Double(z), 2))) * 180 / M_PI;
            if(z>0){
                let sign = (pitch<0) ? -1: (pitch>0) ? 1 :0;
                pitch = Double(sign) * ((90.0 - fabs(pitch)) + 90.0);
            }
            //print("pitch:\(pitch)  theta:\(theta)  omega:\(omega)  phy:\(phy)  ")
            
            if pitch <= 0 {
                deviceUpwards = true
            } else {
                deviceUpwards = false
            }
            
            let date = Date()
            var greeting = "Good morning "
            let hour = (Calendar.current as NSCalendar).component(NSCalendar.Unit.hour, from: date)
            if hour >= 12 && hour < 16 {
                greeting = "Good afternoon "
            } else if hour >= 16 && hour <= 23 {
                greeting = "Good evening "
            }
            
            switch deviceActionMode {
            case .initializinig:
                if settings?.skipCalibration == 1 {
                    deviceActionMode = .broadcasting
                } else {
                    var displayText = "\(greeting)"
                    if !(userLogged?.userName ?? "").isEmpty {
                        displayText += (userLogged?.firstName)!
                    }
                    delegateDevice?.UpdateAlertText("\(displayText)\r\nWelcome back")
                    //removeAlertText()
                    dCalibrateArray.append(PostureData())
                    if dCalibrateArray.count >= 20 {
                        dCalibrateArray.removeAll()
                        deviceActionMode = .calibrating
                        delegateDevice?.UpdateAlertText("")
                    }
                }
                break;
            case .calibrating:
                if opp!.offsetPoint.x == -10000.0 && opp!.offsetPoint.y == -10000.0 {
                    if dCalibrateArray.count % 10 == 0 {
                        var displayText = ""
                        if self.calibratedFlag == false {
                            displayText = "Hello, \(greeting)"
                            let userDefaults = UserDefaults.standard
                            if let flag = userDefaults.object(forKey: "logedInLaterFlag") as? Int, flag == 1 {
                                displayText = "\(greeting)\r\nWelcome back "
                            }
                        }
                        if !(userLogged?.userName ?? "").isEmpty {
                            displayText += (userLogged?.firstName)!
                        }
                        delegateDevice?.UpdateAlertText(displayText + "\r\nLet's take some mindful breaths\r\nto get you going.\r\n\(MinderUtility.SecondsToHMSString((10000 - (dCalibrateArray.count * 100)) / 1000))\r\n\r\nClick anywhere to skip")
                    }
                    let dData: PostureData = PostureData()
                    dData.y = omegaDiff
                    dData.z = phyDiff
                    dCalibrateArray.append(dData)
                    //calibrateArray.append(CGPoint(x: CGFloat(_x), y: CGFloat(_y)))
                    
                    pitch = 0
                    roll = 0
                    yaw = 0
                    
                    if dCalibrateArray.count >= 100 {
                        var sumX: CGFloat = 0
                        var sumY: CGFloat = 0
                        for data in dCalibrateArray {
                            sumX += data.z
                            sumY += data.y
                        }
                        opp!.offsetPoint.x = /*(imageHeatmap!.size.width/2) -*/ (sumX / CGFloat(dCalibrateArray.count))
                        opp!.offsetPoint.y = /*(imageHeatmap!.size.width/2) -*/ (sumY / CGFloat(dCalibrateArray.count))
                        /*sumX = 0
                         sumY = 0
                         print("sumX: \(sumX)  sumY:\(sumY)")
                         for data in calibrateArray {
                         sumX += data.x
                         sumY += data.y
                         }
                         opp?.offsetData.x = CGFloat(postureValueMiddle) - (sumX / CGFloat(dCalibrateArray.count))
                         opp?.offsetData.y = CGFloat(postureValueMiddle) - (sumY / CGFloat(dCalibrateArray.count))
                         opp?.offsetDeviceCordinate = deviceCordinate*/
                        
                        if MinderManager.sharedInstance.isLogEnabled {
                            MinderManager.sharedInstance.deviceLog(_log:"Calibration done. centerPoint: \(centerDeviceView) offsetPoint: \(opp!.offsetPoint) offsetData: \(opp!.offsetData)")
                        }
                        
                        //print("Calibration done. postureValueMiddle:\(postureValueMiddle) sumX:\(sumX)  sumY:\(sumY) count:\(dCalibrateArray.count) offsetData:\(opp!.offsetData)")
                        dCalibrateArray.removeAll()
                        calibrateArray.removeAll()
                        deviceActionMode = .broadcasting
                        delegateDevice?.UpdateAlertText("")
                        // start timer after calibartion to check seatback threshold
                        isTimerStartForCalibrationThreshold = true
                        ballInSeatback = 0
                        SaveSettings()
                    }
                } else {
                    deviceActionMode = .broadcasting
                }
                break;
            case .broadcasting:
                if dCalibrateArray.count > 0
                {
                    dCalibrateArray.removeAll() //ankushç change
                }
                delegateDevice?.UpdateAlertText("")
                let omegaDiffPercent = CGFloat((omegaDiff / 180) * 100)
                let phyDiffPercent = CGFloat((phyDiff / 180) * 100)
                //print("omega:\(omega)  dif:\(omegaDiff)  %:\(omegaDiffPercent)  phy:\(phy)  dif:\(phyDiff)  %:\(phyDiffPercent)")
                
                let pointPosture = NSMakePoint((omegaDiffPercent * centerDeviceViewHeight) / 100 , (phyDiffPercent * centerDeviceViewHeight) / 100)
                delegateDevice?.UpdatePosture(pointPosture.x, withY: pointPosture.y)
                
                break;
            }
        } else if !dataProcessingPaused && meditationModeFlag {
            delegateDevice?.UpdateAccel(CGFloat(x), withY: CGFloat(y), withZ: CGFloat(z), withT: t)
            
            var timeInterval: String = String(Date().timeIntervalSince1970)
            timeInterval = timeInterval.substring(to: (timeInterval.range(of: ".")?.lowerBound)!)
            let dData: PostureData = PostureData()
            dData.dataType = 1
            dData.y = CGFloat(y)
            dData.z = CGFloat(z)
            dData.time = timeInterval
            dDataArray.append(dData)
            
            if elapsedS == 3000 {
                if !(userLogged?.userName ?? "").isEmpty {
                    SetDeviceData(dDataArray, completionHandler: { _ in })
                }
                elapsedS = 0
                dDataArray.removeAll()
            } else {
                elapsedS += 100
            }
        }*/
    }
    
    public func updateAccelWith(x __x: Int32, withY __y: Int32, withZ __z: Int32, withT t: Double) {
        
        print ("Accel : \(__x) , \(__y) ,\(__z) , \(t) ")
        
        let _x = CGFloat(__x)
        let _y = CGFloat(__y)
        let _z = CGFloat(__z)
        //print("x: \(_x) y: \(_y) z: \(_z)")
        delegateDebug?.UpdateAccel1(_x, withY:  _y, withZ: _z, withT: t)//ankush temp
        accelSampleCount += 1
        
        if !dataProcessingPaused && !meditationModeFlag {
            
            if _y >= 0 && _z >= 0  {
                deviceCordinate = .pp
            } else if _y < 0 && _z >= 0 {
                deviceCordinate = .pn
            } else if _y < 0 && _z < 0 {
                deviceCordinate = .nn
            } else {
                deviceCordinate = .np
            }
            
            var theta = atan(Double(_x) / sqrt(pow(Double(_y), 2) + pow(Double(_z), 2))) * 180 / M_PI;
            if(_x > 0){
                let sign = (theta<0) ? -1: (theta>0) ? 1 :0;
                theta = Double(sign) * ((90.0 - fabs(theta)) + 90.0);
            }
            var omega = atan(Double(_y) / sqrt(pow(Double(_x), 2) + pow(Double(_z), 2))) * 180 / M_PI;
            if(_y > 0){
                let sign = (omega<0) ? -1: (omega>0) ? 1 :0;
                omega = Double(sign) * ((90.0 - fabs(omega)) + 90.0);
            }
            var phy = atan(sqrt(pow(Double(_x), 2) + pow(Double(_y), 2)) / Double(_z)) * 180 / M_PI;
            if(_z > 0){
                let sign = (phy<0) ? -1: (phy>0) ? 1 :0;
                phy = Double(sign) * ((90.0 - fabs(phy)) + 90.0);
            }
            
            let _omegaDiff = CGFloat(180 - abs(90 - omega))
            let _phyDiff = CGFloat(180 - (180 - abs(phy)))
            
            var log = "_omegaDiff:\(_omegaDiff) _phyDiff:\(_phyDiff)"
            
            var pitch = atan2(Double(-_x), sqrt(pow(Double(_y), 2) + pow(Double(_z), 2))) * 180 / M_PI;
            if(_z>0){
                let sign = (pitch<0) ? -1: (pitch>0) ? 1 :0;
                pitch = Double(sign) * ((90.0 - fabs(pitch)) + 90.0);
            }
            let opp = settings?.OPPs[(settings?.OPPSelected)!]
            if pitch <= 0 {
               // if opp?.title == "T1"
              //  {
                  deviceUpwards = true
                //}
              //  else if opp?.title == "Front of the body"
             //   {
             //      deviceUpwards = false
              //  }
                
            } else {
                //if opp?.title == "T1"
              //  {
                    deviceUpwards = false
              //  }
             //   else if opp?.title == "Front of the body"
              //  {
              //      deviceUpwards = true
              //  }
            }
            
            
            var thetaDiff = CGFloat(theta) //+ (opp?.offsetPoint.z)!
            var omegaDiff = CGFloat(_omegaDiff) + (opp?.offsetPoint.x)!
            var phyDiff = CGFloat(_phyDiff) + (opp?.offsetPoint.y)!
            
            // The posture value changes in a range 60 - 240. The middle can be considered as 150.0 if the device is kept symmetrically vertical, with variation of 90 both direction making a sum 180.
            
            //changing range is now 60 - 240
            //let postureValueMiddle: CGFloat = 90 //ankush change 150 -> 160
            let postureValueMiddle: CGFloat = 45
            //let postureValueVariation: CGFloat = 86.5 //ankush change 90 -> 80
             let postureValueVariation: CGFloat = 45.0 //ankush change 90 -> 80
            let postureValueLower: CGFloat = 3.5
            //let postureValueHigh: CGFloat = 176.5
            let postureValueHigh: CGFloat = 90.0
            
            if (opp?.offsetPoint.y != 0) {
                if (opp!.offsetDeviceCordinate == .pp || opp!.offsetDeviceCordinate == .pn) {
                    if !deviceUpwards {
                        phyDiff = (postureValueHigh - _phyDiff) + postureValueHigh + (opp?.offsetPoint.y)!
                        //log += " yIh:\(y) calc:\((postureValueHigh - _y) + postureValueHigh)"
                        if phyDiff > postureValueHigh {
                            phyDiff = postureValueHigh
                        }
                        log += "   yFh:\(phyDiff)" //-52.4005877685547
                    }
                } else {
                    if !deviceUpwards {
                        phyDiff = postureValueLower - (_phyDiff - postureValueLower) + (opp?.offsetPoint.y)!
                        //log += "   yIl:\(y) calc:\(postureValueLower - (_y - postureValueLower))"
                        if phyDiff < postureValueLower {
                            phyDiff = postureValueLower
                        }
                        log += "   yFl:\(phyDiff)"
                    }
                }
            }
            
            /*
             var xV = Float(0)
             var yV = Float(0)
             if (opp?.offsetData.x != 0) {
             switch deviceCordinate {
             case .pp, .pn:
             break;
             case .nn:
             //x = postureValueLower - _x
             y = (postureValueLower - _y) + Float((opp?.offsetData.y)!)
             break;
             case .np:
             //xV = postureValueHigh + _x
             y = (postureValueLower - _y) + Float((opp?.offsetData.y)!)
             break;
             }
             }
             */
            
            //let x = _x + xV + Float((opp?.offsetData.x)!)
            //let y = _y + yV + Float((opp?.offsetData.y)!)
            //log += /*"   xV:\(xV) yV:\(yV)*/"   after calculation x:\(omegaDiff) y:\(phyDiff)"
            
            var pointPercent: NSPoint = NSPoint(x: imageHeatmap!.size.width/2, y: imageHeatmap!.size.width/2)
            if prevOmega == 0 && prevPhy == 0 {
                let xDiff = abs(postureValueMiddle - phyDiff)
                let yDiff = abs(postureValueMiddle - phyDiff)
                let xDiffPercent = Double((xDiff / postureValueVariation) * 100)
                let yDiffPercent = Double((yDiff / postureValueVariation) * 100)
                
                if phyDiff > postureValueMiddle {
                    if opp?.title == "T1"
                    {
                         pointPercent.x += ((((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
                    }
                    else if opp?.title == "Front of the body"
                    {
                         pointPercent.x -= ((((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
                    }
                   
                } else {
                    if opp?.title == "T1"
                    {
                        pointPercent.x -= ((((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
                    }
                    else if opp?.title == "Front of the body"
                    {
                        pointPercent.x += ((((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
                    }
                    
                }
                if phyDiff > postureValueMiddle {
                    if opp?.title == "T1"
                    {
                        pointPercent.y += ((((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
                    }
                    else if opp?.title == "Front of the body"
                    {
                        pointPercent.y -= ((((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
                    }
                    
                } else {
                    if opp?.title == "T1"
                    {
                        pointPercent.y -= ((((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
                    }
                    else if opp?.title == "Front of the body"
                    {
                        pointPercent.y += ((((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)) * 0.01
                    }
                    
                }
                log += "_x:_y \(xDiff):\(yDiff) \(pointPercent)"
            } else {
                let xDiff = abs(postureValueMiddle - prevOmega)
                let yDiff = abs(postureValueMiddle - prevPhy)
                let xDiffPercent = Double((xDiff / postureValueVariation) * 100)
                let yDiffPercent = Double((yDiff / postureValueVariation) * 100)
                if prevOmega > postureValueMiddle {
                    if opp?.title == "T1"
                    {
                        pointPercent.x += (((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100)
                    }
                    else if opp?.title == "Front of the body"
                    {
                        pointPercent.x -= (((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100)
                    }
                    
                } else {
                    if opp?.title == "T1"
                    {
                        pointPercent.x -= (((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100)
                    }
                    else if opp?.title == "Front of the body"
                    {
                        pointPercent.x += (((imageHeatmap!.size.width/2) * CGFloat(xDiffPercent)) / 100)
                    }
                    
                }
                
                if prevPhy > postureValueMiddle {
                    if opp?.title == "T1"
                    {
                        pointPercent.y += (((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100)
                    }
                    else if opp?.title == "Front of the body"
                    {
                        pointPercent.y -= (((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100)
                    }
                    
                } else {
                    if opp?.title == "T1"
                    {
                         pointPercent.y -= (((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100)
                    }
                    else if opp?.title == "Front of the body"
                    {
                         pointPercent.y += (((imageHeatmap!.size.width/2) * CGFloat(yDiffPercent)) / 100)
                    }
                   
                }
                
                let dX = abs(prevOmega - omegaDiff)
                let dY = abs(prevPhy - phyDiff)
                let xDP = Double((dX / postureValueVariation) * 100)
                let yDP = Double((dY / postureValueVariation) * 100)
                if omegaDiff > prevOmega {
                    pointPercent.x += (((imageHeatmap!.size.width/2) * CGFloat(xDP)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)
                    if pointPercent.x > (imageHeatmap?.size.width)! {
                        pointPercent.x = (imageHeatmap?.size.width)!
                    }
                } else if omegaDiff < prevOmega {
                    pointPercent.x -= (((imageHeatmap!.size.width/2) * CGFloat(xDP)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)
                    if pointPercent.x < 0 {
                        pointPercent.x = 0
                    }
                }
                
                if phyDiff > prevPhy {
                    pointPercent.y += (((imageHeatmap!.size.width/2) * CGFloat(yDP)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)
                    if pointPercent.y > (imageHeatmap?.size.width)! {
                        pointPercent.y = (imageHeatmap?.size.width)!
                    }
                } else if phyDiff < prevPhy {
                    pointPercent.y -= (((imageHeatmap!.size.width/2) * CGFloat(yDP)) / 100) * sensititvityMultiplier((opp?.sensitivityPosture)!)
                    if pointPercent.y < 0 {
                        pointPercent.y = 0
                    }
                }
                log += "_x:_y \(xDiff):\(yDiff) \(pointPercent)"
            }
           // print(log)
            prevOmega = omegaDiff
            prevPhy = phyDiff
            
            let date = Date()
            var greeting = "Good morning "
            let hour = (Calendar.current as NSCalendar).component(NSCalendar.Unit.hour, from: date)
            if hour >= 12 && hour < 16 {
                greeting = "Good afternoon "
            } else if hour >= 16 && hour <= 23 {
                greeting = "Good evening "
            }
            
            switch deviceActionMode {
            case .initializinig:
                if settings?.skipCalibration == 1 {
                    deviceActionMode = .broadcasting
                } else {
                    var displayText = "\(greeting)"
                    if !(userLogged?.userName ?? "").isEmpty {
                        displayText += (userLogged?.firstName)!
                    }
                    delegateDevice?.UpdateAlertText("\(displayText)\r\nWelcome back")
                    //removeAlertText()
                    calibrateArray.append(AccelData())
                    if calibrateArray.count >= 20 {
                        calibrateArray.removeAll()
                        delegateDevice?.UpdateAlertText("")
                        deviceActionMode = .calibrating
                    }
                }
                break;
            case .calibrating:
                if opp!.offsetPoint.x == 0 && opp!.offsetPoint.y == 0 {
                    if calibrateArray.count % 10 == 0 {
                        var displayText = ""
                        if self.calibratedFlag == false {
                            displayText = "Hello, \(greeting)"
                            let userDefaults = UserDefaults.standard
                            if let flag = userDefaults.object(forKey: "logedInLaterFlag") as? Int, flag == 1 {
                                displayText = "\(greeting)\r\nWelcome back "
                            }
                        }
                        
                        if !(userLogged?.userName ?? "").isEmpty {
                            displayText += (userLogged?.firstName)!
                        }
                        delegateDevice?.UpdateAlertText(displayText + "\r\nLet's take some mindful breaths\r\nto get you going.\r\n\(MinderUtility.SecondsToHMSString((10000 - (calibrateArray.count * 100)) / 1000))\r\n\r\nClick anywhere to skip")
                    }
                    let aData: AccelData = AccelData()
                    aData.x = omegaDiff
                    aData.y = phyDiff
                    aData.z = CGFloat(theta)
                    calibrateArray.append(aData)
                    
                    pitch = 0
                    roll = 0
                    yaw = 0
                    
                    if calibrateArray.count >= 100 {
                        var sumX: CGFloat = 0
                        var sumY: CGFloat = 0
                        for data in calibrateArray {
                            sumX += data.x
                            sumY += data.y
                        }
                        
                        opp?.offsetPoint.x = CGFloat(postureValueMiddle) - (sumX / CGFloat(calibrateArray.count))
                        opp?.offsetPoint.y = CGFloat(postureValueMiddle) - (sumY / CGFloat(calibrateArray.count))
                        opp?.offsetDeviceCordinate = deviceCordinate
                        
                        if MinderManager.sharedInstance.isLogEnabled {
                            MinderManager.sharedInstance.deviceLog(_log:"Calibration done. centerPoint: \(centerDeviceView) offsetPoint: \(opp!.offsetPoint)")
                        }
                        print("Calibration done. postureValueMiddle:\(postureValueMiddle) sumX:\(sumX)  sumY:\(sumY) count:\(calibrateArray.count) offsetPoint:\(opp!.offsetPoint)")
                        calibrateArray.removeAll()
                        deviceActionMode = .broadcasting
                        self.isCalibrationDone = true
                        
                        // start timer after calibartion to check seatback threshold
                        isTimerStartForCalibrationThreshold = true
                        ballInSeatback = 0
                        SaveSettings()
                    }
                } else {
                    deviceActionMode = .broadcasting
                }
                break;
            case .broadcasting:
                saveFirstEntry()
                if calibrateArray.count > 0
                {
                    calibrateArray.removeAll() //ankushç change
                }
                //ankush change
                if(isTimerStartForCalibrationThreshold == true)
                {
                    timerAfterCalibartionValue += 0.1
                }
                if(timerAfterCalibartionValue >= timerAfterCalibartionThreshold)
                {
                    isTimerStartForCalibrationThreshold = false
                    timerAfterCalibartionValue = 0
                }
                //=================
                let hPercentage = (centerDeviceViewHeight * 100) / imageHeatmap!.size.width
                //let pointPosture = NSMakePoint((hPercentage * (pointPercent.x + opp!.offsetPoint.x)) / 100, (hPercentage * (pointPercent.y + opp!.offsetPoint.y)) / 100)
                let pointPosture = NSMakePoint((hPercentage * pointPercent.x) / 100, (hPercentage * pointPercent.y) / 100)
                delegateDevice?.UpdatePosture(pointPosture.x, withY: pointPosture.y)
                if replayState == .none {
                    let xDist: Float = Float(abs(pointPosture.x - centerDeviceView.x))
                    let yDist: Float = Float(abs(pointPosture.y - centerDeviceView.y))
                    let distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
                    
                    log += "   Center: \(centerDeviceView)   offsetPoint: \(opp!.offsetPoint)   distance: \(distance)"
                    
                    // Finding ball space and radius for calculation
                    let radiusBall = (centerDeviceViewHeight * MinderManager.percentageBall) / 100
                    //print("redius of ball is \(radiusBall)")
                    
                    let margin = (centerDeviceViewHeight * MinderManager.percentageOffset) / 100
                    // Finding yellow radius. Yellow zone is MinderManager.percentageZoneYellow percent of height but start after ball space and an offset
                    let percentYellow = (CGFloat(opp!.difficultyYellow) * MinderManager.percentageZoneYellow) / 100
                    let radiusYellow = ((centerDeviceViewHeight * percentYellow) / 100) + radiusBall + margin
                    
                    // Finding red radius. Red zone is MinderManager.percentageZoneRed percent of height and start after ball space, yellow zone space and two offset spaces
                    
                    let radiusYellow100 = (centerDeviceViewHeight * (MinderManager.percentageBall + MinderManager.percentageOffset + MinderManager.percentageZoneYellow)) / 100
                    
                    let percentRed = (CGFloat(opp!.difficultyRed) * MinderManager.percentageZoneRed) / 100
                    let radiusRed = ((centerDeviceViewHeight * percentRed) / 100) + radiusYellow100 + (2 * margin)
                    
                    if !isSeatbackEnabled {
                        if pointPosture.y < centerDeviceView.y && (pointPosture.x - radiusBall) > (centerDeviceView.x - radiusYellow)  {
                            let sYOffset: CGFloat = ((centerDeviceViewHeight / 3) * 50) / 100
                            let sPoint: CGPoint = CGPoint(x: centerDeviceView.x, y: centerDeviceView.y - sYOffset)
                            
                            // Fine tune checking on bottom side
                            // If ball is at bottom check with distance
                            //                            if pointPosture.y > sPoint.y {
                            //                                let xDist = abs(pointPosture.x - sPoint.x)
                            //                                let yDist = abs(pointPosture.y - sPoint.y)
                            //                                let distance = sqrt((xDist * xDist) + (yDist * yDist))
                            if (CGFloat(distance) + radiusBall) < radiusYellow {
                                isBallGoneOutOfZone = false
                            } else if isBallGoneOutOfZone == false {
                                ballInSeatback += 1
                                isBallGoneOutOfZone = true
                            }
                            //                            }
                            //                            } else {
                            //                                isBallGoneOutOfZone = false
                            //                              //  print("ball in 22222")
                            //
                            //                            }
                        } else {
                            // isBallGoneOutOfZone = false
                            // print("ball in 3333")
                        }
                        
                        if ballInSeatback >= noOfTimeOutofZoneThreshold && isTimerStartForCalibrationThreshold == true { //ankush change
                            delegateDevice!.EnableSeatback()
                            //delegateDevice?.UpdateAlertText("Supported Posture Enabled")
                            //removeAlertText()
                            ballInSeatback = 0
                            
                            //as seatback wont start automatically, it will start on manual calibration
                            isTimerStartForCalibrationThreshold = false
                            timerAfterCalibartionValue = 0
                        }
                    }
                    
                    delegateDevice?.UpdateAlertText("")
                    var zType: ZoneType = .green
                    
                
                    /* zoneYellow -> zoneBad if (CGFloat(distance) + radiusBall) > radiusRed  {
                        if isSeatbackEnabled {
                            zType = IsInSeatbackZone(pointPosture, radiusBall: radiusBall, radiusYellow: radiusYellow)
                        }
                        if zType != .seatback {
                            zType = .red
                            _red += 1
                            _intervalYellowVolume = 0
                            _intervalYellowVibrate = 0
                            _alertYellowVolume = true
                            _alertYellowVibrate = true
                            
                            //-174 -175 -176 0 176 175 174 173
                            
                            if _intervalRedVibrate <= 0 && !isRecording {
                                if _alertRedVibrate {
                                    _intervalRedVibrate = (opp?.interval1RedVibrate)!
                                    _alertRedVibrate = false
                                } else {
                                    if opp?.vibrateRed != 0 {
                                        self.performSelector(inBackground: #selector(MinderManager.DeviceVibrate), with: nil)
                                    }
                                    _intervalRedVibrate = (opp?.interval2RedVibrate)!
                                }
                            } else if _intervalRedVibrate > 0 {
                                _intervalRedVibrate -= 100
                            }
                            if _intervalRedVolume <= 0 && !isRecording {
                                if _alertRedVolume {
                                    _intervalRedVolume = (opp?.interval1RedVolume)!
                                    _alertRedVolume = false
                                } else {
                                    DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {() -> Void in
                                        if !(opp?.volumeRedMute)! {
                                            self.playRedRegionSound(tagValue: (opp?.redSoundValue)! )
                                        }
                                    })
                                    _intervalRedVolume = (opp?.interval2RedVolume)!
                                }
                            } else if _intervalRedVolume > 0 {
                                _intervalRedVolume -= 100
                            }
                        }
                    } else */ if (CGFloat(distance) + radiusBall) > radiusYellow {
                        if isSeatbackEnabled {
                            zType = IsInSeatbackZone(pointPosture, radiusBall: radiusBall, radiusYellow: radiusYellow)
                        }
                        if zType != .seatback {
                            zType = .yellow
                            _yellow += 1
                            _intervalRedVolume = 0
                            _intervalRedVibrate = 0
                            _alertRedVibrate = true
                            _alertRedVolume = true
                            
                            if _intervalYellowVibrate <= 0 && !isRecording {
                                if _alertYellowVibrate {
                                    _intervalYellowVibrate = (opp?.interval1YellowVibrate)!
                                    _alertYellowVibrate = false
                                } else {
                                    if opp?.vibrateYellow != 0 {
                                        self.performSelector(inBackground: #selector(MinderManager.DeviceVibrate), with: nil)
                                    }
                                    _intervalYellowVibrate = (opp?.interval2YellowVibrate)!
                                }
                            } else if _intervalYellowVibrate > 0 {
                                _intervalYellowVibrate -= 100
                            }
                            if _intervalYellowVolume <= 0 && !isRecording {
                                if _alertYellowVolume {
                                    _intervalYellowVolume = (opp?.interval1YellowVolume)!
                                    _alertYellowVolume = false
                                } else {
                                    DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {() -> Void in
                                        if !(opp?.volumeYellowMute)! {
                                            self.playYellowRegionSound(tagValue: (opp?.yellowSoundValue)! )
                                        }
                                    })
                                    _intervalYellowVolume = (opp?.interval2YellowVolume)!
                                }
                            } else if _intervalYellowVolume > 0 {
                                _intervalYellowVolume -= 100
                            }
                        }
                    }
                    if zType != .red && zType != .yellow {
                        _green += 1
                        _intervalYellowVolume = 0
                        _intervalYellowVibrate = 0
                        _alertYellowVolume = true
                        _alertYellowVibrate = true
                        
                        _intervalRedVolume = 0
                        _intervalRedVibrate = 0
                        _alertRedVibrate = true
                        _alertRedVolume = true
                    }
                    if replayState == .none && !isRecording  {
                        self.delegateDevice?.UpdateColor(zType)
                    }
                    
                    var timeInterval: String = String(date.timeIntervalSince1970)
                    timeInterval = timeInterval.substring(to: (timeInterval.range(of: ".")?.lowerBound)!)
                    
                    let dData: PostureData = PostureData()
                    dData.y = pointPosture.y
                    dData.z = pointPosture.x
                    dData.state = (zType == .seatback ? ZoneType.green.hashValue : zType.hashValue)
                    dData.time = timeInterval
                    dDataArray.append(dData)
                    
                    if isRecording {
                        postureRecords.append(dData)
                    } else {
                        let heatPoint: NSPoint = NSPoint(x: pointPercent.x + opp!.offsetPoint.x, y: pointPercent.y + opp!.offsetPoint.y)
                        var heatData: HeatmapData = HeatmapData()
                        if hDataArray.contains(where: { $0.rect.contains(heatPoint) }) {
                            heatData = hDataArray[hDataArray.index(where: { $0.rect.contains(heatPoint) })!]
                            heatData.weight += 0.1
                        } else {
                            heatData.rect = NSMakeRect(heatPoint.x - 3, heatPoint.y - 3, 6, 6)
                            heatData.weight = 0.1
                        }
                        hDataArray.append(heatData)
                        
                        let bezPath: NSBezierPath = NSBezierPath(ovalIn: NSMakeRect(heatPoint.x, heatPoint.y, 10, 10))
                        imageHeatmap!.lockFocus()
                        MinderUtility.ColorForValue(heatData.weight).set()
                        bezPath.fill()
                        imageHeatmap!.unlockFocus()
                        
                        if elapsedS == 3000 {
                            if !(userLogged?.userName ?? "").isEmpty {
                                SetDeviceData(dDataArray, completionHandler: { _ in })
                            }
                            elapsedS = 0
                            dDataArray.removeAll()
                            elapsedM += 3000
                            if delegateDevice != nil {
                                delegateDevice!.UpdateUpdateTime(elapsedM)
                            }
                        } else {
                            elapsedS += 100
                        }
                        
                        if elapsedM == 60000 {
                            _green /= 10
                            _yellow /= 10
                            _red /= 10
                            
                            var offset: Int = _red + _yellow + _green   // used same variable to hold total and find offset
                            if offset > 60 {
                                offset = offset - 60
                                if _yellow == 0 && _red == 0 {
                                    _green = 60
                                } else if _green == 0 && _red == 0 {
                                    _yellow = 60
                                } else if _green == 0 && _yellow == 0 {
                                    _red = 60
                                } else if _green > _yellow && _green > _red {
                                    _green -= offset - 1
                                } else if _yellow > _green && _yellow > _red {
                                    _yellow -= offset - 1
                                } else {
                                    _red -= offset - 1
                                }
                            }
                            
                            green += _green
                            yellow += _yellow
                            red += _red
                            
                            let pData: PositionData = PositionData()
                            pData.green = _green
                            pData.yellow = _yellow
                            pData.red = _red
                            pData.time = timeInterval
                            pDataArray.insert(pData, at: 0)
                            if pDataArray.count > 30 {
                                pDataArray.removeLast()
                            }
                            if delegateDevice != nil {
                                delegateDevice?.UpdateDataGraphs()
                            }
                            let sharedPreference = UserDefaults.standard
                            sharedPreference.setValue(NSKeyedArchiver.archivedData(withRootObject: pDataArray), forKey: "pDataArray")
                            sharedPreference.setValue("\(green):\(yellow):\(red)", forKey: "GYR")
                            sharedPreference.synchronize()
                            
                            var appDirPath = self.appDirectory
                            if !(self.userLogged?.userName ?? "").isEmpty {
                                appDirPath += (self.userLogged?.userName)! + "/"
                            } else {
                                appDirPath += "_/"
                            }
                            
                            self.imageHeatmap?.SavePNG(appDirPath + "hm.png")
                            
                            if MinderManager.sharedInstance.isLogEnabled {
                                MinderManager.sharedInstance.deviceLog(_log:"Elapsed 1min. Green: \(_green), Yellow: \(_yellow), Red: \(_red)\tHeatmap updated")
                            }
                            
                            _green = 0
                            _yellow = 0
                            _red = 0
                            elapsedM = 0
                            
                        }
                        break;
                    }
                }
            }
        }
        else if !dataProcessingPaused && meditationModeFlag {
            delegateDevice?.UpdateAccel(_x, withY: _y, withZ: _z, withT: t)
            
            var timeInterval: String = String(Date().timeIntervalSince1970)
            timeInterval = timeInterval.substring(to: (timeInterval.range(of: ".")?.lowerBound)!)
            let dData: PostureData = PostureData()
            dData.dataType = 1
            dData.y = _y
            dData.z = _z
            dData.time = timeInterval
            dDataArray.append(dData)
            
            if elapsedS == 3000 {
                if !(userLogged?.userName ?? "").isEmpty {
                    SetDeviceData(dDataArray, completionHandler: { _ in })
                }
                elapsedS = 0
                dDataArray.removeAll()
            } else {
                elapsedS += 100
            }
        }
        
    }
    
    
    
    
    
    public func updateGyro(withRoll roll: Int32, withPitch pitch: Int32, withYaw yaw: Int32, withT t: Double) {
        NSLog ("Gyro : \(roll) , \(pitch) ,\(yaw) , \(t) ")
        delegateDebug?.UpdateGyroWithRoll1(CGFloat(roll), withPitch:  CGFloat(pitch), withYaw: CGFloat(yaw), withT: t)//ankush temp
        gyroSampleCount += 1
        if !dataProcessingPaused && replayState == .none {
            let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
            //print("GyroWithRoll: \(roll) withPitch \(pitch) withYaw \(yaw) withT \(t)")
            delegateDevice?.UpdateGyroWithRoll(((roll != -1) ? (CGFloat(roll) * CGFloat(opp.sensitivityRotation)) : CGFloat(roll)), withPitch: ((pitch != -1) ? (CGFloat(pitch) * CGFloat(opp.sensitivityRotation)) : CGFloat(pitch)), withYaw: ((yaw != -1) ? (CGFloat(yaw) * CGFloat(opp.sensitivityRotation)) : CGFloat(yaw)), withT: t)
            
            if isRecording {
                var timeInterval: String = String(Date().timeIntervalSince1970)
                timeInterval = timeInterval.substring(to: (timeInterval.range(of: ".")?.lowerBound)!)
                
                let gData: GyroData = GyroData()
                gData.roll = CGFloat(roll)
                gData.pitch = CGFloat(pitch)
                gData.yaw = CGFloat(yaw)
                gData.time = timeInterval
                gyroRecords.append(gData)
            }
        }
    }
    func updateStepCount(_ stepCount: Int32) {
        //if !dataProcessingPaused {
            //print("StepCount: \(stepCount)")
            dStepCount += (Int(stepCount) - tempStep)
            let sharedPreference = UserDefaults.standard
            sharedPreference.setValue("\(dStepCount)", forKey: "Step")
            sharedPreference.synchronize()
            delegateDevice?.UpdateStepCount()
            delegateDebug?.updateStepCount1(Int(stepCount))
            tempStep = Int(stepCount)
       // }
    }
    func playYellowRegionSound(tagValue : Int) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        switch tagValue {
        case 1:
                if let sound = NSSound(named:"yellow") {
                    sound.volume = Float( opp.volumeYellow)
                    sound.play()
                }
                break
        case 2:
                if let sound = NSSound(named:"RingYellow") {
                    sound.volume = Float( opp.volumeYellow)
                    sound.play()
                }
                break
        case 3:
                if let sound = NSSound(named:"DingYellow") {
                    sound.volume = Float( opp.volumeYellow)
                    sound.play()
                }
                break
        case 4:
                if let sound = NSSound(named:"ChimeYellow") {
                    sound.volume = Float( opp.volumeYellow)
                    sound.play()
                }
                break
        default:
            break
        }
        
    }
    func playRedRegionSound(tagValue : Int) {
       let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        switch tagValue {
        case 1:
            if let sound = NSSound(named:"red") {
                sound.volume = Float(opp.volumeRed)
               sound.play()
            }
            break
        case 2:
            if let sound = NSSound(named:"RingRed") {
                sound.volume = Float(opp.volumeRed)
                sound.play()
            }
            break
        case 3:
            if let sound = NSSound(named:"DingRed") {
                sound.volume = Float(opp.volumeRed)
                sound.play()
            }
            break
        case 4:
            if let sound = NSSound(named:"ChimeRed") {
                sound.volume = Float(opp.volumeRed)
                sound.play()
            }
            break
        default:
            break
        }
        
    }
}
