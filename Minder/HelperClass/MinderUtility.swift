//
//  CommonCirclecClass.swift
//  Minder
//
//  Created by Abdul on 8/5/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa
import AppKit

enum ShapeType {
    case circle
    //case Annulus
    case rectTopCurved1By3
    case rectTopCurvedHalf
    case rectTopCurved3By4
    case rectTopCurved1By3Reverse
}

class MinderUtility {
    static func iterateEnum<T: Hashable>(_: T.Type) -> AnyIterator<T> {
        var i = 0
        return AnyIterator {
            let next = withUnsafeBytes(of: &i) { $0.load(as: T.self) }
            if next.hashValue != i { return nil }
            i += 1
            return next
        }
    }
    static func ShapeView(_ view: NSView, type: ShapeType) {
        let width = view.layer!.frame.size.width
        let height = view.layer!.frame.size.height
        
        let mask = CAShapeLayer()
        mask.frame = view.layer!.bounds
        
        let path = CGMutablePath()
        switch type {
            /*
             CGPathMoveToPoint(path, nil, 0, height)
             CGPathAddLineToPoint(path, nil, width/2, height)
             CGPathAddLineToPoint(path, nil, (width/2)+20, 0)
             CGPathAddLineToPoint(path, nil, 0, 0)
             CGPathAddLineToPoint(path, nil, 0, height)
             */
        case .rectTopCurved1By3:
            path.move(to: CGPoint(x: 0, y: height))
            path.addLine(to: CGPoint(x: width/3, y: height))
            path.addLine(to: CGPoint(x: (width/3)+20, y: height-20))
            path.addLine(to: CGPoint(x: width, y: height-20))
            path.addLine(to: CGPoint(x: width, y: 0))
            path.addLine(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: 0, y: height))
            
            break
        case .rectTopCurvedHalf:
            
            path.move(to: CGPoint(x: 0, y: height))
            path.addLine(to: CGPoint(x: width/2, y: height))
            path.addLine(to: CGPoint(x: (width/2)+20, y: height-20))
            path.addLine(to: CGPoint(x: width, y: height-20))
            path.addLine(to: CGPoint(x: width, y: 0))
            path.addLine(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: 0, y: height))
            
            break
        case .rectTopCurved3By4:
            
            path.move(to: CGPoint(x: 0, y: height))
            path.addLine(to: CGPoint(x: width-(width/3), y: height))
            path.addLine(to: CGPoint(x: width-(width/3)+20, y: height-20))
            path.addLine(to: CGPoint(x: width, y: height-20))
            path.addLine(to: CGPoint(x: width, y: 0))
            path.addLine(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: 0, y: height))
        
            break
        case .rectTopCurved1By3Reverse:
            
            path.move(to: CGPoint(x: 0, y: height/2))
            path.addLine(to: CGPoint(x: width-40, y: height/2))
            path.addLine(to: CGPoint(x:  width-30, y: height))
            path.addLine(to: CGPoint(x: width, y: height))
            path.addLine(to: CGPoint(x: width, y: 0))
            path.addLine(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: 0, y: height/2))
            
            break
        /*case .Annulus:
            CGPathMoveToPoint(path, nil, width/2, 0)
            CGPathAddArc(path, nil, width/2, height/2, width/2, CGFloat(-1 * M_PI_2), CGFloat(M_PI_2 * 3), false)
            mask.fillRule = kCAFillRuleEvenOdd
            CGPathMoveToPoint(path, nil, width/6, 0)
            CGPathAddArc(path, nil, width/2, height/2, width/6, CGFloat(-1 * M_PI_2), CGFloat(M_PI_2 * 3), false)
            break*/
        case .circle:
            
            path.move(to: CGPoint(x: width/2, y: 0))
            path.addArc(center: CGPoint(x:width/2, y:height/2) , radius: width/2, startAngle: CGFloat(-1 * M_PI_2), endAngle: CGFloat(M_PI_2 * 3), clockwise: false)
            
//            CGPathMoveToPoint(path, nil, width/2, 0)
//            CGPathAddArc(path, nil, width/2, height/2, width/2, CGFloat(-1 * M_PI_2), CGFloat(M_PI_2 * 3), false)
            break
        }
        mask.path = path
        view.layer!.mask = mask
        
        /*let shape = CAShapeLayer()
        shape.frame = view.layer!.bounds
        shape.path = path
        shape.lineWidth = 1
        shape.fillColor = NSColor.clearColor().CGColor
        view.layer!.insertSublayer(shape, atIndex: 10)*/
    }
    
    static func ResizeImage(_ image:NSImage, destSize:NSSize) -> NSImage {
        let newImage = NSImage(size: destSize)
        newImage.lockFocus()
        image.draw(in: NSMakeRect(0, 0, destSize.width, destSize.height), from: NSMakeRect(0, 0, image.size.width, image.size.height), operation: NSCompositingOperation.sourceOver, fraction: CGFloat(1))
        newImage.unlockFocus()
        newImage.size = destSize
        return NSImage(data: newImage.tiffRepresentation!)!
    }
    
    static fileprivate var kSBAlphaPivotX: CGFloat = 0.333
    static fileprivate var kSBAlphaPivotY: CGFloat = 0.5
    static fileprivate var kSBMaxAlpha: CGFloat = 0.85
    
    static func ColorForValue(_ value: CGFloat) -> NSColor {
        var value = value
        var red: CGFloat
        var green: CGFloat
        var blue: CGFloat
        var alpha: CGFloat
        let maxVal: CGFloat = 255
        if value > 1 {
            value = 1
        }
        value = sqrt(value)
        if value < kSBAlphaPivotY {
            alpha = value * kSBAlphaPivotY / kSBAlphaPivotX
        }
        else {
            alpha = kSBAlphaPivotY + (kSBMaxAlpha - kSBAlphaPivotY) / (1 - kSBAlphaPivotX) * (value - kSBAlphaPivotX)
        }
        if value <= 0 {
            red = 0
            green = 0
            blue = 0
            alpha = 0
        }
        else if value < 0.125 {
            red = 0
            green = 0
            blue = 4 * (value + 0.125)
        } else if value < 0.375 {
            red = 0
            green = 4 * (value - 0.125)
            blue = 1
        } else if value < 0.625 {
            red = 4 * (value - 0.375)
            green = 1
            blue = 1 - 4 * (value - 0.375)
        } else if value < 0.875 {
            red = 1
            green = 1 - 4 * (value - 0.625)
            blue = 0
        } else {
            red = max(1 - 4 * (value - 0.875), 0.5)
            green = 0
            blue = 0
        }
        alpha *= maxVal
        blue *= alpha
        green *= alpha
        red *= alpha
        //print("colorForValue:: Red:\(red) Green:\(green) Blue:\(blue) Alpha:\(alpha)")
        return NSColor.init(calibratedRed: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
        
    static func SecondsToHMSString (_ seconds : Int) -> String {
        //return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        var timeString: String = ""
        let (h,m,s) = (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        if h > 0 {
            if h == 1 {
                timeString += "\(h) hour"
            } else {
                timeString += "\(h) hours"
            }
        }
        if m > 0 || h > 0 {
            if m == 0 || m == 1 {
                timeString += " \(m) minute"
            } else {
                timeString += " \(m) minutes"
            }
        }
        if s > 0 {
            if s == 1 {
                timeString += " \(s) second"
            } else {
                timeString += " \(s) seconds"
            }
        }
        return timeString
    }
}

extension Double {
    static func roundToPlaces(_ value: Double, decimalPlaces: Int) -> Double {
        let divisor = pow(10.0, Double(decimalPlaces))
        return (value * divisor).rounded() / divisor
    }
}

extension String {
    static func isValidPhone(_ string: String) -> Bool {
        let emailRegEx = "\\d{10}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    static func isValidEmail(_ string: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    static func isNumerical(_ string: String) -> Bool {
        let range = string.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted)
        return (range == nil)
    }
    static func replace(_ myString: String, _ index: Int, _ newChar: Character) -> String {
        var modifiedString = String()
        for (i, char) in myString.characters.enumerated() {
            modifiedString += String((i == index) ? newChar : char)
        }
        return modifiedString
    }
}

extension Date {
    static func GetStringDate(_ dateString: String, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        //print("\(dateString) \(dateFormatter.dateFromString(dateString)!)")
        return dateFormatter.date(from: dateString)!
    }
    static func GetDateString(_ date: Date, format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        //print("\(date) \(dateFormatter.stringFromDate(date))")
        return dateFormatter.string(from: date)
    }
    static func GetDateStringInFormat(_ dateString: String, withFormat: String, toFormat: String) -> String {
        return Date.GetDateString(Date.GetStringDate(dateString, format: withFormat), format: toFormat)
    }
}

extension NSImageView {
    func DownloadFrom(link:String?) {
        image = NSImage(named: "placeholder")
        if link != nil, let url = URL(string: link!) {
            URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
                guard let data = data, error == nil else {
                    return
                }
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                    DispatchQueue.main.async {
                        self.image = NSImage(data: data)
                    }
                }
            }) .resume()
        }
    }
}

extension NSImage {
    var ImagePNGRepresentation: Data {
        return NSBitmapImageRep(data: tiffRepresentation!)!.representation(using: .PNG, properties: [:])!
    }
    func SavePNG(_ path:String) -> Bool {
        return ((try? ImagePNGRepresentation.write(to: URL(fileURLWithPath: path), options: [.atomic])) != nil)
    }
}

extension Bundle {
    class var Version: String {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return "ukn"
    }
    class var Build: String {
        if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return build
        }
        return "ukn"
    }
}

extension NSBezierPath {
    func CGPath(forceClose:Bool) -> CGPath? {
        var cgPath:CGPath? = nil
        
        let numElements = self.elementCount
        if numElements > 0 {
            let newPath = CGMutablePath()
            let points = NSPointArray.allocate(capacity: 3)
            var bDidClosePath:Bool = true
            
            for i in 0 ..< numElements {
                
                switch element(at: i, associatedPoints:points) {
                    
                case NSBezierPathElement.moveToBezierPathElement:
                    newPath.move (to: CGPoint(x : points[0].x, y: points[0].y) )
                  //  CGPathMoveToPoint(newPath, nil, points[0].x, points[0].y )
                    
                case NSBezierPathElement.lineToBezierPathElement:
                    newPath.addLine(to: CGPoint(x : points[0].x, y: points[0].y))
                  //  CGPathAddLineToPoint(newPath, nil, points[0].x, points[0].y )
                    bDidClosePath = false
                    
                case NSBezierPathElement.curveToBezierPathElement:
                    newPath.addCurve(to: CGPoint(x: points[0].x, y: points[0].y), control1: CGPoint(x: points[1].x, y: points[1].y), control2: CGPoint(x: points[2].x, y: points[2].y))
                  //  CGPathAddCurveToPoint(newPath, nil, points[0].x, points[0].y, points[1].x, points[1].y, points[2].x, points[2].y )
                    bDidClosePath = false
                    
                case NSBezierPathElement.closePathBezierPathElement:
                    newPath.closeSubpath()
                    bDidClosePath = true
                }
                
                if forceClose && !bDidClosePath {
                    newPath.closeSubpath()
                }
            }
            cgPath = newPath.copy()
        }
        return cgPath
    }
}

/*extension NSView {
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
            }, completion: completion)
    }
}*/
