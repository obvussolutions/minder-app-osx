//
//  WebManager.swift
//  Minder
//
//  Created by Abdul on 8/16/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

protocol WebManagerDelegate {
    func ProgressUpdate(_ progress: Float)
    func DownloadComplete(_ data: Data)
}

class WebManager: NSObject {
    
    static func Get(_ url: String, completionHandler: @escaping (Any) -> Void) {
        let task = Foundation.URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                completionHandler(["status":"Failed", "message":"\(error)"])
            } else {
                let httpResponse = response as! HTTPURLResponse
                if (httpResponse.statusCode == 200) {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? Dictionary<String, Any> {
                            completionHandler(json as AnyObject)
                        } else {
                            completionHandler(["status":"Failed", "message":"Unable to serialize responce json, seems invalid"])
                        }
                    } catch {
                        completionHandler(["status":"Failed", "message":"\(error)"])
                    }
                } else {
                    completionHandler(["status":"Failed", "message":"HttpResponse Status: \(httpResponse.statusCode)"])
                }
            }
        })
        task.resume()
    }
    static func Post(_ params: Array<Dictionary<String, String>>, url: String, completionHandler: @escaping (Dictionary<String, Any>) -> Void) {
        do {
            let url: URL = URL(string: url)!
            let request = NSMutableURLRequest(url: url)
            let session = Foundation.URLSession.shared
            request.httpMethod = "POST"
            
            try request.httpBody = JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                
                if (error != nil) {
                    completionHandler(["status":"Failed", "message":"\(error)"])
                } else {
                    let httpResponse = response as! HTTPURLResponse
                    if (httpResponse.statusCode == 200) {
                        do {
                            if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? Dictionary<String, AnyObject> {                                
                                completionHandler(json)
                            } else {
                                completionHandler(["status":"Failed", "message":"Unable to serialize responce json, seems invalid"])
                            }
                        } catch {
                            completionHandler(["status":"Failed", "message":"\(error)"])
                        }
                    } else {
                        completionHandler(["status":"Failed", "message":"HttpResponse Status: \(httpResponse.statusCode)"])
                    }
                }
            }) 
            task.resume()
        }
        catch {
            completionHandler(["status":"Failed" as AnyObject, "message":"\(error)" as AnyObject])
        }
    }
    
    func DownloadFile(_ urlString: String, fileName: String) {
        if let url = URL(string: urlString) {
            NSLog("Download started \(urlString)")
            let sessionConfiguration = URLSessionConfiguration.default
            let session = Foundation.URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
            let downloadTask = session.downloadTask(with: url)
            downloadTask.taskDescription = fileName
            downloadTask.resume()
        }
    }
}

extension WebManager : URLSessionDelegate {
    
}

extension WebManager : URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let fileManager = FileManager.default
        do {
            var appDirPath = MinderManager.sharedInstance.appDirectory
            if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
                appDirPath += (MinderManager.sharedInstance.userLogged?.userName)! + "/Activities/"
            } else {
                appDirPath += "_/Activities/"
            }
            try fileManager.moveItem(at: location, to: URL(fileURLWithPath: appDirPath + downloadTask.taskDescription!))
            try fileManager.removeItem(at: location)
            NSLog("File downloaded \(downloadTask.taskDescription!)")
        } catch { }
        /*dispatch_async(dispatch_get_main_queue(), {() -> Void in
         //self.progressView.hidden = true
         //self.imageView!.image = UIImage(data: data)!
         })*/
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        var progress: Float = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        /*dispatch_async(dispatch_get_main_queue(), {() -> Void in
         //self.progressView.progress = progress
         })*/
    }
}

