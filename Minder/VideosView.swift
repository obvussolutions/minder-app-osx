//
//  VideosView.swift
//  Minder
//
//  Created by Febin on 4/21/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa

class VideosView: NSView {
    
    let videoView:NSView = {
        let view = NSView()
        view.layer?.frame = CGRect(x: 331, y: 25, width: 10, height: 10)
        view.layer?.backgroundColor = NSColor.init(calibratedRed: 5/255, green: 59/255, blue: 69/255, alpha: 1).cgColor
        return view
    }()

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        // Drawing code here.
        self.addSubview(videoView)
    }
    
    override func awakeFromNib() {
        self.layer?.backgroundColor = CGColor.init(gray: 1, alpha: 1)
        self.addSubview(videoView)
    }
    
}
