//
//  MeditationView.swift
//  Minder
//
//  Created by Abdul on 4/21/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa

class MeditationView: NSView {
    var guideRingRadius: CGFloat = 35
    var ballRadius: CGFloat = 25
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        // Ball
        let radiusBall = (dirtyRect.size.height * ballRadius) / 100
        let bRect = NSMakeRect((dirtyRect.size.height/2) - radiusBall, (dirtyRect.size.height/2) - radiusBall, radiusBall * 2, radiusBall * 2)
        let bPath: NSBezierPath = NSBezierPath(ovalIn: bRect)
        NSColor.init(calibratedRed: 44/255, green: 255/255, blue: 255/255, alpha: 1).set()
        bPath.fill()
        
        // Guiding Ring
        let radiusGuideRing = (dirtyRect.size.height * guideRingRadius) / 100
        let cRect = NSMakeRect((dirtyRect.size.height/2) - radiusGuideRing, (dirtyRect.size.height/2) - radiusGuideRing, radiusGuideRing * 2, radiusGuideRing * 2)
        let cPath: NSBezierPath = NSBezierPath(ovalIn: cRect)
        NSColor.red.set()
        cPath.lineWidth = (dirtyRect.size.height * 0.75 ) / 100
        cPath.stroke()
    }
}
