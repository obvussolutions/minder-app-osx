//
//  BallPathRecord.swift
//  Minder
//
//  Created by Abdul on 10/27/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class BallPathRecord: NSObject, NSCoding {
    var name: String = ""
    var sensitivity: Float = 1
    var difficultyYellow: Float = 0
    var difficultyRed: Float = 0
    
    var posturePaths: [PostureData] = [PostureData]()
    var gyroPaths: [GyroData] = [GyroData]()
    var image: NSImage = NSImage()
    
    override init() {
        
    }
    
    required init(coder aDecoder: NSCoder) {
        if let _name = aDecoder.decodeObject(forKey: "name") as? String {
            self.name = _name
        }
        if let _sensitivity = aDecoder.decodeObject(forKey: "sensitivity") as? Float {
            self.sensitivity = _sensitivity
        }
        if let _difficultyYellow = aDecoder.decodeObject(forKey: "difficultyYellow") as? Float {
            self.difficultyYellow = _difficultyYellow
        }
        if let _difficultyRed = aDecoder.decodeObject(forKey: "difficultyRed") as? Float {
            self.difficultyRed = _difficultyRed
        }
        if let _posturePaths = aDecoder.decodeObject(forKey: "posturePaths") as? [PostureData] {
            self.posturePaths = _posturePaths
        }
        if let _gyroPaths = aDecoder.decodeObject(forKey: "gyroPaths") as? [GyroData] {
            self.gyroPaths = _gyroPaths
        }
    }

    func encode(with aCoder: NSCoder) {
        if let _name = self.name as? String {
            aCoder.encode(_name, forKey: "name")
        }
        if let _sensitivity = self.sensitivity as? Float {
            aCoder.encode(_sensitivity, forKey: "sensitivity")
        }
        if let _difficultyYellow = self.difficultyYellow as? Float {
            aCoder.encode(_difficultyYellow, forKey: "difficultyYellow")
        }
        if let _difficultyRed = self.difficultyRed as? Float {
            aCoder.encode(_difficultyRed, forKey: "difficultyRed")
        }
        if let _posturePaths = self.posturePaths as? [PostureData] {
            aCoder.encode(_posturePaths, forKey: "posturePaths")
        }
        if let _gyroPaths = self.gyroPaths as? [GyroData] {
            aCoder.encode(_gyroPaths, forKey: "gyroPaths")
        }
    }
}
