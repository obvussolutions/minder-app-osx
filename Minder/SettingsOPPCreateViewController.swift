//
//  SettingsAdvancedViewController.swift
//  Minder
//
//  Created by Abdul on 10/11/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class SettingsOPPCreateViewController: NSViewController {
    
    var oppIndex: Int = -1
    var opp: OPP = OPP()
    
    var delegateSettings: DelegateSettings? = nil

    @IBOutlet weak var sensitivity: NSSlider!
    @IBOutlet weak var labelSensitivityPosture: NSTextField!
    @IBOutlet weak var sliderSensitivityRotation: NSSlider!
    @IBOutlet weak var labelSensitivityRotation: NSTextField!
    @IBOutlet weak var sliderDifficultyYellow: NSSlider!
    @IBOutlet weak var sliderDifficultyRed: NSSlider!
    @IBOutlet weak var checkSeatback: NSButton!
    @IBOutlet weak var sliderDifficultySeatback: NSSlider!
    @IBOutlet weak var textOPPTitle: NSTextField!
    @IBOutlet weak var buttonAddUpdate: NSButton!
    @IBOutlet weak var buttonDelete: NSButton!
    @IBOutlet var Oppslidervolume: NSSlider!
    @IBOutlet var Oppslidertransperency: NSSlider!
    @IBOutlet var Oppvibration: NSPopUpButton!
    @IBOutlet weak var popButtonNotificationDevice: NSPopUpButton!
    @IBOutlet weak var oppButtonInterval1Yellow: NSPopUpButton!
    @IBOutlet weak var oppButtonInterval1Red: NSPopUpButton!
    @IBOutlet weak var oppButtonInterval2Yellow: NSPopUpButton!
    @IBOutlet weak var oppButtonInterval2Red: NSPopUpButton!
    @IBOutlet weak var popButtonVibrateYellow: NSPopUpButton!
    @IBOutlet weak var popButtonVibrateRed: NSPopUpButton!
    @IBOutlet weak var oppButtonInterval1YellowVolume: NSPopUpButton!
    @IBOutlet weak var oppButtonInterval2YellowVolume: NSPopUpButton!
    @IBOutlet weak var oppButtonInterval1RedVolume: NSPopUpButton!
    @IBOutlet weak var oppButtonInterval2RedVolume: NSPopUpButton!
    
    @IBOutlet var YellowVolume: NSSlider!
    
    @IBOutlet var YellowDifficulty: NSSlider!
    
    @IBOutlet var RedVolume: NSSlider!
    
    @IBOutlet var RedDifficulty: NSSlider!
    @IBOutlet weak var YellowIntensity: NSSlider!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkSeatback.attributedTitle = NSAttributedString(string: "Supported posture zone", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : NSColor.white ])
    }
    

    override func viewWillAppear() {
        print("oppIndex \(oppIndex)")
        if oppIndex != -1 {
            RefreshControls()
            buttonAddUpdate.title = "Update"
        } else {
            buttonDelete.isHidden = true
        }
    }
    //  refreshing the view
    func RefreshControls() {
        textOPPTitle.stringValue = opp.title
        sensitivity.floatValue = opp.sensitivityPosture
        labelSensitivityPosture.stringValue = "\(round(opp.sensitivityPosture * 100) / 100)"
        sliderSensitivityRotation.floatValue = opp.sensitivityRotation
        labelSensitivityRotation.stringValue = "\(round(opp.sensitivityRotation * 100) / 100)"
        sliderDifficultyYellow.floatValue = 100 - opp.difficultyYellow
        sliderDifficultyRed.floatValue = 100 - opp.difficultyRed
        YellowIntensity.floatValue = 255 + Float(opp.yellowIntensity)
        checkSeatback.state = opp.isSeatBack
        if checkSeatback.state == 1 {
            sliderDifficultySeatback.isHidden = false
            sliderDifficultySeatback.floatValue = 100 - opp.difficultySeatback
        }
    }
    // SensitivityChanged
        @IBAction func YellowIntensityChanged(_ sender: NSSlider) {
            //opp.yellowIntensity = 255 - Int(sender.floatValue)
    }
    @IBAction func SensitivityChanged(_ sender: AnyObject) {
        opp.sensitivityPosture = sender.floatValue
        labelSensitivityPosture.stringValue = "\(round(opp.sensitivityPosture * 100) / 100)"
    }
    @IBAction func SensititvityRotationChanged(_ sender: AnyObject) {
        opp.sensitivityRotation = sender.floatValue
        labelSensitivityRotation.stringValue = "\(round(opp.sensitivityRotation * 100) / 100)"
    }
    @IBAction func NotificationDeviceChanged(_ sender: AnyObject) {
        opp.notificationDevice = (sender.selectedItem?!.tag)!
    }
    // DifficultyYellowChanged
    @IBAction func DifficultyYellowChanged(_ sender: NSSlider) {
        opp.difficultyYellow = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.yellow, value: opp.difficultyYellow)
    }
    // DifficultyRedChanged
    @IBAction func DifficultyRedChanged(_ sender: NSSlider) {
        opp.difficultyRed = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.red, value: opp.difficultyRed)
    }
    // SeatbackCheckTapped
    @IBAction func SeatbackCheckTapped(_ sender: NSButton) {
        if sender.state == 0 {
            sliderDifficultySeatback.isEnabled = false
        } else {
            sliderDifficultySeatback.isEnabled = true
        }
        opp.isSeatBack = sender.state
        delegateSettings?.SetSeatback(opp.isSeatBack)
    }
    // SetbackDifficultychanged
    @IBAction func SetbackDifficultychanged(_ sender: AnyObject) {
        opp.difficultySeatback = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.seatback, value: opp.difficultySeatback)
    }
    // OPPDeleteClicked
    @IBAction func OPPDeleteClicked(_ sender: AnyObject) {
        MinderManager.sharedInstance.settings?.OPPs.remove(at: oppIndex)
        delegateSettings?.OPPClicked(-1)
    }
    // OPPAddClicked
    @IBAction func OPPAddClicked(_ sender: AnyObject) {
        if textOPPTitle.stringValue != "" {
            opp.title = textOPPTitle.stringValue
            print("OPPAddClicked: \(opp.isSeatBack)")
            if oppIndex != -1 {
                MinderManager.sharedInstance.settings?.OPPs.remove(at: oppIndex)
                MinderManager.sharedInstance.settings?.OPPs.insert(opp, at: oppIndex)
                delegateSettings?.OPPClicked(oppIndex)
            } else {
                MinderManager.sharedInstance.settings?.OPPs.append(opp)
                delegateSettings?.OPPClicked((MinderManager.sharedInstance.settings?.OPPs.count)! - 1)
            }
            /*MinderManager.sharedInstance.settings?.sensitivity = opp.sensitivity
            MinderManager.sharedInstance.settings?.zoneYellow?.difficulty = opp.difficultyYellow
            MinderManager.sharedInstance.settings?.zoneRed?.difficulty = opp.difficultyRed
            MinderManager.sharedInstance.settings?.isSeatBack = opp.isSeatBack
            MinderManager.sharedInstance.settings?.difficultySeatback = opp.difficultySeatback*/
            
            MinderManager.sharedInstance.settings?.OPPSelected = (MinderManager.sharedInstance.settings?.OPPs.count)! - 1
            
            MinderManager.sharedInstance.SaveSettings()
            textOPPTitle.stringValue = ""
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Please enter title"
            myPopup.informativeText = "You need to enter title to save the OPP."
            myPopup.alertStyle = .warning
            myPopup.addButton(withTitle: "OK")
            myPopup.runModal()
        }
    }
    // CancelClicked
    @IBAction func CancelClicked(_ sender: AnyObject) {
        delegateSettings?.OPPClicked(-1)
    }    
    
    // OppInterval1YellowChanged
    @IBAction func OppInterval1YellowChanged(_ sender: NSPopUpButton) {
        opp.interval1YellowVibrate = (sender.selectedItem?.tag)!
    }
    
    // OppInterval1RedChanged
    @IBAction func OppInterval1RedChanged(_ sender: NSPopUpButton) {
        opp.interval1RedVibrate = (sender.selectedItem?.tag)!
    }
    
    // OppInterval2YellowChanged
    @IBAction func OppInterval2YellowChanged(_ sender: NSPopUpButton) {
        opp.interval2YellowVibrate = (sender.selectedItem?.tag)!
    }
    
    // OppInterval2RedChanged
    @IBAction func OppInterval2RedChanged(_ sender: NSPopUpButton) {
        opp.interval2RedVibrate = (sender.selectedItem?.tag)!
    }
    
    // OppVibrateYellowChanged
    @IBAction func OppVibrateYellowChanged(_ sender: NSPopUpButton) {
        opp.vibrateYellow = (sender.selectedItem?.tag)!
        if opp.vibrateYellow == 0 {
            oppButtonInterval1Yellow.isEnabled = false
            oppButtonInterval2Yellow.isEnabled = false
        } else {
            oppButtonInterval1Yellow.isEnabled = true
            oppButtonInterval2Yellow.isEnabled = true
        }
    }
    
    // OppVibrateRedChanged
    @IBAction func OppVibrateRedChanged(_ sender: NSPopUpButton) {
        opp.vibrateRed = (sender.selectedItem?.tag)!
        if opp.vibrateRed == 0 {
            oppButtonInterval1Red.isEnabled = false
            oppButtonInterval2Red.isEnabled = false
        } else {
            oppButtonInterval1Red.isEnabled = true
            oppButtonInterval2Red.isEnabled = true
        }
    }
    
    @IBAction func Interval1YellowVolumeChanged(_ sender: AnyObject) {
        opp.interval1YellowVolume = (sender.selectedItem?!.tag)!
    }
    @IBAction func Interval2YellowVolumeChanged(_ sender: AnyObject) {
        opp.interval2YellowVolume = (sender.selectedItem?!.tag)!
    }
    // YellowVolumeChanged
    @IBAction func YellowVolumeChanged(_ sender: AnyObject) {
        opp.volumeYellow = sender.floatValue
        if opp.volumeYellow == 0 {
            oppButtonInterval1YellowVolume.isEnabled = false
            oppButtonInterval2YellowVolume.isEnabled = false
        } else {
            oppButtonInterval1YellowVolume.isEnabled = true
            oppButtonInterval2YellowVolume.isEnabled = true
        }
    }
    
    @IBAction func Interval1RedVolumeChanged(_ sender: AnyObject) {
        opp.interval1RedVolume = (sender.selectedItem?!.tag)!
    }
    @IBAction func Interval2RedVolumeChanged(_ sender: AnyObject) {
        opp.interval2RedVolume = (sender.selectedItem?!.tag)!
    }
    // RedVolumeChanged
    @IBAction func RedVolumeChanged(_ sender: AnyObject) {
        opp.volumeRed = sender.floatValue
        if opp.volumeRed == 0 {
            oppButtonInterval1RedVolume.isEnabled = false
            oppButtonInterval2RedVolume.isEnabled = false
        } else {
            oppButtonInterval1RedVolume.isEnabled = true
            oppButtonInterval2RedVolume.isEnabled = true
        }
    }
    
    // TransperencyChanged
    @IBAction func TransperencyChanged(_ sender: AnyObject) {
         opp.transperancy = sender.floatValue
    }
    
}
