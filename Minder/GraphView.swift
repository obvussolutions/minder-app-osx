//
//  GraphView.swift
//  Minder
//
//  Created by Abdul on 9/13/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

enum GraphType {
    case bar
    case pie
    case heat
    case completed
}

class GraphView: NSView {
    
    var graphType: GraphType = .pie
    internal var positionDataList: [PositionData] = [PositionData]()
    
    // Holds zone precentage value, no need to hold green value
    internal var zoneYellowValue: CGFloat = 0
    internal var zoneRedValue: CGFloat = 0
    
    internal var imageHeatmap: NSImage? = nil
    
    func reDraw() {
        needsDisplay = true
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        switch graphType {
        case .bar:
            let barSpacing: CGFloat = 3
            let barWidth: CGFloat = 15
            var x: CGFloat = 6
            for positionData: PositionData in positionDataList {
                var y: CGFloat = CGFloat(positionData.green) * 2
                let gPath: NSBezierPath = NSBezierPath()
                gPath.move(to: CGPoint(x: x + barWidth, y: 0))
                gPath.line(to: CGPoint(x: x, y: 0))
                gPath.line(to: CGPoint(x: x, y: y))
                gPath.line(to: CGPoint(x: x + barWidth, y: y))
                gPath.line(to: CGPoint(x: x + barWidth, y: 0))
                gPath.close()
                NSColor.green.set()
                gPath.fill()
                if positionData.yellow != 0 {
                    let yPath: NSBezierPath = NSBezierPath()
                    yPath.move(to: CGPoint(x: x + barWidth, y: y))
                    yPath.line(to: CGPoint(x: x, y: y))
                    let yG: CGFloat = y
                    y = yG + (CGFloat(positionData.yellow) * 2)
                    yPath.line(to: CGPoint(x: x, y: y))
                    yPath.line(to: CGPoint(x: x + barWidth, y: y))
                    yPath.line(to: CGPoint(x: x + barWidth, y: yG))
                    yPath.close()
                    NSColor.yellow.set()
                    yPath.fill()
                }
                if positionData.red != 0 {
                    let rPath: NSBezierPath = NSBezierPath()
                    rPath.move(to: CGPoint(x: x + barWidth, y: y))
                    rPath.line(to: CGPoint(x: x, y: y))
                    let yR: CGFloat = y
                    y = yR + (CGFloat(positionData.red) * 2)
                    rPath.line(to: CGPoint(x: x, y: y))
                    rPath.line(to: CGPoint(x: x + barWidth, y: y))
                    rPath.line(to: CGPoint(x: x + barWidth, y: yR))
                    rPath.close()
                    NSColor.red.set()
                    rPath.fill()
                }
                x += barSpacing + barWidth
            }
            break
        case .pie:
            let centerCircle: NSPoint = NSMakePoint(dirtyRect.size.height/2, dirtyRect.size.height/2)
            let gPath: NSBezierPath = NSBezierPath(ovalIn: NSMakeRect(0, 0, dirtyRect.size.height, dirtyRect.size.height))
            gPath.close()
            NSColor.green.set()
            gPath.fill()
            var angleYellow: CGFloat = 0
            var angleRed: CGFloat = 360
            if zoneYellowValue != 0 {
                let yPath: NSBezierPath = NSBezierPath()
                yPath.move(to: centerCircle)
                angleYellow = (zoneYellowValue * 360) / 100
                yPath.appendArc(withCenter: centerCircle, radius: dirtyRect.size.height/2, startAngle: 0, endAngle: angleYellow)
                yPath.close()
                NSColor.yellow.set()
                yPath.fill()
                
                angleYellow = (angleYellow/2) * CGFloat(M_PI) / 180
                let xString = centerCircle.x + (dirtyRect.size.height/4 * cos(angleYellow))
                let yString = centerCircle.y + (dirtyRect.size.height/4 * sin(angleYellow))
                
                let attributes: NSDictionary = [
                    NSFontAttributeName : NSFont.boldSystemFont(ofSize: 9),
                    NSForegroundColorAttributeName : NSColor.black
                ]
                let currentText = NSAttributedString(string: "\(zoneYellowValue)%", attributes: attributes as? [String : AnyObject])
                currentText.draw(at: NSMakePoint(xString-(currentText.size().width/2), yString-(currentText.size().height/2)))
            }
            if zoneRedValue != 0 {
                let rPath: NSBezierPath = NSBezierPath()
                rPath.move(to: centerCircle)
                angleRed = angleRed - ((zoneRedValue * 360) / 100)
                rPath.appendArc(withCenter: centerCircle, radius: dirtyRect.size.height/2, startAngle: angleRed, endAngle: 360)
                rPath.close()
                NSColor.red.set()
                rPath.fill()
                
                angleRed = (angleRed + ((360 - angleRed) / 2)) * CGFloat(M_PI) / 180
                let xString = centerCircle.x + (dirtyRect.size.height/4 * cos(angleRed))
                let yString = centerCircle.y + (dirtyRect.size.height/4 * sin(angleRed))
                
                let attributes: NSDictionary = [
                    NSFontAttributeName : NSFont.boldSystemFont(ofSize: 9),
                    NSForegroundColorAttributeName : NSColor.black
                ]
                let currentText = NSAttributedString(string: "\(zoneRedValue)%", attributes: attributes as? [String : AnyObject])
                currentText.draw(at: NSMakePoint(xString-(currentText.size().width/2), yString-(currentText.size().height/2)))
            }
            
            let angleGreen = angleYellow + ((angleRed - angleYellow) / 2)
            let xString = centerCircle.x + (dirtyRect.size.height/4 * cos(angleGreen))
            let yString = centerCircle.y + (dirtyRect.size.height/4 * sin(angleGreen))
            let attributes: NSDictionary = [
                NSFontAttributeName : NSFont.boldSystemFont(ofSize: 9),
                NSForegroundColorAttributeName : NSColor.black
            ]
            let currentText = NSAttributedString(string: "\(100 - (zoneYellowValue + zoneRedValue))%", attributes: attributes as? [String : AnyObject])
            currentText.draw(at: NSMakePoint(xString-(currentText.size().width/2), yString-(currentText.size().height/2)))
            
            let cPath: NSBezierPath = NSBezierPath(ovalIn: NSMakeRect(centerCircle.x-10, centerCircle.y-10, 20, 20))
            cPath.close()
            NSColor.white.set()
            cPath.fill()
            let ocPath: NSBezierPath = NSBezierPath(ovalIn: NSMakeRect(1, 1, dirtyRect.size.height-2, dirtyRect.size.height-2))
            ocPath.close()
            NSColor.white.set()
            ocPath.lineWidth = 2.0
            ocPath.stroke()
            break
        case .heat:
            if imageHeatmap != nil {
                imageHeatmap!.draw(in: dirtyRect)
            }
            let ocPath: NSBezierPath = NSBezierPath(ovalIn: NSMakeRect(1, 1, dirtyRect.size.height-2, dirtyRect.size.height-2))
            ocPath.close()
            NSColor.white.set()
            ocPath.lineWidth = 2.0
            ocPath.stroke()
            break
        case .completed:
            let ocPath: NSBezierPath = NSBezierPath(ovalIn: NSMakeRect(15, 15, dirtyRect.size.height - 30, dirtyRect.size.height - 30))
            ocPath.close()
            NSColor.white.set()
            ocPath.lineWidth = 2.0
            ocPath.stroke()
            
            if zoneYellowValue != 0 {
                let center: NSPoint = NSMakePoint(dirtyRect.size.height/2, dirtyRect.size.height/2)
                let yAngle = (zoneYellowValue * 360) / 100
                let yPath: NSBezierPath = NSBezierPath()
                yPath.appendArc(withCenter: center, radius: dirtyRect.size.height/3, startAngle: 0, endAngle: yAngle)
                NSColor.blue.set()
                yPath.lineWidth = 8
                yPath.stroke()
            }
            
            let attributes: NSDictionary = [
                NSFontAttributeName : NSFont.boldSystemFont(ofSize: 7),//11 ankush change
                NSForegroundColorAttributeName : NSColor.white
            ]
            let currentText = NSAttributedString(string: "      \(zoneYellowValue)%\r\nCompleted", attributes: attributes as? [String : AnyObject])
            let attrSize = currentText.size()
            currentText.draw(at: NSMakePoint((dirtyRect.size.width / 2) - (attrSize.width / 2), (dirtyRect.size.height / 2) - (attrSize.height / 2)))
            break
        }
    }
}
