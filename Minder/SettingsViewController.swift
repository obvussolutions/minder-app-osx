//
//  SettingsViewController.swift
//  Minder
//
//  Created by Abdul on 8/9/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

enum SettingsType {
    case basic
    case advanced
    case newOPP
}

protocol DelegateSettings: class {
    func UpdateDifficulty(_ type: ZoneType, value: Float)
    func UpdateNotification()
    func SetSeatback(_ value: Int)
    func ConnectDevice()
    func UpdateProcessingPaused()
    func OPPClicked(_ index: Int)
    func OPPChanged()
    func ToggleGyroRotation()
    ///fbn
    func UpdateTransperency()
   // func ShowHideWidgetEntity(entityName : String, isHiddenStatus : Bool)
}

class SettingsViewController: NSViewController {
    
    @IBOutlet weak var buttonSettingsToggle: NSButton!
    @IBOutlet weak var viewPlaceHolder: NSView!
    var delegateSettings: DelegateSettings? = nil
    var typeSettings: SettingsType = .basic
    var viewControllerCurrent: NSViewController!
    var OPPIndex: Int = -1 // need to track oop for manage
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.wantsLayer = true
        
        let styleParagraph = NSMutableParagraphStyle()
        styleParagraph.alignment = .center
        buttonSettingsToggle.attributedTitle = NSAttributedString(string: "ADVANCED", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : styleParagraph ])
        buttonSettingsToggle.layer!.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).cgColor
        
        SwitchContainerViewController()
    }
    // Setting toggle functionality
    @IBAction func SettingsToggleTapped(_ sender: AnyObject) {
        let styleParagraph = NSMutableParagraphStyle()
        styleParagraph.alignment = .center
        switch typeSettings {
        case .basic:
            typeSettings = .advanced
            buttonSettingsToggle.attributedTitle = NSAttributedString(string: "BASIC", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : styleParagraph ])
        case .advanced:
            typeSettings = .basic
            buttonSettingsToggle.attributedTitle = NSAttributedString(string: "ADVANCED", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : styleParagraph ])
        default:
            break;
        }
        SwitchContainerViewController()
    }
    // Switching to different view
    func SwitchContainerViewController() {
        let subViews = self.viewPlaceHolder.subviews
        for view in subViews {
            view.removeFromSuperview()
        }
        
        let mainStoryboard: NSStoryboard = NSStoryboard(name: "Main", bundle: nil)
        switch typeSettings {
        case .basic:
            let vc = mainStoryboard.instantiateController(withIdentifier: "SettingsBasicViewController") as? SettingsBasicViewController
            vc?.delegateSettings = self
            self.viewControllerCurrent = vc
            buttonSettingsToggle.isHidden = false
            break
        case .advanced:
            let vc = mainStoryboard.instantiateController(withIdentifier: "SettingsAdvancedViewController") as? SettingsAdvancedViewController
            vc?.delegateSettings = self
            self.viewControllerCurrent = vc
            break
        case .newOPP:
            let vc = mainStoryboard.instantiateController(withIdentifier: "SettingsOPPCreateViewController") as? SettingsOPPCreateViewController
            vc?.delegateSettings = self
            if OPPIndex != -1 {
                vc?.oppIndex = OPPIndex - 1
                vc?.opp = (MinderManager.sharedInstance.settings?.OPPs[OPPIndex - 1])!
            }
            self.viewControllerCurrent = vc
            buttonSettingsToggle.isHidden = true
            break
        }
        
        self.viewPlaceHolder.addSubview(self.viewControllerCurrent.view)
        self.viewControllerCurrent.view.setFrameOrigin(NSPoint(x: 0, y: 0))
        self.viewControllerCurrent.view.setFrameSize(viewPlaceHolder.frame.size)
    }
    // refreshing the view
    func RefreshControls() {
        if typeSettings == .basic {
            if let viewControllerCurrent = viewControllerCurrent {
                let vcBasicVC = (viewControllerCurrent as! SettingsBasicViewController)
                vcBasicVC.RefreshControls()
            }
        } else if typeSettings == .advanced {
            if let viewControllerCurrent = viewControllerCurrent {
                let vcAdvancedVC = (viewControllerCurrent as! SettingsAdvancedViewController)
                vcAdvancedVC.RefreshControls()
            }
        }
    }
}
 // Settings functionalities
extension SettingsViewController : DelegateSettings {
//     func ShowHideWidgetEntity(entityName: String, isHiddenStatus: Bool) {
//        delegateSettings?.ShowHideWidgetEntity(entityName: "", isHiddenStatus: true)
//    }

    func ToggleGyroRotation() {
        delegateSettings?.ToggleGyroRotation()
    }
    func OPPChanged() {
        delegateSettings?.OPPChanged()
    }
    func SetSeatback(_ value: Int) {
        delegateSettings?.SetSeatback(value)
    }
    func UpdateDifficulty(_ type: ZoneType, value: Float) {
        delegateSettings?.UpdateDifficulty(type, value: value)
    }
    func UpdateNotification() {
        delegateSettings?.UpdateNotification()
    }
    func ConnectDevice() {
        delegateSettings?.ConnectDevice()
    }
    func UpdateProcessingPaused() {
        delegateSettings?.UpdateProcessingPaused()
    }
    func OPPClicked(_ oppIndex: Int) {
        switch typeSettings {
        case .basic:
            typeSettings = .newOPP
            if oppIndex != -1 {
                OPPIndex = oppIndex
            }
            break
        case .newOPP:
            typeSettings = .basic
            if oppIndex != -1 {
                
            }
            OPPIndex = -1
            break
        default:
            break
        }
        SwitchContainerViewController()
        RefreshControls()
        
        
    }
    
    func UpdateTransperency() {
        delegateSettings?.UpdateTransperency()
    }
}
