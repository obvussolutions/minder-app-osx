//
//  ViewController.swift
//  Minder
//
//  Created by Abdul on 8/9/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

// To distiguish different appication modes
enum AppMode {
    case Normal
    case Gadget
}

// To distiguish different operations on gadget
enum GadgetOperationMode {
    case None
    case Click
    case Move
    case Resize
    case ResizeYellow
    case ResizeRed
    case ResizeSeatback
}

// To distiguish different sub view type
enum ViewType {
    case Dashboard
    case Guidance
    case Settings
}

// To distiguish different zone types
enum ZoneType {
    case Green
    case Yellow
    case Red
    case Seatback
    case None
}

protocol DelegateDeviceHandler {
    func UpdateAlertText(text: String)
    func StatusUpdated()
    func UpdateCharge(charge: Float)
    func UpdatePosture(x: CGFloat, withY y: CGFloat)
    func UpdateColor(type: ZoneType)
    func UpdateDataGraphs()
    func UpdateUpdateTime(elapsed: Int)
    func UpdateStepCount()
    func UpdateGyroWithRoll(roll: CGFloat, withPitch pitch: CGFloat, withYaw yaw: CGFloat, withT t: Double)
    func EnableSeatback()
}

class HomeViewController: NSViewController {
    
    @IBOutlet weak var buttonDashboard: NSButton!
    @IBOutlet weak var buttonGuidance: NSButton!
    @IBOutlet weak var buttonSettings: NSButton!
    @IBOutlet weak var buttonWebsite: NSButton!
    @IBOutlet weak var buttonSync: NSButton!
    @IBOutlet weak var buttonNotification: NSButton!
    
    @IBOutlet weak var buttonCalibrate: NSButton!
    @IBOutlet weak var buttonRecord: NSButton!
    
    @IBOutlet weak var buttonClose: NSButton!
    
    @IBOutlet weak var buttonMenuDashboard: NSButton!
    @IBOutlet weak var buttonMenuGuidance: NSButton!
    @IBOutlet weak var buttonMenuNotification: NSButton!
    @IBOutlet weak var buttonMenuProcessPause: NSButton!
    @IBOutlet weak var buttonMenuSettings: NSButton!
    
    @IBOutlet weak var labelTrackingBall: NSTextField!
    @IBOutlet weak var viewTrackball: NSView!
    @IBOutlet weak var viewPosture: PostureView!
    @IBOutlet weak var viewBattery: NSView!
    @IBOutlet weak var viewStatusBar: NSView!
    @IBOutlet weak var imageBattery: NSImageView!
    @IBOutlet weak var viewCurveMenuHolder: NSView!
    @IBOutlet weak var viewAnnulus: AnnulusView!
    @IBOutlet weak var viewBall: NSView!
    @IBOutlet weak var viewGyroControls: NSView!
    
    @IBOutlet weak var viewLogin: NSView!
    @IBOutlet weak var buttonLogin: NSButton!
    @IBOutlet var btnLogoutViewToggle: NSButton!
    @IBOutlet var logOutView: NSView!
    @IBOutlet var logoutLineView: NSView!
    @IBOutlet var userImageRoundView: NSImageView!
    @IBOutlet weak var CurveimageView: NSImageView!
    @IBOutlet var editProfileButon: NSButton!
    @IBOutlet var logoutButon: NSButton!
    @IBOutlet var updownArrow: NSImageView!
    
    var viewControllerCurrent: NSViewController!
    @IBOutlet weak var viewPlaceHolder: NSView!
    
    @IBOutlet var userNameLabel: NSTextField!
    var initialLocation: NSPoint = NSPoint()
    var curveMenu: CurveMenuViewController?
    var viewControllerEditProfile: EditProfileViewcontoller?
    
    var originNormalView: NSPoint? = nil   // holds normal view origin
    var originGadgetView: NSPoint? = nil   // holds gadget view origin
    var sizeNormalView: NSSize? = nil   // holds normal view size
    var sizeGadgetView: CGFloat? = nil   // holds gadget view size
    
    var delegateLogin: DelegateMainView? = nil
    var trackingDeviceResizeLocation:NSTrackingArea?
    var gadgetOperation: GadgetOperationMode = .None
    
    var isResizeReset: Bool = false
    var isSwitchingView: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.wantsLayer = true
        
        // Assign "Escape" as close button key equivalent
        buttonClose.keyEquivalent = "\u{1b}"
        
        let trackingProfileImageHover:NSTrackingArea = NSTrackingArea(rect: userImageRoundView.bounds, options: [NSTrackingAreaOptions.MouseEnteredAndExited,NSTrackingAreaOptions.ActiveAlways], owner: self, userInfo: ["section": "ProfileImage"])
        userImageRoundView.addTrackingArea(trackingProfileImageHover)
        
        let appManager = MinderManager.sharedInstance
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        var logString: String = "Position-Data not found"
        if let data = userDefaults.objectForKey("pDataArray") as? NSData{
            appManager.pDataArray = (NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [PositionData])!
            logString = "Position-Data loaded"
        }
        NSLog(logString)
        
        logString = "GYR values not found"
        if let data = userDefaults.objectForKey("GYR") as? String {
            let gyr = data.characters.split{$0 == ":"}.map(String.init)
            appManager.green = Int(gyr[0])!
            appManager.yellow = Int(gyr[1])!
            appManager.red = Int(gyr[2])!
            logString = "GYR values loaded"
        }
        NSLog(logString)
        
        logString = "Steps not found"
        if let data = userDefaults.objectForKey("Step") as? String {
            appManager.dStepCount = Int(data)!
            logString = "Steps loaded"
        }
        NSLog(logString)
        
        logString = "originGadgetView not found"
        if let data = userDefaults.objectForKey("originGadgetView") as? NSString {
            originGadgetView = NSPointFromString(data as String)
            /*if MinderManager.sharedInstance.appMode == .Gadget {
             view.window?.setFrameOrigin(originGadgetView!)
             }*/
            logString = "originGadgetView loaded"
        }
        NSLog(logString)
        logString = "originNormalView not found"
        if let data = userDefaults.objectForKey("originNormalView") as? NSString {
            originNormalView = NSPointFromString(data as String)
            /*if MinderManager.sharedInstance.appMode == .Normal {
             view.window?.setFrameOrigin(originNormalView!)
             }*/
            logString = "originNormalView loaded"
        }
        NSLog(logString)
        logString = "sizeGadgetView not found"
        if let data = userDefaults.objectForKey("sizeGadgetView") as? CGFloat {
            sizeGadgetView = data
            if MinderManager.sharedInstance.appMode == .Gadget {
                //GadgetResize(sizeGadgetView!)
            }
            logString = "sizeGadgetView loaded"
        }
        NSLog(logString)
        logString = "sizeNormalView not found"
        if let data = userDefaults.objectForKey("sizeNormalView") as? NSString {
            sizeNormalView = NSSizeFromString(data as String)
            if MinderManager.sharedInstance.appMode == .Normal {
                view.window?.setFrame(NSMakeRect(originNormalView!.x, originNormalView!.y, sizeNormalView!.width, sizeNormalView!.height), display: true)
            }
            logString = "sizeNormalView loaded"
        }
        NSLog(logString)
        
        var appDirPath = appManager.appDirectory
        if !(appManager.userLogged?.userName ?? "").isEmpty {
            appDirPath += (appManager.userLogged?.userName)! + "/"
            appManager.GetZoneData({ _ in })
            GetActivities(false)
            NSLog("UserToken: \(appManager.userLogged?.token)")
        } else {
            appDirPath += "_/"
        }
        
        if appManager.settings?.OPPs.count == 0 {
            let opp: OPP = OPP()
            opp.title = "Default"
            opp.sensitivityPosture = (opp.sensitivityPosture)
            opp.difficultyRed = (opp.difficultyRed)
            opp.difficultyYellow = (opp.difficultyYellow)
            opp.isSeatBack = (opp.isSeatBack)
            opp.difficultySeatback = (opp.difficultySeatback)
            appManager.settings?.OPPs.append(opp)
            appManager.SaveSettings()
        }
        
        logString = "Heatmap not found"
        if NSFileManager.defaultManager().fileExistsAtPath(appDirPath + "hm.png") {
            appManager.imageHeatmap = NSImage(contentsOfFile: appDirPath + "hm.png")
            logString = "Heatmap loaded"
        }
        NSLog(logString)
        
        // To get padding of device view
        appManager.centerDeviceView = NSPoint(x: viewPosture.frame.size.width / 2, y: viewPosture.frame.size.height / 2)
        appManager.centerDeviceViewHeight = viewPosture.frame.size.height
        
        // Setup
        let mainStoryboard: NSStoryboard = NSStoryboard(name: "Main", bundle: nil)
        viewCurveMenuHolder.alphaValue = 0
        
        let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        viewPosture.idelegate = self
        viewPosture.UpdateDifficulty(.Red, difficulty: (opp.difficultyRed))
        viewPosture.UpdateDifficulty(.Yellow, difficulty: (opp.difficultyYellow))
        SetSeatback(opp.isSeatBack)
        viewPosture.UpdateDifficulty(.Seatback, difficulty: (opp.difficultySeatback))
        
        viewControllerEditProfile = (mainStoryboard.instantiateControllerWithIdentifier("ViewcontollerEditProfile") as? EditProfileViewcontoller)!
        viewControllerEditProfile?.delegateProfileEdit = self
        NSLog("Views Setup done")
        
        // Shaping n Coloring
        self.view.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1).CGColor
        
        viewGyroControls.layer?.backgroundColor = NSColor.brownColor().CGColor
        viewBall.layer?.backgroundColor = NSColor.whiteColor().CGColor
        
        MinderUtility.ShapeView(viewAnnulus, type: .Circle)
        MinderUtility.ShapeView(viewBall, type: .Circle)
        
        viewTrackball.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        MinderUtility.ShapeView(viewTrackball, type: .RectTopCurved1By3)
        
        viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 46/255, green: 204/255, blue: 114/255, alpha: CGFloat(opp.transperancy)).CGColor
        MinderUtility.ShapeView(viewPosture, type: .Circle)
        
        viewPlaceHolder.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        MinderUtility.ShapeView(viewPlaceHolder, type: .RectTopCurved1By3)
        
        viewBattery.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        MinderUtility.ShapeView(viewBattery, type: .RectTopCurved1By3)
        
        viewStatusBar.layer?.backgroundColor = NSColor.init(calibratedRed: 27/255, green: 36/255, blue: 42/255, alpha: 1).CGColor
        MinderUtility.ShapeView(viewStatusBar, type: .RectTopCurved1By3Reverse)
        
        viewLogin.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        logOutView.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        
        viewCurveMenuHolder.layer?.backgroundColor = NSColor.clearColor().CGColor
        
        buttonDashboard.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).CGColor
        buttonGuidance.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        buttonSettings.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        buttonWebsite.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        buttonSync.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        buttonNotification.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
        buttonCalibrate.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).CGColor
        buttonRecord.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).CGColor
        
        userImageRoundView.layer!.cornerRadius = self.userImageRoundView.frame.size.width / 2
        userImageRoundView.layer!.borderWidth = 1.0
        userImageRoundView.layer!.cornerRadius = 45/2
        userImageRoundView.layer!.masksToBounds = true
        
        viewLogin.hidden = true
        
        NSLog("Views Shaped and Coloured")
        
        // Naming
        let styleParagraph = NSMutableParagraphStyle()
        styleParagraph.alignment = .Center
        let attributesButton: [String : AnyObject] = [ NSFontAttributeName : NSFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : styleParagraph ]
        
        editProfileButon.attributedTitle = NSAttributedString(string: "EDIT PROFILE", attributes: [ NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : styleParagraph])
        logoutButon.attributedTitle = NSAttributedString(string: "LOGOUT", attributes: [ NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : styleParagraph])
        logoutLineView.layer?.backgroundColor = NSColor.whiteColor().CGColor
        buttonLogin.attributedTitle = NSAttributedString(string: "LOGIN", attributes: [ NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : styleParagraph])
        
        buttonDashboard.attributedTitle = NSAttributedString(string: "DASHBOARD", attributes: attributesButton)
        buttonGuidance.attributedTitle = NSAttributedString(string: "GUIDANCE", attributes: attributesButton)
        buttonSettings.attributedTitle = NSAttributedString(string: "SETTINGS", attributes: attributesButton)
        buttonWebsite.attributedTitle = NSAttributedString(string: "WEBSITE", attributes: attributesButton)
        buttonSync.attributedTitle = NSAttributedString(string: "SYNC", attributes: attributesButton)
        buttonNotification.attributedTitle = NSAttributedString(string: "RING", attributes: attributesButton)
        buttonCalibrate.attributedTitle = NSAttributedString(string: "CALIBRATE", attributes: attributesButton)
        buttonRecord.attributedTitle = NSAttributedString(string: "RECORD START", attributes: attributesButton)
        UpdateNotification()
        
        NSLog("Views Named")
        
        NSWorkspace.sharedWorkspace().notificationCenter.addObserver(self, selector: #selector(self.appDeactivated), name: NSWorkspaceDidDeactivateApplicationNotification, object: nil)
        
        if appManager.minderlib == nil {
            appManager.initDevice(self)
        }
        
        OPPChanged()
    }
    
    override func viewWillAppear() {
        SwitchContainerViewController()
        UpdateDataGraphs()
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        if originNormalView == nil {
            originNormalView = view.window!.frame.origin
            if let screenSize = NSScreen.mainScreen()!.frame.size as? CGSize {
                originGadgetView = NSMakePoint((screenSize.width/2) - (viewTrackball.frame.size.width/2) , (screenSize.height/2) - (viewTrackball.frame.size.height/2))
            }
            sizeNormalView = view.window?.frame.size
            sizeGadgetView = viewTrackball.frame.size.height
            viewBall.frame.origin = NSMakePoint(-10000, -10000)
            saveDimensions()
        }
        setupWindow()
        ResetDrawByResize()
    }
    
    //  AppDeactivation-While clicking OutWindow
    func appDeactivated(_ notification: NSNotification) {
        NSLog("app deactivated")
        if MinderManager.sharedInstance.appMode == .Normal {
            SwitchToGadget()
        }
        ResetDrawByResize()
    }
    
    //OnOffGyroControl
    func OnOffGyroControl() {
        if MinderManager.sharedInstance.isGyroOn {
            MinderManager.sharedInstance.isGyroOn = false
            (NSApp.delegate as! AppDelegate).SetMenuNotificationTitle("Rotation Rendering On")
            viewAnnulus.hidden = true
        } else {
            MinderManager.sharedInstance.isGyroOn = true
            (NSApp.delegate as! AppDelegate).SetMenuGyroOnOffTitle("Rotation Rendering Off")
            viewAnnulus.hidden = false
        }
        if MinderManager.sharedInstance.currentView == .Settings {
            if let viewControllerCurrent = viewControllerCurrent {
                let vcSettingsVC = (viewControllerCurrent as! SettingsViewController)
                vcSettingsVC.RefreshControls()
            }
        }
    }
    //ToggleGyroControl
    func ToggleGyroControl() {
        if MinderManager.sharedInstance.isGyroOn {
            if MinderManager.sharedInstance.isGyroFromDevice {
                MinderManager.sharedInstance.isGyroFromDevice = false
                viewGyroControls.hidden = false
            } else {
                MinderManager.sharedInstance.isGyroFromDevice = true
                viewGyroControls.hidden = true
            }
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Please On rotation rendering"
            myPopup.informativeText = "You need to switch on rotation rendering for this feature."
            myPopup.alertStyle = .Warning
            myPopup.addButtonWithTitle("OK")
            myPopup.runModal()
        }
    }
    
    override func keyUp(event: NSEvent) {
        
    }
    
    // MARK:MouseClickFunctions
    override func mouseEntered(theEvent: NSEvent) {
        if let userData = theEvent.trackingArea?.userInfo as? [String : String] {
            let section = userData["section"]!
            if section == "ProfileImage" {
                userImageRoundView.frame = CGRectMake(userImageRoundView.frame.origin.x-10, userImageRoundView.frame.origin.y-10, userImageRoundView.frame.width+15, userImageRoundView.frame.height+15)
            } else if section == "DeviceView" {
                if MinderManager.sharedInstance.appMode == .Gadget {
                    //viewPosture.showTriangle = true
                }
            }
        }
    }
    override func mouseExited(theEvent: NSEvent) {
        if let userData = theEvent.trackingArea?.userInfo as? [String : String] {
            let section = userData["section"]!
            if section == "ProfileImage" {
                userImageRoundView.frame = CGRectMake(userImageRoundView.frame.origin.x + 10, userImageRoundView.frame.origin.y + 10, userImageRoundView.frame.width - 15, userImageRoundView.frame.height - 15)
            } else if section == "DeviceView" {
                /*if gadgetOperation != .Resize {
                 NSCursor.arrowCursor().set()
                 }*/
            }
        }
    }
    
    override func mouseMoved(theEvent: NSEvent) {
        let windowFrame = self.view.window!.frame
        var location = NSEvent.mouseLocation()
        location.x -= windowFrame.origin.x
        location.y -= windowFrame.origin.y
        
        if MinderManager.sharedInstance.appMode == .Normal {
            location.x -= (viewTrackball.frame.origin.x + viewPosture.frame.origin.x)
            location.y -= (viewTrackball.frame.origin.y + viewPosture.frame.origin.y)
        }
        viewPosture.overZoneCircle = .None
        let marginResize = (viewPosture.frame.size.height * 4) / 100
        let radiusMove = (viewPosture.frame.size.height / 2) - marginResize
        let xDist = Float( abs(location.x - MinderManager.sharedInstance.centerDeviceView.x))
        let yDist = Float( abs(location.y - MinderManager.sharedInstance.centerDeviceView.x))
        let distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
        
        let lineWidth = viewPosture.frame.size.height / 100
        let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
        let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
        let percentYellow = (CGFloat(viewPosture.difficultyYellow) * MinderManager.percentageZoneYellow) / 100
        let radiusYellow = ((viewPosture.frame.size.height * percentYellow) / 100) + radiusBall + margin
        let radiusYellow100 = (viewPosture.frame.size.height * (MinderManager.percentageBall + MinderManager.percentageOffset + MinderManager.percentageZoneYellow)) / 100
        
        let percentRed = (viewPosture.difficultyRed * MinderManager.percentageZoneRed) / 100
        let radiusRed = ((viewPosture.frame.size.height * percentRed) / 100) + radiusYellow100 + (2 * margin)
        if CGFloat(distance) > (viewPosture.frame.size.height / 2) {
            //NSCursor.arrowCursor().set()
        } else if CGFloat(distance) >= radiusMove && CGFloat(distance) <= (viewPosture.frame.size.height / 2) && MinderManager.sharedInstance.appMode == .Gadget {
            //if (location.x > MinderManager.sharedInstance.centerDeviceView.x) && (location.y < MinderManager.sharedInstance.centerDeviceView.y) {
            NSCursor.resizeLeftRightCursor().set()
            viewPosture.overZoneCircle = .Green
            /*} else {
             NSCursor.openHandCursor().set()
             }*/
        } else if CGFloat(distance) >= (radiusYellow - lineWidth) && CGFloat(distance) <= (radiusYellow + lineWidth) {
            if MinderManager.sharedInstance.isSeatbackEnabled {
                if location.y >= MinderManager.sharedInstance.centerDeviceView.y {
                    NSCursor.resizeLeftRightCursor().set()
                    viewPosture.overZoneCircle = .Yellow
                }
            } else {
                NSCursor.resizeLeftRightCursor().set()
                viewPosture.overZoneCircle = .Yellow
            }
        } else if CGFloat(distance) >= (radiusRed - lineWidth) && CGFloat(distance) <= (radiusRed + lineWidth) {
            NSCursor.resizeLeftRightCursor().set()
            viewPosture.overZoneCircle = .Red
        } else {
            let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
            
            let sPath: NSBezierPath = NSBezierPath()
            let sYOffset: CGFloat = ((viewPosture.frame.size.height / 3) * CGFloat(opp.difficultySeatback)) / 100
            let sPoint: CGPoint = CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x, y: MinderManager.sharedInstance.centerDeviceView.y - sYOffset)
            // radian of angle 180 = 3.14159 and 360 = 6.28319
            sPath.moveToPoint(CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x + (radiusYellow * cos(3.14159)), y: MinderManager.sharedInstance.centerDeviceView.y + (radiusYellow * sin(3.14159))))
            sPath.lineToPoint(CGPoint(x: sPoint.x + (radiusYellow * cos(3.14159)), y: sPoint.y + (radiusYellow * sin(3.14159))))
            sPath.appendBezierPathWithArcWithCenter(sPoint, radius: radiusYellow, startAngle: 180, endAngle: 360)
            sPath.lineToPoint(CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x + (radiusYellow * cos(6.28319)), y: MinderManager.sharedInstance.centerDeviceView.y + (radiusYellow * sin(6.28319))))
            sPath.lineWidth = lineWidth
            
            let allPath: NSBezierPath = sPath.bezierPathByFlatteningPath
            if allPath.containsPoint(location) {
                NSCursor.resizeLeftRightCursor().set()
                viewPosture.overZoneCircle = .Seatback
            } else  if gadgetOperation != .Resize {
                NSCursor.openHandCursor().set()
            }
        }
        viewPosture.needsDisplay = true
        
    }
    
    var heightPosture: CGFloat? = nil
    var pointMouseDown: CGPoint? = nil
    var centerP: CGPoint? = nil
    override func mouseDown(theEvent: NSEvent) {
        gadgetOperation = .Click
        pointMouseDown = NSEvent.mouseLocation()
        let windowFrame = view.window!.frame
        initialLocation = NSEvent.mouseLocation()
        initialLocation.x -= windowFrame.origin.x
        initialLocation.y -= windowFrame.origin.y
        centerP = NSMakePoint(viewTrackball.frame.origin.x + (viewPosture.frame.size.width / 2), viewTrackball.frame.origin.y + (viewPosture.frame.size.height / 2))
        if MinderManager.sharedInstance.deviceActionMode == .Broadcasting {
            if MinderManager.sharedInstance.appMode == .Normal {
                initialLocation.x -= (viewTrackball.frame.origin.x + viewPosture.frame.origin.x)
                initialLocation.y -= (viewTrackball.frame.origin.y + viewPosture.frame.origin.y)
            }
            let marginResize = (viewPosture.frame.size.height * 4) / 100
            let radiusMove = (viewPosture.frame.size.height / 2) - marginResize
            let xDist = Float( abs(initialLocation.x - MinderManager.sharedInstance.centerDeviceView.x))
            let yDist = Float( abs(initialLocation.y - MinderManager.sharedInstance.centerDeviceView.x))
            let distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
            
            let lineWidth = viewPosture.frame.size.height / 100
            let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
            let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
            
            let percentYellow = (CGFloat(viewPosture.difficultyYellow) * MinderManager.percentageZoneYellow) / 100
            let radiusYellow = ((viewPosture.frame.size.height * percentYellow) / 100) + radiusBall + margin
            
            let radiusYellow100 = (viewPosture.frame.size.height * (MinderManager.percentageBall + MinderManager.percentageOffset + MinderManager.percentageZoneYellow)) / 100
            let percentRed = (viewPosture.difficultyRed * MinderManager.percentageZoneRed) / 100
            let radiusRed = ((viewPosture.frame.size.height * percentRed) / 100) + radiusYellow100 + (2 * margin)
            
            if CGFloat(distance) >= radiusMove && CGFloat(distance) <= (viewPosture.frame.size.height / 2)  && MinderManager.sharedInstance.appMode == .Gadget {
                //if (initialLocation.x > MinderManager.sharedInstance.centerDeviceView.x) && (initialLocation.y < MinderManager.sharedInstance.centerDeviceView.y) {
                gadgetOperation = .Resize
                heightPosture = viewPosture.frame.size.height
                viewCurveMenuHolder.alphaValue = 0
                viewPosture.ToggleShowProgressCircleFlag(true)
                
                viewPosture.UpdateAlertText("")
                NSCursor.resizeLeftRightCursor().set()
                /*} else {
                 NSCursor.closedHandCursor().set()
                 gadgetOperation = .Move
                 }*/
            } else if CGFloat(distance) >= (radiusYellow - lineWidth) && CGFloat(distance) <= (radiusYellow + lineWidth) {
                if MinderManager.sharedInstance.isSeatbackEnabled {
                    if initialLocation.y >= MinderManager.sharedInstance.centerDeviceView.y {
                        gadgetOperation = .ResizeYellow
                        viewBall.hidden = true
                        heightPosture = radiusYellow
                        viewCurveMenuHolder.alphaValue = 0
                        viewPosture.ToggleShowProgressCircleFlag(true)
                        NSCursor.resizeLeftRightCursor().set()
                    }
                } else {
                    gadgetOperation = .ResizeYellow
                    viewBall.hidden = true
                    heightPosture = radiusYellow
                    viewCurveMenuHolder.alphaValue = 0
                    viewPosture.ToggleShowProgressCircleFlag(true)
                    NSCursor.resizeLeftRightCursor().set()
                }
            } else if CGFloat(distance) >= (radiusRed - lineWidth) && CGFloat(distance) <= (radiusRed + lineWidth) {
                gadgetOperation = .ResizeRed
                viewBall.hidden = true
                heightPosture = radiusRed
                viewCurveMenuHolder.alphaValue = 0
                viewPosture.ToggleShowProgressCircleFlag(true)
                NSCursor.resizeLeftRightCursor().set()
            } else {
                let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                
                let sPath: NSBezierPath = NSBezierPath()
                let sYOffset: CGFloat = ((viewPosture.frame.size.height / 3) * CGFloat(opp.difficultySeatback)) / 100
                let sPoint: CGPoint = CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x, y: MinderManager.sharedInstance.centerDeviceView.y - sYOffset)
                // radian of angle 180 = 3.14159 and 360 = 6.28319
                sPath.moveToPoint(CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x + (radiusYellow * cos(3.14159)), y: MinderManager.sharedInstance.centerDeviceView.y + (radiusYellow * sin(3.14159))))
                sPath.lineToPoint(CGPoint(x: sPoint.x + (radiusYellow * cos(3.14159)), y: sPoint.y + (radiusYellow * sin(3.14159))))
                sPath.appendBezierPathWithArcWithCenter(sPoint, radius: radiusYellow, startAngle: 180, endAngle: 360)
                sPath.lineToPoint(CGPoint(x: MinderManager.sharedInstance.centerDeviceView.x + (radiusYellow * cos(6.28319)), y: MinderManager.sharedInstance.centerDeviceView.y + (radiusYellow * sin(6.28319))))
                sPath.lineWidth = lineWidth
                
                let allPath: NSBezierPath = sPath.bezierPathByFlatteningPath
                if allPath.containsPoint(initialLocation) {
                    gadgetOperation = .ResizeSeatback
                    viewBall.hidden = true
                    heightPosture = sYOffset
                    viewCurveMenuHolder.alphaValue = 0
                    viewPosture.ToggleShowProgressCircleFlag(true)
                    NSCursor.resizeLeftRightCursor().set()
                } else {
                    NSCursor.closedHandCursor().set()
                    gadgetOperation = .Move
                }
            }
            /*} else {
             NSCursor.closedHandCursor().set()
             gadgetOperation = .Move
             }*/
            NSLog("mouseDown \(gadgetOperation)")
            logOutView.hidden = true
            viewLogin.hidden = true
        }
    }
    
    func GadgetResize(sizePosture : CGFloat) {
        viewPosture.setFrameSize(NSMakeSize(sizePosture, sizePosture))
        viewCurveMenuHolder?.setFrameSize(NSMakeSize(viewPosture.frame.size.width * 0.41, viewPosture.frame.size.height * 0.86))
        viewTrackball.setFrameSize(NSMakeSize(viewPosture.frame.origin.x + viewPosture.frame.size.width + (viewCurveMenuHolder.frame.size.width / 2), viewPosture.frame.size.height))
        view.window!.setFrame(NSMakeRect(view.window!.frame.origin.x, view.window!.frame.origin.y, viewTrackball.frame.origin.x + viewTrackball.frame.size.width, viewPosture.frame.size.height), display: true)
        viewCurveMenuHolder?.setFrameOrigin(NSMakePoint(viewTrackball.frame.size.width - viewCurveMenuHolder.frame.size.width - ((5.5 * viewCurveMenuHolder.frame.size.width) / 100), (viewPosture.frame.size.height / 2) - (viewCurveMenuHolder.frame.size.height / 2) + ((0.4 * viewCurveMenuHolder.frame.size.height) / 100)))
        
        let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
        viewBall.frame.size = NSMakeSize(radiusBall * 2, radiusBall * 2)
        
        MinderUtility.ShapeView(viewPosture, type: .Circle)
        MinderUtility.ShapeView(viewAnnulus, type: .Circle)
        MinderUtility.ShapeView(viewBall, type: .Circle)
        
        MinderManager.sharedInstance.centerDeviceViewHeight = sizePosture
        MinderManager.sharedInstance.centerDeviceView = NSPoint(x: viewPosture.frame.size.width / 2, y: viewPosture.frame.size.height / 2)
    }
    
    override func mouseDragged(theEvent: NSEvent) {
        let windowFrame = view.window!.frame
        var currentLocation: NSPoint = NSEvent.mouseLocation()
        if gadgetOperation == .Move {
            var newOrigin: NSPoint = NSPoint()
            let screenFrame = NSScreen.mainScreen()!.frame
            newOrigin.x = currentLocation.x - initialLocation.x
            newOrigin.y = currentLocation.y - initialLocation.y
            if (newOrigin.y + windowFrame.size.height) > (screenFrame.origin.y + screenFrame.size.height) {
                newOrigin.y = screenFrame.origin.y + (screenFrame.size.height - windowFrame.size.height)
            }
            if MinderManager.sharedInstance.appMode == .Gadget {
                originGadgetView = newOrigin
            } else {
                originNormalView = newOrigin
            }
            self.view.window!.setFrameOrigin(newOrigin)
        } else if gadgetOperation != .None && gadgetOperation != .Click {
            currentLocation.x -= windowFrame.origin.x
            currentLocation.y -= windowFrame.origin.y
            let xDist = Float( abs(currentLocation.x - initialLocation.x))
            let yDist = Float( abs(currentLocation.y - initialLocation.y))
            var distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
            if distance > 0 {
                distance /= 2
                var _sizePosture: CGFloat = 0
                
                let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
                let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
                let radiusYellow100 = (viewPosture.frame.size.height * (MinderManager.percentageBall + MinderManager.percentageOffset + MinderManager.percentageZoneYellow)) / 100
                
                if gadgetOperation == .Resize {
                    viewBall.hidden = true
                    if initialLocation.x > centerP!.x && initialLocation.y > centerP!.y {
                        if currentLocation.x > initialLocation.x && currentLocation.y > initialLocation.y {
                            _sizePosture = heightPosture! + CGFloat(distance)
                        } else {
                            _sizePosture = heightPosture! - CGFloat(distance)
                        }
                    } else if initialLocation.x < centerP!.x && initialLocation.y > centerP!.y {
                        if currentLocation.x < initialLocation.x && currentLocation.y > initialLocation.y {
                            _sizePosture = heightPosture! + CGFloat(distance)
                        } else {
                            _sizePosture = heightPosture! - CGFloat(distance)
                        }
                    } else if initialLocation.x < centerP!.x && initialLocation.y < centerP!.y {
                        if currentLocation.x < initialLocation.x && currentLocation.y < initialLocation.y {
                            _sizePosture = heightPosture! + CGFloat(distance)
                        } else {
                            _sizePosture = heightPosture! - CGFloat(distance)
                        }
                    } else {
                        if currentLocation.x < initialLocation.x && currentLocation.y > initialLocation.y {
                            _sizePosture = heightPosture! - CGFloat(distance)
                        } else {
                            _sizePosture = heightPosture! + CGFloat(distance)
                        }
                    }
                    
                    let screenFrame = NSScreen.mainScreen()!.frame
                    // Restrict minimum and maximum scaling
                    if _sizePosture > (screenFrame.size.height / 5.5) && _sizePosture < sizeNormalView!.height {
                        GadgetResize(_sizePosture)
                    }
                } else if gadgetOperation == .ResizeYellow {
                    let dx = abs(currentLocation.x - (centerP?.x)!)
                    let dy = abs(currentLocation.y - (centerP?.y)!)
                    _sizePosture =  sqrt((dx * dx) + (dy * dy))
                    
                    let yellowActualRadius = _sizePosture - (radiusBall + margin)
                    let yellowPercent = (yellowActualRadius * 100) / viewPosture.frame.size.height
                    let yellowDifficulty = (yellowPercent * 100) / MinderManager.percentageZoneYellow
                    if yellowDifficulty < 100 && yellowDifficulty >= 0 {
                        let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                        opp.difficultyYellow = Float(yellowDifficulty)
                        UpdateDifficulty(.Yellow, value: Float(yellowDifficulty))
                        MinderManager.sharedInstance.SaveSettings()
                    }
                    //ResetDrawByResize()
                } else if gadgetOperation == .ResizeRed {
                    let dx = abs(currentLocation.x - (centerP?.x)!)
                    let dy = abs(currentLocation.y - (centerP?.y)!)
                    _sizePosture =  sqrt((dx * dx) + (dy * dy))
                    
                    let redActualRadius = _sizePosture - (radiusYellow100 + (4 * margin))
                    let redPercent = (redActualRadius * 100) / viewPosture.frame.size.height
                    let redDifficulty = (redPercent * 100) / MinderManager.percentageZoneYellow
                    if redDifficulty < 100 && redDifficulty >= 0 {
                        let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                        opp.difficultyRed = Float(redDifficulty)
                        UpdateDifficulty(.Red, value: Float(redDifficulty))
                        MinderManager.sharedInstance.SaveSettings()
                    }
                    //ResetDrawByResize()
                } else if gadgetOperation == .ResizeSeatback {
                    let dx = abs(currentLocation.x - (centerP?.x)!)
                    let dy = currentLocation.y - (centerP?.y)!
                    let radius = sqrt((dx * dx) + (abs(dy) * abs(dy)))
                    _sizePosture =  dy
                    if _sizePosture < 0 {
                        let yellowPercent = (abs(_sizePosture) * 100) / viewPosture.frame.size.height
                        var seatBackDifficulty = (yellowPercent * 100) / MinderManager.percentageZoneYellow
                        seatBackDifficulty = seatBackDifficulty - radius
                        if seatBackDifficulty < 100 && seatBackDifficulty >= 0 {
                            let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                            opp.difficultySeatback = Float(seatBackDifficulty)
                            UpdateDifficulty(.Seatback, value: Float(seatBackDifficulty))
                            MinderManager.sharedInstance.SaveSettings()
                        }
                    }
                }
                //
            }
        }
    }
    func ResetDrawByResize() {
        isResizeReset = true
        let time: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC / 8))
        dispatch_after(time, dispatch_get_main_queue(), dispatch_block_create(DISPATCH_BLOCK_INHERIT_QOS_CLASS) {
            let height = self.viewPosture.frame.size.height + 1
            self.GadgetResize(height)
            })
        dispatch_after(time, dispatch_get_main_queue(), dispatch_block_create(DISPATCH_BLOCK_INHERIT_QOS_CLASS) {
            let height = self.viewPosture.frame.size.height - 1
            self.GadgetResize(height)
            })
        dispatch_after(time, dispatch_get_main_queue(), dispatch_block_create(DISPATCH_BLOCK_INHERIT_QOS_CLASS) {
            //self.viewPosture.hideCircleBall = false
            self.viewPosture.needsDisplay = true
            self.viewAnnulus.needsDisplay = true
            self.viewBall.hidden = false
            self.isResizeReset = false
            })
    }
    override func mouseUp(theEvent: NSEvent) {
        let windowFrame = view.window!.frame
        var currlocation: NSPoint = NSEvent.mouseLocation()
        if theEvent.clickCount == 2 {
            if MinderManager.sharedInstance.isRecording {
                RecordTapped(NSButton())
            } else if theEvent.type == NSLeftMouseUp {
                currlocation.x -= windowFrame.origin.x// + 28
                currlocation.y -= windowFrame.origin.y// + 51
                if viewPosture.frame.contains(currlocation) {
                    if MinderManager.sharedInstance.appMode == .Gadget {
                        menuButtonTapped(MinderManager.sharedInstance.currentView)
                    } else {
                        CloseTapped(theEvent)
                    }
                }
            } else {
            }
        } else {
            if MinderManager.sharedInstance.deviceActionMode != .Broadcasting {
                MinderManager.sharedInstance.deviceActionMode = .Broadcasting
            } else {
                switch gadgetOperation {
                case .Move:
                    let xD = Float( abs(currlocation.x - pointMouseDown!.x))
                    let yD = Float( abs(currlocation.y - pointMouseDown!.y))
                    let dist: Float = sqrt((xD * xD) + (yD * yD))
                    if dist <= 3 {
                        curveMenuClick(abs(Int(viewCurveMenuHolder.alphaValue) - 1))
                    }
                case .Resize:
                    currlocation.x -= windowFrame.origin.x// + 28
                    currlocation.y -= windowFrame.origin.y// + 51
                    let xDist = Float( abs(currlocation.x - initialLocation.x))
                    let yDist = Float( abs(currlocation.y - initialLocation.y))
                    var distance: Float = sqrt((xDist * xDist) + (yDist * yDist))
                    if distance > 0 {
                        distance = distance / 2
                        viewPosture.removeTrackingArea(trackingDeviceResizeLocation!)
                        trackingDeviceResizeLocation = NSTrackingArea(rect: viewPosture.bounds, options: [NSTrackingAreaOptions.MouseMoved, NSTrackingAreaOptions.MouseEnteredAndExited, NSTrackingAreaOptions.ActiveAlways], owner: self, userInfo: ["section": "DeviceView"])
                        viewPosture.addTrackingArea(trackingDeviceResizeLocation!)
                    }
                    sizeGadgetView = viewPosture.frame.size.height
                    viewBall.hidden = false
                    viewAnnulus.needsDisplay = true
                    //viewPosture.hideCircleBall = false
                    viewPosture.needsDisplay = true
                case .ResizeRed, .ResizeYellow, .ResizeSeatback:
                    if MinderManager.sharedInstance.appMode == .Gadget {
                        ResetDrawByResize()
                    }
                    self.viewBall.hidden = false
                    break
                default:
                    viewPosture.needsDisplay = true
                    curveMenuClick(abs(Int(viewCurveMenuHolder.alphaValue) - 1))
                    break
                }
                saveDimensions()
            }
            
        }
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC / 8))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.gadgetOperation = .None
        }
        if MinderManager.sharedInstance.appMode == .Gadget {
            NSCursor.openHandCursor().set()
        } else {
            //NSCursor.arrowCursor().set()
        }
    }
    
    func saveDimensions() {
        let sharedPreference = NSUserDefaults.standardUserDefaults()
        
        sharedPreference.setObject(NSStringFromPoint(originNormalView!), forKey: "originNormalView")
        sharedPreference.setObject(NSStringFromPoint(originGadgetView!), forKey: "originGadgetView")
        sharedPreference.setObject(NSStringFromSize(sizeNormalView!), forKey: "sizeNormalView")
        sharedPreference.setObject(sizeGadgetView!, forKey: "sizeGadgetView")
        
        sharedPreference.synchronize()
    }
    
    // MARK: setupWindow
    func setupWindow() {
        let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        NSLog("setupWindow for \(MinderManager.sharedInstance.appMode)")
        isSwitchingView = true
        viewBall.hidden = true
        viewBall.frame.origin = NSMakePoint(-1000, -1000)
        viewPosture.UpdateAlertText("")
        
        switch MinderManager.sharedInstance.appMode {
        case .Gadget:
            if MinderManager.sharedInstance.isRecording {
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: CGFloat(opp.transperancy)).CGColor
            } else {
                viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 46/255, green: 204/255, blue: 114/255, alpha: CGFloat(opp.transperancy)).CGColor
            }
            view.subviews.forEach({
                if $0 != viewTrackball {
                    $0.hidden = true
                }
            })
            
            view.window?.backgroundColor = NSColor.clearColor()
            view.layer?.backgroundColor = NSColor.clearColor().CGColor
            
            viewTrackball.setFrameOrigin(NSMakePoint(0, 0))
            viewTrackball.layer?.backgroundColor = NSColor.clearColor().CGColor
            viewTrackball.layer?.mask = nil
            
            /*
             NSAnimationContext.runAnimationGroup({(_ context: NSAnimationContext) -> Void in
             context.duration = 0.5
             self.view.window!.animator().setFrameOrigin(NSMakePoint(self.originGadgetView!.x, self.originGadgetView!.y))
             }, completionHandler: {() -> Void in
             })*/
            
            
            self.viewPosture.setFrameOrigin(NSMakePoint(0, 0))
            self.viewPosture.setFrameSize(NSMakeSize(self.sizeGadgetView!, self.sizeGadgetView!))
            self.viewCurveMenuHolder?.setFrameSize(NSMakeSize(self.viewPosture.frame.size.width * 0.41, self.viewPosture.frame.size.height * 0.86))
            self.viewTrackball.setFrameSize(NSMakeSize(self.viewPosture.frame.origin.x + self.viewPosture.frame.size.width + (self.viewCurveMenuHolder.frame.size.width / 2), self.viewPosture.frame.size.height))
            self.view.window!.setFrame(NSMakeRect(self.originGadgetView!.x, self.originGadgetView!.y, self.viewTrackball.frame.origin.x + self.viewTrackball.frame.size.width, self.viewPosture.frame.size.height), display: true)
            self.viewCurveMenuHolder?.setFrameOrigin(NSMakePoint(self.viewTrackball.frame.size.width - self.viewCurveMenuHolder.frame.size.width - ((5.5 * self.viewCurveMenuHolder.frame.size.width) / 100), (self.viewPosture.frame.size.height / 2) - (self.viewCurveMenuHolder.frame.size.height / 2) + ((0.4 * self.viewCurveMenuHolder.frame.size.height) / 100)))
            
            labelTrackingBall.hidden = true
            buttonCalibrate.hidden = true
            buttonRecord.hidden = true
            viewCurveMenuHolder.hidden = false
            viewCurveMenuHolder.alphaValue = 0
            // Making application window level to floating - top of all other applications
            view.window!.level = Int(CGWindowLevelForKey(.MaximumWindowLevelKey))
            
        //GadgetResize(viewPosture.frame.size.height)
        case .Normal:
            //NSCursor.arrowCursor().set()
            viewCurveMenuHolder.hidden = true
            
            if MinderManager.sharedInstance.isRecording {
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: CGFloat(opp.transperancy)).CGColor
            } else {
                viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 46/255, green: 204/255, blue: 114/255, alpha: 1).CGColor
            }
            view.subviews.forEach({
                if $0 != viewTrackball {
                    $0.hidden = false
                }
            })
            viewTrackball.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
            
            /*NSAnimationContext.runAnimationGroup({(_ context: NSAnimationContext) -> Void in
             context.duration = 0.5
             view.window?.animator().setFrame(NSMakeRect(originNormalView!.x, originNormalView!.y, sizeNormalView!.width, sizeNormalView!.height), display: true)
             }, completionHandler: {() -> Void in
             
             })*/
            self.view.window?.setFrame(NSMakeRect(self.originNormalView!.x, self.originNormalView!.y, self.sizeNormalView!.width, self.sizeNormalView!.height), display: true)
            viewTrackball.frame = NSMakeRect(7, 35, 557, 520)
            viewPosture.frame = NSMakeRect(20, 16, 450, 450)
            view.layer?.backgroundColor = NSColor.init(calibratedRed: 30/255, green: 39/255, blue: 46/255, alpha: 1).CGColor
            MinderUtility.ShapeView(viewTrackball, type: .RectTopCurved1By3)
            MinderUtility.ShapeView(viewPosture, type: .Circle)
            UpdateLoggedUserDetails()
            labelTrackingBall.hidden = false
            buttonCalibrate.hidden = false
            buttonRecord.hidden = false
            // Set window level to normal
            view.window!.level = Int(CGWindowLevelForKey(.NormalWindowLevelKey))
            //ResetDrawByResize()
        }
        
        if trackingDeviceResizeLocation != nil {
            viewPosture.removeTrackingArea(trackingDeviceResizeLocation!)
        }
        trackingDeviceResizeLocation = NSTrackingArea(rect: viewPosture.bounds, options: [NSTrackingAreaOptions.MouseMoved, NSTrackingAreaOptions.MouseEnteredAndExited, NSTrackingAreaOptions.ActiveAlways], owner: self, userInfo: ["section": "DeviceView"])
        viewPosture.addTrackingArea(trackingDeviceResizeLocation!)
        
        viewBall.hidden = false
        //viewAnnulus.hidden = false
        viewAnnulus.needsDisplay = true
        //viewPosture.hideCircleBall = false
        MinderUtility.ShapeView(viewPosture, type: .Circle)
        viewPosture.needsDisplay = true
        
        let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
        let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
        // Finding yellow radius. Yellow zone is MinderManager.percentageZoneYellow percent of height but start after ball space and an offset
        let percentYellow = (CGFloat(viewPosture.difficultyYellow) * MinderManager.percentageZoneYellow) / 100
        let radiusYellow = ((viewPosture.frame.size.height * percentYellow) / 100) + radiusBall + margin
        
        viewBall.frame.size = NSMakeSize(radiusBall * 2, radiusBall * 2)
        MinderUtility.ShapeView(viewAnnulus, type: .Circle)
        MinderUtility.ShapeView(viewBall, type: .Circle)
        
        MinderManager.sharedInstance.centerDeviceViewHeight = viewPosture.frame.size.height
        MinderManager.sharedInstance.centerDeviceView = NSPoint(x: viewPosture.frame.size.width / 2, y: viewPosture.frame.size.height / 2)
        
        isSwitchingView = false
        
        //if MinderManager.sharedInstance.deviceStatus == .Connected {
        if MinderManager.sharedInstance.dataProcessingPaused {
            viewPosture.UpdateAlertText("Posture is paused.")
        }
        /*} else {
         viewPosture.UpdateAlertText("No device connected.")
         }*/
    }
    
    @IBAction func CalibrateTapped(sender: AnyObject) {
        if MinderManager.sharedInstance.deviceActionMode == .Broadcasting {
            MinderManager.sharedInstance.calibratedFlag = true
            viewBall.hidden = true
            //viewAnnulus.hidden = true
            let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
            opp.offsetPoint.x = -10000.0
            opp.offsetPoint.y = -10000.0
            
            MinderManager.sharedInstance.deviceActionMode = .Calibrating
            UpdateColor(.Green)
            MinderManager.sharedInstance.roll = 0
            MinderManager.sharedInstance.pitch = 0
            MinderManager.sharedInstance.yaw = 0
            GyroTheView()
            NSLog("Calibration started")
        }
    }
    func SkipCalibration() {
        MinderManager.sharedInstance.deviceActionMode = .Broadcasting
        UpdateColor(.Green)
        NSLog("Calibration Skipped")
    }
    
    // MARK:UpdateLoggedUserDetails
    func UpdateLoggedUserDetails() {
        let appManager = MinderManager.sharedInstance
        if !(appManager.userLogged?.userName ?? "").isEmpty {
            userNameLabel.attributedStringValue = NSAttributedString(string: (appManager.userLogged?.name)!, attributes: [ NSFontAttributeName: NSFont.boldSystemFontOfSize(14), NSForegroundColorAttributeName : NSColor.whiteColor()])
            if let userimage = appManager.userLogged?.image {
                userImageRoundView.image = userimage
            }
        } else {
            userNameLabel.stringValue = ""
            userImageRoundView.hidden = true
        }
        logOutView.hidden = true
        viewLogin.hidden = true
    }
    
    override var representedObject: AnyObject? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    // MARK:buttonToggleView
    @IBAction func buttonToggleView(sender: AnyObject) {
        if userImageRoundView.hidden {
            if viewLogin.hidden {
                viewLogin.hidden = false
            } else {
                viewLogin.hidden = true
            }
        } else {
            if logOutView.hidden {
                logOutView.hidden = false
            } else {
                logOutView.hidden = true
            }
        }
    }
    // MARK:ActivitiesAlert
    func GetActivities(showAlert: Bool) {
        MinderManager.sharedInstance.GetActivities({ _ in
            if MinderManager.sharedInstance.currentView == .Guidance {
                let vcGuidanceVC = (self.viewControllerCurrent as! GuidanceViewController)
                vcGuidanceVC.GetGuidanceActivity()
            }
            if showAlert {
                dispatch_async(dispatch_get_main_queue(),{
                    let myPopup: NSAlert = NSAlert()
                    myPopup.messageText = "Activity synchronization done."
                    myPopup.informativeText = "Your activity schedules are now upto date. Videos for new schedules are downloading in background."
                    myPopup.alertStyle = .Informational
                    myPopup.addButtonWithTitle("OK")
                    myPopup.runModal()
                })
            }
        })
    }
    // MARK:SwitchToGadget
    func SwitchToGadget() {
        if !MinderManager.sharedInstance.isRecording && MinderManager.sharedInstance.replayState == .None {
            MinderManager.sharedInstance.appMode = .Gadget
            setupWindow()
            disableDeviceViewClick = false
        }
    }
    // MARK:CloseTapped
    @IBAction func CloseTapped(sender: AnyObject) {
        if MinderManager.sharedInstance.appMode == .Normal {
            SwitchToGadget()
        } else {
            curveMenuClick(0)
        }
    }
    // MARK:MinimiseTapped
    @IBAction func MinimiseTapped(sender: AnyObject) {
        self.view.window!.setIsMiniaturized(true)
    }
    // MARK:DashboardTapped
    @IBAction func DashboardTapped(sender: AnyObject) {
        mainButtonTap(.Dashboard)
    }
    // MARK:GuidanceTapped
    @IBAction func GuidanceTapped(sender: AnyObject) {
        mainButtonTap(.Guidance)
    }
    // MARK:SettingsTapped
    @IBAction func SettingsTapped(sender: AnyObject) {
        mainButtonTap(.Settings)
    }
    // MARK:WebsiteTapped
    @IBAction func WebsiteTapped(sender: AnyObject) {
        if let checkURL = NSURL(string: "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro") {
            NSWorkspace.sharedWorkspace().openURL(checkURL)
        }
    }
    // MARK:SyncTapped
    @IBAction func SyncTapped(sender: AnyObject) {
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            GetActivities(true)
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Please login"
            myPopup.informativeText = "You have to login with your Minder credentials to sync your activities."
            myPopup.alertStyle = .Informational
            myPopup.addButtonWithTitle("OK")
            myPopup.runModal()
        }
    }
    // MARK:NotificationTapped
    @IBAction func NotificationTapped(sender: AnyObject) {
        let  opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.volumeYellowMute && opp.vibrateYellow == 0 {
            opp.volumeYellowMute = false
            opp.vibrateYellow = 1
            opp.volumeRedMute = true
            opp.vibrateRed = 1
        } else if !opp.volumeYellowMute {
            opp.volumeYellowMute = true
            opp.vibrateYellow = 1
            opp.volumeRedMute = true
            opp.vibrateRed = 1
        } else {
            opp.volumeYellowMute = true
            opp.vibrateYellow = 0
            opp.volumeRedMute = true
            opp.vibrateRed = 0
        }
        MinderManager.sharedInstance.SaveSettings()
        UpdateNotification()
    }
    func mainButtonTap(type: ViewType) {
        MinderManager.sharedInstance.currentView = type
        UnselectButtons()
        switch MinderManager.sharedInstance.currentView {
        case .Dashboard:
            dispatch_async(dispatch_get_main_queue(),{
                self.buttonDashboard.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).CGColor
                self.buttonDashboard.image = NSImage(named: "Dashboard")
                self.buttonRecord.hidden = true
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(false)
            })
            break
        case .Guidance:
            dispatch_async(dispatch_get_main_queue(),{
                self.buttonGuidance.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).CGColor
                self.buttonGuidance.image = NSImage(named: "Guidance")
                self.buttonRecord.hidden = false
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(true)
            })
            break
        case .Settings:
            dispatch_async(dispatch_get_main_queue(),{
                self.buttonSettings.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).CGColor
                self.buttonSettings.image = NSImage(named: "Settings")
                self.buttonRecord.hidden = true
                (NSApp.delegate as! AppDelegate).SetMenuRecordEnabled(false)
            })
            break
        }
        SwitchContainerViewController()
    }
    func SwitchContainerViewController() {
        dispatch_async(dispatch_get_main_queue(),{
            let subViews = self.viewPlaceHolder.subviews
            for view in subViews {
                view.removeFromSuperview()
            }
            
            let mainStoryboard: NSStoryboard = NSStoryboard(name: "Main", bundle: nil)
            switch MinderManager.sharedInstance.currentView {
            case .Dashboard:
                self.viewControllerCurrent = mainStoryboard.instantiateControllerWithIdentifier("ViewControllerDashboard") as? DashboardViewController
                break
            case .Guidance:
                let vc = mainStoryboard.instantiateControllerWithIdentifier("ViewControllerGuidance") as? GuidanceViewController
                vc?.delegateGuidance = self
                self.viewControllerCurrent = vc
                break
            case .Settings:
                let vc = mainStoryboard.instantiateControllerWithIdentifier("ViewControllerSettings") as? SettingsViewController
                vc?.delegateSettings = self
                self.viewControllerCurrent = vc
                break
            default:
                break
            }
            self.viewPlaceHolder.addSubview(self.viewControllerCurrent.view)
            self.viewControllerCurrent.view.setFrameOrigin(NSPoint(x: 0, y: 0))
            self.viewControllerCurrent.view.setFrameSize(self.viewPlaceHolder.frame.size)
        })
    }
    func UnselectButtons() {
        dispatch_async(dispatch_get_main_queue(), {
            self.buttonDashboard.image = NSImage(named: "DashboardUn")
            self.buttonDashboard.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
            self.buttonGuidance.image = NSImage(named: "GuidanceUn")
            self.buttonGuidance.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
            self.buttonSettings.image = NSImage(named: "SettingsUn")
            self.buttonSettings.layer!.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).CGColor
            
            self.logOutView.hidden = true
            self.viewLogin.hidden = true
        })
    }
    
    @IBAction func editProfileShowButtonAction(sender: AnyObject) {
        self.view.addSubview(self.viewControllerEditProfile!.view)
        self.viewControllerEditProfile!.view.setFrameOrigin(NSPoint(x: 0, y: 0))
        self.viewControllerEditProfile!.RefreshControls()
        logOutView.hidden = true
    }
    @IBAction func logoutTapped(sender: AnyObject) {
        MinderManager.sharedInstance.userLogged = User()
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("UserDetails")
        userDefaults.synchronize()
        
        // Create user folders - Activities, Records
        let fileManager = NSFileManager.defaultManager()
        var isDir : ObjCBool = true
        var path: String = MinderManager.sharedInstance.appDirectory + "_/Activities/"
        if !fileManager.fileExistsAtPath(path, isDirectory: &isDir) {
            do {
                try fileManager.createDirectoryAtPath(path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Error while creating Activities folder. \(error)")
            }
        }
        path = MinderManager.sharedInstance.appDirectory + "_/Records/"
        if !fileManager.fileExistsAtPath(path, isDirectory: &isDir) {
            do {
                try fileManager.createDirectoryAtPath(path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Error while creating Records folder. \(error)")
            }
        }
        
        MinderManager.sharedInstance.LoadSettings()
        if MinderManager.sharedInstance.currentView == .Settings {
            if let viewControllerCurrent = viewControllerCurrent {
                let vcSettingsVC = (viewControllerCurrent as! SettingsViewController)
                vcSettingsVC.RefreshControls()
            }
        }
        
        userImageRoundView.hidden = true
        //btnLogoutViewToggle.hidden = true
        logOutView.hidden = true
        userNameLabel.stringValue = ""
        
        let myPopup: NSAlert = NSAlert()
        myPopup.messageText = "Logged out"
        myPopup.informativeText = "You have been logged out from the Minder. From now on your activities are not analysed."
        myPopup.alertStyle = .Informational
        myPopup.addButtonWithTitle("OK")
        myPopup.runModal()
    }
    @IBAction func loginTapped(sender: AnyObject) {
        MinderManager.sharedInstance.DeviceDisconnect()
        sleep(2)
        viewLogin.hidden = true
        delegateLogin?.updateMainView(.Login, typeLogin: .withOutLogin)
    }
    @IBAction func RecordTapped(sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        
        if MinderManager.sharedInstance.deviceStatus == .Connected {
            let style = NSMutableParagraphStyle()
            style.alignment = .Center
            if MinderManager.sharedInstance.isRecording {
                MinderManager.sharedInstance.SaveRecorded(NSImage(data: viewPosture.dataWithPDFInsideRect(viewPosture.bounds))!)
                MinderManager.sharedInstance.isRecording = false
                viewPosture.isRecordView = false
                viewAnnulus.viewMode = false
                viewBall.layer?.backgroundColor = NSColor.whiteColor().CGColor
                viewPosture.idealPath = nil
                buttonRecord.attributedTitle = NSAttributedString(string: "RECORD START", attributes: [NSFontAttributeName : NSFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : style])
                buttonRecord.image = NSImage(named: "RecordStart")
                (NSApp.delegate as! AppDelegate).SetMenuRecordTitle("Start")
                if MinderManager.sharedInstance.currentView == .Guidance {
                    if let viewControllerCurrent = self.viewControllerCurrent {
                        let vcGuidanceVC = (viewControllerCurrent as! GuidanceViewController)
                        if vcGuidanceVC.currentViewType == .Record {
                            vcGuidanceVC.GetRecords()
                        }
                    }
                }
                buttonCalibrate.hidden = false
                //viewAnnulus.hidden = false
                //viewBall.hidden = false
            } else {
                MinderManager.sharedInstance.isRecording = true
                viewPosture.isRecordView = true
                viewAnnulus.viewMode = true
                let radiusBall = (MinderManager.sharedInstance.centerDeviceViewHeight * MinderManager.percentageBall) / 100
                viewBall.frame.origin = NSMakePoint(MinderManager.sharedInstance.centerDeviceView.x - radiusBall, MinderManager.sharedInstance.centerDeviceView.y - radiusBall)
                viewBall.layer?.backgroundColor = NSColor.clearColor().CGColor
                buttonRecord.attributedTitle = NSAttributedString(string: "RECORD STOP", attributes: [NSFontAttributeName : NSFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : style])
                buttonRecord.image = NSImage(named: "RecordStop")
                (NSApp.delegate as! AppDelegate).SetMenuRecordTitle("Stop")
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: CGFloat(opp.transperancy)).CGColor
                }
                buttonCalibrate.hidden = true
                //viewAnnulus.hidden = true
            }
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Record can't start."
            myPopup.informativeText = "Please connect your device to start recording."
            myPopup.alertStyle = .Informational
            myPopup.addButtonWithTitle("OK")
            myPopup.runModal()
        }
    }
    
    @IBAction func RedZoneIncrementTapped(sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        
        if opp.difficultyRed  < 100 {
            opp.difficultyRed += 1
            viewPosture.UpdateDifficulty(.Red, difficulty: (opp.difficultyRed))
            
            MinderManager.sharedInstance.SaveSettings()
        }
    }
    @IBAction func RedZoneDecrementTapped(sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.difficultyRed > 0 {
            opp.difficultyRed -= 1
            viewPosture.UpdateDifficulty(.Red, difficulty: (opp.difficultyRed))
            
            MinderManager.sharedInstance.SaveSettings()
        }
    }
    @IBAction func YellowZoneIncrementTapped(sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.difficultyYellow < 100 {
            opp.difficultyYellow += 1
            viewPosture.UpdateDifficulty(.Yellow, difficulty: (opp.difficultyYellow))
            
            MinderManager.sharedInstance.SaveSettings()
        }
    }
    @IBAction func YellowZoneDecrementTapped(sender: AnyObject) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if opp.difficultyYellow > 0 {
            opp.difficultyYellow -= 1
            viewPosture.UpdateDifficulty(.Yellow, difficulty: (opp.difficultyYellow))
            
            MinderManager.sharedInstance.SaveSettings()
        }
    }
    
    var disableDeviceViewClick = false
    var prevZoneType: ZoneType = .Green
    
    @IBAction func dashboardTapped(sender: AnyObject) {
        menuButtonTapped(.Dashboard)
    }
    @IBAction func guidanceTapped(sender: AnyObject) {
        menuButtonTapped(.Guidance)
    }
    @IBAction func notificationTapped(sender: AnyObject) {
        NotificationTapped(sender)
    }
    @IBAction func ProcessPauseTapped(sender: AnyObject) {
        UpdateProcessingPaused()
        
        NSLog("ProcessPauseTapped")
    }
    @IBAction func settingsTapped(sender: AnyObject) {
        menuButtonTapped(.Settings)
        NSLog("settingsTapped")
    }
    
    @IBAction func PitchChanged(sender: AnyObject) {
        MinderManager.sharedInstance.pitch = CGFloat(sender.floatValue) * CGFloat(M_PI) / 180
        GyroTheView()
    }
    @IBAction func RollChanged(sender: AnyObject) {
        MinderManager.sharedInstance.roll = CGFloat(sender.floatValue) * CGFloat(M_PI) / 180
        GyroTheView()
    }
    @IBAction func YawChanged(sender: AnyObject) {
        MinderManager.sharedInstance.yaw = CGFloat(sender.floatValue) * CGFloat(M_PI) / 180
        GyroTheView()
    }
    
    func GyroTheView() {
        var transform = CATransform3D()
        transform = CATransform3DMakeRotation(MinderManager.sharedInstance.pitch, 1, 0, 0)
        transform = CATransform3DRotate(transform, MinderManager.sharedInstance.roll, 0, 1, 0)
        transform = CATransform3DRotate(transform, MinderManager.sharedInstance.yaw, 0, 0, 1)
        let frame = viewAnnulus.layer!.frame
        let center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame))
        self.viewAnnulus.layer!.position = center
        self.viewAnnulus.layer!.anchorPoint = CGPointMake(0.5, 0.5)
        self.viewAnnulus.layer!.transform = transform
    }
}

extension HomeViewController : DelegateProfileEdit {
    func ProfileEdited() {
        dispatch_async(dispatch_get_main_queue(),{
            self.UpdateLoggedUserDetails()
        })
    }
}

extension HomeViewController : DelegateGuidance {
    func Replay(postureData: [PostureData], gyroData: [GyroData], isReplay: Bool) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {() -> Void in
            NSLog("Replay started.")
            self.viewBall.frame.origin = NSMakePoint(self.viewPosture.frame.size.height/2 - self.viewBall.frame.size.height/2, self.viewPosture.frame.size.height/2 - self.viewBall.frame.size.height/2)
            self.viewPosture.isRecordView = true
            self.viewPosture.isReplay = isReplay
            self.viewAnnulus.viewMode = true
            MinderManager.sharedInstance.BackupGyro()
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 0/255, green: 0/255, blue: 0/255, alpha: CGFloat(opp.transperancy)).CGColor
                self.buttonCalibrate.hidden = true
                self.buttonRecord.hidden = true
                self.viewBall.layer?.backgroundColor = NSColor.clearColor().CGColor
                //self.viewAnnulus.hidden = true
            }
            MinderManager.sharedInstance.replayState = .Play
            var progress: Int = 0
            for pData in postureData {
                dispatch_async(dispatch_get_main_queue(),{
                    self.viewPosture.UpdateRecordCordinates(pData.z , y: pData.y)
                    let gData = gyroData[progress]
                    self.UpdateGyroWithRoll(gData.roll, withPitch: gData.pitch, withYaw: gData.yaw, withT: 0.0)
                    progress += 1
                    if MinderManager.sharedInstance.currentView == .Guidance {
                        if let viewControllerCurrent = self.viewControllerCurrent {
                            let vcGuidanceVC = (viewControllerCurrent as! GuidanceViewController)
                            dispatch_async(dispatch_get_main_queue(), {
                                vcGuidanceVC.UpdateReplayProgress(progress)
                            })
                        }
                    }
                })
                usleep(100000)
                while(MinderManager.sharedInstance.replayState == .Pause) {}
                if MinderManager.sharedInstance.replayState == .None {
                    break;
                }
            }
            if !MinderManager.sharedInstance.isActivityPlaying {
                self.ReplayClosed()
            }
            NSLog("Replay ballpath ended.")
        })
    }
    func ReplayClosed() {
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.buttonCalibrate.hidden = false
            self.buttonRecord.hidden = false
            //self.viewAnnulus.hidden = false
        }
        self.viewPosture.isRecordView = false
        self.viewPosture.isReplay = false
        self.viewAnnulus.viewMode = false
        self.viewPosture.recordPath = nil
        self.viewPosture.idealPath = nil
        self.viewBall.layer?.backgroundColor = NSColor.whiteColor().CGColor
        MinderManager.sharedInstance.RestoreGyro()
        MinderManager.sharedInstance.replayState = .None
        if MinderManager.sharedInstance.currentView == .Guidance {
            if let viewControllerCurrent = self.viewControllerCurrent {
                let vcGuidanceVC = (viewControllerCurrent as! GuidanceViewController)
                dispatch_async(dispatch_get_main_queue(), {
                    vcGuidanceVC.ReplayFinished()
                })
            }
        }
        self.viewPosture.needsDisplay = true
    }
    
}

extension HomeViewController : DelegateImitationView {
    func curveMenuClick(show: Int) {
        if !disableDeviceViewClick {
            if show == 1 {
                viewCurveMenuHolder?.setFrameSize(NSMakeSize(viewPosture.frame.size.width * 0.41, viewPosture.frame.size.height * 0.86))
                viewCurveMenuHolder?.setFrameOrigin(NSMakePoint(viewTrackball.frame.size.width - viewCurveMenuHolder.frame.size.width - ((5.5 * viewCurveMenuHolder.frame.size.width) / 100), (viewPosture.frame.size.height / 2) - (viewCurveMenuHolder.frame.size.height / 2) + ((0.4 * viewCurveMenuHolder.frame.size.height) / 100)))
            }
            NSAnimationContext.runAnimationGroup({(_ context: NSAnimationContext) -> Void in
                context.duration = 0.5
                viewCurveMenuHolder.animator().alphaValue = CGFloat(show)
                }, completionHandler: {() -> Void in
                    self.viewCurveMenuHolder.alphaValue = CGFloat(show)
            })
            viewPosture.ToggleShowProgressCircleFlag(show == 1 ? false : true)
        }
    }
}

extension HomeViewController : DelegateDeviceHandler {
    func UpdateAlertText(text: String) {
        if !isSwitchingView {
            /*if text == "" {
             viewPosture.showTriangle = true
             viewAnnulus.hidden = false
             } else {
             viewPosture.showTriangle = false
             viewAnnulus.hidden = true
             }*/
            if text != "" {
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 1/255, green: 204/255, blue: 1/255, alpha: 1).CGColor
                viewBall.frame.origin = NSPoint(x: -10000, y: -10000)
            }
            viewPosture.UpdateAlertText(text)
        }
    }
    //Status Updation
    func StatusUpdated() {
        if MinderManager.sharedInstance.deviceStatus == .Connected {
            viewPosture.hideCircleBall = false
            dispatch_async(dispatch_get_main_queue(), {
                if MinderManager.sharedInstance.currentView == .Settings {
                    if let viewControllerCurrent = self.viewControllerCurrent {
                        let vcSettingsVC = (viewControllerCurrent as! SettingsViewController)
                        vcSettingsVC.RefreshControls()
                    }
                }
            })
        }
    }
    
    //ChargeUpdation
    func UpdateCharge(charge: Float) {
        print("updateCharge: \(charge)")
        if charge > 70 {
            imageBattery.image = NSImage(named: "Battery100")
        } else if charge > 40 {
            imageBattery.image = NSImage(named: "Battery70")
        } else if charge > 10 {
            imageBattery.image = NSImage(named: "Battery40")
        } else {
            imageBattery.image = NSImage(named: "Battery10")
        }
        MinderManager.sharedInstance.deviceCharge = charge
    }
    //UpdateGyroWithRoll
    func UpdateGyroWithRoll(roll: CGFloat, withPitch pitch: CGFloat, withYaw yaw: CGFloat, withT t: Double) {
        if MinderManager.sharedInstance.deviceActionMode == .Broadcasting && !isSwitchingView && MinderManager.sharedInstance.isGyroOn {
            viewBall.hidden = false
            //viewAnnulus.hidden = false
        }
        if MinderManager.sharedInstance.isGyroFromDevice && !isSwitchingView && MinderManager.sharedInstance.deviceActionMode == .Broadcasting {
            if roll != -1 {
                MinderManager.sharedInstance.roll += roll / 10    //    (360 / CGFloat(M_PI_2)) * CGFloat(roll)    //
            }
            if pitch != -1 {
                MinderManager.sharedInstance.pitch += pitch / 10  //    (360 / CGFloat(M_PI_2)) * CGFloat(pitch)
            }
            if yaw != -1 {
                MinderManager.sharedInstance.yaw += yaw / 10  //    (360 / CGFloat(M_PI_2)) * CGFloat(yaw)  //
            }
        }
        self.GyroTheView()
    }
    //PostureUpdation
    func UpdatePosture(x: CGFloat, withY y: CGFloat) {
        if gadgetOperation == .None && !isResizeReset {
            if MinderManager.sharedInstance.replayState == .None && !MinderManager.sharedInstance.isRecording {
                viewBall.frame.origin = NSPoint(x: x - (viewBall.frame.size.width/2), y: y - (viewBall.frame.size.width/2))
            } else {
                viewPosture.UpdateBallCordinates(x , y: y)
            }
        } else {
            viewBall.frame.origin = NSPoint(x: -10000, y: -10000)
        }
        /*if MinderManager.sharedInstance.appMode == .Gadget {
         if (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!.transperancy != 1 {
         self.view.window?.backgroundColor = NSColor.clearColor()
         self.view.layer?.backgroundColor = NSColor.clearColor().CGColor
         self.viewTrackball.layer?.backgroundColor = NSColor.clearColor().CGColor
         MinderUtility.ShapeView(viewAnnulus, type: .Circle)
         MinderUtility.ShapeView(viewPosture, type: .Circle)
         MinderUtility.ShapeView(viewBall, type: .Circle)
         }
         }*/
    }
    func UpdateStepCount() {
        if MinderManager.sharedInstance.currentView == .Dashboard {
            if let viewControllerCurrent = viewControllerCurrent {
                (viewControllerCurrent as! DashboardViewController).setStepCount()
            }
        }
    }
    func EnableSeatback() {
        MinderManager.sharedInstance.isSeatbackEnabled = true
        viewPosture.SetSeatback(1)
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        viewPosture.UpdateDifficulty(.Seatback, difficulty: opp.difficultySeatback)
        UpdateAlertText("Supported Posture Enabled")
        let time: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC * 3))
        dispatch_after(time, dispatch_get_main_queue(), dispatch_block_create(DISPATCH_BLOCK_INHERIT_QOS_CLASS) {
            self.UpdateAlertText("")
            })
        dispatch_async(dispatch_get_main_queue(), {
            if MinderManager.sharedInstance.currentView == .Settings {
                if let viewControllerCurrent = self.viewControllerCurrent {
                    let vcSettingsVC = (viewControllerCurrent as! SettingsViewController)
                    vcSettingsVC.RefreshControls()
                }
            }
        })
        ResetDrawByResize()
    }
    //ColorUpdation
    func UpdateColor(type: ZoneType) {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if gadgetOperation != .Resize {
            switch type {
            case .Green:
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 1/255, green: 255/255, blue: 1/255, alpha: CGFloat(opp.transperancy)).CGColor
            case .Yellow:
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 225/255, green: 225/255, blue: 1/225, alpha: CGFloat(opp.transperancy)).CGColor
            case .Red:
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 1/255, blue: 1/255, alpha: CGFloat(opp.transperancy)).CGColor
            case .Seatback:
                self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 1/255, green: 204/255, blue: 1/255, alpha: CGFloat(opp.transperancy)).CGColor
                //self.viewPosture.layer?.backgroundColor = NSColor.init(calibratedRed: 1/255, green: 204/255, blue: 204/255, alpha: CGFloat(opp.transperancy)).CGColor
            default: break
            }
        }
        prevZoneType = type
    }
    //GraphUpdateswithData
    func UpdateDataGraphs() {
        if MinderManager.sharedInstance.currentView == .Dashboard && !MinderManager.sharedInstance.showPreviousData {
            if let viewControllerCurrent = viewControllerCurrent {
                let vcDashboardVC = (viewControllerCurrent as! DashboardViewController)
                
                let manager = MinderManager.sharedInstance
                vcDashboardVC.viewGraphBar.positionDataList = manager.pDataArray
                
                if manager.imageHeatmap != nil {
                    vcDashboardVC.viewGraphHeat.imageHeatmap = MinderUtility.ResizeImage(manager.imageHeatmap!, destSize: vcDashboardVC.viewGraphHeat.frame.size)
                }
                
                vcDashboardVC.viewGraphCompleted.positionDataList = manager.pDataArray
                
                if manager.green > 0 || manager.yellow > 0 || manager.red > 0 {
                    let gyrTotal = CGFloat(manager.green + manager.yellow + manager.red)
                    var greenPercent: CGFloat = 0
                    if manager.green > 0 {
                        greenPercent = round(((CGFloat(manager.green) / gyrTotal) * 100) * 10) / 10
                    }
                    var yellowPercent: CGFloat = 0
                    if manager.yellow > 0 {
                        yellowPercent = round(((CGFloat(manager.yellow) / gyrTotal) * 100) * 10) / 10
                    }
                    var redPercent: CGFloat = 0
                    if manager.red > 0 {
                        redPercent = round(((CGFloat(manager.red) / gyrTotal) * 100) * 10) / 10
                    }
                    
                    vcDashboardVC.labelGreenPosition.stringValue = "Green: \(MinderUtility.SecondsToHMSString(manager.green)) (\(greenPercent)%)"
                    vcDashboardVC.labelYellowPosition.stringValue = "Yellow: \(MinderUtility.SecondsToHMSString(manager.yellow)) (\(yellowPercent)%)"
                    vcDashboardVC.labelRedPosition.stringValue = "Red: \(MinderUtility.SecondsToHMSString(manager.red)) (\(redPercent)%)"
                    
                    vcDashboardVC.viewGraphPie.zoneYellowValue = yellowPercent
                    vcDashboardVC.viewGraphPie.zoneRedValue = redPercent
                    
                    vcDashboardVC.refreshGraphs()
                    viewPosture.UpdateZonePercentage(greenPercent, yellow: yellowPercent, red: redPercent)
                }
            }
        }
    }
    //Date&Time Updation
    func UpdateUpdateTime(elapsed: Int) {
        if MinderManager.sharedInstance.currentView == .Dashboard {
            let remaining: Int = 60 - (elapsed / 1000)
            if remaining == 0 {
                if let viewControllerCurrent = viewControllerCurrent {
                    (viewControllerCurrent as! DashboardViewController).labelRemaingIndicator.stringValue = "update in 1 minute"
                }
            } else {
                if let viewControllerCurrent = viewControllerCurrent {
                    (viewControllerCurrent as! DashboardViewController).labelRemaingIndicator.stringValue = "update in \(remaining) seconds"
                }
            }
            UpdateDataGraphs()
        }
    }
}

extension HomeViewController : DelegateCurveMenu {
    func menuButtonTapped(type: ViewType) {
        dispatch_async(dispatch_get_global_queue(0,0), {
            dispatch_async(dispatch_get_main_queue(), {
                self.curveMenuClick(abs(Int(self.viewCurveMenuHolder.alphaValue) - 1))
                self.showViewByType(type)
            });
        });
    }
    func showViewByType(type: ViewType) {
        self.disableDeviceViewClick = true
        MinderManager.sharedInstance.appMode = .Normal
        self.setupWindow()
        self.mainButtonTap(type)
    }
    
    func UpdateProcessingPaused() {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        if MinderManager.sharedInstance.dataProcessingPaused {
            MinderManager.sharedInstance.dataProcessingPaused = false
            buttonMenuProcessPause.image = NSImage(named: "CMPosturePlay")
            viewPosture.UpdateAlertText("")
            buttonMenuProcessPause.toolTip = "Posture playing"
        } else {
            MinderManager.sharedInstance.dataProcessingPaused = true
            buttonMenuProcessPause.image = NSImage(named: "CMPosturePause")
            UpdateColor(.Green)
            viewPosture.UpdateAlertText("Posture is paused")
            buttonMenuProcessPause.toolTip = "Posture paused"
        }
        if MinderManager.sharedInstance.currentView == .Settings {
            if let viewControllerCurrent = self.viewControllerCurrent {
                let vcSettingsVC = (viewControllerCurrent as! SettingsViewController)
                vcSettingsVC.RefreshControls()
            }
        }
    }
}
extension HomeViewController : DelegateSettings {
    func ToggleGyroRotation() {
        OnOffGyroControl()
    }
    //OPP Changing
    func OPPChanged() {
        if MinderManager.sharedInstance.settings?.OPPSelected != 0 {
            self.labelTrackingBall.stringValue = "\((MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!].title)!) Posture"
        } else {
            labelTrackingBall.stringValue = "Posture"
        }
    }
    //ConnectDevice
    func ConnectDevice() {
        dispatch_async(dispatch_get_main_queue(),{
            MinderManager.sharedInstance.DeviceConnect()
        })
    }
    //UpdateSetSeatbackValue
    func SetSeatback(value: Int) {
        MinderManager.sharedInstance.isSeatbackEnabled = value == 0 ? false : true
        print("isSeatbackEnabled \(MinderManager.sharedInstance.isSeatbackEnabled)")
        viewPosture.SetSeatback(value)
    }
    //UpdateDifficultySettings
    func UpdateDifficulty(type: ZoneType, value: Float) {
        viewPosture.UpdateDifficulty(type, difficulty: value)
        if type == .Yellow {
            // Finding ball space and radius for calculation
            let radiusBall = (viewPosture.frame.size.height * MinderManager.percentageBall) / 100
            
            let margin = (viewPosture.frame.size.height * MinderManager.percentageOffset) / 100
            // Finding yellow radius. Yellow zone is MinderManager.percentageZoneYellow percent of height but start after ball space and an offset
            let percentYellow = (CGFloat(value) * MinderManager.percentageZoneYellow) / 100
            let radiusYellow = ((viewPosture.frame.size.height * percentYellow) / 100) + radiusBall + margin
            
            viewBall.frame.size = NSMakeSize(radiusBall * 2, radiusBall * 2)
            //viewAnnulus.frame.origin = NSMakePoint((viewPosture.frame.size.height/2) - (viewAnnulus.frame.size.height/2), (viewPosture.frame.size.height/2) - (viewAnnulus.frame.size.height/2))
            MinderUtility.ShapeView(viewAnnulus, type: .Circle)
            MinderUtility.ShapeView(viewBall, type: .Circle)
        }
    }
    //UpdateNotification
    func UpdateNotification() {
        let opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        let styleParagraph = NSMutableParagraphStyle()
        styleParagraph.alignment = .Center
        let attributesButton: [String : AnyObject] = [ NSFontAttributeName : NSFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : styleParagraph ]
        dispatch_async(dispatch_get_main_queue(), {
            if opp.volumeYellowMute {
                if opp.vibrateYellow == 0 {
                    self.buttonNotification.attributedTitle = NSAttributedString(string: "MUTE", attributes: attributesButton)
                    self.buttonNotification.image = NSImage(named: "VolumeMute")
                    self.buttonMenuNotification.image = NSImage(named: "CMVolumeMute")
                } else {
                    self.buttonNotification.attributedTitle = NSAttributedString(string: "VIBRATE", attributes: attributesButton)
                    self.buttonNotification.image = NSImage(named: "Vibrate")
                    self.buttonMenuNotification.image = NSImage(named: "CMVibrate")
                }
            } else {
                self.buttonNotification.attributedTitle = NSAttributedString(string: "RING", attributes: attributesButton)
                if opp.volumeYellow > 0.65 {
                    self.buttonNotification.image = NSImage(named: "VolumeMax")
                    self.buttonMenuNotification.image = NSImage(named: "CMVolumeMax")
                } else if opp.volumeYellow > 0.35 {
                    self.buttonNotification.image = NSImage(named: "VolumeMid")
                    self.buttonMenuNotification.image = NSImage(named: "CMVolumeMid")
                } else if opp.volumeYellow > 0 {
                    self.buttonNotification.image = NSImage(named: "VolumeMin")
                    self.buttonMenuNotification.image = NSImage(named: "CMVolumeMin")
                } else {
                    if opp.vibrateYellow == 0 {
                        self.buttonNotification.attributedTitle = NSAttributedString(string: "MUTE", attributes: attributesButton)
                        self.buttonNotification.image = NSImage(named: "VolumeMute")
                        self.buttonMenuNotification.image = NSImage(named: "CMVolumeMute")
                    } else {
                        self.buttonNotification.attributedTitle = NSAttributedString(string: "VIBRATE", attributes: attributesButton)
                        self.buttonNotification.image = NSImage(named: "Vibrate")
                        self.buttonMenuNotification.image = NSImage(named: "CMVibrate")
                    }
                }
            }
            if MinderManager.sharedInstance.dataProcessingPaused {
                self.buttonMenuProcessPause.image = NSImage(named: "CMPosturePause")
                self.buttonMenuProcessPause.toolTip = "Posture paused"
            } else {
                self.buttonMenuProcessPause.image = NSImage(named: "CMPosturePlay")
                self.buttonMenuProcessPause.toolTip = "Posture playing"
            }
            if MinderManager.sharedInstance.currentView == .Settings {
                if let viewControllerCurrent = self.viewControllerCurrent {
                    let vcSettingsVC = (viewControllerCurrent as! SettingsViewController)
                    vcSettingsVC.RefreshControls()
                }
            }
        })
    }
    func OPPClicked(index: Int) {
    }
}
