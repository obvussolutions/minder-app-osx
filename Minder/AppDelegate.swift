//
//  AppDelegate.swift
//  Minder
//
//  Created by Abdul on 8/9/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa
import Fabric
import Crashlytics

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    // We get menu event in appdelegate. So to link functionalities with menu item we need to get window controller
    var windowController: NSWindowController!
    
    @IBOutlet weak var menuRecord: NSMenuItem!
    @IBOutlet weak var menuNotification: NSMenuItem!
    @IBOutlet weak var menuViewType: NSMenuItem!
    @IBOutlet weak var menuPauseProcess: NSMenuItem!
    @IBOutlet weak var menuGyroOnOff: NSMenuItem!
      
    /// for the back ground process
    var timer:Timer!
    var timerCount:Int = 0
    var mainViewController:NSViewController!
    
    func checkConnctivity() {
        if (MinderManager.sharedInstance.deviceFeaturesEnabled == true && windowController == nil ) {
            //if (timerCount == 3) {
            MinderManager.sharedInstance.deviceStatus = .initializing
            DispatchQueue.main.async {
                if (self.windowController == nil) {
                    
                    MinderManager.sharedInstance.minderlib = nil
                    self.windowController = NSStoryboard(name : "Main", bundle: nil).instantiateController(withIdentifier: "MainWindowController") as! NSWindowController
                    if let screenSize = NSScreen.main()!.frame.size as? CGSize {
                        self.windowController.window?.setFrameOrigin(NSPoint(x: (screenSize.width/2) - 600, y: (screenSize.height/2) - 350))
                    }
                    self.mainViewController = NSStoryboard(name:"Main", bundle: nil).instantiateController(withIdentifier: "MainViewController") as! MainViewController
                    self.windowController.window?.contentViewController = self.mainViewController
                }
                NSApp.setActivationPolicy(.regular)
                //self.windowController.showWindow(NSApp)
                self.windowController.window?.makeKeyAndOrderFront(nil)
                NSApp.activate(ignoringOtherApps: true)
            }
            timer?.invalidate()
        }
        //timerCount += 1
    }
    
    func switchToBackgroundMode() {
        autoreleasepool {
            timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(AppDelegate.checkConnctivity), userInfo: nil, repeats: true)
        }
        NSApp.setActivationPolicy(.accessory)
    }
    
    func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
        
        MinderManager.sharedInstance.deviceFeaturesEnabled = true
        MinderManager.sharedInstance.deviceStatus = .connected
        MinderManager.sharedInstance.isAppInBackground = false
        MinderManager.sharedInstance.appMode = .gadget//ankush change
        NSApplication.shared().orderedWindows[0].setIsVisible(true)//ankush change
        return true
    }
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
        
    }


    ///
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        MinderManager.sharedInstance.minderlib?.disconnect()
        
//        var alreadyRunning : Bool = false
//        
//        let ws = NSWorkspace.shared()
//        let apps = ws.runningApplications
//        for currentApp in apps
//        {
//            //if(currentApp.activationPolicy == .regular){
//            print(currentApp.localizedName!)
//            if(currentApp.localizedName! == "Minder")
//            {
//                alreadyRunning = true
//            }
//            
//            //}
//        }
        
        Fabric.with([Crashlytics.self])
        
        //  Loading MainViewController explicitly to get the controller handle
        windowController = NSStoryboard(name : "Main", bundle: nil).instantiateController(withIdentifier: "MainWindowController") as! NSWindowController
        if let screenSize = NSScreen.main()!.frame.size as? CGSize {
            windowController.window?.setFrameOrigin(NSPoint(x: (screenSize.width/2) - 500, y: (screenSize.height/2) - 350))
        }
        mainViewController = NSStoryboard(name:"Main", bundle: nil).instantiateController(withIdentifier: "MainViewController") as! MainViewController
        windowController.window?.contentViewController = mainViewController
        windowController.window?.makeKeyAndOrderFront(nil)
        
        
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Disconnect if any device is connected when terminate
        MinderManager.sharedInstance.DeviceDisconnect()
        
        if MinderManager.sharedInstance.isLogEnabled {
            MinderManager.sharedInstance.deviceLog(_log:"Application will terminate.")
        }
    }
    
    // MARK: - Top Menu bar events
    
    @IBAction func MenuViewTypeClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home && MinderManager.sharedInstance.appMode == .normal {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).CloseTapped(sender)
        }
    }
    @IBAction func MenuCurveMenuClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home && MinderManager.sharedInstance.appMode == .gadget {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).curveMenuClick(abs(Int(((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).viewCurveMenuHolder.alphaValue) - 1))
        }
    }
    @IBAction func MenuDashboardClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).showViewByType(.dashboard)
        }
    }
    @IBAction func MenuGuidanceClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).showViewByType (.guidance)
        }
    }
    @IBAction func MenuSettingsClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).showViewByType(.settings)
        }
    }
    @IBAction func MenuWebsiteClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).WebsiteTapped(sender)
        }
    }
    @IBAction func MenuSyncClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).SyncTapped(sender)
        }
    }
    @IBAction func MenuCalibrateClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).CalibrateTapped(sender)
        }
    }
    @IBAction func SkipCalibrationClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).SkipCalibration()
        }
    }
    @IBAction func MenuRecordClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).RecordTapped(sender)
        }
    }
    @IBAction func PauseProcessClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).UpdateProcessingPaused()
        }
    }
    @IBAction func MenuNotificationClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).NotificationTapped(sender)
        }
    }
    @IBAction func MenuDifficultyYellowIncClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).YellowZoneIncrementTapped(sender)
        }
    }
    @IBAction func MenuDifficultyYellowDecClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).YellowZoneDecrementTapped(sender)
        }
    }
    @IBAction func MenuDifficultyRedIncClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).RedZoneIncrementTapped(sender)
        }
    }
    @IBAction func MenuDifficultyRedDecClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).RedZoneDecrementTapped(sender)
        }
    }
    @IBAction func GyroOnOffClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).OnOffGyroControl()
        }
    }
    @IBAction func ToggleGyroControlClicked(_ sender: AnyObject) {
        if
            (windowController.contentViewController as! MainViewController).currentViewType == .home {
            ((windowController.contentViewController as! MainViewController).currentViewController as! HomeViewController).ToggleGyroControl()
        }
    }
    
    func SetMenuRecordEnabled(_ enable: Bool) {
        menuRecord.isEnabled = enable
    }
    func SetMenuRecordTitle(_ title: String) {
        menuRecord.title = title
    }
    func SetMenuNotificationTitle(_ title: String) {
        menuNotification.title = title
    }
    func SetMenuGyroOnOffTitle(_ title: String) {
        menuGyroOnOff.title = title
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "obVus.Minder" in the user's Application Support directory.
        let urls = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask)
        let appSupportURL = urls[urls.count - 1]
        return appSupportURL.appendingPathComponent("obVus.Minder")
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "Minder", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.) This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        let fileManager = FileManager.default
        var failError: NSError? = nil
        var shouldFail = false
        var failureReason = "There was an error creating or loading the application's saved data."
        
        // MAIN LINE OF CODE TO ADD
        let mOptions = [NSMigratePersistentStoresAutomaticallyOption: true,
                        NSInferMappingModelAutomaticallyOption: true]
        
        // Make sure the application files directory is there
        do {
            let properties = try (self.applicationDocumentsDirectory as NSURL).resourceValues(forKeys: [URLResourceKey.isDirectoryKey])
            if !(properties[URLResourceKey.isDirectoryKey]! as AnyObject).boolValue {
                failureReason = "Expected a folder to store application data, found a file \(self.applicationDocumentsDirectory.path)."
                shouldFail = true
            }
        } catch  {
            let nserror = error as NSError
            if nserror.code == NSFileReadNoSuchFileError {
                do {
                    try fileManager.createDirectory(atPath: self.applicationDocumentsDirectory.path, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    failError = nserror
                }
            } else {
                failError = nserror
            }
        }
        
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = nil
        if failError == nil {
            coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
            let url = self.applicationDocumentsDirectory.appendingPathComponent("CocoaAppCD.storedata")
            do {
                try coordinator!.addPersistentStore(ofType: NSXMLStoreType, configurationName: nil, at: url, options: mOptions)
            } catch {
                failError = error as NSError
            }
        }
        
        if shouldFail || (failError != nil) {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            if failError != nil {
                dict[NSUnderlyingErrorKey] = failError
            }
            let error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSApplication.shared().presentError(error)
            abort()
        } else {
            return coordinator!
        }
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving and Undo support
    
    @IBAction func saveAction(_ sender: AnyObject!) {
        // Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
        if !managedObjectContext.commitEditing() {
            NSLog("\(NSStringFromClass(type(of: self))) unable to commit editing before saving")
        }
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSApplication.shared().presentError(nserror)
            }
        }
    }
    
    func windowWillReturnUndoManager(_ window: NSWindow) -> UndoManager? {
        // Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
        return managedObjectContext.undoManager
    }
    
    func applicationShouldTerminate(_ sender: NSApplication) -> NSApplicationTerminateReply {
        
        // Save changes in the application's managed object context before the application terminates.
        
        /// move to background
//        if NSApplication.shared().occlusionState != .visible {
//            MinderManager.sharedInstance.DeviceDisconnect()
//            MinderManager.sharedInstance.deviceFeaturesEnabled = false
//            MinderManager.sharedInstance.deviceStatus = .disConnected
//            MinderManager.sharedInstance.appMode = .gadget
//            
//            mainViewController = nil
//            windowController = nil
//            switchToBackgroundMode()
//            return NSApplicationTerminateReply.terminateCancel
//        }
//        ///
//        
//        if !managedObjectContext.commitEditing() {
//            NSLog("\(NSStringFromClass(type(of: self))) unable to commit editing to terminate")
//            return .terminateCancel
//        }
//        
//        if !managedObjectContext.hasChanges {
//            
//            return .terminateNow
//        }
//        
//        do {
//            try managedObjectContext.save()
//        } catch {
//            let nserror = error as NSError
//            // Customize this code block to include application-specific recovery steps.
//            let result = sender.presentError(nserror)
//            if (result) {
//                return .terminateCancel
//            }
//            
//            let question = NSLocalizedString("Could not save changes while quitting. Quit anyway?", comment: "Quit without saves error question message")
//            let info = NSLocalizedString("Quitting now will lose any changes you have made since the last successful save", comment: "Quit without saves error question info");
//            let quitButton = NSLocalizedString("Quit anyway", comment: "Quit anyway button title")
//            let cancelButton = NSLocalizedString("Cancel", comment: "Cancel button title")
//            let alert = NSAlert()
//            alert.messageText = question
//            alert.informativeText = info
//            alert.addButton(withTitle: quitButton)
//            alert.addButton(withTitle: cancelButton)
//            
//            let answer = alert.runModal()
//            if answer == NSAlertFirstButtonReturn {
//                return .terminateCancel
//            }
//        }
        // If we got here, it is time to quit.
        return .terminateNow
    }
    
    
}

