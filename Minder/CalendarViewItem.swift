//
//  CalendarViewItem.swift
//  Minder
//
//  Created by Febin on 4/12/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa

class CalendarViewItem: NSCollectionViewItem {


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        super.viewDidLoad()
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.init(calibratedRed: 45/255, green: 56/255, blue: 62/255, alpha: 1).cgColor
    }
    
}
