//
//  NSDatePickerWindowController.swift
//  NSDatePickerSampleCode
//
//  Created by Debasis Das on 09/02/16.
//  Copyright © 2016 Knowstack. All rights reserved.
//

import Cocoa
@objc protocol DatePickerProtocol:class
{
    func selectedDate(_ date:Date)
}

class NSDatePickerWindowController: NSWindowController
{
    @IBOutlet weak var datePicker:NSDatePicker!
    
    weak var delegate:DatePickerProtocol!
    
    override func windowDidLoad()
    {
        super.windowDidLoad()
        self.datePicker.dateValue = Date()
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }
    
    @IBAction func selectDate(_ sender:AnyObject)
    {
        
        self.delegate.selectedDate(self.datePicker.dateValue)
        
    
        self.window?.close()
    }
}
