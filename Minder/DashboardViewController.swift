//
//  DashboardViewController.swift
//  Minder
//
//  Created by Abdul on 8/9/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class DashboardViewController: NSViewController, NSApplicationDelegate, NSWindowDelegate, DatePickerProtocol {
    
    @IBOutlet weak var labelGreenPosition: NSTextField!
    @IBOutlet weak var labelYellowPosition: NSTextField!
    @IBOutlet weak var labelRedPosition: NSTextField!
    @IBOutlet weak var labelRemaingIndicator: NSTextField!
    
    @IBOutlet weak var viewLabelsHolder: NSView!
    @IBOutlet weak var viewGraphBarHolder: NSView!
    @IBOutlet weak var viewGraphPieHolder: NSView!
    @IBOutlet weak var viewGraphHeatHolder: NSView!
    @IBOutlet weak var viewGraphCompletedHolder: NSView!
    @IBOutlet weak var viewStepsHolder: NSView!
    
    @IBOutlet weak var labelSteps: NSTextField!
    
    @IBOutlet weak var viewGraphBar: GraphView!
    @IBOutlet weak var viewGraphPie: GraphView!
    @IBOutlet weak var viewGraphHeat: GraphView!
    @IBOutlet weak var viewGraphCompleted: GraphView!
    
    @IBOutlet weak var buttonCheckPrevious: NSButton!
    @IBOutlet weak var viewCheckPrevious: NSView!
    @IBOutlet weak var buttonDateFrom: NSButton!
    @IBOutlet weak var buttonDateTo: NSButton!
    @IBOutlet weak var buttonShowData: NSButton!
    @IBOutlet weak var progressIndicator: NSProgressIndicator!
    
    var datePickerWindowController:NSDatePickerWindowController!
    
    @IBOutlet weak var checkPreviousHR: NSButton!
    @IBOutlet weak var viewCheckHRAverage: NSView!
    @IBOutlet weak var popupHRDateType: NSPopUpButton!
    @IBOutlet weak var popupHRDateYear: NSPopUpButton!
    @IBOutlet weak var popupHRDateMonth: NSPopUpButton!
    @IBOutlet weak var buttonDateHRAverage: NSButton!
    @IBOutlet weak var buttonShowHR: NSButton!
    @IBOutlet weak var labelHRAverage: NSTextField!
    
    var stringButton : String!
    var dateFrom : Date!
    var dateTo : Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewGraphBar.graphType = .bar
        viewGraphPie.graphType = .pie
        viewGraphHeat.graphType = .heat
        viewGraphCompleted.graphType = .completed
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            print("dashboard getactivity ")
            GetGuidanceActivity()
        }
    }
    override func viewWillAppear() {
        viewGraphBarHolder.wantsLayer = true
        viewGraphBarHolder.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).cgColor
        MinderUtility.ShapeView(viewGraphBarHolder, type: .rectTopCurved3By4)
        viewGraphPieHolder.wantsLayer = true
        viewGraphPieHolder.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).cgColor
        MinderUtility.ShapeView(viewGraphPieHolder, type: .rectTopCurved3By4)
        viewGraphHeatHolder.wantsLayer = true
        viewGraphHeatHolder.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).cgColor
        MinderUtility.ShapeView(viewGraphHeatHolder, type: .rectTopCurved3By4)
        viewGraphCompletedHolder.wantsLayer = true
        viewGraphCompletedHolder.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).cgColor
        MinderUtility.ShapeView(viewGraphCompletedHolder, type: .rectTopCurved3By4)
        viewStepsHolder.wantsLayer = true
        viewStepsHolder.layer?.backgroundColor = NSColor.init(calibratedRed: 50/255, green: 60/255, blue: 65/255, alpha: 0.9).cgColor
        MinderUtility.ShapeView(viewStepsHolder, type: .rectTopCurved3By4)
        
        buttonCheckPrevious.attributedTitle = NSAttributedString(string:"Check previous graphs", attributes: [ NSForegroundColorAttributeName : NSColor.white ])
        checkPreviousHR.attributedTitle = NSAttributedString(string:"Check previous HR average", attributes: [ NSForegroundColorAttributeName : NSColor.white ])
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        buttonDateFrom.attributedTitle = NSAttributedString(string:"Select Date", attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style ])
        buttonDateTo.attributedTitle = NSAttributedString(string:"Select Date", attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style ])
        buttonShowData.attributedTitle = NSAttributedString(string:"Show Graphs", attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style ])
        buttonShowHR.attributedTitle = NSAttributedString(string:"Show avg", attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style ])
        buttonDateHRAverage.attributedTitle = NSAttributedString(string:"Select Date", attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style ])
        
        refreshGraphs()
    }
    func refreshGraphs() {
        viewGraphBar.reDraw()
        viewGraphPie.reDraw()
        viewGraphHeat.reDraw()
        viewGraphCompleted.reDraw()
        setStepCount()
    }
    func setStepCount() {
        /*var fontSize = labelSteps.font?.pointSize;
         while NSAttributedString(string: "\(MinderManager.sharedInstance.dStepCount)", attributes: [
         NSFontAttributeName : [NSFont.systemFontOfSize(fontSize!)]
         ]).size().width > labelSteps.frame.size.width {
         fontSize = fontSize! - 1
         }
         labelSteps.font = NSFont.systemFontOfSize(fontSize!)*/
        labelSteps.stringValue = "\(MinderManager.sharedInstance.dStepCount)"
        labelSteps.font = NSFont.systemFont(ofSize: updateFontSize(labelSteps))
    }
    func updateFontSize(_ textField: NSTextField) -> CGFloat {
        let labelSteps = textField.stringValue
        var fontSize = CGFloat(54.0)
        if labelSteps.characters.count > 3 && labelSteps.characters.count < 10{
            fontSize = fontSize - (CGFloat(labelSteps.characters.count * 5) - (3))
        }else if labelSteps.characters.count > 9 {
            fontSize = CGFloat(12.0)
        }
        return fontSize
    }
    func GetGuidanceActivity() {
        let appDelegate = NSApplication.shared().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        var arrSchedules: [Activity] = [Activity]()
        let fetchRequestActivitySchedule = NSFetchRequest<NSFetchRequestResult>(entityName: "Activity")
        fetchRequestActivitySchedule.predicate = NSPredicate(format: "user_email == %@", (MinderManager.sharedInstance.userLogged?.email)!)
        do {
            arrSchedules = try managedContext.fetch(fetchRequestActivitySchedule) as! [Activity]
        } catch {
                NSLog("Failed to fetch employees: \(error)")
        }
        var exersiceCompletedCount = 0
        for schedule in arrSchedules {
            exersiceCompletedCount += schedule.exercise_status!.intValue
        }
        if exersiceCompletedCount > 0 {
            viewGraphCompleted.zoneYellowValue = round(((CGFloat(exersiceCompletedCount) / CGFloat(arrSchedules.count)) * 100) * 10) / 10
        }
        
        print("GetGuidanceActivity : \(arrSchedules.count)")
    }
    @IBAction func CheckPreviousClicked(_ sender: AnyObject) {
        MinderManager.sharedInstance.showPreviousHR = false
        checkPreviousHR.state = 0
        MinderManager.sharedInstance.showPreviousData = buttonCheckPrevious.state == 0 ? false : true
        viewCheckHRAverage.isHidden = !MinderManager.sharedInstance.showPreviousHR
        viewCheckPrevious.isHidden = !MinderManager.sharedInstance.showPreviousData
    }
    @IBAction func DateFromClicked(_ sender: AnyObject) {
        stringButton = "From"
        let windowController = NSDatePickerWindowController(windowNibName: "NSDatePickerWindowController")
        self.datePickerWindowController = windowController
        self.datePickerWindowController.delegate = self
        self.view.window!.delegate = self
        self.view.window!.beginSheet(self.datePickerWindowController.window!, completionHandler: nil)
    }
    @IBAction func DateToClicked(_ sender: AnyObject) {
        stringButton = "To"
        let windowController = NSDatePickerWindowController(windowNibName: "NSDatePickerWindowController")
        self.datePickerWindowController = windowController
        self.datePickerWindowController.delegate = self
        self.view.window!.delegate = self
        self.view.window!.beginSheet(self.datePickerWindowController.window!, completionHandler: nil)
    }
    @IBAction func CheckPreviousHRClicked(_ sender: Any) {
        MinderManager.sharedInstance.showPreviousData = false
        buttonCheckPrevious.state = 0
        MinderManager.sharedInstance.showPreviousHR = checkPreviousHR.state == 0 ? false : true
        viewCheckHRAverage.isHidden = !MinderManager.sharedInstance.showPreviousHR
        viewCheckPrevious.isHidden = !MinderManager.sharedInstance.showPreviousData
    }
    @IBAction func popupHRTypeClicked(_ sender: Any) {
        if popupHRDateType.selectedItem?.tag == 0 {
            popupHRDateYear.isHidden = false
            popupHRDateMonth.isHidden = true
            buttonDateHRAverage.isHidden = true
        } else if popupHRDateType.selectedItem?.tag == 1 {
            popupHRDateYear.isHidden = false
            popupHRDateMonth.isHidden = false
            buttonDateHRAverage.isHidden = true
        } else if popupHRDateType.selectedItem?.tag == 3 {
            popupHRDateYear.isHidden = true
            popupHRDateMonth.isHidden = true
            buttonDateHRAverage.isHidden = false
        } else {
            popupHRDateYear.isHidden = true
            popupHRDateMonth.isHidden = true
            buttonDateHRAverage.isHidden = true
        }
        
    }
    @IBAction func DateHRClicked(_ sender: Any) {
        stringButton = "HR"
        let windowController = NSDatePickerWindowController(windowNibName: "NSDatePickerWindowController")
        self.datePickerWindowController = windowController
        self.datePickerWindowController.delegate = self
        self.view.window!.delegate = self
        self.view.window!.beginSheet(self.datePickerWindowController.window!, completionHandler: nil)
    }
    @IBAction func ShowHRAverageClicked(_ sender: Any) {
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
                var year: String = "-2"
                var month: String = "-2"
                var week: String = "-2"
                var day: String = "-2"
                if popupHRDateType.selectedItem?.tag == 0 {
                    year = popupHRDateYear.titleOfSelectedItem!
                    month = "-1"
                    week = "-1"
                    day = "-1"
                } else if popupHRDateType.selectedItem?.tag == 1 {
                    year = popupHRDateYear.titleOfSelectedItem!
                    month = popupHRDateMonth.titleOfSelectedItem!
                    week = "-1"
                    day = "-1"
                } else if popupHRDateType.selectedItem?.tag == 3 {
                    if dateFrom != nil {
                        year = Date.GetDateString(dateFrom, format: "yyyy")
                        month = Date.GetDateString(dateFrom, format: "MM")
                        week = "-1"
                        day = Date.GetDateString(dateFrom, format: "dd")
                    }
                } else {
                    popupHRDateYear.isHidden = true
                    popupHRDateMonth.isHidden = true
                    buttonDateHRAverage.isHidden = true
                }
            if year != "-2" && month != "-2" && week != "-2" && day != "-2" {
                progressIndicator.isHidden = false
                progressIndicator.startAnimation(nil)
                MinderManager.sharedInstance.GetHRAverage(year, month: month, week: week, day: day, completionHandler: { data -> Void in
                    if data["status"]! as? String == "Success" {
                        if let result = data["result"] as? Dictionary<String, AnyObject> {
                            if let hravg = result["hr_avg"] as? String {
                                print("hravg \(hravg)")
                                DispatchQueue.main.async(execute: {
                                    self.labelHRAverage.stringValue = "Average heart rate: \(hravg)"
                                })
                                
                            }
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        self.progressIndicator.stopAnimation(nil)
                        self.progressIndicator.isHidden = true
                    })
                })
            } else {
                var msg = ""
                if popupHRDateType.selectedItem?.tag == 0 {
                    msg = "year"
                } else if popupHRDateType.selectedItem?.tag == 1 {
                    msg = "year and month"
                } else if popupHRDateType.selectedItem?.tag == 3 {
                    msg = "date"
                } else {
                    msg = "year, month and week"
                }
                let myPopup: NSAlert = NSAlert()
                myPopup.messageText = "Please select date"
                myPopup.informativeText = "You need to select \(msg) to view data."
                myPopup.alertStyle = .warning
                myPopup.addButton(withTitle: "OK")
                myPopup.runModal()
            }
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Please login."
            myPopup.informativeText = "You need to login to view data."
            myPopup.alertStyle = .warning
            myPopup.addButton(withTitle: "OK")
            myPopup.runModal()
        }
    }
    
    func window(_ window: NSWindow, willPositionSheet sheet: NSWindow, using rect: NSRect) -> NSRect {
        if sheet == self.datePickerWindowController.window {
            var rectToReturn: NSRect?
            if stringButton == "From" {
                rectToReturn = buttonDateFrom.frame
            } else if stringButton == "To" {
                rectToReturn = buttonDateTo.frame
            } else {
                rectToReturn = buttonDateHRAverage.frame
            }
            print("rectToReturn:\(rectToReturn) view.frame:\(view.frame) viewCheckPrevious.frame:\(viewCheckPrevious.frame)")
            rectToReturn!.origin.x = 0 + viewCheckPrevious.frame.origin.x + rectToReturn!.origin.x
            rectToReturn!.origin.y = 35 + viewCheckPrevious.frame.origin.y + rectToReturn!.origin.y
            return rectToReturn!
        } else {
            return rect
        }
    }
    func selectedDate(_ date: Date) {
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        if stringButton == "From" {
            dateFrom = date
            buttonDateFrom.attributedTitle = NSAttributedString(string:Date.GetDateString(date, format: "MM-dd-yyyy"), attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style ])
        } else if stringButton == "To" {
            dateTo = date
            buttonDateTo.attributedTitle = NSAttributedString(string:Date.GetDateString(date, format: "MM-dd-yyyy"), attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style ])
        } else {
            dateFrom = date
            buttonDateHRAverage.attributedTitle = NSAttributedString(string:Date.GetDateString(date, format: "MM-dd-yyyy"), attributes: [ NSForegroundColorAttributeName : NSColor.white, NSParagraphStyleAttributeName : style ])
        }
    }
     // MARK: ShowDataClicked
    @IBAction func ShowDataClicked(_ sender: AnyObject) {
        if !(MinderManager.sharedInstance.userLogged?.userName ?? "").isEmpty {
            if dateTo != nil && dateFrom != nil {
                progressIndicator.isHidden = false
                progressIndicator.startAnimation(nil)
                MinderManager.sharedInstance.GetZoneData(Date.GetDateString(dateFrom, format: "yyyy-MM-dd"), edate: Date.GetDateString(dateTo, format: "yyyy-MM-dd"), completionHandler: { data -> Void in
                    if data["status"]! as? String == "Success" {
                        let green = (data["green"]! as? Int)!
                        let yellow = (data["yellow"]! as? Int)!
                        let red = (data["red"]! as? Int)!
                        
                        if green > 0 || yellow > 0 || red > 0 {
                            let gyrTotal = CGFloat(green + yellow + red)
                            var greenPercent: CGFloat = 0
                            if green > 0 {
                                greenPercent = round(((CGFloat(green) / gyrTotal) * 100) * 10) / 10
                            }
                            var yellowPercent: CGFloat = 0
                            if yellow > 0 {
                                yellowPercent = round(((CGFloat(yellow) / gyrTotal) * 100) * 10) / 10
                            }
                            var redPercent: CGFloat = 0
                            if red > 0 {
                                redPercent = round(((CGFloat(red) / gyrTotal) * 100) * 10) / 10
                            }
                            
                            DispatchQueue.main.async(execute: {
                                self.labelGreenPosition.stringValue = "Green: \(MinderUtility.SecondsToHMSString(green)) (\(greenPercent)%)"
                                self.labelYellowPosition.stringValue = "Yellow: \(MinderUtility.SecondsToHMSString(yellow)) (\(yellowPercent)%)"
                                self.labelRedPosition.stringValue = "Red: \(MinderUtility.SecondsToHMSString(red)) (\(redPercent)%)"
                                
                                self.viewGraphPie.zoneYellowValue = yellowPercent
                                self.viewGraphPie.zoneRedValue = redPercent
                                
                                self.refreshGraphs()
                            })
                        }
                    }
                })
                MinderManager.sharedInstance.GetDeviceData(Date.GetDateString(dateFrom, format: "yyyy-MM-dd"), edate: Date.GetDateString(dateTo, format: "yyyy-MM-dd"), completionHandler: { data -> Void in
                    if data["status"]! as? String == "Success" {
                        if let result = data["result"] as? Dictionary<String, AnyObject> {
                            var pDataArray: [PositionData] = [PositionData]()
                            var hDataArray: [HeatmapData] = [HeatmapData]()
                            let imageHeatmap: NSImage? = NSImage(size: NSMakeSize(900, 900))
                            
                            if let dDatas = result["raw_data"] as? [AnyObject] {
                                print("Data Count: \(dDatas.count)")
                                var _green = 0
                                var _yellow = 0
                                var _red = 0
                                var elapsedM = 0
                                var processed = 0
                                for dData in dDatas {
                                    let heatPoint: NSPoint = NSPoint(x: dData["z"]! as! CGFloat, y: dData["y"]! as! CGFloat)
                                    var heatData: HeatmapData = HeatmapData()
                                    if hDataArray.contains(where: { $0.rect.contains(heatPoint) }) {
                                        heatData = hDataArray[hDataArray.index(where: { $0.rect.contains(heatPoint) })!]
                                        heatData.weight += 0.1
                                    } else {
                                        heatData.rect = NSMakeRect(heatPoint.x - 3, heatPoint.y - 3, 6, 6)
                                        heatData.weight = 0.1
                                    }
                                    hDataArray.append(heatData)
                                    
                                    let state: Int = dData["state"] as! Int
                                    switch state {
                                    case 0:
                                        _green += 1
                                        break
                                    case 1:
                                        _yellow += 1
                                        break
                                    case 2:
                                        _red += 1
                                        break
                                    default:
                                        _green += 1
                                        break
                                    }
                                    
                                    elapsedM += 100
                                    if elapsedM == 60000 {
                                        let pData: PositionData = PositionData()
                                        pData.green = _green
                                        pData.yellow = _yellow
                                        pData.red = _red
                                        pDataArray.insert(pData, at: 0)
                                        _green = 0
                                        _yellow = 0
                                        _red = 0
                                        elapsedM = 0
                                    }
                                    
                                    
                                    
                                    let bezPath: NSBezierPath = NSBezierPath(ovalIn: NSMakeRect(heatPoint.x, heatPoint.y, 10, 10))
                                    imageHeatmap!.lockFocus()
                                    MinderUtility.ColorForValue(heatData.weight).set()
                                    bezPath.fill()
                                    imageHeatmap!.unlockFocus()
                                    
                                    processed += 1
                                    print("processed: \(processed)")
                                }
                                
                                if pDataArray.count > 30 {
                                    pDataArray.removeSubrange((30 ..< pDataArray.count - 1))
                                }
                                
                                DispatchQueue.main.async(execute: {
                                    self.viewGraphBar.positionDataList = pDataArray
                                    if imageHeatmap != nil {
                                        self.viewGraphHeat.imageHeatmap = MinderUtility.ResizeImage(imageHeatmap!, destSize: self.viewGraphHeat.frame.size)
                                    }
                                    self.refreshGraphs()
                                })
                                
                            }
                        }
                    } else {
                        
                    }
                    DispatchQueue.main.async(execute: {
                        self.progressIndicator.stopAnimation(nil)
                        self.progressIndicator.isHidden = true
                    })
                })
            } else {
                let myPopup: NSAlert = NSAlert()
                myPopup.messageText = "Please select date"
                myPopup.informativeText = "You need to select from and to date to view data."
                myPopup.alertStyle = .warning
                myPopup.addButton(withTitle: "OK")
                myPopup.runModal()
            }
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.messageText = "Please login."
            myPopup.informativeText = "You need to login to view data."
            myPopup.alertStyle = .warning
            myPopup.addButton(withTitle: "OK")
            myPopup.runModal()
        }
    }
}
