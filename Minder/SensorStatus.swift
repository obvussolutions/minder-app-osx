//
//  SensorStatus.swift
//  Minder
//
//  Created by Pegasus on 10/01/18.
//  Copyright © 2018 WHI. All rights reserved.
//

import Cocoa

class SensorStatus: NSObject {
    
    var isAccelEnable: Bool = true
    var isHeartRateEnable: Bool = true
    var isLraMotorEnable: Bool = true
    var ledColor: String? = "0"
    var ledDutyCycle: String? = "0"
    var ledPeriod: String? = "0"

    
    override init() {}
    
    required init(coder aDecoder: NSCoder) {
        if let accelStatus = aDecoder.decodeBool(forKey: "accelStatus") as? Bool {
            self.isAccelEnable = accelStatus
        }
        if let heartRateStatus = aDecoder.decodeBool(forKey: "heartRateStatus") as? Bool {
            self.isHeartRateEnable = heartRateStatus
        }
        if let lraStatus = aDecoder.decodeBool(forKey: "lraStatus") as? Bool {
            self.isLraMotorEnable = lraStatus
        }
        if let _ledColor = aDecoder.decodeObject(forKey: "ledColor") as? String {
            self.ledColor = _ledColor
        }
        if let _ledDutyCycle = aDecoder.decodeObject(forKey: "ledDutyCycle") as? String {
            self.ledDutyCycle = _ledDutyCycle
        }
        if let _ledPeriod = aDecoder.decodeObject(forKey: "ledPeriod") as? String {
            self.ledPeriod = _ledPeriod
        }
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let accelStatus = self.isAccelEnable as? Bool {
            aCoder.encode(accelStatus, forKey: "accelStatus")
        }
        if let heartRateStatus = self.isHeartRateEnable as? Bool {
            aCoder.encode(heartRateStatus, forKey: "heartRateStatus")
        }
        if let lraStatus = self.isLraMotorEnable as? Bool {
            aCoder.encode(lraStatus, forKey: "lraStatus")
        }
        if let _ledColor = self.ledColor {
            aCoder.encode(_ledColor, forKey: "ledColor")
        }
        if let _ledDutyCycle = self.ledDutyCycle {
            aCoder.encode(_ledDutyCycle, forKey: "ledDutyCycle")
        }
        if let _ledPeriod = self.ledPeriod {
            aCoder.encode(_ledPeriod, forKey: "ledPeriod")
        }
    }
}
