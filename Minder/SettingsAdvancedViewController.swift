//
//  SettingsAdvancedViewController.swift
//  Minder
//
//  Created by Abdul on 10/11/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

class SettingsAdvancedViewController: NSViewController {
    
    var delegateSettings: DelegateSettings? = nil
    var oppIndex: Int = -1
    var opp: OPP = OPP()
    
    @IBOutlet weak var popButtonNotificationDevice: NSPopUpButton!
    
    @IBOutlet weak var popButtonVibrateYellow: NSPopUpButton!
    @IBOutlet weak var popButtonInterval1Yellow: NSPopUpButton!
    @IBOutlet weak var popButtonInterval2Yellow: NSPopUpButton!
    
    @IBOutlet weak var popButtonVibrateRed: NSPopUpButton!
    @IBOutlet weak var popButtonInterval1Red: NSPopUpButton!
    @IBOutlet weak var popButtonInterval2Red: NSPopUpButton!
    
    @IBOutlet weak var popButtonInterval1YellowVolume: NSPopUpButton!
    @IBOutlet weak var popButtonInterval2YellowVolume: NSPopUpButton!
    @IBOutlet weak var buttonVolumeYellowMute: NSButton!
    @IBOutlet weak var sliderVolumeYellow: NSSlider!
    
    @IBOutlet weak var popButtonInterval1RedVolume: NSPopUpButton!
    @IBOutlet weak var popButtonInterval2RedVolume: NSPopUpButton!
    @IBOutlet weak var buttonVolumeRedMute: NSButton!
    @IBOutlet weak var sliderVolumeRed: NSSlider!
    
    @IBOutlet weak var sliderDifficultyYellow: NSSlider!
    @IBOutlet weak var sliderDifficultyRed: NSSlider!
    
    @IBOutlet weak var checkSeatback: NSButton!
    @IBOutlet weak var sliderDifficultySeatback: NSSlider!
    @IBOutlet weak var labelDifficultySeatback: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkSeatback.attributedTitle = NSAttributedString(string: "Supported posture zone", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : NSColor.white ])
        
        opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        
        RefreshControls()
    }
    
    // refreshing the view
    func RefreshControls() {
        popButtonNotificationDevice.selectItem(withTag: opp.notificationDevice)
        
        popButtonVibrateYellow.selectItem(withTag: opp.vibrateYellow)
        popButtonInterval1Yellow.selectItem(withTag: opp.interval1YellowVibrate)
        popButtonInterval2Yellow.selectItem(withTag: opp.interval2YellowVibrate)
        if opp.vibrateYellow == 0 {
            popButtonInterval1Yellow.isEnabled = false
            popButtonInterval2Yellow.isEnabled = false
        } else {
            popButtonInterval1Yellow.isEnabled = true
            popButtonInterval2Yellow.isEnabled = true
        }
        popButtonVibrateRed.selectItem(withTag: opp.vibrateRed)
        popButtonInterval1Red.selectItem(withTag: opp.interval1RedVibrate)
        popButtonInterval2Red.selectItem(withTag: opp.interval2RedVibrate)
        if opp.vibrateRed == 0 {
            popButtonInterval1Red.isEnabled = false
            popButtonInterval2Red.isEnabled = false
        } else {
            popButtonInterval1Red.isEnabled = true
            popButtonInterval2Red.isEnabled = true
        }
        
        popButtonInterval1YellowVolume.selectItem(withTag: opp.interval1YellowVolume)
        popButtonInterval2YellowVolume.selectItem(withTag: opp.interval2YellowVolume)
        if opp.volumeYellowMute {
            sliderVolumeYellow.floatValue = 0
            buttonVolumeYellowMute.toolTip = "Unmute yellow volume"
            buttonVolumeYellowMute.image = NSImage(named: "VolumeMute")
            popButtonInterval1YellowVolume.isEnabled = false
            popButtonInterval2YellowVolume.isEnabled = false
        } else {
            popButtonInterval1YellowVolume.isEnabled = true
            popButtonInterval2YellowVolume.isEnabled = true
            sliderVolumeYellow.floatValue = opp.volumeYellow
            buttonVolumeYellowMute.toolTip = "Mute yellow volume"
            if opp.volumeYellow > 0.65 {
                buttonVolumeYellowMute.image = NSImage(named: "VolumeMax")
            } else if opp.volumeYellow > 0.35 {
                buttonVolumeYellowMute.image = NSImage(named: "VolumeMid")
            } else if opp.volumeYellow > 0 {
                buttonVolumeYellowMute.image = NSImage(named: "VolumeMin")
            } else {
                buttonVolumeYellowMute.image = NSImage(named: "VolumeMute")
                popButtonInterval1YellowVolume.isEnabled = false
                popButtonInterval2YellowVolume.isEnabled = false
            }
        }
        popButtonInterval1RedVolume.selectItem(withTag: opp.interval1RedVolume)
        popButtonInterval2RedVolume.selectItem(withTag: opp.interval2RedVolume)
        if opp.volumeRedMute {
            sliderVolumeRed.floatValue = 0
            buttonVolumeRedMute.toolTip = "Unmute red volume"
            buttonVolumeRedMute.image = NSImage(named: "VolumeMute")
            popButtonInterval1RedVolume.isEnabled = false
            popButtonInterval2RedVolume.isEnabled = false
        } else {
            popButtonInterval1RedVolume.isEnabled = true
            popButtonInterval2RedVolume.isEnabled = true
            sliderVolumeRed.floatValue = opp.volumeRed
            buttonVolumeRedMute.toolTip = "Mute red volume"
            if opp.volumeRed > 0.65 {
                buttonVolumeRedMute.image = NSImage(named: "VolumeMax")
            } else if opp.volumeRed > 0.35 {
                buttonVolumeRedMute.image = NSImage(named: "VolumeMid")
            } else if opp.volumeRed > 0 {
                buttonVolumeRedMute.image = NSImage(named: "VolumeMin")
            } else {
                buttonVolumeRedMute.image = NSImage(named: "VolumeMute")
                popButtonInterval1RedVolume.isEnabled = false
                popButtonInterval2RedVolume.isEnabled = false
            }
        }
        sliderDifficultyYellow.floatValue = 100 - opp.difficultyYellow
        sliderDifficultyRed.floatValue = 100 - opp.difficultyRed
        checkSeatback.state = opp.isSeatBack
        sliderDifficultySeatback.floatValue = 100 - opp.difficultySeatback
        if checkSeatback.state == 1 {
            sliderDifficultySeatback.isEnabled = true
        }
        popButtonNotificationDevice.selectItem(withTag: opp.notificationDevice)
    }
    
    @IBAction func NotificationDeviceChanged(_ sender: AnyObject) {
        opp.notificationDevice = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // Interval1YellowChanged
    @IBAction func Interval1YellowChanged(_ sender: NSPopUpButton) {
        opp.interval1YellowVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // Interval1RedChanged
    @IBAction func Interval1RedChanged(_ sender: NSPopUpButton) {
        opp.interval1RedVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // Interval2YellowChanged
    @IBAction func Interval2YellowChanged(_ sender: NSPopUpButton) {
        opp.interval2YellowVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // Interval2RedChanged
    @IBAction func Interval2RedChanged(_ sender: NSPopUpButton) {
        opp.interval2RedVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // VibrateYellowChanged
    @IBAction func VibrateYellowChanged(_ sender: NSPopUpButton) {
        opp.vibrateYellow = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    // VibrateRedChanged
    @IBAction func VibrateRedChanged(_ sender: NSPopUpButton) {
        opp.vibrateRed = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    @IBAction func Interval1YellowVolumeChanged(_ sender: AnyObject) {
        opp.interval1YellowVolume = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    @IBAction func Interval2YellowVolumeChanged(_ sender: AnyObject) {
        opp.interval2YellowVolume = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    // VolumeYellowChanged
    @IBAction func VolumeYellowChanged(_ sender: NSSlider) {
        if sender.floatValue > 0 {
            opp.volumeYellowMute = false
        }
        opp.volumeYellow = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    // MuteYellowClicked
    @IBAction func MuteYellowClicked(_ sender: AnyObject) {
        if opp.volumeYellowMute {
            opp.volumeYellowMute = false
        } else {
            opp.volumeYellowMute = true
        }
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    @IBAction func Interval1RedVolumeChanged(_ sender: AnyObject) {
        opp.interval1RedVolume = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    @IBAction func Interval2RedVolumeChanged(_ sender: AnyObject) {
        opp.interval2RedVolume = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // VolumeRedChanged
    @IBAction func VolumeRedChanged(_ sender: NSSlider) {
        if sender.floatValue > 0 {
            opp.volumeRedMute = false
        }
        opp.volumeRed = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    // MuteRedClicked
    @IBAction func MuteRedClicked(_ sender: AnyObject) {
        if opp.volumeRedMute {
            opp.volumeRedMute = false
        } else {
            opp.volumeRedMute = true
        }
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    // DifficultyYellowChanged
    @IBAction func DifficultyYellowChanged(_ sender: NSSlider) {
        let difficulty = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.yellow, value: difficulty)
        opp.difficultyYellow = difficulty
        
        MinderManager.sharedInstance.SaveSettings()
    }
    
     // DifficultyRedChanged
    @IBAction func DifficultyRedChanged(_ sender: NSSlider) {
        let difficulty = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.red, value: difficulty)
        opp.difficultyRed = difficulty
        
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // SeatbackCheckTapped
    @IBAction func SeatbackCheckTapped(_ sender: NSButton) {
        if sender.state == 0 {
            labelDifficultySeatback.isEnabled = false
            sliderDifficultySeatback.isEnabled = false
        } else {
            labelDifficultySeatback.isEnabled = true
            sliderDifficultySeatback.isEnabled = true
            opp.isSeatBack = sender.state
            opp.difficultySeatback = sliderDifficultySeatback.floatValue
        }
        delegateSettings?.SetSeatback(sender.state)
        opp.isSeatBack = sender.state
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // DifficultySeatbackChanged
    @IBAction func DifficultySeatbackChanged(_ sender: AnyObject) {
        let difficulty = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.seatback, value: difficulty)
        opp.difficultySeatback = difficulty
        
        MinderManager.sharedInstance.SaveSettings()
    }
}
