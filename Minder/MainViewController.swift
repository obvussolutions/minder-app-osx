//
//  MainViewController.swift
//  Minder
//
//  Created by Abdul on 8/25/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa

// To distiguish the sub views to be shown
enum MainSubViewType {
    case home
    case login
}

// Delegate to MainViewController for other sub types
protocol DelegateMainView : class{
    func updateMainView(_ typeView: MainSubViewType, typeLogin: EnterType)
}

// This is the main ViewController. Both the sub views will populate here
class MainViewController: NSViewController {
    var currentViewType: MainSubViewType = .login
    // Hold the current subview populated
    var currentViewController: NSViewController?
    
    var hostReachability: Reachability? = nil
    var homeViewController:HomeViewController? // fbn change
    
    // Recursively deleting all files in directory and its sub directories
    func deleteDirectoryFiles(_ path: String) {
        let fileManager = FileManager.default
        if let enumerator = fileManager.enumerator(atPath: path) {
            while let fileName = enumerator.nextObject() as? String {
                let cPath = "\(path)\(fileName)"
                var isDir : ObjCBool = false
                if fileManager.fileExists(atPath: cPath, isDirectory:&isDir) {
                    if isDir.boolValue {
                        deleteDirectoryFiles(cPath)
                    }
                }
                do {
                    try FileManager.default.removeItem(atPath: cPath)
                }
                catch {
                    NSLog("clearUserDefaults error for \(cPath)")
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //* Setting up logging to file for debugging on Documents/obVusMinderLog/
        MinderManager.sharedInstance.nsLogPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/obVusMinderLog"
        var isDir : ObjCBool = true
        if !FileManager.default.fileExists(atPath: MinderManager.sharedInstance.dLogPath, isDirectory: &isDir) {
        do {
            try FileManager.default.createDirectory(atPath: MinderManager.sharedInstance.nsLogPath, withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        }
        MinderManager.sharedInstance.nsLogPath += "/\(Date.GetDateString(Date(), format: "MM-dd-yyyy")).log"
        let cstr = (MinderManager.sharedInstance.nsLogPath as NSString).utf8String
        freopen(cstr, "a+", stderr)
        
        NSLog("Initialised logging at path \(MinderManager.sharedInstance.nsLogPath)")
        // */
        
        //  Setting application resources saving directory
        let appManager = MinderManager.sharedInstance
        appManager.appDirectory = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)[0] + "/obVus.Minder/"
        NSLog("Application resources saving directory set to \(appManager.appDirectory)")
        
        //  Setting up Reachability to detect server communication can be made or not
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(MainViewController.reachabilityChanged(_:)),
            name: NSNotification.Name.reachabilityChanged,
            object: nil)
        hostReachability = Reachability(hostName: appManager.mainUrl)
        hostReachability!.startNotifier()
        NSLog("Notification set for server reachability")
        
        let userDefaults = UserDefaults.standard
        
        //* Check whether application has updated by validating saved VersionBuild. If versions first run delete all previous versions data
        var clearUserDefaults: Bool = true
        let stringVersionBuild: String = "\(Bundle.Version).\(Bundle.Build)"
        if userDefaults.value(forKey: "VersionBuild") != nil {
            if userDefaults.string(forKey: "VersionBuild") == stringVersionBuild {
                clearUserDefaults = false
            }
        }        
        //  Clearing previous versions UserDefaults
        if clearUserDefaults {
            deleteDirectoryFiles(MinderManager.sharedInstance.appDirectory)
            for item in userDefaults.dictionaryRepresentation() {
                userDefaults.removeObject(forKey: item.0)
            }
            userDefaults.setValue(stringVersionBuild, forKey: "VersionBuild")
            userDefaults.synchronize()
            NSLog("First run \(stringVersionBuild). Cleared existing UserDefaults")
        }   // */
        
        //  Retrieve saved details from UserDefaults, check log strings for more detail
        var logString: String = "User details not found. View type set to Login"
        if let data = userDefaults.object(forKey: "UserDetails") as? Data {
            appManager.userLogged = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            MinderManager.sharedInstance.deviceName = (appManager.userLogged?.deviceName)!
            currentViewType = .home
            logString = "User details loaded. View type set to Home"
        }
        NSLog(logString)
        
        appManager.LoadSettings()
        // Polpulate sub view according to currentType
        switchViewType()
    }
    
    //  Switching view according to view type
    func switchViewType() {
        self.view.subviews.forEach({ $0.removeFromSuperview() })
        
        let mainStoryboard: NSStoryboard = NSStoryboard(name: "Main", bundle: nil)
        switch currentViewType {
        case .login:
            self.currentViewController = nil
            homeViewController = nil
            let vc: LoginViewController = (mainStoryboard.instantiateController(withIdentifier: "ViewControllerLogin") as? LoginViewController)!
            vc.delegateLogin = self
            self.currentViewController = vc
            break
        case .home:
            homeViewController = mainStoryboard.instantiateController(withIdentifier: "ViewControllerHome") as? HomeViewController
            homeViewController!.delegateLogin = self
            self.currentViewController = homeViewController
            break
        }
        self.view.addSubview(self.currentViewController!.view)
        self.currentViewController!.view.setFrameOrigin(NSPoint(x: 0, y: 0))
       
        print("Switched view according to view type \(self.currentViewController)")
    }
    
    deinit {
        print("Main Controller denited")
        //NSNotificationCenter.defaultCenter().removeObserver(self)
        self.currentViewController = nil
        homeViewController = nil
        
    }
    
    override func viewDidAppear() {
        if let screenSize = NSScreen.main()!.frame.size as? CGSize {
            view.window!.setFrameOrigin(NSPoint(x: screenSize.width/2 - view.window!.frame.size.width/2, y: (screenSize.height - view.window!.frame.size.height)/2))
        }
//        self.view.window?.styleMask.insert(NSWindowStyleMask.Resizable)
    }

    //  Reachability notification method
    func reachabilityChanged(_ notification:Notification) {
        if let networkReachability = notification.object as? Reachability {
            let remoteHostStatus: NetworkStatus = networkReachability.currentReachabilityStatus()
            if remoteHostStatus == NotReachable {
                NSLog("Connection not reachable")
            } else if remoteHostStatus == ReachableViaWiFi {
                NSLog("Connection reachable")
            }
        } else {
            NSLog("Connection status unknown")
        }
    }
}

extension MainViewController : DelegateMainView {
    func updateMainView(_ typeView: MainSubViewType, typeLogin: EnterType) {
        
        currentViewType = typeView
        DispatchQueue.main.async(execute: {
            self.switchViewType()
        })
    }
}
