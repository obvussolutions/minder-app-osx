//
//  MinderLibDebug.swift
//  Minder
//
//  Created by Pegasus on 18/12/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa
import IOBluetoothUI
import AVFoundation


protocol DelegateDebugMinder : class{
      func UpdateGyroWithRoll1(_ roll: CGFloat, withPitch pitch: CGFloat, withYaw yaw: CGFloat, withT t: Double)
      func UpdateAccel1(_ x: CGFloat, withY y: CGFloat, withZ z: CGFloat, withT t: Double)
      func updateRawAngle1(_ x: Int32, withY y: Int32, withZ z: Int32)
      func updateDataFrequencyStatus1(_ angle: Int, withY accel: Int, withZ gyro: Int)
      func updatePosture1(_ x: Float, withY y: Float, withT t: Double)
      func updateVSysCode (vsysCode : Float)
      func updateFWVersion(version : String)
      func updateTemperature1(_ temperature: Double)
      func updateButtonStatus(_ status: Int)
      func updateStatus1(_ status: Int)
      func updateBLEStatus1(_ status: Int)
      func updateStepCount1(_ stepCount: Int)
      func updateLRA1(_ wave: Int)
      func updateHeartRate_1(_ heartRate: Int)
      func updateHeartRate2_1(_ heartRate: Int)
      func updateChargingStatus1(_ charging: Int)
      func updateLEDStatus1(_ red: Int, withGreen green: Int, withBlue blue: Int)
      func updateCharge1(_ charge: Float)
      func updateWirelessChargerStatus1(_ status: Int)

}

class MinderLibDebugController: NSViewController {
    
    
    weak var repeatingTimer: Timer?
    @IBOutlet weak var debugMinderScrollView: NSScrollView!
    @IBOutlet weak var accelSeg: NSSegmentedControl!
    @IBOutlet weak var lraMotorSeg: NSSegmentedControl!
    @IBOutlet weak var heartrateSeg: NSSegmentedControl!
    @IBOutlet weak var connectField: NSTextField!
    @IBOutlet weak var vibrationField: NSTextField!
    @IBOutlet weak var gasGuageField: NSTextField!
    @IBOutlet weak var waveField: NSTextField!
    @IBOutlet weak var colorField: NSTextField!
    @IBOutlet weak var durationField: NSTextField!
    @IBOutlet weak var periodField: NSTextField!
    @IBOutlet weak var redLEDField: NSTextField!
    @IBOutlet weak var greenLEDField: NSTextField!
    @IBOutlet weak var blueLEDField: NSTextField!
    @IBOutlet weak var heartRateField: NSTextField!
    @IBOutlet weak var temperatureField: NSTextField!
    @IBOutlet weak var button1StatusField: NSTextField!
    @IBOutlet weak var button2StatusField: NSTextField!
    @IBOutlet weak var wirelessStatusField: NSTextField!
    @IBOutlet weak var stepCountField: NSTextField!
    @IBOutlet weak var deviceListField: NSComboBox!
    @IBOutlet weak var chargingStatusField: NSTextField!
    @IBOutlet weak var angleFreqField: NSTextField!
    @IBOutlet weak var accelFreqField: NSTextField!
    @IBOutlet weak var gyroFreqField: NSTextField!
    @IBOutlet weak var bleStatusField: NSTextField!
    @IBOutlet weak var bytesSentField: NSTextField!
    @IBOutlet weak var appVersionField: NSTextField!
    @IBOutlet weak var redLEDStatusField: NSTextField!
    @IBOutlet weak var greenLEDStatusField: NSTextField!
    @IBOutlet weak var blueLEDStatusField: NSTextField!
    @IBOutlet weak var angleXField: NSTextField!
    @IBOutlet weak var angleYField: NSTextField!
    @IBOutlet weak var angleTimeField: NSTextField!
    @IBOutlet weak var accelXField: NSTextField!
    @IBOutlet weak var accelYField: NSTextField!
    @IBOutlet weak var accelZField: NSTextField!
    @IBOutlet weak var accelTimeField: NSTextField!
    @IBOutlet weak var gyroXField: NSTextField!
    @IBOutlet weak var gyroYField: NSTextField!
    @IBOutlet weak var gyroZField: NSTextField!
    @IBOutlet weak var gyroTimeField: NSTextField!
    @IBOutlet weak var fwVersionField: NSTextField!
    @IBOutlet weak var vsysField: NSTextField!
    @IBOutlet weak var angleXCalcField: NSTextField!
    @IBOutlet weak var angleYCalcField: NSTextField!
    @IBOutlet weak var rawAngleXField: NSTextField!
    @IBOutlet weak var rawAngleYField: NSTextField!
    @IBOutlet weak var rawAngleZField: NSTextField!
    @IBOutlet weak var heartRate2Field: NSTextField!
    @IBOutlet weak var picVersionField: NSTextField!
    @IBOutlet weak var rssiField: NSTextField!
    @IBOutlet weak var selectedDeviceName: NSTextField!
    
    var angleSampleCount :Int = 0
    var accelSampleCount : Int = 0
    var gyroSampleCount : Int = 0
    
    var minderlib: MinderLibBLE? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        let newpoints: NSPoint = NSMakePoint(0, 300)
        debugMinderScrollView.documentView!.scroll(newpoints)
        MinderManager.sharedInstance.initDeviceDebug(self)
        
        if (MinderManager.sharedInstance.sensorState?.isHeartRateEnable)!
        {
           heartrateSeg.setSelected(true, forSegment: 0)
        }
        else{
            heartrateSeg.setSelected(true, forSegment: 1)
        }
        
        if (MinderManager.sharedInstance.sensorState?.isAccelEnable)!
        {
            accelSeg.setSelected(true, forSegment: 0)
        }
        else{
            accelSeg.setSelected(true, forSegment: 1)
        }
        
        if (MinderManager.sharedInstance.sensorState?.isLraMotorEnable)!
        {
            lraMotorSeg.setSelected(true, forSegment: 0)
        }
        else{
            lraMotorSeg.setSelected(true, forSegment: 1)
        }
        self.colorField.stringValue = (MinderManager.sharedInstance.sensorState?.ledColor)!
        self.durationField.stringValue = (MinderManager.sharedInstance.sensorState?.ledDutyCycle)!
        self.periodField.stringValue = (MinderManager.sharedInstance.sensorState?.ledPeriod)!
        selectedDeviceName.stringValue = MinderManager.sharedInstance.deviceName
        
    }
    @IBAction func vibrateDevice(_ sender: AnyObject) {
        let pattern = self.vibrationField.stringValue
        MinderManager.sharedInstance.minderlib?.setVibrationPattern(UInt8(Int(pattern) ?? 0)) 
        MinderManager.sharedInstance.minderlib?.vibrateDevice()
    }
    @IBAction func blinkLED(_ sender: AnyObject) {
        let color = self.colorField.stringValue
        let dutycycle = self.durationField.stringValue
        let period = self.periodField.stringValue
        print ("value is \(UInt8("200"))")
        if color == "" || dutycycle == "" || period == "" || UInt8(color) == nil
        {
            return
        }
        MinderManager.sharedInstance.sensorState?.ledColor = color
        MinderManager.sharedInstance.sensorState?.ledPeriod = period
        MinderManager.sharedInstance.sensorState?.ledDutyCycle = dutycycle
        MinderManager.sharedInstance.SaveSettings()
        MinderManager.sharedInstance.minderlib?.blinkLED(UInt8(color)!, withPeriod: Int16(period)!, withDutyCycle: Int16(dutycycle)!)
        
    }
    
    @IBAction func blinkRedLEDwithValue(_ sender: AnyObject) {
        let value = self.redLEDField.stringValue
        if value != ""
        {
            MinderManager.sharedInstance.minderlib?.blinkDwithColor(UInt8(1) , withIntensity: (UInt8(value))!)
        }
        
    }
    
    @IBAction func blinkGreenLEDwithValue(_ sender: AnyObject) {
        let value = self.greenLEDField.stringValue
        if value != ""
        {
        MinderManager.sharedInstance.minderlib?.blinkDwithColor(UInt8(1), withIntensity: (UInt8(value))!)
        }
    }
    
    @IBAction func blinkBlueLEDwithValue(_ sender: AnyObject) {
        let value = self.blueLEDField.stringValue
        if value != ""
        {
        MinderManager.sharedInstance.minderlib?.blinkDwithColor(UInt8(1), withIntensity: (UInt8(value))!)
        }
    }
    
    @IBAction func getDevices(_ sender: AnyObject) {
    }
    
   
    @IBAction func enableHeartRate(_ sender: AnyObject) {
    }
    
    
    @IBAction func disableHeartRate(_ sender: AnyObject) {
    }
    
    
    @IBAction func loggingSeg(_ sender: AnyObject) {
    }
    
    @IBAction func angleSeg(_ sender: AnyObject) {

    }
    
    @IBAction func accelSeg(_ sender: AnyObject) {
        let segAccel: Int = sender.selectedSegment
        if segAccel == 0 {
            MinderManager.sharedInstance.minderlib?.enableAccel()
            MinderManager.sharedInstance.sensorState?.isAccelEnable = true
            MinderManager.sharedInstance.SaveSettings()
        }
        else {
            MinderManager.sharedInstance.minderlib?.disableAccel()
            MinderManager.sharedInstance.sensorState?.isAccelEnable = false
            MinderManager.sharedInstance.SaveSettings()

        }
    }
    @IBAction func lraSeg(_ sender: AnyObject) {
        let segLra: Int = sender.selectedSegment
        if segLra == 0 {
            MinderManager.sharedInstance.sensorState?.isLraMotorEnable = true
            MinderManager.sharedInstance.SaveSettings()
        }
        else {
            MinderManager.sharedInstance.sensorState?.isLraMotorEnable = false
            MinderManager.sharedInstance.SaveSettings()
            
        }
    }
    @IBAction func stepSeg(_ sender: AnyObject) {
    }
    
    @IBAction func gyroSeg(_ sender: AnyObject) {
    }
    
     @IBAction func heartRateSeg(_ sender: AnyObject) {
        let segHeartRate: Int = sender.selectedSegment
        if segHeartRate == 0 {
            MinderManager.sharedInstance.minderlib?.enableHeartRate()
            MinderManager.sharedInstance.sensorState?.isHeartRateEnable = true
            MinderManager.sharedInstance.SaveSettings()

        }
        else {
            MinderManager.sharedInstance.minderlib?.disableHeartRate()
            MinderManager.sharedInstance.sensorState?.isHeartRateEnable = false
            MinderManager.sharedInstance.SaveSettings()

        }
    }
    
    @IBAction func getRSSI(_ sender: AnyObject) {
    }
    

    
    func updatePicVersion(_ version: String) {
    }
    

    
    func updateRSSI(_ rssi: NSNumber) {

    }
    
    func clearFields() {
    }
}
// MinderLib deleates
extension MinderLibDebugController : DelegateDebugMinder {
    func UpdateGyroWithRoll1(_ roll: CGFloat, withPitch pitch: CGFloat, withYaw yaw: CGFloat, withT t: Double)
    {
        gyroSampleCount += 1
        self.gyroXField.stringValue = "\(roll)"
        self.gyroYField.stringValue = "\(pitch)"
        self.gyroZField.stringValue = "\(yaw)"
        self.gyroTimeField.stringValue = "\(t)"
    }
    func UpdateAccel1(_ x: CGFloat, withY y: CGFloat, withZ z: CGFloat, withT t: Double)
    {
        self.accelXField.stringValue = "\(x)"
        self.accelYField.stringValue = "\(y)"
        self.accelZField.stringValue = "\(z)"
        self.accelTimeField.stringValue = "\(Int(t))"
        
        var roll: Float = atan2(Float(y), Float(z)) * 180 / .pi
        if y < 0 {
            let signr: Float = (roll < 0) ? -1 : (roll > 0) ? +1 : 0
            roll = Float(signr * ((90.0 - fabsf(Float(roll))) + 90.0))
        }
        var pitch: Float = atan2(Float(-x), sqrt(pow(Float(y), 2) + pow(Float(z), 2))) * 180 / .pi
        if z > 0 {
            let sign: Float = (pitch < 0) ? -1 : (pitch > 0) ? +1 : 0
            pitch = Float(sign * ((90.0 - fabsf(Float(pitch))) + 90.0))
        }
        self.angleXCalcField.stringValue = String(format: "%.02f", roll)
        self.angleYCalcField.stringValue = String(format: "%.02f", pitch)
    }
    func updateRawAngle1(_ x: Int32, withY y: Int32, withZ z: Int32)
    {
        self.rawAngleXField.stringValue = "\(x)"
        self.rawAngleYField.stringValue = "\(y)"
        self.rawAngleZField.stringValue = "\(z)"
    }
    func updatePosture1(_ x: Float, withY y: Float, withT t: Double) {
        angleSampleCount += 1
        self.angleXField.stringValue = String(format: "%.02f", x)
        self.angleYField.stringValue = String(format: "%.02f", y)
        self.angleTimeField.stringValue = "\(Int(t))"
    }
    func updateDataFrequencyStatus1(_ angle: Int, withY accel: Int, withZ gyro: Int)
    {
        self.angleFreqField.stringValue = "\(angle)"
        self.accelFreqField.stringValue = "\(accel)"
        self.gyroFreqField.stringValue = "\(gyro)"
    }
    func updateVSysCode (vsysCode : Float)
    {
        self.vsysField.stringValue =  String(format: "%.01f", vsysCode)
    }
    func updateFWVersion(version : String)
    {
        self.fwVersionField.stringValue = version
    }
//    func updateDataSent(_ data: Data) {
//        let capacity = Int(data.count)
//        var sbuf = "" /* String.reserveCapacity(capacity) */
//        let buf = data.bytes as? [UInt8]
//        var i: Int
//        for i in 0..<data.count {
//            sbuf += String(format: "%02X", UInt8(buf?[i]))
//        }
//        var str = "\(sbuf)"
//        let range: NSRange = (str as NSString).range(of: "0*$", options: .regularExpression)
//        str = (str as NSString).replacingCharacters(in: range, with: "")
//        self.bytesSentField.stringValue = str
//    }
    func updateTemperature1(_ temperature: Double) {
        let tempValue = String(format: "%.02f\n", temperature)
        self.temperatureField.stringValue = tempValue
    }
    func updateButtonStatus(_ status: Int) {
        let button2: Int = status & 1
        let button1: Int = (status & 2) >> 1
        let s1 = "\(button1)\n"
        self.button1StatusField.stringValue = s1
        let s2 = "\(button2)\n"
        self.button2StatusField.stringValue = s2
    }
    func updateStatus1(_ status: Int) {
        //0 = disconnected, 1 = connecting, 2 = connected
        switch status {
        case 0:
            self.connectField.stringValue = "Initializing"
        case 1:
            self.connectField.stringValue = "Connecting"
        case 2:
            self.connectField.stringValue = "Connected"
        case 3:
            self.connectField.stringValue = "Disconnected"
        case 4:
            self.connectField.stringValue = "Disconnecting"
        default:
            break
        }

    }
    func updateBLEStatus1(_ status: Int) {
        switch status {
        case 1:
            self.bleStatusField.stringValue = "Powered Off"
        case 2:
            self.bleStatusField.stringValue = "Powered On"
        case 3:
            self.bleStatusField.stringValue = "Unauthorized"
        case 4:
            self.bleStatusField.stringValue = "Unknown State"
        case 5:
            self.bleStatusField.stringValue = "Unsuppported"
        default:
            break
        }
    }
    func updateStepCount1(_ stepCount: Int) {
        self.stepCountField.stringValue = "\(stepCount)\n"
    }
    
    func updateLRA1(_ wave: Int) {
        let waveValue = "\(wave)\n"
        self.waveField.stringValue = waveValue
    }
    
    func updateHeartRate_1(_ heartRate: Int) {
        let hrValue = "\(heartRate)\n"
        self.heartRateField.stringValue = hrValue
    }
    
    func updateHeartRate2_1(_ heartRate: Int) {
        let hrValue = "\(heartRate)\n"
        self.heartRate2Field.stringValue = hrValue
    }
    func updateChargingStatus1(_ charging: Int) {
        let c = "\(charging)\n"
        self.chargingStatusField.stringValue = c
    }
    
    func updateLEDStatus1(_ red: Int, withGreen green: Int, withBlue blue: Int) {
//        self.redLEDStatusField.stringValue = "\(red)"
//        self.greenLEDStatusField.stringValue = "\(green)"
//        self.blueLEDStatusField.stringValue = "\(blue)"
    }
    func updateCharge1(_ charge: Float) {
        self.gasGuageField.stringValue = String(format: "%.02f\n", charge)
    }
    func updateWirelessChargerStatus1(_ status: Int) {
        self.wirelessStatusField.stringValue = "\(status)"
    }
}
