//
//  SettingUpdatedViewController.swift
//  Minder
//
//  Created by Pegasus on 15/03/17.
//  Copyright © 2017 WHI. All rights reserved.
//

import Cocoa

class SettingUpdatedViewController: NSViewController {

    var oppIndex: Int = -1
    weak var delegateSettings: DelegateSettings? = nil
    var opp: OPP = OPP()
    var isEditClicked : ObjCBool = false
    
    @IBOutlet weak var buttonConnection: NSButton!
        @IBOutlet weak var lraMotorSeg: NSSegmentedControl!
//    @IBOutlet weak var popButtonPreferedDevice: NSPopUpButton!
    @IBOutlet weak var sliderTransperancy: NSSlider!
    @IBOutlet weak var sliderSensitivity: NSSlider!
    @IBOutlet weak var labelSensitivity: NSTextField!
    @IBOutlet weak var sliderSensitivityRotation: NSSlider!
    @IBOutlet weak var labelSensitivityRotation: NSTextField!
    @IBOutlet weak var labelTransparency: NSTextField!
    
    @IBOutlet weak var labelYellowIntensity: NSTextField!
    @IBOutlet weak var popButtonNotificationDevice: NSPopUpButton!
    
//    @IBOutlet weak var popButtonVibrateYellow: NSPopUpButton!
    @IBOutlet weak var popButtonInterval1Yellow: NSPopUpButton!
    @IBOutlet weak var popButtonInterval2Yellow: NSPopUpButton!
    
//    @IBOutlet weak var popButtonVibrateRed: NSPopUpButton!
    @IBOutlet weak var popButtonInterval1Red: NSPopUpButton!
    @IBOutlet weak var popButtonInterval2Red: NSPopUpButton!
    
    @IBOutlet weak var popButtonInterval1YellowVolume: NSPopUpButton!
    @IBOutlet weak var popButtonInterval2YellowVolume: NSPopUpButton!
    @IBOutlet weak var buttonVolumeYellowMute: NSButton!
    @IBOutlet weak var sliderVolumeYellow: NSSlider!
    
    @IBOutlet weak var popButtonInterval1RedVolume: NSPopUpButton!
    @IBOutlet weak var popButtonInterval2RedVolume: NSPopUpButton!
    @IBOutlet weak var buttonVolumeRedMute: NSButton!
    @IBOutlet weak var sliderVolumeRed: NSSlider!
    
    @IBOutlet weak var sliderDifficultyYellow: NSSlider!
    @IBOutlet weak var sliderDifficultyRed: NSSlider!
    
    @IBOutlet weak var labelYellowDifficulty: NSTextField!
    @IBOutlet weak var labelRedDifficulty: NSTextField!
    
    @IBOutlet weak var checkSeatback: NSButton!
    @IBOutlet weak var sliderDifficultySeatback: NSSlider!
    @IBOutlet weak var textOPPTitle: NSTextField!
    @IBOutlet weak var buttonAddUpdate: NSButton!
    @IBOutlet weak var buttonCancel: NSButton!
    @IBOutlet weak var buttonEdit: NSButton!
    @IBOutlet weak var myScrollView: NSScrollView!
    
    @IBOutlet weak var popButtonOPP: NSPopUpButton!
    @IBOutlet weak var typeOfYellowVolume: NSPopUpButton!
    @IBOutlet weak var typeOfRedVolume: NSPopUpButton!
    @IBOutlet weak var typeOfYellowHaptic: NSPopUpButton!
    @IBOutlet weak var typeOfRedHaptic: NSPopUpButton!
    @IBOutlet weak var YellowIntensity: NSSlider!
//    @IBOutlet weak var labelDifficultySeatback: NSTextField!
    @IBOutlet weak var popupHRMonitor: NSPopUpButton!
    @IBOutlet weak var popButtonShowHideWidgetTimer: NSPopUpButton!
    @IBOutlet weak var popButtonShowHideWidgetHR: NSPopUpButton!
    @IBOutlet weak var popButtonShowHideWidgetShoulder: NSPopUpButton!
    @IBOutlet weak var popButtonNotificationVibration: NSPopUpButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //to make scrollview start from the top
        let newpoints: NSPoint = NSMakePoint(0, 300)
        myScrollView.documentView!.scroll(newpoints)
        
        buttonConnection.attributedTitle = NSAttributedString(string: "DISCONNECTED FROM SENSOR", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 14), NSForegroundColorAttributeName : NSColor.white ])
        checkSeatback.attributedTitle = NSAttributedString(string: "Supported posture", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 12), NSForegroundColorAttributeName : NSColor.white ])
        opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        
        buttonAddUpdate.isHidden = true
        buttonCancel.isHidden = true
        textOPPTitle.isHidden = true
        
       
        customisePopUpButton(popButtonOPP, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(popButtonNotificationDevice, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(popButtonInterval1Yellow, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(popButtonInterval2Yellow, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(popButtonInterval1Red, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(popButtonInterval2Red, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(popButtonInterval1YellowVolume, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(popButtonInterval2YellowVolume, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(popButtonInterval1RedVolume, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(popButtonInterval2RedVolume, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(typeOfYellowVolume, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(typeOfRedVolume, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(typeOfYellowHaptic, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        customisePopUpButton(typeOfRedHaptic, fontsizise: 12, color: NSColor.orange,fontStyle: "Bold")
        
        
        
        
        RefreshControls()
    }
    //  refreshing the view
    func RefreshControls() {
        
        if MinderManager.sharedInstance.deviceStatus == .connected {
            let styleParagraph = NSMutableParagraphStyle()
            styleParagraph.alignment = .center
             self.buttonConnection.attributedTitle = NSAttributedString(string: "CONNECTED TO SENSOR", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 14), NSForegroundColorAttributeName : NSColor.white , NSParagraphStyleAttributeName : styleParagraph])
            DispatchQueue.main.async(execute: {
             self.buttonConnection.layer!.backgroundColor = NSColor.init(calibratedRed: 255/255, green: 91/255, blue: 29/255, alpha: 1).cgColor
                self.buttonConnection.image = NSImage(named: "ConnectedButton")
            })
            
            if MinderManager.sharedInstance.deviceFeaturesEnabled == true {
                if MinderManager.sharedInstance.dataProcessingPaused == true {
                   // buttonPauseProcessing.title = "Resume Posture"
                } else {
                  //  buttonPauseProcessing.title = "Pause Posture"
                }
            } else {
               // buttonEnableFeatures.enabled = true
              //  buttonPauseProcessing.enabled = false
            }
        } else {
            let styleParagraph = NSMutableParagraphStyle()
            styleParagraph.alignment = .center
             self.buttonConnection.attributedTitle = NSAttributedString(string: "DISCONNECTED FROM SENSOR", attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: 14), NSForegroundColorAttributeName : NSColor.white , NSParagraphStyleAttributeName : styleParagraph])
            DispatchQueue.main.async(execute: {
            self.buttonConnection.layer!.backgroundColor = NSColor.init(calibratedRed: 51/255, green: 78/255, blue: 93/255, alpha: 1).cgColor
            self.buttonConnection.image = NSImage(named: "DisconnectedButton")
             })
        }
        //ankush change , updating opp
        opp = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!])!
        print("opp.yellowIntensity : \(opp.yellowIntensity)")
        labelYellowIntensity.stringValue = "\(Int(255.0 - Double(opp.yellowIntensity)))"
        sliderTransperancy.floatValue = opp.transperancy
        labelTransparency.stringValue = "\(round(opp.transperancy * 100) / 100)"
        sliderSensitivity.floatValue = opp.sensitivityPosture
        labelSensitivity.stringValue = "\(round(opp.sensitivityPosture * 100) / 100)"
        sliderSensitivityRotation.floatValue = opp.sensitivityRotation
        labelSensitivityRotation.stringValue = "\(round(opp.sensitivityRotation * 100) / 100)"
        
        popButtonNotificationDevice.selectItem(withTag: opp.notificationDevice)
        popButtonInterval1Yellow.selectItem(withTag: opp.interval1YellowVibrate)
        popButtonInterval2Yellow.selectItem(withTag: opp.interval2YellowVibrate)
        if opp.vibrateYellow == 0 {
            popButtonInterval1Yellow.isEnabled = false
            popButtonInterval2Yellow.isEnabled = false
        } else {
            popButtonInterval1Yellow.isEnabled = true
            popButtonInterval2Yellow.isEnabled = true
        }

        popButtonInterval1Red.selectItem(withTag: opp.interval1RedVibrate)
        popButtonInterval2Red.selectItem(withTag: opp.interval2RedVibrate)
        if opp.vibrateRed == 0 {
            popButtonInterval1Red.isEnabled = false
            popButtonInterval2Red.isEnabled = false
        } else {
            popButtonInterval1Red.isEnabled = true
            popButtonInterval2Red.isEnabled = true
        }
        
        popButtonInterval1YellowVolume.selectItem(withTag: opp.interval1YellowVolume)
        popButtonInterval2YellowVolume.selectItem(withTag: opp.interval2YellowVolume)
        
        if opp.volumeYellowMute {
            sliderVolumeYellow.floatValue = 0
            buttonVolumeYellowMute.toolTip = "Unmute yellow volume"
            buttonVolumeYellowMute.image = NSImage(named: "VolumeMute")
            popButtonInterval1YellowVolume.isEnabled = false
            popButtonInterval2YellowVolume.isEnabled = false
        } else {
            popButtonInterval1YellowVolume.isEnabled = true
            popButtonInterval2YellowVolume.isEnabled = true
            sliderVolumeYellow.floatValue = opp.volumeYellow
            buttonVolumeYellowMute.toolTip = "Mute yellow volume"
            if opp.volumeYellow > 0.65 {
                buttonVolumeYellowMute.image = NSImage(named: "VolumeMax")
            } else if opp.volumeYellow > 0.35 {
                buttonVolumeYellowMute.image = NSImage(named: "VolumeMid")
            } else if opp.volumeYellow > 0 {
                buttonVolumeYellowMute.image = NSImage(named: "VolumeMin")
            } else {
                buttonVolumeYellowMute.image = NSImage(named: "VolumeMute")
                popButtonInterval1YellowVolume.isEnabled = false
                popButtonInterval2YellowVolume.isEnabled = false
            }
        }
        popButtonInterval1RedVolume.selectItem(withTag: opp.interval1RedVolume)
        popButtonInterval2RedVolume.selectItem(withTag: opp.interval2RedVolume)
        if opp.volumeRedMute {
            sliderVolumeRed.floatValue = 0
            buttonVolumeRedMute.toolTip = "Unmute red volume"
            buttonVolumeRedMute.image = NSImage(named: "VolumeMute")
            popButtonInterval1RedVolume.isEnabled = false
            popButtonInterval2RedVolume.isEnabled = false
        } else {
            popButtonInterval1RedVolume.isEnabled = true
            popButtonInterval2RedVolume.isEnabled = true
            sliderVolumeRed.floatValue = opp.volumeRed
            buttonVolumeRedMute.toolTip = "Mute red volume"
            if opp.volumeRed > 0.65 {
                buttonVolumeRedMute.image = NSImage(named: "VolumeMax")
            } else if opp.volumeRed > 0.35 {
                buttonVolumeRedMute.image = NSImage(named: "VolumeMid")
            } else if opp.volumeRed > 0 {
                buttonVolumeRedMute.image = NSImage(named: "VolumeMin")
            } else {
                buttonVolumeRedMute.image = NSImage(named: "VolumeMute")
                popButtonInterval1RedVolume.isEnabled = false
                popButtonInterval2RedVolume.isEnabled = false
            }
        }
        YellowIntensity.floatValue = Float(255.0 - Double(opp.yellowIntensity))
        sliderDifficultyYellow.floatValue = 100 - opp.difficultyYellow
        sliderDifficultyRed.floatValue = 100 - opp.difficultyRed
        labelYellowDifficulty.stringValue = "\(round(sliderDifficultyYellow.floatValue * 100) / 100)"
        labelRedDifficulty.stringValue = "\(round(sliderDifficultyRed.floatValue * 100) / 100)"
        checkSeatback.state = opp.isSeatBack
        sliderDifficultySeatback.floatValue = 100 - opp.difficultySeatback
        if checkSeatback.state == 1 {
            sliderDifficultySeatback.isEnabled = true
        }
        else
        {
            sliderDifficultySeatback.isEnabled = false
        }
        popButtonNotificationDevice.selectItem(withTag: opp.notificationDevice)
        
        popButtonOPP.removeAllItems()
        var index: Int = 0
        for opp in MinderManager.sharedInstance.settings!.OPPs {
            let menuItem: NSMenuItem = NSMenuItem()
            menuItem.title = opp.title
            menuItem.tag = index
            popButtonOPP.menu?.addItem(menuItem)
            index += 1
        }
        popButtonOPP.selectItem(withTag: (MinderManager.sharedInstance.settings?.OPPSelected)!)
        
        let menuItem: NSMenuItem = NSMenuItem()
        menuItem.title = "*Add New Opp*"
        menuItem.tag = MinderManager.sharedInstance.settings!.OPPs.count
        popButtonOPP.menu?.addItem(menuItem)
        
        customisePopUpButton(popButtonOPP, fontsizise: 14, color: NSColor.orange,fontStyle: "Bold") //ankush change
        
        typeOfYellowHaptic.selectItem(withTag: opp.yellowHapticValue)
        typeOfRedHaptic.selectItem(withTag: opp.redHapticValue)
        
        typeOfYellowVolume.selectItem(withTag: opp.yellowSoundValue)
        typeOfRedVolume.selectItem(withTag: opp.redSoundValue)
        
        self.setYellowHaptic(selectedTag: opp.yellowHapticValue)
        self.setRedHaptic(selectedTag: opp.redHapticValue)


        /*popupHRMonitor.removeAllItems()
        index = 0
        for type in MinderUtility.iterateEnum(HRMonitorType) {
            let menuItem: NSMenuItem = NSMenuItem()
            menuItem.title = type.description
            menuItem.tag = index
            popupHRMonitor.menu?.addItem(menuItem)
            index += 1
        }*/
        popupHRMonitor.selectItem(withTag: (MinderManager.sharedInstance.settings?.hrMonitorSelected)!)

        if opp.isWidgetTimerHidden
        {
            popButtonShowHideWidgetTimer.selectItem(withTag: 1)
        }
        else{
            popButtonShowHideWidgetTimer.selectItem(withTag: 0)
        }
        
        
        if opp.isWidgetHRHidden
        {
            popButtonShowHideWidgetHR.selectItem(withTag: 1)
        }
        else{
            popButtonShowHideWidgetHR.selectItem(withTag: 0)
        }
        
        
        if opp.isWidgetShoulderHidden
        {
            popButtonShowHideWidgetShoulder.selectItem(withTag: 1)
        }
        else{
            popButtonShowHideWidgetShoulder.selectItem(withTag: 0)
        }
        
//        if (MinderManager.sharedInstance.sensorState?.isLraMotorEnable)!
//        {
//            lraMotorSeg.setSelected(true, forSegment: 0)
//        }
//        else{
//            lraMotorSeg.setSelected(true, forSegment: 1)
//        }
        
//        @IBOutlet weak var popButtonShowHideWidgetHR: NSPopUpButton!
//        @IBOutlet weak var popButtonShowHideWidgetShoulder: NSPopUpButton!
//        if(HRMonitorType(rawValue: (MinderManager.sharedInstance.settings?.hrMonitorSelected)!) == Minder.HRMonitorType.Off) {
//            MinderManager.sharedInstance.DeviceHeartRateDisable()
//        } else {
//            MinderManager.sharedInstance.DeviceHeartRateEnable()
//        }
    }
    @IBAction func HRMonitorChanged(_ sender: NSPopUpButton) {
        
        print ("HRMonitorType(rawValue: (MinderManager.sharedInstance.settings?.hrMonitorSelected)!) is \(HRMonitorType(rawValue: (MinderManager.sharedInstance.settings?.hrMonitorSelected)!))")
        
        MinderManager.sharedInstance.settings?.hrMonitorSelected = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
        
//        if(HRMonitorType(rawValue: (MinderManager.sharedInstance.settings?.hrMonitorSelected)!) == Minder.HRMonitorType.Off) {
//            MinderManager.sharedInstance.DeviceHeartRateDisable()
//        } else {
//            MinderManager.sharedInstance.DeviceHeartRateEnable()
//        }
    }
    
    // ConnectionClicked
    @IBAction func ConnectionClicked(_ sender: AnyObject) {
        if buttonConnection.title == "CONNECTED TO SENSOR" {
            MinderManager.sharedInstance.forceDisconnect = true
            MinderManager.sharedInstance.DeviceDisconnect()
            sleep(2)
            MinderManager.sharedInstance.deviceName = ""
    //        buttonConnection.attributedTitle = NSAttributedString(string: "DISCONNECTED FROM SENSOR", attributes: [ NSFontAttributeName : NSFont.boldSystemFontOfSize(14), NSForegroundColorAttributeName : NSColor.whiteColor() ])
    //        self.buttonConnection.image = NSImage(named: "DisconnectedButton")
     //       buttonConnection.layer!.backgroundColor = NSColor.init(calibratedRed: 51/255, green: 78/255, blue: 93/255, alpha: 1).CGColor
            MinderManager.sharedInstance.dataProcessingPaused = false
            MinderManager.sharedInstance.deviceFeaturesEnabled = false
        } else {
            MinderManager.sharedInstance.DeviceConnect()
            sleep(2)
        }
    }
    
    // TransperancyChanged
    @IBAction func yellowIntensityChanged(_ sender: NSSlider) {
        opp.yellowIntensity = Float(255.0 - Double(sender.floatValue))
        MinderManager.sharedInstance.SaveSettings()
        labelYellowIntensity.stringValue = "\(Int(sender.floatValue))"
    }
    @IBAction func TransperancyChanged(_ sender: AnyObject) {
        opp.transperancy = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        labelTransparency.stringValue = "\(round(opp.transperancy * 100) / 100)"
        delegateSettings?.UpdateTransperency() // fbn
    }
    // SensitivityChanged
    @IBAction func SensitivityChanged(_ sender: NSSlider) {
        opp.sensitivityPosture = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        labelSensitivity.stringValue = "\(round(opp.sensitivityPosture * 100) / 100)"
    }
    @IBAction func SensitivityRotationChanged(_ sender: AnyObject) {
        opp.sensitivityRotation = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        labelSensitivityRotation.stringValue = "\(round(opp.sensitivityRotation * 100) / 100)"
    }
    
    @IBAction func Interval1YellowVolumeChanged(_ sender: AnyObject) {
        opp.interval1YellowVolume = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    @IBAction func Interval2YellowVolumeChanged(_ sender: AnyObject) {
        opp.interval2YellowVolume = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    @IBAction func Interval1RedVolumeChanged(_ sender: AnyObject) {
        opp.interval1RedVolume = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    @IBAction func Interval2RedVolumeChanged(_ sender: AnyObject) {
        opp.interval2RedVolume = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // OppInterval1YellowChanged
    @IBAction func OppInterval1YellowChanged(_ sender: NSPopUpButton) {
        opp.interval1YellowVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // OppInterval1RedChanged
    @IBAction func OppInterval1RedChanged(_ sender: NSPopUpButton) {
        opp.interval1RedVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // OppInterval2YellowChanged
    @IBAction func OppInterval2YellowChanged(_ sender: NSPopUpButton) {
        opp.interval2YellowVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
//    @IBAction func lraSeg(_ sender: AnyObject) {
//        let segLra: Int = sender.selectedSegment
//        if segLra == 0 {
//            MinderManager.sharedInstance.sensorState?.isLraMotorEnable = true
//            MinderManager.sharedInstance.SaveSettings()
//        }
//        else {
//            MinderManager.sharedInstance.sensorState?.isLraMotorEnable = false
//            MinderManager.sharedInstance.SaveSettings()
//            
//        }
//    }
    
    // OppInterval2RedChanged
    @IBAction func OppInterval2RedChanged(_ sender: NSPopUpButton) {
        opp.interval2RedVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    @IBAction func NotificationDeviceChanged(_ sender: AnyObject) {
        opp.notificationDevice = (sender.selectedItem?!.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // Interval1YellowChanged
    @IBAction func Interval1YellowChanged(_ sender: NSPopUpButton) {
        opp.interval1YellowVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    @IBAction func tyoeOfYellowHapticChanged(_ sender: NSPopUpButton) {
        if sender.selectedItem?.tag == 0
        {
            MinderManager.sharedInstance.sensorState?.isLraMotorEnable = false
            opp.interval1YellowVibrate = (sender.selectedItem?.tag)!
            opp.yellowHapticValue = (sender.selectedItem?.tag)!
            self.setYellowHaptic(selectedTag: opp.yellowHapticValue)
            MinderManager.sharedInstance.SaveSettings()
        }
        else {
            MinderManager.sharedInstance.sensorState?.isLraMotorEnable = true
            opp.interval1YellowVibrate = (sender.selectedItem?.tag)!
            opp.yellowHapticValue = (sender.selectedItem?.tag)!
            self.setYellowHaptic(selectedTag: opp.yellowHapticValue)
            MinderManager.sharedInstance.SaveSettings()
        }
    }
    @IBAction func tyoeOfRedHapticChanged(_ sender: NSPopUpButton) {

        if sender.selectedItem?.tag == 0
        {
            MinderManager.sharedInstance.sensorState?.isLraMotorEnable = false
            opp.interval1RedVibrate = (sender.selectedItem?.tag)!
            opp.redHapticValue = (sender.selectedItem?.tag)!
            self.setRedHaptic(selectedTag: opp.redHapticValue)
            MinderManager.sharedInstance.SaveSettings()
        }
        else {
            MinderManager.sharedInstance.sensorState?.isLraMotorEnable = true
            opp.interval1RedVibrate = (sender.selectedItem?.tag)!
            opp.redHapticValue = (sender.selectedItem?.tag)!
            self.setRedHaptic(selectedTag: opp.redHapticValue)
            MinderManager.sharedInstance.SaveSettings()
        }
        
    }
    
    @IBAction func tyoeOfYellowSoundChanged(_ sender: NSPopUpButton) {
        opp.interval1YellowVolume = (sender.selectedItem?.tag)!
        opp.yellowSoundValue = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    @IBAction func tyoeOfRedSoundChanged(_ sender: NSPopUpButton) {
        opp.interval1RedVolume = (sender.selectedItem?.tag)!
        opp.redSoundValue = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // Interval1RedChanged
    @IBAction func Interval1RedChanged(_ sender: NSPopUpButton) {
        opp.interval1RedVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // Interval2YellowChanged
    @IBAction func Interval2YellowChanged(_ sender: NSPopUpButton) {
        opp.interval2YellowVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // Interval2RedChanged
    @IBAction func Interval2RedChanged(_ sender: NSPopUpButton) {
        opp.interval2RedVibrate = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // VibrateYellowChanged
    @IBAction func VibrateYellowChanged(_ sender: NSPopUpButton) {
        opp.vibrateYellow = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    // VibrateRedChanged
    @IBAction func VibrateRedChanged(_ sender: NSPopUpButton) {
        opp.vibrateRed = (sender.selectedItem?.tag)!
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    // VolumeYellowChanged
    @IBAction func VolumeYellowChanged(_ sender: NSSlider) {
        if sender.floatValue > 0 {
            opp.volumeYellowMute = false
        }
        opp.volumeYellow = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
        
    }
    
    // MuteYellowClicked
    @IBAction func MuteYellowClicked(_ sender: AnyObject) {
        if opp.volumeYellowMute {
            opp.volumeYellowMute = false
        } else {
            opp.volumeYellowMute = true
        }
        
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    // VolumeRedChanged
    @IBAction func VolumeRedChanged(_ sender: NSSlider) {
        if sender.floatValue > 0 {
            opp.volumeRedMute = false
        }
        opp.volumeRed = sender.floatValue
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    // MuteRedClicked
    @IBAction func MuteRedClicked(_ sender: AnyObject) {
        if opp.volumeRedMute {
            opp.volumeRedMute = false
        } else {
            opp.volumeRedMute = true
        }
        MinderManager.sharedInstance.SaveSettings()
        delegateSettings?.UpdateNotification()
    }
    
    // DifficultyYellowChanged
    @IBAction func DifficultyYellowChanged(_ sender: NSSlider) {
        let difficulty = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.yellow, value: difficulty)
        opp.difficultyYellow = difficulty
        labelYellowDifficulty.stringValue = "\(round((100 - opp.difficultyYellow) * 100) / 100)"
        MinderManager.sharedInstance.SaveSettings()
    }
    
    // DifficultyRedChanged
    @IBAction func DifficultyRedChanged(_ sender: NSSlider) {
        let difficulty = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.red, value: difficulty)
        opp.difficultyRed = difficulty
        labelRedDifficulty.stringValue = "\(round((100 - opp.difficultyRed) * 100) / 100)"
        MinderManager.sharedInstance.SaveSettings()
    }
    
   //  SeatbackCheckTapped
    @IBAction func SeatbackCheckTapped(_ sender: NSButton) {
        if sender.state == 0 {
//            labelDifficultySeatback.enabled = false
            sliderDifficultySeatback.isEnabled = false
        } else {
//            labelDifficultySeatback.enabled = true
            sliderDifficultySeatback.isEnabled = true
            opp.isSeatBack = sender.state
            opp.difficultySeatback = 100 - sliderDifficultySeatback.floatValue
        }
        delegateSettings?.SetSeatback(sender.state)
        opp.isSeatBack = sender.state
        MinderManager.sharedInstance.SaveSettings()
    }
    
   //  DifficultySeatbackChanged
    @IBAction func DifficultySeatbackChanged(_ sender: AnyObject) {
        let difficulty = 100 - sender.floatValue
        delegateSettings?.UpdateDifficulty(.seatback, value: difficulty)
        opp.difficultySeatback = difficulty
        
        MinderManager.sharedInstance.SaveSettings()
    }
    // OPPAddClicked
    @IBAction func OPPAddClicked(_ sender: AnyObject) {
        if(isEditClicked).boolValue
        {
            if textOPPTitle.stringValue != ""
                {
                     opp = MinderManager.sharedInstance.settings!.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!]
                    opp.title = textOPPTitle.stringValue
                    MinderManager.sharedInstance.SaveSettings()
                    textOPPTitle.stringValue = ""
                    
                    popButtonOPP.isHidden = false
                    buttonEdit.isHidden = false
                    
                    textOPPTitle.isHidden = true
                    buttonAddUpdate.isHidden = true
                    buttonCancel.isHidden = true
                    
                    isEditClicked  = false
                    RefreshControls()
                    
                }
                else
                {
                    let myPopup: NSAlert = NSAlert()
                    myPopup.messageText = "Please enter title"
                    myPopup.informativeText = "You need to enter title to save the OPP."
                    myPopup.alertStyle = .warning
                    myPopup.addButton(withTitle: "OK")
                    myPopup.runModal()
                }
           
        }
        else
        {
            if textOPPTitle.stringValue != "" {
                var oppNew: OPP = OPP()
                oppNew.title = textOPPTitle.stringValue
                if oppIndex != -1 {
                    MinderManager.sharedInstance.settings?.OPPs.remove(at: oppIndex)
                    MinderManager.sharedInstance.settings?.OPPs.insert(oppNew, at: oppIndex)
                    delegateSettings?.OPPClicked(oppIndex)
                } else {
                    //                MinderManager.sharedInstance.settings?.OPPs.insert(opp, atIndex: (MinderManager.sharedInstance.settings?.OPPs.count)! - 1)
                    MinderManager.sharedInstance.settings?.OPPs.append(oppNew)
                    
                    delegateSettings?.OPPClicked((MinderManager.sharedInstance.settings?.OPPs.count)! - 1 )
                }
                
                MinderManager.sharedInstance.settings?.OPPSelected = (MinderManager.sharedInstance.settings?.OPPs.count)! - 1
                MinderManager.sharedInstance.SaveSettings()
                textOPPTitle.stringValue = ""
                
                popButtonOPP.isHidden = false
                buttonEdit.isHidden = false
                
                textOPPTitle.isHidden = true
                buttonAddUpdate.isHidden = true
                buttonCancel.isHidden = true
                RefreshControls()
                MinderManager.sharedInstance.SaveSettings()
            } else {
                let myPopup: NSAlert = NSAlert()
                myPopup.messageText = "Please enter title"
                myPopup.informativeText = "You need to enter title to save the OPP."
                myPopup.alertStyle = .warning
                myPopup.addButton(withTitle: "OK")
                myPopup.runModal()
            }
        }
        
    }
    // CancelClicked
    @IBAction func CancelClicked(_ sender: AnyObject) {
        delegateSettings?.OPPClicked(-1)
        popButtonOPP.isHidden = false
        buttonEdit.isHidden = false
        
        textOPPTitle.isHidden = true
        textOPPTitle.stringValue = ""
        buttonAddUpdate.isHidden = true
        buttonCancel.isHidden = true
        isEditClicked  = false
    }
    // CancelClicked
    @IBAction func EditClicked(_ sender: AnyObject) {
        delegateSettings?.OPPClicked(-1)
        isEditClicked = true;
        
        textOPPTitle.stringValue = (MinderManager.sharedInstance.settings?.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!].title)!

        popButtonOPP.isHidden = true
        buttonEdit.isHidden = true
        
        textOPPTitle.isHidden = false
        buttonAddUpdate.isHidden = false
        buttonCancel.isHidden = false
    }
    
    // OPP Changing functionality
    @IBAction func OPPChanged(_ sender: NSPopUpButton) {
        
        if(sender.selectedItem?.title == "*Add New Opp*")
        {
            popButtonOPP.isHidden = true
            buttonEdit.isHidden = true
            
            textOPPTitle.isHidden = false
            buttonAddUpdate.isHidden = false
            buttonCancel.isHidden = false
            
            MinderManager.sharedInstance.SaveSettings()
            delegateSettings!.OPPChanged()
            RefreshControls()   
        }
        else
        {
            MinderManager.sharedInstance.settings?.OPPSelected = sender.selectedTag()
            opp = MinderManager.sharedInstance.settings!.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!]
            
            sliderSensitivity.floatValue = opp.sensitivityPosture
            labelSensitivity.stringValue = "\(round(opp.sensitivityPosture * 100) / 100)"
            sliderSensitivityRotation.floatValue = opp.sensitivityRotation
            labelSensitivityRotation.stringValue = "\(round(opp.sensitivityRotation * 100) / 100)"
            delegateSettings?.UpdateDifficulty(.yellow, value: opp.difficultyYellow)
            //        sliderDifficulty.floatValue = 100 - opp.difficultyYellow
            delegateSettings?.UpdateDifficulty(.red, value: opp.difficultyRed)
            delegateSettings?.SetSeatback(opp.isSeatBack)
            delegateSettings?.UpdateDifficulty(.seatback, value: opp.difficultySeatback)
            //        sliderVolume.floatValue = opp.volumeYellow
            sliderTransperancy.floatValue = opp.transperancy
            //        popButtonPreferedDevice.selectItemWithTag(opp.notificationDevice)
            if opp.offsetPoint.x == 0 && opp.offsetPoint.y == 0 {
                //            labelNotCalibrated.hidden = false
            } else {
                //            labelNotCalibrated.hidden = true
            }
            
            MinderManager.sharedInstance.SaveSettings()
            delegateSettings!.OPPChanged()
            RefreshControls()
        }
       
    }
    @IBAction func ShowHideWidgetTimerClicked(_ sender: AnyObject) {
        opp = MinderManager.sharedInstance.settings!.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!]
        if sender.selectedTag() == 0
        {
            //delegateSettings?.ShowHideWidgetEntity(entityName : "Timer", isHiddenStatus : false)
            opp.isWidgetTimerHidden = false
        }
        else
        {
            //delegateSettings?.ShowHideWidgetEntity(entityName : "Timer", isHiddenStatus : true)
            opp.isWidgetTimerHidden = true
        }
        MinderManager.sharedInstance.SaveSettings()
    }
    @IBAction func ShowHideWidgetHRClicked(_ sender: AnyObject) {
        opp = MinderManager.sharedInstance.settings!.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!]
        if sender.selectedTag() == 0
        {
            opp.isWidgetHRHidden = false
        }
        else
        {
            opp.isWidgetHRHidden = true
        }
        MinderManager.sharedInstance.SaveSettings()
    }
    @IBAction func ShowHideWidgetShoulderClicked(_ sender: AnyObject) {
        opp = MinderManager.sharedInstance.settings!.OPPs[(MinderManager.sharedInstance.settings?.OPPSelected)!]
        if sender.selectedTag() == 0
        {
            opp.isWidgetShoulderHidden = false
        }
        else
        {
            opp.isWidgetShoulderHidden = true
        }
        MinderManager.sharedInstance.SaveSettings()
    }
    @IBAction func notificationVibrationClicked(_ sender: AnyObject) {
        if sender.selectedTag() == 0
        {
            MinderManager.sharedInstance.isNotificationVibrate = true
        }
        else
        {
            MinderManager.sharedInstance.isNotificationVibrate = false
        }
    }
    //func to change nspopbutton looks
    func customisePopUpButton(_ popUpButton : NSPopUpButton, fontsizise : NSInteger, color : NSColor, fontStyle : NSString)  {
        let itemArray: NSArray = popUpButton.itemArray as NSArray
        
        for i in 0..<itemArray.count {
            let item: NSMenuItem? = (itemArray[i] as? NSMenuItem)
            if(i == itemArray.count - 1)//last element
            {
                let styleParagraph = NSMutableParagraphStyle()
                styleParagraph.alignment = .center
                item!.attributedTitle =  NSAttributedString(string: (item?.title)!, attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: CGFloat(fontsizise)), NSForegroundColorAttributeName : color , NSParagraphStyleAttributeName : styleParagraph])
            }
            if(fontStyle == "Bold")
            {
                let styleParagraph = NSMutableParagraphStyle()
                styleParagraph.alignment = .center
                item!.attributedTitle =  NSAttributedString(string: (item?.title)!, attributes: [ NSFontAttributeName : NSFont.boldSystemFont(ofSize: CGFloat(fontsizise)), NSForegroundColorAttributeName : color , NSParagraphStyleAttributeName : styleParagraph])
                
            }
            else
            {
                let styleParagraph = NSMutableParagraphStyle()
                styleParagraph.alignment = .center
                item!.attributedTitle =  NSAttributedString(string: (item?.title)!, attributes: [ NSFontAttributeName : NSFont.systemFont(ofSize: CGFloat(fontsizise)), NSForegroundColorAttributeName : color , NSParagraphStyleAttributeName : styleParagraph])
            }
            
        }
    }

    func setYellowHaptic (selectedTag : Int)
    {
        switch selectedTag {
        case 1:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(10)
            break
        case 2:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(80)
            break
        case 3:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(60)
            break
        case 4:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(40)
            break
            
        default:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(10)
        }
    }
    func setRedHaptic (selectedTag : Int)
    {
        switch selectedTag {
        case 1:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(10)
            break
        case 2:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(80)
            break
        case 3:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(60)
            break
        case 4:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(40)
            break
            
        default:
            MinderManager.sharedInstance.minderlib?.setVibrationPattern(10)
        }
    }
}
