//
//  CollectionViewItem.swift
//  SlidesMagic
//
//  Created by Gabriel Miro on 28/11/15.
//  Copyright © 2015 razeware. All rights reserved.
//

import Cocoa

class CollectionViewItemList: NSCollectionViewItem {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.lightGray.cgColor
        view.layer?.borderWidth = 0.0
        view.layer?.borderColor = NSColor.white.cgColor
    }
    
    func setHighlight(_ selected: Bool) {
        view.layer?.borderWidth = selected ? 5.0 : 0.0
    }
}
