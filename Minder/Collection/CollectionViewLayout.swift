//
//  CollectionViewLayout.swift
//  Minder
//
//  Created by Abdul on 10/27/16.
//  Copyright © 2016 obVus. All rights reserved.
//

import Cocoa


class CollectionViewLayout: NSCollectionViewLayout {
    
    let DaysPerWeek: Int = 7
    let HoursPerDay: Int = 24
    let HorizontalSpacing: CGFloat = 10
    let HeightPerHour: CGFloat = 50
    let DayHeaderHeight: CGFloat = 40
    let HourHeaderWidth: CGFloat = 100
    
    var itemHeight: CGFloat = 100
    var verticalSpacing: CGFloat = 0
    var containerPadding: EdgeInsets = NSEdgeInsetsZero
    
    override var collectionViewContentSize: NSSize {
        get {
            let count = self.collectionView?.numberOfItems(inSection: 0)
            if (count == 0) {
                return NSZeroSize
            }
            var size = self.collectionView!.superview!.bounds.size
            size.height = (CGFloat(count!) * (self.itemHeight + self.verticalSpacing)) - self.verticalSpacing + self.containerPadding.top + self.containerPadding.bottom
            return size
        }
    }
    override func prepare() {
        super.prepare()
    }
    override func layoutAttributesForItem(at indexPath: IndexPath) -> NSCollectionViewLayoutAttributes? {
        let count = self.collectionView?.numberOfItems(inSection: 0)
        if (count == 0) {
            return nil
        }
        let frame = NSMakeRect(self.containerPadding.left, self.containerPadding.top + ((self.itemHeight + self.verticalSpacing) * CGFloat(indexPath.item)), self.collectionViewContentSize.width - self.containerPadding.left - self.containerPadding.right, self.itemHeight)
        let itemAttributes = NSCollectionViewLayoutAttributes(forItemWith: indexPath)
        itemAttributes.frame = frame
        return itemAttributes
    }
    override func layoutAttributesForElements(in rect: NSRect) -> [NSCollectionViewLayoutAttributes] {
        let count = self.collectionView?.numberOfItems(inSection: 0) as Int!
        var attributes = [NSCollectionViewLayoutAttributes]()
        for index in 0...(count! - 1) {
            let indexPath = IndexPath(item: index, section: 0)
            if let itemAttributes = self.layoutAttributesForItem(at: indexPath) {
                attributes.append(itemAttributes)
            }
        }
        return attributes
    }
    override func shouldInvalidateLayout(forBoundsChange newBounds: NSRect) -> Bool {
        return true
    }
}
